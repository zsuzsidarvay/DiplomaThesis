-----------------------------
Pre-compiled Windows binaries
-----------------------------
You can run the pre-compiled Windows binary version of the application after
downloading it from the link:

https://gitlab.com/zsuzsidarvay/DiplomaThesis

---------------------
Run-time requirements
---------------------
Windows 7+
Multi-core CPU
GPU that is compatible at least with OpenGL 3.0
Microsoft Visual C++ Redistributable Package for Visual Studio 2019
https://support.microsoft.com/en-us/topic/the-latest-supported-visual-c-downloads-2647da03-1eea-4433-9aff-95f26a218cc0

------------------------------------------------------------------------------
Compilation and IDE-based testing requirements for compiling/running the source
of the project
------------------------------------------------------------------------------
Community Edition of Visual Studio 2019
Qt SDK Community Edition 7.0.0+
Windows SDK with only Debugging Tools for Windows


-----------------
Test environments
-----------------
We have tested the application on a Windows 10-based PC with an Intel Core i7-7500u CPU
(@2.70GHz, 2 cores, 4 threads).