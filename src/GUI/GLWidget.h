#pragma once

#include <GL/glew.h>
#include <QOpenGLWidget>

//#include <QGLFormat>
#include <QMouseEvent>

#include <QFont>
#include "BSurface/BSurface3.h"
#include "BSurface/BSurface3composite.h"
#include "BSurface/OptimizedBSurfaces3.h"

#include "Core/Materials.h"
#include "Core/ShaderPrograms.h"
#include "Parametric/ParametricSurfaces3.h"

#include "BCurve/BCurves3.h"
#include "BCurve/BCurves3Composite.h"

#include "../Core/Colors4.h"
#include "../Core/HCoordinates3.h"
#include "../Core/Lights.h"
#include "../GUI/SideWidget.h"

#include "BCurve/HermiteBCurve3Composite.h"

#include "QFileDialog"
#include "QMessageBox"

using namespace std;

namespace cagd
{
class GLWidget: public QOpenGLWidget
{
    Q_OBJECT

private:

    // variables defining the projection matrix
    double       _aspect;            // aspect ratio of the rendering window
    double       _fovy;              // field of view in direction y
    double       _z_near, _z_far;    // distance of near and far clipping planes

    // variables defining the model-view matrix
    double       _eye[3], _center[3], _up[3];

    // variables needed by transformations
    int         _angle_x, _angle_y, _angle_z;
    double      _zoom;
    double      _trans_x, _trans_y, _trans_z;

    // your other declarations

    int _page_number;
    GLint page_previous = -1;

    //BSURFACE3

    BSurface3* _bsurface = nullptr;
    TriangulatedMesh3* _img_bsurface = nullptr;
    BSurface3* _bsurface_2 = nullptr;
    TriangulatedMesh3* _img_bsurface_2 = nullptr;
    GLint _n_bsurface = 2;
    GLint _m_bsurface = 2;
    GLdouble _alpha_bsurface = PI/3;
    GLdouble _beta_bsurface = PI/2;
    RowMatrix<TensorProductSurface3::ImageColorScheme> _color_scheme;
    TensorProductSurface3::ImageColorScheme _color_scheme_type = TensorProductSurface3::DEFAULT_NULL_FRAGMENT;
    enum ShaderType{TOON, DIRECTIONAL_LIGHT, REFLECTION_LINES, UNIFORM_TWO_SIDED_LIGHTING, HEAT_MAP_TWO_SIDED_LIGHTING};
    GLvoid showEnergies();

    BSurface3Composite * _bsurfaces;
    GLint _selected_bsurface = -1;
    GLint _selected_bsurface2 = -1;
    enum SurfaceDirection{RIGHT, LEFT, UP, DOWN};
    GLint _order_surface = 0;

    RowMatrix<GLdouble> w;
    std::vector< pair<GLint, GLint> > fixed_index_pairs;
    GLint u_div, v_div;
    OptimizedBSurfaces3* _opt_bsurface;
    TriangulatedMesh3* _img_opt_bsurface = nullptr;
    GLint theta = 3;
    GLboolean _optimize = GL_FALSE;
    GLint sf_toolBox = -1;



    GLvoid _renderSpheres();

    GLvoid setFixedIndexes();

    //BCURVE3

    BCurve3* _bcurves;
    GenericCurve3 * _image_of_arc;
    BCurve3Composite* _bcurvess;
    GLboolean _show_bcurve_tangent;
    GLboolean _show_bcurve_acceleration;
    GLboolean _show_curvature_comb;
    GLuint _basis_type = 0;
    GLdouble _alpha = 1.56;
    GLuint _n = 4;
    GLint _selected_bcurve = -1;
    GLint _order_of_derivative_join1 = 0;
    GLint _order_of_derivative_join2 = 0;
    BCurve3Composite* joined_bcurves;
    DCoordinate3 _selected_control_point;
    GLdouble _ray_of_sphere = 0.0;
    GLint r1 = 2, r2 = 2;

    BCurve3* _bcurve_global_min = nullptr;
    BCurve3* _bcurve_sphere = nullptr;
    GenericCurve3 *_img_bcurve_global_min = nullptr, *_img_bcurve_sphere = nullptr;

    HermiteBCurve3Composite *_hermite_bcurve = nullptr;
    GLuint hermite_m = 5; //dimension
    GLuint hermite_rho = 2; //order of derivatives
    GLdouble _scale = 0.5;
    GLuint hermite_n = 5;   //order of bcurves in HermiteBCurve3Composite

    GLboolean _show_text = false;
    GLboolean _show_energy = false;
    GLboolean _show_sphere = false;


    GLint select_join = -2;
    enum MergeOrJoin {NONE, MERGE, JOIN};
    MergeOrJoin mergeOrJoin = NONE;

    //CURVE

    GLuint _index_of_curve_1;
    GLuint _index_of_curve_2;
    GLuint _direction_of_curve_1; //bcurvenel is
    GLuint _direction_of_curve_2;

    Color4* _red;

    //PATCH
    GLuint _selected_surface;
    RowMatrix<ShaderProgram*> _shaders;
    GLuint _shader_selected;
    RowMatrix<Material*> _materials;
    GLuint _material_selected;
    DirectionalLight* _light;
    GLboolean _show_u_pd;
    GLboolean _show_v_pd;
    GLboolean _show_u_ip;
    GLboolean _show_v_ip;
    GLboolean _show_normals;
    GLboolean _show_control;
    GLuint _index_of_surface_1;
    GLuint _index_of_surface_2;
    GLuint _direction_of_surface_1;
    GLuint _direction_of_surface_2;
    GLuint _manipulated_surface_index;
    GLuint _manipulated_point_i;
    GLuint _manipulated_point_j;
    Color4* _yellow;
    GLfloat _shader_values[4];

    //BCurve


    // SPHERE

    TriangulatedMesh3    _sphere;

    // SPEHERE END

    //MOUSE EVENT START
    GLdouble             _sphere_scale_factor;
    TriangulatedMesh3    _unit_sphere;

    GLuint               _row_count =10, _column_count=10;
    Matrix<DCoordinate3> _positions;

    GLboolean            _named_object_clicked;
    GLint                _row;
    GLint                _column;
    GLdouble             _reposition_unit;

    GLdouble _sphere_alpha = PI/4;
    GLdouble _sphere_beta = 0;

    DCoordinate3 _glob_min;
    GLboolean _render_tangent = false;
    GLboolean _opt_bcurve = false;


    //MOUSE EVENT END


    SideWidget* _side_widget;

    BCurve3 *_c1 = nullptr;
    BCurve3 *_c2 = nullptr;
    GLuint  n = 2, m = 3, r = 1;
    GLdouble a1 = PI / 2.0, a2 = 2*PI / 3;
    GLuint rho = 2, rho1 = 3, rho2 = 3;

    GenericCurve3 *_img_c1 = nullptr, *_img_c2 = nullptr;


protected:


    GLboolean mouseDragStarted = false;
    QPoint lastPosition;
    GLvoid mousePressEvent(QMouseEvent * event);
    void wheelEvent(QWheelEvent *event);
    GLvoid keyPressEvent(QKeyEvent * event);
    GLvoid modifyCurveAndNeighbors();
    GLvoid modifyControlPointCoordinate(GLint indexDCoordinate3, GLdouble modifyCoordinate);
    GLvoid modifyTangentVectorCoordinate(GLint i, GLdouble modify_r, GLdouble modify_theta, GLdouble modify_fi);

public:
    // special and default constructor
    // the format specifies the properties of the rendering window
    GLWidget(QWidget* parent = 0);

    // redeclared virtual functions
    void initializeGL();
    void initializeShaders();
    void initializeLight();
    void initializeMaterials();
    void paintGL();
    void paintBCurve();
    void paintSurface();
    void resizeGL(int w, int h);

    inline GLint project(GLdouble objx, GLdouble objy, GLdouble objz,
                         const GLdouble model[16], const GLdouble proj[16],
    const GLint viewport[4],
    GLdouble * winx, GLdouble * winy, GLdouble * winz);

    void renderText(DCoordinate3 &textPosWorld, QString text);

    GLvoid renderTexts();

    inline void transformPoint(GLdouble out[4], const GLdouble m[16], const GLdouble in[4]);

    GLvoid setWeightValue(GLdouble val, GLint index);

    GLvoid initalizeBSurface();
    GLvoid initializeColorScheme();

    GLvoid _renderSpheresOptimal();


    void initializeNewBCurve();

    void change_color(int value);
    GLvoid _renderSphere();
    GLvoid calculateEnergySphere();
    GLvoid renderPoints();
    GLvoid renderAttractorPoints();
    GLvoid setWeights(GLboolean val, GLint index);
    GLvoid renderTangent();

    GLvoid open();
    GLvoid save();

    GLvoid refreshData();

    GLvoid calculateEnergy();
    GLvoid calculateOptimalEnergy();


    GLvoid update_table_widget();
    GLvoid generate_bsurface_image(GLuint index);
    GLvoid create_bsurface_image();
    GLvoid u_modify_fixed_index_pairs(GLuint z = 1);
    GLvoid v_modify_fixed_index_pairs(GLuint z = 1);



    inline void set_side_widget(SideWidget *side_widget){_side_widget = side_widget;}

public slots:
    // public event handling methods/slots
    void set_angle_x(int value);
    void set_angle_y(int value);
    void set_angle_z(int value);

    void set_zoom_factor(double value);

    void set_trans_x(double value);
    void set_trans_y(double value);
    void set_trans_z(double value);

    void change_page(int number);

    //PATCH
    void insert_surface();
    //    void change_selected_surface(int value);
    //    void set_material(int index);
    void set_shader(int index);
    void set_u_derivatives(bool value);
    void set_v_derivatives(bool value);
    void set_u_isoparametric(bool value);
    void set_v_isoparametric(bool value);
    void set_normal(bool value);
    void set_control(bool value);


    void set_direction_of_surface1(int value);
    void set_direction_of_surface2(int value);
    void mergeSurface();
    void joinSurface();
    //    void continueSurface();

    //    void set_manipulated_surface(int index);
    void set_manipulated_point_i(int index);
    void set_manipulated_point_j(int index);

    void updateShader();
    void set_shader_1(double);
    void set_shader_2(double);
    void set_shader_3(double);

    void set_index_of_surface1(int index);
    void set_index_of_surface2(int index);


    //BCURVE
    void on_insert_bcurve_button_clicked();
    void on_show_bcurve_tangent_check_box_stateChanged(bool value);
    void on_show_bcurve_accelaration_check_box_stateChanged(bool value);
    void on_type_comboBox_currentIndexChanged(int index);
    void on_subdivision_button_clicked();
    void on_increase_order_button_clicked();
    void on_setOrder_spinbox_valueChanged(int arg1);
    void on_setAlpha_doubleSpinBox_valueChanged(double arg1);
    void on_selected_bcurve_spinBox_valueChanged(int arg1);
    void on_join_curve_button_3_clicked();
    void on_order_of_derivative_join_valueChanged(int arg1);
    void on_first_selected_bcurve_join_valueChanged(int arg1);
    void on_second_selected_bcurve_spinBox_3_valueChanged(int arg1);
    void on_order_of_derivative_join_3_valueChanged(int arg1);
    void on_curve_first_direction_combo_box_2_currentIndexChanged(int index);
    void on_curve_second_direction_combo_box_2_currentIndexChanged(int index);
    void on_deleteButton_clicked();
    void on_global_min_checkBox_clicked(bool checked);
    void on_curvatureComb_checkBox_clicked(bool checked);
    void on_hermite_pushButton_clicked();
    void on_scale_doubleSpinBox_valueChanged(double arg1);
    void on_curve_attractorcheckBox_clicked(bool checked);
    void on_setOrderOfDerivatives_spinbox_valueChanged(int arg1);
    void on_point_attractor_checkBox_clicked(bool checked);
    void on_w0_spinBox_valueChanged(double arg1);
    void on_w1_spinBox_valueChanged(double arg1);
    void on_w2_spinBox_valueChanged(double arg1);
    void on_w3_spinBox_valueChanged(double arg1);
    void on_w4_spinBox_valueChanged(double arg1);
    void on_w5_spinBox_valueChanged(double arg1);
    void on_show_text_checkBox_clicked(bool checked);

    void on_energy_checkBox_clicked(bool checked);

    void on_render_tangent_checkBox_clicked(bool checked);
    void on_energy_on_sphere_checkBox_clicked(bool checked);

    //BSURFACE

    void on_setN_bsurface_spinbox_valueChanged(int arg1);
    void on_setM_bsurface_spinbox_valueChanged(int arg1);
    void on_setAlpha_bsurface_doubleSpinBox_valueChanged(double arg1);
    void on_setBeta_doubleSpinBox_valueChanged(double arg1);
    void on_energy_comboBox_currentIndexChanged(int index);


    void on_toolBox_currentChanged(int index);
    void on_hermite_m_spinBox_valueChanged(int arg1);

    void on_hermite_rho_spinBox_valueChanged(int arg1);

    void on_optimize_bsurface_pushButton_clicked();

    void on_optimize_bsurface_checkBox_clicked(bool checked);

    void on_hermite_n_spinBox_valueChanged(int arg1);

    void on_tableWidget_cellClicked(int row, int column);

    void on_u_increase_order_pushButton_clicked();

    void on_v_increase_order_pushButton_clicked();

    void on_u_subdivision_pushButton_clicked();
    void on_v_subdivision_pushButton_clicked();

    void change_selected_surface(int value);
    void on_surface_toolBox_currentChanged(int index);

    void on_merge_pushButton_clicked();

    void on_order_surface_spinBox_valueChanged(int arg1);

    void on_delete_surface_pushButton_clicked();



};
}

