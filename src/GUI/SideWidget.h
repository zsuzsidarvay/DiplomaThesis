#pragma once

#include <QWidget>
#include "ui_SideWidget.h"

namespace cagd
{
    class SideWidget: public QWidget, public Ui::SideWidget
    {
    public:
        // special and default constructor
        SideWidget(QWidget *parent = 0);

    private slots:
        void _homeworkIDChanged(int index);
//        void on_setN_bsurface_spinbox_valueChanged(int arg1);
//        void on_setM_bsurface_spinbox_valueChanged(int arg1);
//        void on_setAlpha_bsurface_doubleSpinBox_valueChanged(double arg1);
//        void on_setBeta_doubleSpinBox_valueChanged(double arg1);
//        void on_energy_comboBox_currentIndexChanged(int index);
//        void on_toolBox_currentChanged(int index);
//        void on_hermite_m_spinBox_valueChanged(int arg1);
//        void on_hermite_rho_spinBox_valueChanged(int arg1);
//        void on_optimize_bsurface_pushButton_clicked();
//        void on_optimize_bsurface_checkBox_clicked(bool checked);
//        void on_hermite_n_spinBox_valueChanged(int arg1);
//        void on_tableWidget_cellClicked(int row, int column);
        void on_u_increase_order_pushButton_clicked();
        void on_v_increase_order_pushButton_clicked();
        void on_u_subdivision_pushButton_clicked();
        void on_v_subdivision_pushButton_clicked();
        void on_surface_toolBox_currentChanged(int index);
        void on_merge_pushButton_clicked();
        void on_order_surface_spinBox_valueChanged(int arg1);
        void on_delete_surface_pushButton_clicked();
    };
}
