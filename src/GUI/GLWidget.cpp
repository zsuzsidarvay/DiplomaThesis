#include "GLWidget.h"

#if !defined(__APPLE__)
#include <GL/glu.h>
#endif

#include <iostream>
#include <QPainter>
using namespace std;

#include <Core/Exceptions.h>

namespace cagd
{
//--------------------------------
// special and default constructor
//--------------------------------
GLWidget::GLWidget(QWidget *parent): QOpenGLWidget(parent)
{
}

//--------------------------------------------------------------------------------------
// this virtual function is called once before the first call to paintGL() or resizeGL()
//--------------------------------------------------------------------------------------
void GLWidget::initializeGL()
{
    // creating a perspective projection matrix
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    _aspect = (double)width() / (double)height();
    _z_near = 1.0;
    _z_far  = 1000.0;
    _fovy   = 45.0;

    gluPerspective(_fovy, _aspect, _z_near, _z_far);

    // setting the model view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    _eye[0] = _eye[1] = 0.0; _eye[2] = 6.0;
    _center[0] = _center[1] = _center[2] = 0.0;
    _up[0] = _up[2] = 0.0; _up[1] = 1.0;

    gluLookAt(_eye[0], _eye[1], _eye[2], _center[0], _center[1], _center[2], _up[0], _up[1], _up[2]);

    // enabling the depth test
    glEnable(GL_DEPTH_TEST);

    // setting the background color
    //    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

    setFocusPolicy(Qt::StrongFocus);

    // initial values of transformation parameters
    _angle_x = _angle_y = _angle_z = 0.0;
    _trans_x = _trans_y = _trans_z = 0.0;
    _zoom = 1.0;

    try
    {
        // initializing the OpenGL Extension Wrangler library
        GLenum error = glewInit();

        if (error != GLEW_OK)
        {
            throw Exception("Could not initialize the OpenGL Extension Wrangler Library!");
        }

        if (!glewIsSupported("GL_VERSION_2_0"))
        {
            throw Exception("Your graphics card is not compatible with OpenGL 2.0+! "
                            "Try to update your driver or buy a new graphics adapter!");
        }

        // create and store your geometry in display lists or vertex buffer objects

        initializeColorScheme();

        _glob_min.x() = 0.0;
        _glob_min.y() = 0.0;
        _glob_min.z() = 0.0;


        // SPHERE START
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_NORMALIZE);


        _selected_control_point.x() = 0;
        _selected_control_point.y() = 0;
        _selected_control_point.z() = 0;
        if (!_sphere.LoadFromOFF("Models/sphere.off"))
        {
            throw Exception("Could not load the model file: sphere.off!");
        }

        if (!_sphere.UpdateVertexBufferObjects())
        {
            throw Exception("Could not update the VBOs of the sphere's triangulated mesh!");
        }

        //     glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_NORMALIZE);

        // SPHERE END

        for(GLint i = 3; i< 6; i++){
            setWeights(false,i);
            setWeights(true, i-3);
        }


        //MOUSE EVENT START

        _named_object_clicked = GL_FALSE;
        _reposition_unit     = 0.05;
        //MOUSE EVENT END


        /*   _c1 = new (nothrow) BCurve3(Basis::TRIGONOMETRIC, n, a1);
        _c2 = new (nothrow) BCurve3(Basis::TRIGONOMETRIC, m, a2);
        cout << _c1 << " " << _c2 << endl;
        {
            GLdouble step = TWO_PI / (2*n+1);
            for (GLuint i = 0; i <= 2*n; i++)
            {
                GLdouble u = i * step;
                DCoordinate3 &cp = (*_c1)[i];
                cp[0] = cos(u);
                cp[1] = 1 + sin(u);
            }
        }

        {
            GLdouble step = TWO_PI / (2*m+1);
            for (GLuint i = 0; i <= 2*m; i++)
            {
                GLdouble u = i * step;
                DCoordinate3 &cp = (*_c2)[i];
                cp[0] = cos(u);
                cp[1] = -1 + sin(u);
            }
        }

        _c1->UpdateVertexBufferObjectsOfData();
        _img_c1 = _c1->GenerateImage(rho1, 50);
        _img_c1->UpdateVertexBufferObjects();

        RowMatrix<GLdouble> F;
        Matrix<GLdouble> dF;
        _c1->BlendingFunctionValues(a1, F);
        _c1->BlendingFunctionDerivatives(rho1, F, dF);

        cout << dF << endl;

        RowMatrix<GLdouble> G;
        Matrix<GLdouble> dG;
        _c2->BlendingFunctionValues(0.0, G);
        _c2->BlendingFunctionDerivatives(rho2, G, dG);

        (*_c2)[0] = (*_c1)[2*n];

        for (GLuint r = 1; r <= rho; r++)
        {
            DCoordinate3 left_sum;
            for (GLuint j = 0; j <= r; j++)
            {
                left_sum += (*_c1)[2*n-j] * dF(r, 2*n-j);
            }

            DCoordinate3 right_sum;
            for (GLuint j = 0; j < r; j++)
            {
                right_sum += (*_c2)[j] * dG(r, j);
            }

            (*_c2)[r] = (left_sum-right_sum) / dG(r, r);
        }

        _c2->UpdateVertexBufferObjectsOfData();

        _img_c2 = _c2->GenerateImage(rho2, 50);
        _img_c2->UpdateVertexBufferObjects();*/

        _page_number = 0;

        //bcurve
        //_bcurves = new BCurve3(Basis::Type(1), 4, PI);
        _show_bcurve_tangent = GL_FALSE;
        _show_bcurve_acceleration = GL_FALSE;


        //   _hermite_bcurve = new HermiteBCurve3Composite();


        _bcurvess = new BCurve3Composite();
        //        joined_bcurves = new BCurve3Composite();
        _basis_type = 0;
        _show_curvature_comb = GL_FALSE;


        _index_of_curve_1 = 0;
        _index_of_curve_2 = 0;
        _direction_of_curve_1 = 0;
        _direction_of_curve_2 = 0;

        _red = new Color4(1.0f, 0.0f, 0.0f, 0.0f);


        _side_widget->hermite_m_spinBox->setValue(hermite_m);
        _side_widget->hermite_rho_spinBox->setValue(hermite_rho);
        _side_widget->hermite_n_spinBox->setValue(hermite_n);

        _side_widget->setOrder_spinbox->setValue(_n);
        _side_widget->setAlpha_doubleSpinBox->setValue(_alpha);

        _side_widget->setAlpha_bsurface_doubleSpinBox->setValue(_alpha_bsurface);
        _side_widget->setN_bsurface_spinbox->setValue(_n_bsurface);
        _side_widget->setM_bsurface_spinbox->setValue(_m_bsurface);
        _side_widget->setBeta_doubleSpinBox->setValue(_beta_bsurface);


        //patch
        _bsurfaces = new BSurface3Composite();
        _selected_surface = 0;
        _shader_selected = 0;
        initializeShaders();
        initializeMaterials();

        //        initializeLight();
        _show_u_ip = GL_FALSE;
        _show_v_ip = GL_FALSE;
        _show_u_pd = GL_FALSE;
        _show_v_pd = GL_FALSE;
        _show_control = GL_TRUE;
        _show_normals = GL_FALSE;
        _yellow = new Color4(1.0f, 1.0f, 0.0f, 0.0f);
        _index_of_surface_1 = 0;
        _index_of_surface_2 = 1;
        _direction_of_surface_1 = 0;
        _direction_of_surface_2 = 0;
        _manipulated_surface_index = 0;
        _manipulated_point_i = 0;
        _manipulated_point_j = 0;



        //Spheres start
        if (!_unit_sphere.LoadFromOFF("Models/sphere.off"))
        {
            throw Exception("Could not load the model file: sphere.off!");
        }
        if (!_unit_sphere.UpdateVertexBufferObjects())
        {
            throw Exception("Could not update the VBOs of the sphere's triangulated mesh!");
        }
        _sphere_scale_factor = 0.02;
        _named_object_clicked = GL_FALSE;
        _reposition_unit     = 0.05;
        //Spheres end


    }
    catch (Exception &e)
    {
        cout << e << endl;
    }
}


GLvoid GLWidget::_renderSphere() {
    //    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


    glPushMatrix();

    glTranslated(_glob_min.x(), _glob_min.y(), _glob_min.z());
    glScalef(_ray_of_sphere, _ray_of_sphere, _ray_of_sphere);

    _materials[4]->SetShininess(GL_FRONT_AND_BACK, 0.1f);
    _materials[4]->SetTransparency(0.03f);
    _materials[4]->Apply();


    _shaders[3]->Enable();
    _sphere.Render();
    _shaders[3]->Disable();

    glPopMatrix();
    glDepthMask(GL_TRUE);

    glDisable(GL_BLEND);
    glDisable(GL_LIGHTING);
    //  glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHT0);
    glDisable(GL_NORMALIZE);

}


GLvoid GLWidget::initalizeBSurface(){

    _bsurface = new BSurface3(_alpha_bsurface, _n_bsurface, _beta_bsurface, _m_bsurface);
    //    _opt_bsurface = new OptimizedBSurfaces3(_alpha_bsurface, _n_bsurface, _beta_bsurface, _m_bsurface);

    GLint sizeN = _n_bsurface*2+1;
    GLint sizeM = _m_bsurface*2+1;

    srand((unsigned) time(0));
    GLdouble scale = 1.0;
    for (GLint i = 0; i < sizeN; ++i) {
        for (GLint j = 0; j < sizeM; ++j) {

            GLdouble x = scale * (i - _n_bsurface) / _n_bsurface;
            GLdouble y = scale * (j - _m_bsurface) / _m_bsurface - 3*_bsurfaces -> getSize();
            GLdouble z = scale * rand() / (GLdouble) RAND_MAX * 2.0 - 1.0;

            _bsurface->SetData(i, j, x, y, z);

            pair<GLint, GLint> p(i, j);

            //            fixed_index_pairs.push_back(p);
            //            _bsurfaces->addFixedIndex(_selected_bsurface,p);
        }
    }

    _bsurfaces->insertNewSurface(_bsurface, _materials[6], _shaders[_shader_selected]);


    if( ! _bsurface){

        throw Exception( "Jajjj! " ) ;
    }

    if( ! _bsurface->UpdateVertexBufferObjectsOfData( )){

        throw Exception( "Jajjj! " ) ;
    }

    on_energy_comboBox_currentIndexChanged(_color_scheme[0]);


    w.ResizeColumns(theta);
    for (GLint i = 0; i < theta; ++i) {
        w(i) = 1.0;
    }

    u_div =30;
    v_div =30;

}

GLvoid GLWidget::initializeColorScheme(){
    _color_scheme.ResizeColumns(6);

    _color_scheme[0] = TensorProductSurface3::ImageColorScheme::DEFAULT_NULL_FRAGMENT;
    _color_scheme[1] = TensorProductSurface3::ImageColorScheme::GAUSSIAN_CURVATURE_FRAGMENT;
    _color_scheme[2] = TensorProductSurface3::ImageColorScheme::MEAN_CURVATURE_FRAGMENT;
    _color_scheme[3] = TensorProductSurface3::ImageColorScheme::WILLMORE_ENERGY_FRAGMENT;
    _color_scheme[4] = TensorProductSurface3::ImageColorScheme::LOG_UMBILIC_DEVIATION_ENERGY_FRAGMENT;
    _color_scheme[5] = TensorProductSurface3::ImageColorScheme::TOTAL_CURVATURE_ENERGY_FRAGMENT;

}




void GLWidget::initializeNewBCurve() {

    BCurve3* _temp_arc = nullptr;
    //  _n=5;
    _temp_arc = new BCurve3(Basis::Type(_basis_type), _n, _alpha);
    //    GLdouble step = PI / (2*_n+1);

    // EZ VISSZA *******
    //    GLdouble a = 3.0;
    //    GLdouble step = 2 * a / (2 *_n + 1);
    //    GLdouble u = -a;
    //    for (GLuint i = 0; i < 2*_n+1; i++)
    //    {
    //        DCoordinate3 &cp = (*_temp_arc)[i];
    //        cp.x() = u;
    //        cp.y() = 0.0;
    //        cp.z() = 0.0;
    //        u += step;
    //    }

    //EDDIG***

    GLdouble step = 2  * PI / (2*_n+1);
    GLdouble u = 0.0;
    for (GLuint i = 0; i < 2*_n+1; i++)
    {
        u = i*step;
        DCoordinate3 &cp = (*_temp_arc)[i];
        // cp.x() = cos(u);
        // cp.y()= sin(u);
        cp.x() = cos(u) ;
        cp.y() = sin(u) - 2*_bcurvess -> getSize();
        cp.z() = 0.0;
    }
    //    //    GLdouble step = _alpha / (2*_n+1);
    //    GLdouble u = 0.0;
    //    for (GLuint i = 0; i < 2*_n+1; i++)
    //    {
    //        u += step;
    //        DCoordinate3 &cp = (*_temp_arc)[i];
    //        cp.x() = cos(4*u) + 4.0 * _bcurvess -> getSize();
    //        cp.y() = sin(4*u);
    //        cp.z() = 0.0;
    //    }
    //    Color4* color = new Color4(rand()%100*0.01f, rand()%100*0.01f, rand()%100*0.01f, 0.0f);
    //    Color4* color = new Color4(1.0f, 0.5f, 0.0f, 0.0f);
    //    Color4* color = new Color4(0.0f, 0.502f, 0.502f, 0.0f);

    Color4* color = new Color4(colors::baby_blue);

    _bcurvess -> insertNewArc(_temp_arc, color);
    _bcurvess ->CreateImage(_scale);

    delete color;
    delete _temp_arc;


}


void GLWidget::initializeShaders()
{
    _shader_selected = 0;
    int _number_of_shaders = 5;
    _shaders.ResizeColumns(_number_of_shaders);
    _shader_values[0] = 0.2f;
    _shader_values[1] = 0.2f;
    _shader_values[2] = 0.3f;
    _shader_values[3] = 0.0f;
    for(GLint index = 0; index < _number_of_shaders; index++)
    {
        _shaders[index] = new ShaderProgram();
        switch (index) {
        case 0:
            if(!_shaders[index] -> InstallShaders("Shaders/toon.vert", "Shaders/toon.frag"))
            {
                throw Exception("SHADER problem!");
            }
            _shaders[index] -> Enable();
            _shaders[index] -> SetUniformVariable4fv("default_outline_color", 1, _shader_values);
            _shaders[index] -> Disable();
            break;
        case 1:
            if(!_shaders[index] -> InstallShaders("Shaders/directional_light.vert", "Shaders/directional_light.frag"))
            {
                throw Exception("SHADER problem!");
            }
            break;
        case 2:
            if(!_shaders[index] -> InstallShaders("Shaders/reflection_lines.vert", "Shaders/reflection_lines.frag"))
            {
                throw Exception("SHADER problem!");
            }
            _shaders[index] -> Enable();
            _shaders[index] -> SetUniformVariable1f("scale_factor", 4);
            _shaders[index] -> SetUniformVariable1f("smoothing", 2);
            _shaders[index] -> SetUniformVariable1f("shading", 1);
            _shaders[index] -> Disable();
            break;
        case 3:
            if(!_shaders[index] -> InstallShaders("Shaders/two_sided_lighting.vert", "Shaders/two_sided_lighting.frag", GL_TRUE))
            {
                throw Exception("SHADER problem!");
            }
            break;
        case 4:
            if(!_shaders[index] -> InstallShaders("Shaders/two_sided_lighting_color.vert", "Shaders/two_sided_lighting_color.frag", 0))
            {
                throw Exception("SHADER problem!");
            }
            break;
        }
    }
}

void GLWidget::initializeMaterials()
{
    _material_selected = 0;
    _materials.ResizeColumns(7);
    _materials[0] = &MatFBBrass;
    _materials[1] = &MatFBEmerald;
    _materials[2] = &MatFBGold;
    _materials[3] = &MatFBPearl;
    _materials[4] = &MatFBRuby;
    _materials[5] = &MatFBSilver;
    _materials[6] = &MatFBTurquoise;
}


GLvoid GLWidget::refreshData(){
    if(_selected_bcurve != -1){
        BCurve3* temp = _bcurvess->getBCurve(_selected_bcurve);
        _side_widget->setOrder_spinbox->setValue(temp->getN());
        _side_widget->setAlpha_doubleSpinBox->setValue(temp->getAlpha());
        _side_widget->w0_spinBox->setValue(temp->getWeights()[0]);
        _side_widget->w1_spinBox->setValue(temp->getWeights()[1]);
        _side_widget->w2_spinBox->setValue(temp->getWeights()[2]);
        _side_widget->w3_spinBox->setValue(temp->getWeights()[3]);
        _side_widget->w4_spinBox->setValue(temp->getWeights()[4]);
        _side_widget->w5_spinBox->setValue(temp->getWeights()[5]);
        _side_widget->setOrderOfDerivatives_spinbox->setValue(temp->getMaxOrderOfDerivatives());
    }
}

inline void GLWidget::transformPoint(GLdouble out[4], const GLdouble m[16], const GLdouble in[4])
{
#define M(row,col)  m[col*4+row]
    out[0] =
            M(0, 0) * in[0] + M(0, 1) * in[1] + M(0, 2) * in[2] + M(0, 3) * in[3];
    out[1] =
            M(1, 0) * in[0] + M(1, 1) * in[1] + M(1, 2) * in[2] + M(1, 3) * in[3];
    out[2] =
            M(2, 0) * in[0] + M(2, 1) * in[1] + M(2, 2) * in[2] + M(2, 3) * in[3];
    out[3] =
            M(3, 0) * in[0] + M(3, 1) * in[1] + M(3, 2) * in[2] + M(3, 3) * in[3];
#undef M
}


inline GLint GLWidget::project(GLdouble objx, GLdouble objy, GLdouble objz,
                               const GLdouble model[16], const GLdouble proj[16],
const GLint viewport[4],
GLdouble * winx, GLdouble * winy, GLdouble * winz)
{
    GLdouble in[4], out[4];

    in[0] = objx;
    in[1] = objy;
    in[2] = objz;
    in[3] = 1.0;
    transformPoint(out, model, in);
    transformPoint(in, proj, out);

    if (in[3] == 0.0)
        return GL_FALSE;

    in[0] /= in[3];
    in[1] /= in[3];
    in[2] /= in[3];

    *winx = viewport[0] + (1 + in[0]) * viewport[2] / 2;
    *winy = viewport[1] + (1 + in[1]) * viewport[3] / 2;

    *winz = (1 + in[2]) / 2;
    return GL_TRUE;
}

void GLWidget::renderText(DCoordinate3 &textPosWorld, QString text) {
    //  int width = this->width();
    int height = this->height();

    GLdouble model[4][4], proj[4][4];
    GLint view[4];
    glGetDoublev(GL_MODELVIEW_MATRIX, &model[0][0]);
    glGetDoublev(GL_PROJECTION_MATRIX, &proj[0][0]);
    glGetIntegerv(GL_VIEWPORT, &view[0]);
    GLdouble textPosX = 0, textPosY = 0, textPosZ = 0;

    project(textPosWorld.x(), textPosWorld.y(), textPosWorld.z(),
            &model[0][0], &proj[0][0], &view[0],
            &textPosX, &textPosY, &textPosZ);

    textPosY = height - textPosY; // y is inverted

    QPainter painter(this);
    painter.setPen(Qt::black);
    painter.setFont(QFont("Helvetica", 8));
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    painter.drawText(textPosX, textPosY, text); // z = pointT4.z + distOverOp / 4
    painter.end();
}

GLvoid GLWidget::renderTexts(){
    for(GLuint i = 0; i< _bcurvess->getSize(); i++){
        GLuint size = 2*_bcurvess->getBCurve(i)->getN()+1;
        DCoordinate3 x = _bcurvess->getBCurve(i)->getControlPoint(size/2);
        x.x() += 0.2;
        x.y() += 0.2;
        QString string = "[ ";
        string.append(QChar(i + 97));
        string.append(", ");
        string.append(QChar(i + 117));
        string.append((" ]"));

        //        string.append(QString::number(i));
        renderText(x, string);

        // KONTROLPONT-NAL IS TEXT
        //        for(GLuint j = 0; j<size; j++){
        //            DCoordinate3 x = _bcurvess->getBCurve(i)->getControlPoint(j);
        //            x.x() += 0.1;
        //            x.y() += 0.1;
        //            QString string = QByteArray::fromHex(QString::number(i + 61).toLocal8Bit());
        //            string.append(QString::number(j));
        //            renderText(x, string);
        //        }

        if(_bcurvess->getShowUseAttractor(i)){
            QString string = "";
            //            QString string = "[ ";
            //            string.append(QChar(i + 97));
            //            string.append(", ");
            string.append(QChar(i + 117));
            //            string.append((" ]"));

            DCoordinate3 x = _bcurvess->getAttractor(i)->getControlPoint(size/2);
            x.x() += 0.1;
            x.y() += 0.1;

            //        GLuint size = 2*_bcurvess->getAttractor(i)->getN()+1;

            //            for(GLuint j = 0; j<size; j++){
            //                DCoordinate3 x = _bcurvess->getAttractor(i)->getControlPoint(j);
            //                x.x() += 0.1;
            //                x.y() += 0.1;
            //                QString string = QByteArray::fromHex(QString::number(i + 81).toLocal8Bit());
            //                string.append(QString::number(j));
            renderText(x, string);
            //            }
        }
    }
}

GLvoid GLWidget::_renderSpheres()
{

    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHT0);


    for (GLuint i = 0; i < 2*(*_bsurfaces)[_selected_bsurface]->getN()+1; i++)
    {
        for (GLuint j = 0; j < 2*(*_bsurfaces)[_selected_bsurface]->getM()+1; j++)
        {
            DCoordinate3 &point = (*(*_bsurfaces)[_selected_bsurface])(i,j);
            pair<GLint, GLint> p(i, j);


            glLoadName(i * 1000 + j);
            glPushMatrix();
            glTranslated(point[0], point[1], point[2]);
            glScalef(_sphere_scale_factor, _sphere_scale_factor, _sphere_scale_factor);


            //            if (::find(fixed_index_pairs.begin(), fixed_index_pairs.end(), p) == fixed_index_pairs.end())
            //            if (::find(_bsurfaces->getFixedIndex(_selected_bsurface).begin(), _bsurfaces->getFixedIndex(_selected_bsurface).end(), p) == _bsurfaces->getFixedIndex(_selected_bsurface).end())
            if(_bsurfaces->findFixedIndex(_selected_bsurface,p))
            {
                _materials[2]->Apply();

                //                _side_widget->tableWidget->item(i,j)->setForeground(QBrush(QColor(0, 255, 0)));

                _side_widget->tableWidget->item(i,j)->setBackground(QBrush(QColor(0, 255, 0)));

            } else{
                _materials[4]->Apply();
                //                _side_widget->tableWidget->item(i,j)->setForeground(QBrush(QColor(0, 0, 0)));
                _side_widget->tableWidget->item(i,j)->setBackground(QBrush(QColor(255, 255, 255)));

            }
            _unit_sphere.Render();
            glPopMatrix();
        }
    }
    glDisable(GL_LIGHTING);
    glDisable(GL_NORMALIZE);
    glDisable(GL_LIGHT0);

}

GLvoid GLWidget::_renderSpheresOptimal()
{

    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHT0);
    for (GLuint i = 0; i < 2*_opt_bsurface->getN()+1; i++)
    {
        for (GLuint j = 0; j < 2*_opt_bsurface->getM()+1; j++)
        {
            DCoordinate3 &point = (*_opt_bsurface)(i,j);
            pair<GLint, GLint> p(i, j);


            glLoadName(i * 1000 + j);
            glPushMatrix();
            glTranslated(point[0], point[1], point[2]);
            glScalef(_sphere_scale_factor, _sphere_scale_factor, _sphere_scale_factor);


            //            if (::find(fixed_index_pairs.begin(), fixed_index_pairs.end(), p) == fixed_index_pairs.end())
            //            if (::find(_bsurfaces->getFixedIndex(_selected_bsurface).begin(), _bsurfaces->getFixedIndex(_selected_bsurface).end(), p) == _bsurfaces->getFixedIndex(_selected_bsurface).end())
            if(_bsurfaces->findFixedIndex(_selected_bsurface,p))
            {
                _materials[5]->Apply();
            } else{
                _materials[4]->Apply();
            }
            _unit_sphere.Render();
            glPopMatrix();
        }
    }
    glDisable(GL_LIGHTING);
    glDisable(GL_NORMALIZE);
    glDisable(GL_LIGHT0);
}


//-----------------------
// the rendering function
//-----------------------
void GLWidget::paintGL()
{
    // clears the color and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // stores/duplicates the original model view matrix
    glPushMatrix();

    // applying transformations
    glRotatef(_angle_x, 1.0, 0.0, 0.0);
    glRotatef(_angle_y, 0.0, 1.0, 0.0);
    glRotatef(_angle_z, 0.0, 0.0, 1.0);
    glTranslated(_trans_x, _trans_y, _trans_z);
    glScaled(_zoom, _zoom, _zoom);




    // render your geometry (this is oldest OpenGL rendering technique, later we will use some advanced methods)
    switch(_page_number){
    case 0:
        paintBCurve();
        break;
    case 1:
        paintSurface();
        break;
    case 2:
        switch(page_previous){
        case 0:
            paintBCurve();
            break;
        case 1:
            paintSurface();
            break;
        }
        break;

    }

    // pops the current matrix stack, replacing the current matrix with the one below it on the stack,
    // i.e., the original model view matrix is restored
    glPopMatrix();
}

void GLWidget::paintSurface()
{


    if(_bsurfaces) {
        if(_shader_selected == 2)
            _bsurfaces->Render( _materials[6], _shaders[_shader_selected], GL_TRUE);
        else
            _bsurfaces->Render( _materials[6], _shaders[_shader_selected], GL_FALSE);

        glColor3d(0.0, 0.0, 0.5);
        _bsurfaces->RenderData(GL_LINE_STRIP);
    }

    if(_img_opt_bsurface){
        glEnable(GL_LIGHTING);
        glEnable(GL_NORMALIZE);
        glEnable(GL_LIGHT0);
        glEnable(GL_BLEND);
        glDepthMask(GL_FALSE);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        _shaders[_shader_selected]->Enable();
        _materials[1]->SetShininess(GL_FRONT_AND_BACK, 0.01f);
        _materials[1]->SetTransparency(0.1f);
        _materials[1]->Apply();
        _img_opt_bsurface->Render();
        _shaders[_shader_selected]->Disable();
        glDepthMask(GL_TRUE);

        glDisable(GL_BLEND);

        glDisable(GL_LIGHT0);
        glDisable(GL_NORMALIZE);
        glDisable(GL_LIGHTING);
    }


    if(_opt_bsurface && _optimize){
        _renderSpheresOptimal();
        glColor3d(0.0, 0.5, 0.0);
        glEnable(GL_LINE_STIPPLE);
        glLineWidth(3.0);
        glLineStipple(1, 0xF0F0);
        _opt_bsurface->RenderData(GL_LINE_STRIP);
        glDisable(GL_LINE_STIPPLE);
        _opt_bsurface->RenderData(GL_POINTS);
        glLineWidth(1.0);
        //        glColor3d(1.0, 0.0, 0.5);
        //        if(_bsurfaces){
        //        _bsurfaces->generateImage(_color_scheme_type);
        //        _bsurfaces->getBSurface(_selected_bsurface)->RenderData(GL_LINE_STRIP);
        //        }
    }

    //    if(_bsurface && _show_control){
    if(_selected_bsurface != -1){
        _renderSpheres();
        //        glColor3d(0.0, 0.0, 0.0);
        //        (*_bsurfaces)[_selected_bsurface]->RenderData(GL_LINE_STRIP);
        //        (*_bsurfaces)[_selected_bsurface]->RenderData(GL_POINTS);
    }
    if(_bsurfaces){
        if(sf_toolBox == 3)
            _bsurfaces->renderIsoparametric();

        //    if(_bsurface_2 && _show_control){
        //        //        _renderSpheres();
        //        glColor3d(0.0, 0.0, 0.0);
        //        _bsurface_2->RenderData(GL_LINE_STRIP);
        //        _bsurface_2->RenderData(GL_POINTS);
        //    }


        //    if(_show_control)
        //    {
        //        _surfaces -> RenderControlPoints();
        //        _surfaces -> RenderControlNet();
        //    }
        if(_show_u_ip)
        {
            _bsurfaces -> renderUIsoparametric();
        }
        if(_show_v_ip)
        {
            _bsurfaces -> renderVIsoparametric();
        }
        if(_show_u_pd)
        {
            _bsurfaces -> renderUPartialDerivatives();
        }
        if(_show_v_pd)
        {
            _bsurfaces -> renderVPartialDerivatives();
        }
        if(_show_normals)
        {
            _bsurfaces -> RenderNormals();
        }
    }
}

GLvoid GLWidget::renderTangent(){
    glColor3f(0.0f, 0.0f, 0.0f);
    for(GLuint i = 0; i<_hermite_bcurve->getM();i++){
        for(GLuint j = 0; j<1;j++){
            glBegin(GL_LINES);
            {
                glVertex3dv(& _hermite_bcurve->getP(i)[0]);
                DCoordinate3 sum = _hermite_bcurve->getP(i) + _hermite_bcurve->getT(j,i);
                glVertex3dv(&sum[0]);
            }
            glEnd();
        }
    }
}


void GLWidget::paintBCurve() {


    if(_show_text)
        renderTexts();


    if(_selected_bcurve != -1){
        //        if(select_join >= 0){
        //            _bcurvess->RenderPoints(select_join, 0, _red);
        //            _bcurvess -> RenderData(GL_LINE_STRIP, select_join, _red);
        //            _bcurvess -> RenderDerivative(0, GL_LINE_STRIP, select_join, _red);
        //        }

        //        _bcurvess->RenderPoints(_selected_bcurve, 0, _red);

        if(_bcurvess->getShowUseAttractor(_selected_bcurve)){
            Color4* color =new Color4(0.0f, 0.0f, 0.0f);
            _bcurvess->RenderPoints(_selected_bcurve, 1, color);
            delete color;
        }

        //        Color4* color = new Color4(1.0, 0.0, 0.0, 0.0);
        //        _bcurvess -> RenderData(GL_LINE_STRIP, _selected_bcurve, _red);
        //        _bcurvess -> RenderDerivative(0, GL_LINE_STRIP, _selected_bcurve, color);
        //        delete color;


        if(_show_bcurve_tangent)
        {
            _bcurvess->RenderDerivative(1, GL_LINES, _selected_bcurve);
        }
        if(_show_bcurve_acceleration)
        {
            _bcurvess -> RenderDerivative(2, GL_LINES, _selected_bcurve);
        }

        if(_show_curvature_comb){
            _bcurvess -> RenderDerivative(3, GL_LINES, _selected_bcurve);
        }

        if(_show_sphere){
            _renderSphere();
        }

    } else {

        if(_show_bcurve_tangent)
        {
            _bcurvess -> RenderDerivatives(1, GL_LINES);

        }
        if(_show_bcurve_acceleration)
        {
            _bcurvess -> RenderDerivatives(2, GL_LINES);
        }

        if(_show_curvature_comb){
            _bcurvess -> RenderDerivatives(3, GL_LINES);
        }

    }

    glPointSize(3.0);
    _bcurvess -> RenderData(GL_POINTS, _red);
    _bcurvess -> RenderData(GL_LINE_STRIP, _red);
    _bcurvess -> RenderDerivatives(0, GL_LINE_STRIP);


    if(_selected_control_point != 0) {
        _renderSphere();
    }


    //    if(_bcurve_sphere && _img_bcurve_sphere){
    //        glColor3d(0.0, 1.0, 0.0);
    //        _bcurve_sphere->RenderData(GL_LINE_STRIP);
    //        _img_bcurve_sphere->RenderDerivatives(0, GL_LINE_STRIP);
    //    }

    if(_render_tangent){
        renderTangent();
    }



}

//----------------------------------------------------------------------------
// when the main window is resized one needs to redefine the projection matrix
//----------------------------------------------------------------------------
void GLWidget::resizeGL(int w, int h)
{
    // setting the new size of the rendering context
    glViewport(0, 0, w, h);

    // redefining the projection matrix
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();

    _aspect = (double)w / (double)h;

    gluPerspective(_fovy, _aspect, _z_near, _z_far);

    // switching back to the model view matrix
    glMatrixMode(GL_MODELVIEW);

    update();
}

//-----------------------------------
// implementation of the public slots
//-----------------------------------

void GLWidget::set_angle_x(int value)
{
    if (_angle_x != value)
    {
        _angle_x = value;
        update();
    }
}

void GLWidget::set_angle_y(int value)
{
    if (_angle_y != value)
    {
        _angle_y = value;
        update();
    }
}

void GLWidget::set_angle_z(int value)
{
    if (_angle_z != value)
    {
        _angle_z = value;
        update();
    }
}

void GLWidget::set_zoom_factor(double value)
{
    if (_zoom != value)
    {
        _zoom = value;
        update();
    }
}

void GLWidget::set_trans_x(double value)
{
    if (_trans_x != value)
    {
        _trans_x = value;
        update();
    }
}

void GLWidget::set_trans_y(double value)
{
    if (_trans_y != value)
    {
        _trans_y = value;
        update();
    }
}

void GLWidget::set_trans_z(double value)
{
    if (_trans_z != value)
    {
        _trans_z = value;
        update();
    }
}

void GLWidget::on_insert_bcurve_button_clicked()
{
    if (_bcurvess != nullptr && !_bcurvess->getIsHermite())
    {
        initializeNewBCurve();
        update();
    }
}

void GLWidget::on_subdivision_button_clicked()
{
    if(_selected_bcurve >= 0){
        _bcurvess->setSubdivision(_selected_bcurve);
//        mergeOrJoin = MERGE;

        //        update();
        //        srand(time(0));
        //        RowMatrix<BCurve3*> *subdivision = _bcurvess->getBCurve(_selected_bcurve)->Subdivision(0.5*_bcurvess->getAlpha(_selected_bcurve));
        //        for(GLuint i = 0;i < subdivision->GetColumnCount(); i++) {
        //            Color4* color = new Color4(rand()%100*0.01f, rand()%100*0.01f, rand()%100*0.01f, 0.0f);
        //            _bcurvess -> insertNewArc((*subdivision)(i), color);
        //            delete color;
        //        }
        //        _bcurvess ->CreateImage(_scale);
    }


    update();
}

void GLWidget::on_increase_order_button_clicked()
{
    if(_selected_bcurve >= 0){
        //        srand(time(0));
        //        BCurve3* increaseorder;
        //        for(GLint i = 1; i<= 4;i++) {
        //            Color4* color = new Color4(rand()%100*0.01f, rand()%100*0.01f, rand()%100*0.01f, 0.0f);
        //            increaseorder= _bcurvess->getBCurve(_selected_bcurve)->IncreaseOrder(i);
        //            _bcurvess->insertNewArc(increaseorder);
        //            delete color;
        //        }
        //        delete increaseorder;

        (*_bcurvess)[_selected_bcurve]->setIncreaseOrder(1);
        _bcurvess ->CreateImage(_scale);
    }

    update();

}


GLvoid GLWidget::calculateEnergy() {
    QString string = "";
    if(_show_energy){

        if(_selected_bcurve >= 0){
            (*_bcurvess)[_selected_bcurve]->setEnergy();
            string.setNum((*_bcurvess)[_selected_bcurve]->getEnergy());
            _side_widget->energyFunctional_edit->setText(string);
        }
        update();
    }
    _side_widget->energyFunctional_edit->setText(string);

}

GLvoid GLWidget::calculateOptimalEnergy() {
    if(_opt_bcurve){
        QString string = "";
        if(_selected_bcurve >= 0){
            (*_bcurvess)[_selected_bcurve]->setEnergy();
            string.setNum((*_bcurvess)[_selected_bcurve]->getEnergy());
            _side_widget->global_min_energy_edit->setText(string);
        }
        update();
    }
}

void GLWidget::on_energy_checkBox_clicked(bool checked)
{
    _show_energy = checked;
    if(_show_energy){
        calculateEnergy();
        //        AbstractCurveEnergyFragment *temp = new LengthEnergyFragment(_bcurvess->getBCurve(_selected_bcurve));
        //        //        cout << "Lenght " <<  _bcurvess->getBCurve(_selected_bcurve)->calculateEnergy(temp) << endl;
        //        delete temp;
        //        temp = nullptr;
        //        temp = new CurvatureEnergyFragment(_bcurvess->getBCurve(_selected_bcurve));
        //        cout << "Curvature " <<  _bcurvess->getBCurve(_selected_bcurve)->calculateEnergy(temp) << endl;
        //        delete temp;
        //        temp = nullptr;
    }
    update();
}


void GLWidget::on_hermite_pushButton_clicked()
{
    _bcurvess->deleteAllArcAttributes();
    _selected_bcurve = -1;
    _bcurvess->setIsHermite(true);
    _side_widget->insert_bcurve_button->setEnabled(false);
    _side_widget->hermite_pushButton->setEnabled(false);
    _side_widget->hermite_m_spinBox->setEnabled(false);
    _side_widget->hermite_n_spinBox->setEnabled(false);
    if ( _hermite_bcurve != nullptr)
        delete _hermite_bcurve;
    _hermite_bcurve = new HermiteBCurve3Composite(hermite_m, hermite_rho, hermite_n, _bcurvess);
    //    _hermite_bcurve = new HermiteBCurve3Composite(hermite_m, hermite_rho, _bcurvess);


    _hermite_bcurve ->GenerateOptimalHermiteComposite();
    update();
}

void GLWidget::on_global_min_checkBox_clicked(bool checked)
{
    QString string = "";
    _opt_bcurve = checked;
    if(checked && _selected_bcurve >= 0) {
        (*_bcurvess)[_selected_bcurve]->getBCurve()->CalculateGlobalMin();
        (*_bcurvess)[_selected_bcurve]->generateImages();
        string.setNum((*_bcurvess)[_selected_bcurve]->getBCurve()->CalculateEnergyFunctional(), 'f');

        if((*_bcurvess)[_selected_bcurve]->getBCurve()->countMovableControlPoints() == 1){
            _glob_min.x() = (*_bcurvess)[_selected_bcurve]->getBCurve()->getControlPoint((*_bcurvess)[_selected_bcurve]->getBCurve()->getMovableControlPoints()[0]).x();
            _glob_min.y() = (*_bcurvess)[_selected_bcurve]->getBCurve()->getControlPoint((*_bcurvess)[_selected_bcurve]->getBCurve()->getMovableControlPoints()[0]).y();
            _glob_min.z() = (*_bcurvess)[_selected_bcurve]->getBCurve()->getControlPoint((*_bcurvess)[_selected_bcurve]->getBCurve()->getMovableControlPoints()[0]).z();

        }

        fstream f("numerikus.txt", std::ios_base::out | std::ios_base::app);

        if (!f || !f.good())
        {
            f.close();
            return;
        }

        f << "_n : " << (*_bcurvess)[_selected_bcurve]->getBCurve()->getN() << endl;
        f << "_alpha : " << (*_bcurvess)[_selected_bcurve]->getBCurve()->getAlpha() << endl;
        f << "_type : " << (*_bcurvess)[_selected_bcurve]->getBCurve()->getType() << endl;
        f << "_max_order : " << (*_bcurvess)[_selected_bcurve]->getBCurve()->getMaxOrderOfDerivatives() << endl;
        for(GLint i=0; i<(*_bcurvess)[_selected_bcurve]->getBCurve()->getMaxOrderOfDerivatives(); i++){
            f << "_weight : " << (*_bcurvess)[_selected_bcurve]->getBCurve()->getWeights()[i] << endl;
        }
        f << "energy : " << string.toStdString() << endl;






    }



    _side_widget->global_min_energy_edit->setText(string);


    update();
}

void GLWidget::on_energy_on_sphere_checkBox_clicked(bool checked)
{
    _show_sphere = checked;
    calculateEnergySphere();
    update();
}

void GLWidget::calculateEnergySphere(){
    QString string = "";
    if(_show_sphere){

        if(_selected_bcurve != -1 && (*_bcurvess)[_selected_bcurve]->getBCurve()->countMovableControlPoints() == 1){
            (*_bcurvess)[_selected_bcurve]->getBCurve()->SetControlPoint((*_bcurvess)[_selected_bcurve]->getBCurve()->getMovableControlPoints()[0],  _sphere_alpha,  _sphere_beta, _ray_of_sphere, _glob_min);
            (*_bcurvess)[_selected_bcurve]->generateImages();

            string.setNum( (*_bcurvess)[_selected_bcurve]->getBCurve()->CalculateEnergyFunctional(), 'f');
        }

    } else {
        on_global_min_checkBox_clicked(true);
    }
    _side_widget->energy_ray_edit->setText(string);
}


void GLWidget::change_color(int value)
{
    Color4* color;
    switch (value) {
    case 0:
        color = new Color4(1.0f, 1.0f, 0.0f, 0.0f);
        break;
    case 1:
        color = new Color4(0.0f, 1.0f, 1.0f, 1.0f);
        break;
    case 2:
        color = new Color4(0.0f, 8.0f, 0.0f, 1.0f);
        break;
    case 3:
        color = new Color4(1.0f, 0.5f, 0.0f, 0.0f);
        break;
    case 4:
        color = new Color4(1.0f, 0.0f, 1.0f, 0.0f);
        break;
    case 5:
        color = new Color4(1.0f, 1.0f, 1.0f, 0.0f);
        break;
    }
    delete color;
    //***     _curves -> SetColor(_selected_curve, color);
    update();
}


void GLWidget::on_curvatureComb_checkBox_clicked(bool checked)
{
    _show_curvature_comb = checked;
    update();
}

void GLWidget::on_scale_doubleSpinBox_valueChanged(double arg1)
{
    _scale = arg1;
    _bcurvess->CreateImage(_scale);
    update();
}


void GLWidget::on_show_bcurve_tangent_check_box_stateChanged(bool value)
{
    _show_bcurve_tangent = value;
    update();
}


void GLWidget::on_join_curve_button_3_clicked()
{
    if (_selected_bcurve >= 0 &&  select_join >= 0) {
        mergeOrJoin = JOIN;
        BCurve3* joined_curve = _bcurvess->getBCurve(_selected_bcurve);
        //        joined_curve->setWeights(_bcurvess->getBCurve(_selected_bcurve)->getWeights());
        _bcurvess->joinArcs(joined_curve, _selected_bcurve, select_join, _direction_of_curve_1, _direction_of_curve_2, _order_of_derivative_join1, _order_of_derivative_join2, GL_FALSE);
    }
    update();
}

void GLWidget::on_merge_pushButton_clicked()
{
    if (_selected_bcurve >= 0 &&  select_join >= 0) {
        mergeOrJoin = MERGE;
        _bcurvess->mergeArcs(_selected_bcurve, select_join, _direction_of_curve_1, _direction_of_curve_2, _order_of_derivative_join1);
    }
    update();
}

void GLWidget::on_deleteButton_clicked()
{
    //  _bcurvess->generateMatlabCode();
    if (_bcurvess->getIsHermite()) {
        _bcurvess->deleteAllArcAttributes();
        _bcurvess->setIsHermite(false);
        _side_widget->insert_bcurve_button->setEnabled(true);
        _side_widget->hermite_pushButton->setEnabled(true);
        _side_widget->hermite_m_spinBox->setEnabled(true);
        _side_widget->hermite_n_spinBox->setEnabled(true);
        _selected_bcurve = -1;
        _side_widget->render_tangent_checkBox->setCheckState(Qt::Unchecked);
        _render_tangent =false;
        delete _hermite_bcurve;
        _hermite_bcurve = nullptr;
    } else
        if (_selected_bcurve >= 0) {
            _bcurvess->deleteArc(_selected_bcurve);
            _bcurvess->setSelected(-1);
            _selected_bcurve = -1;
            calculateEnergy();
        }
    update();
}


void GLWidget::on_order_of_derivative_join_valueChanged(int arg1)
{
    if (select_join < 0) {
        _order_of_derivative_join1 = 0;
        _side_widget->order_of_derivative_join->setValue(_order_of_derivative_join1);
    }
    else if (arg1 <= 2 * (GLint) _bcurvess->getBCurve(select_join)->getN())
        _order_of_derivative_join1 = arg1;
    else
        _side_widget->order_of_derivative_join->setValue(_order_of_derivative_join1);
    if (_selected_bcurve >= 0 &&  select_join >= 0) {

        if (mergeOrJoin == MERGE) {
            _bcurvess->mergeArcs(_selected_bcurve, select_join, _direction_of_curve_1, _direction_of_curve_2, _order_of_derivative_join1);
        }
        else if(mergeOrJoin == JOIN) {
            BCurve3* joined_curve;
            _bcurvess->joinArcs(joined_curve, _selected_bcurve, select_join, _direction_of_curve_1, _direction_of_curve_2, _order_of_derivative_join1, _order_of_derivative_join2, GL_TRUE);
        }
    }
    update();
}


void GLWidget::on_order_of_derivative_join_3_valueChanged(int arg1)
{
    if (_selected_bcurve < 0) {
        _order_of_derivative_join2 = 0;
        _side_widget->order_of_derivative_join_3->setValue(_order_of_derivative_join2);
    }
    else if (arg1 <= 2 * (GLint) _bcurvess->getBCurve(_selected_bcurve)->getN())
        _order_of_derivative_join2 = arg1;
    else
        _side_widget->order_of_derivative_join_3->setValue(_order_of_derivative_join2);

    if (mergeOrJoin == JOIN && _selected_bcurve >= 0 &&  select_join >= 0) {
        BCurve3* joined_curve;
        _bcurvess->joinArcs(joined_curve, _selected_bcurve, select_join, _direction_of_curve_1, _direction_of_curve_2, _order_of_derivative_join1, _order_of_derivative_join2, GL_TRUE);
    }
    update();
}

void GLWidget::on_first_selected_bcurve_join_valueChanged(int arg1)
{
    _index_of_curve_1 = arg1;
}

GLvoid GLWidget::setWeightValue(GLdouble val, GLint index) {
    switch(index){
    case 0:
        _side_widget->w0_spinBox->setValue(val);
        break;
    case 1:
        _side_widget->w1_spinBox->setValue(val);
        break;
    case 2:
        _side_widget->w2_spinBox->setValue(val);
        break;
    case 3:
        _side_widget->w3_spinBox->setValue(val);
        break;
    case 4:
        _side_widget->w4_spinBox->setValue(val);
        break;
    case 5:
        _side_widget->w5_spinBox->setValue(val);
        break;
    }
}


GLvoid GLWidget::setWeights(GLboolean val, GLint index){
    switch(index){
    case 0:
        _side_widget->w0_spinBox->setVisible(val);
        _side_widget->label_9->setVisible(val);
        break;
    case 1:
        _side_widget->w1_spinBox->setVisible(val);
        _side_widget->label_10->setVisible(val);
        break;
    case 2:
        _side_widget->w2_spinBox->setVisible(val);
        _side_widget->label_11->setVisible(val);
        break;
    case 3:
        _side_widget->w3_spinBox->setVisible(val);
        _side_widget->label_12->setVisible(val);
        break;
    case 4:
        _side_widget->w4_spinBox->setVisible(val);
        _side_widget->label_13->setVisible(val);
        break;
    case 5:
        _side_widget->w5_spinBox->setVisible(val);
        _side_widget->label_14->setVisible(val);
        break;

    }
}

void GLWidget::on_setOrderOfDerivatives_spinbox_valueChanged(int arg1)
{
    if(page_previous == 0 && _page_number == 2){
        if(_selected_bcurve >= 0){
            _bcurvess->getBCurve(_selected_bcurve)->SetMaxOrderOfDerivatives(arg1);
        }
    }
    if(page_previous == 1 && _page_number == 2) {
        theta = arg1+1;
        w.ResizeColumns(theta);
    }

    for(GLint i = 0; i< 6; i++){
        if(i<=arg1){
            setWeights(true, i);
        } else{
            setWeights(false,i);
        }
    }


}

void GLWidget::on_w0_spinBox_valueChanged(double arg1)
{
    if(page_previous == 0 && _page_number == 2) {
        if(_selected_bcurve >=0){
            _bcurvess->getBCurve(_selected_bcurve)->setWeight(0, arg1);

            //            if(_opt_bcurve) {
            //                if((*_bcurvess)[_selected_bcurve]->useAttractor)
            (*_bcurvess)[_selected_bcurve]->involveAttractor();
            //                else
            //                    on_global_min_checkBox_clicked(_opt_bcurve);
            //                 calculateOptimalEnergy();
            //            } else
            calculateEnergy();
            if (_bcurvess->getIsHermite()) {
                _hermite_bcurve->ModifyBCurve3(_selected_bcurve);
                (*_bcurvess)[_selected_bcurve]->generateImages();

            }
            _side_widget->global_min_checkBox->setCheckState(Qt::Unchecked);
            _side_widget->energy_checkBox->setCheckState(Qt::Unchecked);
            _side_widget->energy_on_sphere_checkBox->setCheckState(Qt::Unchecked);
        }
        if(page_previous == 1 && _page_number == 2) {
            w[0] = arg1;
            _side_widget->optimize_bsurface_checkBox->setCheckState(Qt::Unchecked);
        }
    }
    update();
}

void GLWidget::on_w1_spinBox_valueChanged(double arg1)
{
    if(_selected_bcurve >=0){
        _bcurvess->getBCurve(_selected_bcurve)->setWeight(1, arg1);
        //        if(_opt_bcurve) {
        //            if((*_bcurvess)[_selected_bcurve]->useAttractor)
        (*_bcurvess)[_selected_bcurve]->involveAttractor();
        //            else
        //                on_global_min_checkBox_clicked(_opt_bcurve);
        //             calculateOptimalEnergy();
        //        } else
        calculateEnergy();
        if (_bcurvess->getIsHermite()) {
            _hermite_bcurve->ModifyBCurve3(_selected_bcurve);
            (*_bcurvess)[_selected_bcurve]->generateImages();

        }
        _side_widget->global_min_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_on_sphere_checkBox->setCheckState(Qt::Unchecked);
    }

    if(page_previous == 1 && _page_number == 2) {
        w[1] = arg1;
        _side_widget->optimize_bsurface_checkBox->setCheckState(Qt::Unchecked);

    }
    update();
}

void GLWidget::on_w2_spinBox_valueChanged(double arg1)
{
    if(_selected_bcurve >=0){
        _bcurvess->getBCurve(_selected_bcurve)->setWeight(2, arg1);
        //        if(_opt_bcurve) {
        //            if((*_bcurvess)[_selected_bcurve]->useAttractor)
        (*_bcurvess)[_selected_bcurve]->involveAttractor();
        //            else
        //                on_global_min_checkBox_clicked(_opt_bcurve);
        //             calculateOptimalEnergy();
        //        } else
        calculateEnergy();
        if (_bcurvess->getIsHermite()) {
            _hermite_bcurve->ModifyBCurve3(_selected_bcurve);
            (*_bcurvess)[_selected_bcurve]->generateImages();

        }
        _side_widget->global_min_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_on_sphere_checkBox->setCheckState(Qt::Unchecked);
    }

    if(page_previous == 1 && _page_number == 2) {
        w[2] = arg1;
        _side_widget->optimize_bsurface_checkBox->setCheckState(Qt::Unchecked);

    }
    update();
}

void GLWidget::on_w3_spinBox_valueChanged(double arg1)
{
    if(_selected_bcurve >=0){
        _bcurvess->getBCurve(_selected_bcurve)->setWeight(3, arg1);
        //        if(_opt_bcurve) {
        //            if((*_bcurvess)[_selected_bcurve]->useAttractor)
        (*_bcurvess)[_selected_bcurve]->involveAttractor();
        //            else
        //                on_global_min_checkBox_clicked(_opt_bcurve);
        //             calculateOptimalEnergy();
        //        } else
        calculateEnergy();
        if (_bcurvess->getIsHermite()) {
            _hermite_bcurve->ModifyBCurve3(_selected_bcurve);
            (*_bcurvess)[_selected_bcurve]->generateImages();

        }
        _side_widget->global_min_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_on_sphere_checkBox->setCheckState(Qt::Unchecked);
    }

    if(page_previous == 1 && _page_number == 2) {
        w[3] = arg1;
        _side_widget->optimize_bsurface_checkBox->setCheckState(Qt::Unchecked);

    }
    update();
}

void GLWidget::on_w4_spinBox_valueChanged(double arg1)
{
    if(_selected_bcurve >=0){
        _bcurvess->getBCurve(_selected_bcurve)->setWeight(4, arg1);
        //        if(_opt_bcurve) {
        //            if((*_bcurvess)[_selected_bcurve]->useAttractor)
        (*_bcurvess)[_selected_bcurve]->involveAttractor();
        //            else
        //                on_global_min_checkBox_clicked(_opt_bcurve);
        //             calculateOptimalEnergy();
        //        } else
        calculateEnergy();
        if (_bcurvess->getIsHermite()) {
            _hermite_bcurve->ModifyBCurve3(_selected_bcurve);
            (*_bcurvess)[_selected_bcurve]->generateImages();

        }
        _side_widget->global_min_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_on_sphere_checkBox->setCheckState(Qt::Unchecked);

    }

    if(page_previous == 1 && _page_number == 2) {
        w[4] = arg1;
        _side_widget->optimize_bsurface_checkBox->setCheckState(Qt::Unchecked);

    }
    update();
}

void GLWidget::on_w5_spinBox_valueChanged(double arg1)
{
    if(_selected_bcurve >=0){
        _bcurvess->getBCurve(_selected_bcurve)->setWeight(5, arg1);
        //        if(_opt_bcurve) {
        //            if((*_bcurvess)[_selected_bcurve]->useAttractor)
        (*_bcurvess)[_selected_bcurve]->involveAttractor();
        //            else
        //                on_global_min_checkBox_clicked(_opt_bcurve);
        //             calculateOptimalEnergy();
        //        } else
        calculateEnergy();
        if (_bcurvess->getIsHermite()) {
            _hermite_bcurve->ModifyBCurve3(_selected_bcurve);
            (*_bcurvess)[_selected_bcurve]->generateImages();

        }
        _side_widget->global_min_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_checkBox->setCheckState(Qt::Unchecked);
        _side_widget->energy_on_sphere_checkBox->setCheckState(Qt::Unchecked);
    }

    if(page_previous == 1 && _page_number == 2) {
        w[5] = arg1;
        _side_widget->optimize_bsurface_checkBox->setCheckState(Qt::Unchecked);

    }
    update();
}

void GLWidget::on_render_tangent_checkBox_clicked(bool checked)
{
    _render_tangent = checked;
    if (_hermite_bcurve != nullptr) {
        update();
    }
    else {
        _render_tangent = false;
        _side_widget->render_tangent_checkBox->setCheckState(Qt::Unchecked);
    }
}


void GLWidget::on_second_selected_bcurve_spinBox_3_valueChanged(int arg1)
{
    _index_of_curve_2 = arg1;
}

void GLWidget::on_show_bcurve_accelaration_check_box_stateChanged(bool value)
{
    _show_bcurve_acceleration = value;
    update();
}

void GLWidget::on_curve_first_direction_combo_box_2_currentIndexChanged(int index)
{
    _direction_of_curve_1 = index;
}

void GLWidget::on_curve_second_direction_combo_box_2_currentIndexChanged(int index)
{
    _direction_of_curve_2 = index;
}

void GLWidget::on_type_comboBox_currentIndexChanged(int index)
{
    _basis_type = index;
}


void GLWidget::on_setOrder_spinbox_valueChanged(int arg1) {
    _n = arg1;
}

void GLWidget::on_setAlpha_doubleSpinBox_valueChanged(double arg1) {
    _alpha = arg1;
    //    if(_selected_bcurve != -1){
    //        _bcurvess->getBCurve(_selected_bcurve)->SetAlpha(_alpha);
    //        (*_bcurvess)[_selected_bcurve]->generateImages();
    //    }
    update();
}

void GLWidget::on_selected_bcurve_spinBox_valueChanged(int value)
{
    if(value < (GLint)_bcurvess -> getSize())
    {
        _selected_bcurve = value;
        // renderPoints();



        Color4* _color_saved = _bcurvess->getColor(_selected_bcurve);
        _bcurvess -> SetColor(_selected_bcurve, _red);
        update();
        _bcurvess -> SetColor(_selected_bcurve, _color_saved);

        delete _color_saved;
    }
}


void GLWidget::on_curve_attractorcheckBox_clicked(bool checked)
{
    if(_selected_bcurve >=0){
        //        _bcurvess->setUseAttractor(_selected_bcurve, checked);
        if(_bcurvess->getAttributes()[_selected_bcurve]->useAttractor == 2){
            _side_widget->point_attractor_checkBox->setCheckState(Qt::Unchecked);
            _bcurvess->getAttractor(_selected_bcurve)->RestoreControlPoints();
            GLuint size =  _bcurvess->getAttractor(_selected_bcurve)->getN()*2+1;
            for(GLuint i = 0; i < size; i++){
                _bcurvess->getAttractor(_selected_bcurve)->removeMovableControlPoint(i);
            }
        }
        if(checked)
            _bcurvess->getAttributes()[_selected_bcurve]->useAttractor = 1;
        else
            _bcurvess->getAttributes()[_selected_bcurve]->useAttractor = 0;

        _bcurvess->involveAttractor(_selected_bcurve);
        calculateOptimalEnergy();
        if (checked == false && _bcurvess->getIsHermite()) {
            _hermite_bcurve->ModifyBCurve3(_selected_bcurve);
        }
        (*_bcurvess)[_selected_bcurve]->generateImages();
    }
    update();
}

void GLWidget::on_point_attractor_checkBox_clicked(bool checked)
{
    if (_selected_bcurve != -1) {
        _bcurvess->setUseAttractor(_selected_bcurve, checked);
        if (checked) {
            _bcurvess->getAttractor(_selected_bcurve)->IdenticalControlPoints();
            _bcurvess->getAttributes()[_selected_bcurve]->useAttractor = 2;
            _side_widget->curve_attractorcheckBox->setCheckState(Qt::Unchecked);
        }
        else {
            _bcurvess->getAttractor(_selected_bcurve)->RestoreControlPoints();
            GLuint size =  _bcurvess->getAttractor(_selected_bcurve)->getN()*2+1;
            for(GLuint i = 0; i < size; i++){
                _bcurvess->getAttractor(_selected_bcurve)->removeMovableControlPoint(i);
            }
            _bcurvess->getAttributes()[_selected_bcurve]->useAttractor = 0; // vagy 0 ?
        }
        _bcurvess->involveAttractor(_selected_bcurve);
        calculateOptimalEnergy();
        _bcurvess->getAttributes()[_selected_bcurve]->generateImages();
        update();
    }
}

void GLWidget::on_toolBox_currentChanged(int index)
{
    if(index == 2){
        select_join = -1;
        _bcurvess->setSelected2(-1);
    } else{
        select_join = -2;
        _bcurvess->setSelected2(-2);
    }
}


void GLWidget::on_hermite_m_spinBox_valueChanged(int arg1)
{
    hermite_m = arg1;
}


void GLWidget::on_hermite_rho_spinBox_valueChanged(int arg1)
{
    if (arg1 < (GLint) hermite_n)
        hermite_rho = arg1;
    else
        _side_widget->hermite_rho_spinBox->setValue(hermite_rho);

    if (_hermite_bcurve != nullptr) {
        _hermite_bcurve->setRho(hermite_rho);
        _hermite_bcurve->modifyTRThetaFi();
        _bcurvess->deleteAllArcAttributes();
        _selected_bcurve = -1;
        _hermite_bcurve->GenerateOptimalHermiteComposite();
        update();
    }
}

void GLWidget::on_hermite_n_spinBox_valueChanged(int arg1)
{
    hermite_n = arg1;
    if (arg1 <= (GLint) hermite_rho) {
        hermite_rho = arg1 - 1;
        _side_widget->hermite_rho_spinBox->setValue(hermite_rho);
    }
}


//SURFACE

void GLWidget::set_index_of_surface1(int index){
    _selected_bsurface = index;
    if(_selected_bsurface > 0){
        setFixedIndexes();
    }
    update();
}

GLvoid GLWidget::setFixedIndexes(){

}


void GLWidget::set_index_of_surface2(int index){
    _selected_bsurface2 = index;
    update();
}


void GLWidget::on_surface_toolBox_currentChanged(int index)
{
    sf_toolBox = index;
    if(index == 3 && _selected_bsurface != -1){
        _side_widget->first_surface_spinbox->setValue(_selected_bsurface);
    }
    update();
}

GLvoid GLWidget::showEnergies(){


    AbstractSurfaceEnergyFragment *surface = new NormalLengthEnergyFragment((*_bsurfaces)[_selected_bsurface]);
    //    cout << "NormalLengthEnergyFragment " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
    delete surface;

    surface = new GaussianCurvatureEnergyFragment((*_bsurfaces)[_selected_bsurface]);
    //    cout << "GaussianCurvatureEnergyFragment: " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
    _side_widget->gaussianEnergy_doubleSpinBox->setValue((*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100));
    delete surface;

    surface = new WilmoreEnergyFragment((*_bsurfaces)[_selected_bsurface]);
    //    cout << "WilmoreEnergyFragment: " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
    _side_widget->wilmoreEnergy_doubleSpinBox->setValue((*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100));

    delete surface;

    surface = new MeanCurvatureEnergyFragment((*_bsurfaces)[_selected_bsurface]);
    //    cout << "MeanCurvatureEnergyFragment: " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
    _side_widget->meanEnergy_doubleSpinBox->setValue((*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100));

    delete surface;

    surface = new UmblicDeviationEnergyFragment((*_bsurfaces)[_selected_bsurface]);
    //    cout << "UmblicDeviationEnergyFragment: " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
    _side_widget->umblicEnergy_doubleSpinBox->setValue((*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100));
    delete surface;

    surface = new TotalCurvatureEnergyFragment(_bsurface);
    //    cout << "TotalCurvatureEnergyFragment: " << _bsurface->calculateEnergy(surface, 100, 100) << endl;
    _side_widget->totalEnergy_doubleSpinBox->setValue(_bsurface->calculateEnergy(surface, 100, 100));
    delete surface;

    if(_optimize){
        surface = new GaussianCurvatureEnergyFragment(_opt_bsurface);
        //        cout << "GaussianCurvatureEnergyFragment: " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
        _side_widget->optimal_gaussianEnergy_doubleSpinBox->setValue((*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100));
        delete surface;

        surface = new WilmoreEnergyFragment(_opt_bsurface);
        //        cout << "WilmoreEnergyFragment: " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
        _side_widget->optimal_wilmoreEnergy_doubleSpinBox->setValue((*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100));

        delete surface;

        surface = new MeanCurvatureEnergyFragment(_opt_bsurface);
        //        cout << "MeanCurvatureEnergyFragment: " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
        _side_widget->optimal_meanEnergy_doubleSpinBox->setValue((*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100));

        delete surface;

        surface = new UmblicDeviationEnergyFragment(_opt_bsurface);
        //        cout << "UmblicDeviationEnergyFragment: " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
        _side_widget->optimal_umblicEnergy_doubleSpinBox->setValue((*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100));
        delete surface;

        surface = new TotalCurvatureEnergyFragment(_opt_bsurface);
        //        cout << "TotalCurvatureEnergyFragment: " << (*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100) << endl;
        _side_widget->optimal_totalEnergy_doubleSpinBox->setValue((*_bsurfaces)[_selected_bsurface]->calculateEnergy(surface, 100, 100));
        delete surface;

    }
}


void GLWidget::on_optimize_bsurface_checkBox_clicked(bool checked)
{
    _optimize = checked;
    if(_optimize && _selected_bsurface != -1){


        BSurface3* temp = _bsurfaces->getBSurface(_selected_bsurface);
        //        BSurface3* temp2 = new BSurface3(temp->getAlpha(), temp->getN(), temp->getBeta(), temp->getM());
        //        _opt_bsurface = new OptimizedBSurfaces3(*temp);
        //    _opt_bsurface = temp;
        _opt_bsurface = new OptimizedBSurfaces3(temp->getAlpha(), temp->getN(), temp->getBeta(), temp->getM());

        //        _side_widget->quadraticEnergy_doubleSpinBox->setValue(_opt_bsurface->calculateEnergy());
        _opt_bsurface->setBSurface(_bsurfaces->getBSurface(_selected_bsurface));


        if( ! _opt_bsurface){

            throw Exception( "Jajjj! " ) ;
        }

        if(_opt_bsurface){

            _opt_bsurface->setData(w, u_div, v_div);

            _side_widget->quadraticEnergy_doubleSpinBox->setValue(_opt_bsurface->calculateEnergy());

            _opt_bsurface->performOptimization(w,_bsurfaces->getFixedIndex(_selected_bsurface), u_div, v_div);

            if( ! _opt_bsurface->UpdateVertexBufferObjectsOfData( )){

                throw Exception( "Jajjj! " ) ;
            }


            QString string = "";
            string.setNum(_opt_bsurface->calculateEnergy());

            _side_widget->energy_surface_lineEdit->setText(string);
            _side_widget->optimal_quadraticEnergy_doubleSpinBox->setValue(_opt_bsurface->calculateEnergy());

            if (_img_opt_bsurface)
            {
                delete _img_opt_bsurface;
                _img_opt_bsurface = nullptr;
            }

            _img_opt_bsurface = _opt_bsurface -> GenerateImage(u_div, v_div, GL_STATIC_DRAW, _color_scheme_type);

            if( !_img_opt_bsurface){

                throw Exception( "Jajjj ! " ) ;
            }

            if( ! _img_opt_bsurface -> UpdateVertexBufferObjects()){

                throw Exception( "Jajjj ! " ) ;
            }
        }
    }
    showEnergies();


    update();
}

void GLWidget::on_optimize_bsurface_pushButton_clicked()
{
    if(_optimize && _selected_bsurface != -1){
        //        (*_bsurface) = (*_opt_bsurface);

        //        _img_opt_bsurface = nullptr;

        _bsurfaces->setBSurface(_selected_bsurface, _opt_bsurface);

        //        on_energy_comboBox_currentIndexChanged(_color_scheme_type);
        if (_img_opt_bsurface)
        {
            delete _img_opt_bsurface;
            _img_opt_bsurface = nullptr;
        }

        _optimize = GL_FALSE;
        _side_widget->optimize_bsurface_checkBox->setCheckState(Qt::Unchecked);
    }
    update();
}

void GLWidget::insert_surface()
{
    _side_widget->tableWidget->clear();
    initalizeBSurface();
    if(_bsurfaces->getSize() == 1)
        _selected_bsurface = 0;
    update_table_widget();

    _shader_selected = 3;
    _side_widget->shader_combo_box->setCurrentIndex(_shader_selected);



    update();
}

void GLWidget::joinSurface(){
    _bsurfaces->joinSurfaces(_index_of_surface_1, _index_of_surface_2, _direction_of_surface_1, _direction_of_surface_2, 0,0,_shaders[1], _materials[0]);
    update();
}

void GLWidget::mergeSurface(){
    if (_index_of_surface_1 >= 0 &&  _index_of_surface_2 >= 0 && _direction_of_surface_1 < 2 &&  _direction_of_surface_2 < 2) {
        _bsurfaces->UmergeSurfaces(_index_of_surface_1, _index_of_surface_2, _direction_of_surface_1, _direction_of_surface_2, _order_surface);
    }else if (_index_of_surface_1 >= 0 &&  _index_of_surface_2 >= 0 && _direction_of_surface_1 >= 2 &&  _direction_of_surface_2 >= 2){
        _bsurfaces->VmergeSurfaces(_index_of_surface_1, _index_of_surface_2, _direction_of_surface_1, _direction_of_surface_2, _order_surface);
    }
    _bsurfaces->generateImage(_color_scheme_type);
    update();
}

void GLWidget::on_delete_surface_pushButton_clicked()
{
    if(_selected_bsurface != -1) {
        _bsurfaces->deleteSurface(_selected_bsurface);
        _selected_bsurface = -1;
    }
    update();
}

void GLWidget::on_order_surface_spinBox_valueChanged(int arg1)
{
    _order_surface = arg1;
}

GLvoid GLWidget::on_tableWidget_cellClicked(int row, int column)
{
    _side_widget->optimize_bsurface_checkBox->setCheckState(Qt::Unchecked);

    pair<GLint, GLint> p(row, column);


    //    if (::find(_bsurfaces->getFixedIndex(_selected_bsurface).begin(), _bsurfaces->getFixedIndex(_selected_bsurface).end(), p) == _bsurfaces->getFixedIndex(_selected_bsurface).end())
    if(_bsurfaces->findFixedIndex(_selected_bsurface,p))
    {
        //        fixed_index_pairs.push_back(p);
        _bsurfaces->addFixedIndex(_selected_bsurface,p);

    } else {
        //        fixed_index_pairs.erase(std::remove(fixed_index_pairs.begin(), fixed_index_pairs.end(), p), fixed_index_pairs.end());
        _bsurfaces->eraseFixedIndex(_selected_bsurface,p);

    }

    update();

}

void GLWidget::change_selected_surface(int value){
    if(value < _bsurfaces->getSize()){
        _selected_bsurface = value;
        update_table_widget();
    }
    update();
}

GLvoid GLWidget::create_bsurface_image() {
    if( ! _bsurface){

        throw Exception( "Error: _bsurface is nullptr in create_bsurface_image()." ) ;
    }

    if( ! _bsurface->UpdateVertexBufferObjectsOfData( )){

        throw Exception( "Error: UpdateVertexBufferObjectsOfData returned false in create_bsurface_image()." ) ;
    }

    if(_bsurface_2){
        if( ! _bsurface_2->UpdateVertexBufferObjectsOfData( )){

            throw Exception( "Error: UpdateVertexBufferObjectsOfData returned false in create_bsurface_image()." ) ;
        }

    }


    generate_bsurface_image(0);
}

GLvoid GLWidget::u_modify_fixed_index_pairs(GLuint z) {
    if(_selected_bsurface != -1){
        GLint sizeN = 2*(*_bsurfaces)[_selected_bsurface]->getN() + 1;
        GLint sizeM = 2*(*_bsurfaces)[_selected_bsurface]->getM() + 1;
        if (sizeN - 2 * z > 0) {
            for (GLint i = sizeN - 2 * z; i < sizeN; ++i) {
                for (GLint j = 0; j < sizeM; ++j) {
                    pair<GLint, GLint> p(i, j);
                    //                    fixed_index_pairs.push_back(p);
                    _bsurfaces->addFixedIndex(_selected_bsurface,p);
                }
            }
        }
    }
}

GLvoid GLWidget::v_modify_fixed_index_pairs(GLuint z) {
    if(_selected_bsurface != -1){
        GLint sizeN = 2*(*_bsurfaces)[_selected_bsurface]->getN() + 1;
        GLint sizeM = 2*(*_bsurfaces)[_selected_bsurface]->getM() + 1;

        if (sizeM - 2 * z > 0) {
            for (GLint i = 0; i < sizeN; ++i) {
                for (GLint j = sizeM - 2 * z; j < sizeM; ++j) {
                    pair<GLint, GLint> p(i, j);
                    //                    fixed_index_pairs.push_back(p);
                    _bsurfaces->addFixedIndex(_selected_bsurface,p);
                }
            }
        }
    }
}

void GLWidget::on_u_increase_order_pushButton_clicked()
{
    //    if(_bsurface != nullptr) {
    //        BSurface3* oldBsurface = _bsurface;

    //        GLuint z = 1;

    //        _bsurface = _bsurface->UIncreaseOrder(z);

    //        delete oldBsurface;

    //        _n_bsurface += z;
    if(_selected_bsurface != -1){
        _bsurfaces->setIncreaseOrder(_selected_bsurface, 0, _shaders[_shader_selected]);
    }

    u_modify_fixed_index_pairs();

    //        create_bsurface_image();

    update_table_widget();

    //    }

    update();


}

void GLWidget::on_v_increase_order_pushButton_clicked()
{
    //    if(_bsurface != nullptr) {
    //        BSurface3* oldBsurface = _bsurface;

    //        GLuint z = 1;

    //        _bsurface = _bsurface->VIncreaseOrder(z);
    //        delete oldBsurface;


    //        _m_bsurface += z;
    if(_selected_bsurface != -1){
        _bsurfaces->setIncreaseOrder(_selected_bsurface, 1, _shaders[_shader_selected]);
    }

    v_modify_fixed_index_pairs();

    //    create_bsurface_image();

    update_table_widget();

    //}

    update();
}

void GLWidget::on_u_subdivision_pushButton_clicked()
{
    if(_selected_bsurface != -1){
        _bsurfaces->setSubdivision(_selected_bsurface, 0, _shaders[_shader_selected]);
    }


    update_table_widget();

    _selected_bsurface = -1;


    //    if(_bsurface != nullptr) {
    //        BSurface3* oldBsurface = _bsurface;

    //        GLdouble umin, umax;
    //        _bsurface->GetUInterval(umin, umax);

    //        RowMatrix<BSurface3*>* subdivisionSurfaces = _bsurface->USubdivision(0.5 * umax);


    //        delete oldBsurface;

    //        _bsurface_2 = (*subdivisionSurfaces)[0];
    //        _bsurface = (*subdivisionSurfaces)[1];

    //        create_bsurface_image();
    //    }

    update();

}

void GLWidget::on_v_subdivision_pushButton_clicked()
{
    //    if(_bsurface != nullptr) {
    //        BSurface3* oldBsurface = _bsurface;

    //        GLdouble vmin, vmax;
    //        _bsurface->GetVInterval(vmin, vmax);

    //        RowMatrix<BSurface3*>* subdivisionSurfaces = _bsurface->VSubdivision(0.5 * vmax);


    //        delete oldBsurface;

    //        _bsurface_2 = (*subdivisionSurfaces)[0];
    //        _bsurface = (*subdivisionSurfaces)[1];

    //        create_bsurface_image();
    //    }
    if(_selected_bsurface != -1){
        _bsurfaces->setSubdivision(_selected_bsurface, 1, _shaders[_shader_selected]);
    }


    update_table_widget();

    _selected_bsurface = -1;


    update();
}



GLvoid GLWidget::generate_bsurface_image(GLuint index) {
    if (_img_bsurface)
    {
        delete _img_bsurface;
        _img_bsurface = nullptr;
    }

    _img_bsurface = _bsurface -> GenerateImage(30, 30, GL_STATIC_DRAW, _color_scheme[index]);

    if( !_img_bsurface){

        throw Exception( "Error: GenerateImage returned nullptr in generate_bsurface_image()." ) ;
    }

    if( ! _img_bsurface -> UpdateVertexBufferObjects()){

        throw Exception( "Error: UpdateVertexBufferObjects returned false in generate_bsurface_image()." ) ;
    }


    if (_img_bsurface_2)
    {
        delete _img_bsurface_2;
        _img_bsurface_2 = nullptr;
    }

    if(_bsurface_2){

        _img_bsurface_2 = _bsurface_2 -> GenerateImage(30, 30, GL_STATIC_DRAW, _color_scheme[index]);

        if( !_img_bsurface_2){

            throw Exception( "Error: GenerateImage returned nullptr in generate_bsurface_image()." ) ;
        }

        if( ! _img_bsurface_2 -> UpdateVertexBufferObjects()){

            throw Exception( "Error: UpdateVertexBufferObjects returned false in generate_bsurface_image()." ) ;
        }
    }
}


GLvoid GLWidget::update_table_widget() {
    if(_selected_bsurface != -1){
        GLint sizeN = (*_bsurfaces)[_selected_bsurface]->getN()*2+1;
        GLint sizeM = (*_bsurfaces)[_selected_bsurface]->getM()*2+1;

        for(GLint i = 0; i< sizeN;i++){
            for(GLint j = 0; j< sizeM; j++){
                delete _side_widget->tableWidget->cellWidget(i, j);
            }
        }
        _side_widget->tableWidget->clear();

        _side_widget->tableWidget->setColumnCount(sizeM);
        _side_widget->tableWidget->resizeColumnsToContents();

        for(GLint i = 0; i< sizeN;i++){
            _side_widget->tableWidget->insertRow(i);
            _side_widget->tableWidget->resizeRowsToContents();

            for(GLint j = 0; j< sizeM; j++){
                QTableWidgetItem *item = new QTableWidgetItem(QString("(%1,%2)").arg(i).arg(j));
                _side_widget->tableWidget->setItem(i,j,item);
            }
        }
    }
}


void GLWidget::on_setN_bsurface_spinbox_valueChanged(int arg1)
{
    _n_bsurface = arg1;
}

void GLWidget::on_setM_bsurface_spinbox_valueChanged(int arg1)
{
    _m_bsurface = arg1;
}

void GLWidget::on_setAlpha_bsurface_doubleSpinBox_valueChanged(double arg1)
{
    _alpha_bsurface = arg1;
}

void GLWidget::on_setBeta_doubleSpinBox_valueChanged(double arg1)
{
    _beta_bsurface = arg1;
}

GLvoid GLWidget::on_energy_comboBox_currentIndexChanged(int index)
{
    //    if(_selected_bsurface != -1){

    _color_scheme_type = static_cast<TensorProductSurface3::ImageColorScheme>(index);
    _shader_selected = _color_scheme_type > TensorProductSurface3::DEFAULT_NULL_FRAGMENT ? HEAT_MAP_TWO_SIDED_LIGHTING : UNIFORM_TWO_SIDED_LIGHTING;
    _side_widget->shader_combo_box->setCurrentIndex(_shader_selected);
    _bsurfaces->generateImage(_color_scheme[index]);
    //    _shader_selected = 4;
    //    generate_bsurface_image(index);

    //    }
    //    }
    update();
}
/*
void GLWidget::set_material(int indexOfMaterial)
{
    if(_selected_surface < (GLuint) _bsurface -> getSize())
    {
        _surfaces -> SetMaterial(_selected_surface, _materials[indexOfMaterial]);
        _surfaces -> SetControlColor(_selected_surface, _red);
        update();
        _surfaces -> SetControlColor(_selected_surface, _yellow);
    }
}
*/
void GLWidget::set_shader(int indexOfShader)
{
    //    if(_selected_bsurface < (GLint) _bsurfaces -> getSize()){
    if( _color_scheme_type > TensorProductSurface3::DEFAULT_NULL_FRAGMENT ) {
        _shader_selected = 4;
        //        : UNIFORM_TWO_SIDED_LIGHTING;
    } else {
        //        _bsurfaces -> SetShader(_selected_bsurface, _shaders[indexOfShader]);
        //        _surfaces -> SetControlColor(_selected_surface, _red);
        //        update();
        //        _surfaces -> SetControlColor(_selected_surface, _yellow);
        _shader_selected = indexOfShader;
    }
    update();
    //    }
}

void GLWidget::set_u_derivatives(bool value)
{
    _show_u_pd = value;
    update();
}

void GLWidget::set_v_derivatives(bool value)
{
    _show_v_pd = value;
    update();
}

void GLWidget::set_u_isoparametric(bool value)
{
    _show_u_ip = value;
    update();
}

void GLWidget::set_v_isoparametric(bool value)
{
    _show_v_ip = value;
    update();
}

void GLWidget::set_control(bool value)
{
    _show_control = value;
    update();
}

void GLWidget::set_normal(bool value)
{
    _show_normals = value;
    update();
}

void GLWidget::set_direction_of_surface1(int direction)
{
    _direction_of_surface_1 = direction;
}

void GLWidget::set_direction_of_surface2(int direction)
{
    _direction_of_surface_2 = direction;
}
/*

void GLWidget::joinSurface()
{
    _surfaces -> joinPatches(_index_of_surface_1, _index_of_surface_2, _direction_of_surface_1, _direction_of_surface_2, _shaders[1], _materials[0]);
    _surfaces -> SetControlColor(_index_of_surface_1, _red);
    _surfaces -> SetControlColor(_index_of_surface_2, _red);
    update();
    _surfaces -> SetControlColor(_index_of_surface_1, _yellow);
    _surfaces -> SetControlColor(_index_of_surface_2, _yellow);
}


void GLWidget::set_manipulated_surface(int index)
{
    if(index < _surfaces -> getSize())
    {
        _manipulated_surface_index = index;
        _surfaces -> SetControlColor(_manipulated_surface_index, _red);
        update();
        _surfaces -> SetControlColor(_manipulated_surface_index, _yellow);
    }
}

*/
void GLWidget::set_manipulated_point_i(int index)
{
    _manipulated_point_i = index;
}

void GLWidget::set_manipulated_point_j(int index)
{
    _manipulated_point_j = index;
}

void GLWidget::change_page(int number)
{
    page_previous = _page_number;
    _page_number = number;
    update();
}

void GLWidget:: updateShader()
{
    if (_shader_selected == 0){
        _shaders[0] -> Enable();
        _shaders[0] -> SetUniformVariable4fv("default_outline_color", 1,_shader_values);
        _shaders[0]-> Disable();
        //        _surfaces -> SetShader(_selected_surface, _shaders[0]);
        //        _surfaces -> SetControlColor(_selected_surface, _red);
        update();
        //        _surfaces -> SetControlColor(_selected_surface, _yellow);
    }
    else{
        _shaders[2] -> Enable();
        _shaders[2] -> SetUniformVariable1f("scale_factor", _shader_values[0]);
        _shaders[2] -> SetUniformVariable1f("smoothing", _shader_values[1]);
        _shaders[2] -> SetUniformVariable1f("shading", _shader_values[2]);
        _shaders[2]-> Disable();
        //        _surfaces -> SetShader(_selected_surface, _shaders[2]);
        //        _surfaces -> SetControlColor(_selected_surface, _red);
        update();
        //        _surfaces -> SetControlColor(_selected_surface, _yellow);
    }

}
void GLWidget::set_shader_1(double value)
{
    _shader_values[0] = (float)value;
    if (_shader_selected == 0 || _shader_selected == 2 )
        updateShader();
}

void GLWidget::set_shader_2(double value)
{
    _shader_values[1] = (float)value;
    if (_shader_selected == 0 || _shader_selected == 2 )
        updateShader();
}

void GLWidget::set_shader_3(double value)
{
    _shader_values[2] = (float)value;
    if (_shader_selected == 0 || _shader_selected == 2 )
        updateShader();
}

void GLWidget::on_show_text_checkBox_clicked(bool checked)
{
    _show_text = checked;
    update();
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    makeCurrent();

    event->accept();

    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    //itt?
    GLuint size = 4 * _row_count * _column_count;
    GLuint *pick_buffer = new GLuint[size];
    glSelectBuffer(size, pick_buffer);

    glRenderMode(GL_SELECT);

    glInitNames();
    glPushName(0);

    GLfloat projection_matrix[16];
    glGetFloatv(GL_PROJECTION_MATRIX, projection_matrix);

    glMatrixMode(GL_PROJECTION);

    glPushMatrix();

    glLoadIdentity();
    gluPickMatrix((GLdouble)event->x(), (GLdouble)(viewport[3] - event->y()), 5.0, 5.0, viewport);

    glMultMatrixf(projection_matrix);

    glMatrixMode(GL_MODELVIEW);

    glPushMatrix();

    // one should use the same model-view matrix as in the paintGL method, therefore:

    // rotating around the coordinate axes
    glRotatef(_angle_x, 1.0, 0.0, 0.0);
    glRotatef(_angle_y, 0.0, 1.0, 0.0);
    glRotatef(_angle_z, 0.0, 0.0, 1.0);

    // translate
    glTranslated(_trans_x, _trans_y, _trans_z);

    // scaling
    glScalef(_zoom, _zoom, _zoom);

    // render only the clickable geometries!
    //        renderPoints();
    if(_page_number == 0 || (page_previous == 0 && _page_number == 2)){
        if(_bcurvess->getIsHermite() && _selected_bcurve == -1){
            for (GLuint i = 0; i < _bcurvess->getSize(); i++)
                _bcurvess->RenderPoints(i, 0, _red);

        }

        if(_selected_bcurve >=0){
            _bcurvess->RenderPoints(_selected_bcurve, 0, _red);


            if(_bcurvess->getShowUseAttractor(_selected_bcurve)){
                //            renderAttractorPoints();
                Color4* color =new Color4(0.0f, 0.0f, 0.0f);

                _bcurvess->RenderPoints(_selected_bcurve, 1, color);

                delete color;

            }
        }

        _bcurvess->RenderDerivatives(0, GL_LINE_STRIP);
    }
    if(_page_number == 1 || (page_previous == 1 && _page_number == 2)){
        if(_selected_bsurface != -1)
            _renderSpheres();
        _bsurfaces->Render(_materials[6], _shaders[_shader_selected]);
    }

    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);

    // creating and analysing the selection buffer based on the detected hit counts
    int hit_count = glRenderMode(GL_RENDER);
    //        cout << hit_count << endl;

    // GLint closest_selected = -1;

    if (hit_count)
    {
        GLint closest_selected = pick_buffer[3];
        GLuint closest_depth    = pick_buffer[1];


        for (int i = 1; i < hit_count; ++i)
        {
            GLuint offset = i * 4;
            if (pick_buffer[offset + 1] < closest_depth)
            {
                closest_selected = pick_buffer[offset + 3];
                closest_depth    = pick_buffer[offset + 1];
            }
        }

        //            cout << "closest selected point depth " << closest_selected << " " << closest_depth << endl;

        if (event->button() == Qt::LeftButton && event->modifiers() == Qt::ControlModifier) {
            if(_page_number == 1){
                _row    = closest_selected / 1000;
                _column = closest_selected % 1000;

                pair<GLint, GLint> p(_row, _column);

                //                fixed_index_pairs.erase(std::remove(fixed_index_pairs.begin(), fixed_index_pairs.end(), p), fixed_index_pairs.end());
                _bsurfaces->eraseFixedIndex(_selected_bsurface,p);
            }

            if(_bcurvess->getIsHermite() && _selected_bcurve == -1){
                cout << "closest : " << closest_selected << endl;
                _bcurvess->setSelectedP(closest_selected);
            }


            if(_selected_bcurve != -1 && _bcurvess->getShowUseAttractor(_selected_bcurve)){
                _bcurvess->getAttractor(_selected_bcurve)->addMovableControlPoint(closest_selected/100);
            } else if(_selected_bcurve != -1 && closest_selected / 100 == 0){

                //                _bcurvess->getBCurve(_selected_bcurve)->addMovableControlPoint(closest_selected);

                BCurve3 * actualBCurve = _bcurvess->getBCurve(_selected_bcurve);
                //                actualBCurve->addMovableControlPoint(closest_selected);

                if (_bcurvess->getIsHermite()) {
                    if (closest_selected == 0 || closest_selected == 2 * (GLint) actualBCurve->getN()) {
                        _bcurvess->setSelectedPIndex(_selected_bcurve, closest_selected);                    }
                    if (closest_selected == 0 && _selected_bcurve >0) {

                        (*_bcurvess)[_selected_bcurve -1]->setSelectedPIndex((*_bcurvess)[_selected_bcurve -1]->getBCurve()->getN() * 2);

                        //                        BCurve3 * previousBCurve = _bcurvess->getBCurve(_selected_bcurve - 1);
                        //                        previousBCurve->addMovableControlPoint(2 * previousBCurve->getN());
                    }
                    if ( closest_selected ==  2 * (GLint) (*_bcurvess)[_selected_bcurve ]->getBCurve()->getN() && _selected_bcurve < (GLint) _bcurvess->getSize() - 1) {
                        (*_bcurvess)[_selected_bcurve +1]->setSelectedPIndex(0);

                    }
                    //                    if ( closest_selected == 2 * actualBCurve->getN() && _selected_bcurve < _bcurvess->getSize() - 1) {
                    //                        BCurve3 * nextBCurve = _bcurvess->getBCurve(_selected_bcurve + 1);
                    //                        nextBCurve->addMovableControlPoint(0);
                    //                    }
                } else{
                    BCurve3 * actualBCurve = _bcurvess->getBCurve(_selected_bcurve);
                    actualBCurve->addMovableControlPoint(closest_selected);
                }

            }
        } else if (event->button() == Qt::LeftButton && event->modifiers() == Qt::AltModifier)
        {
            if(_page_number == 1){
                _row    = closest_selected / 1000;
                _column = closest_selected % 1000;

                pair<GLint, GLint> p(_row, _column);

                //                fixed_index_pairs.push_back(p);
                _bsurfaces->addFixedIndex(_selected_bsurface,p);
            }

            if(_selected_bcurve != -1 && _bcurvess->getShowUseAttractor(_selected_bcurve)){
                _bcurvess->getAttractor(_selected_bcurve)->removeMovableControlPoint(closest_selected/100);
            } else if(_selected_bcurve != -1 && closest_selected / 100 == 0){
                //                _bcurvess->getBCurve(_selected_bcurve)->addMovableControlPoint(closest_selected);
                BCurve3 * actualBCurve = _bcurvess->getBCurve(_selected_bcurve);
                //                actualBCurve->removeMovableControlPoint(closest_selected);
                if (_bcurvess->getIsHermite()) {
                    if (closest_selected == 0 || closest_selected == 2 * (GLint) actualBCurve->getN())
                        _bcurvess->setSelectedPIndex(_selected_bcurve, -1);

                } else {
                    actualBCurve->removeMovableControlPoint(closest_selected);
                }

            }
        } else if (event->button() == Qt::LeftButton) {
            if(_page_number == 1){
                _selected_bsurface = closest_selected;
                cout << "a " << closest_selected << endl;
                update_table_widget();
            }
            if(select_join == -2){

                if (_selected_bcurve != closest_selected) {
                    if (_bcurvess->getIsHermite() && _selected_bcurve != -1) {
                        if (!_bcurvess->neighborsInSelectedPoint(_selected_bcurve, closest_selected)) {
                            _bcurvess->setSelectedPIndex(_selected_bcurve, -1);
                        }
                    }
                    _selected_bcurve = closest_selected;
                    _bcurvess->setSelected(closest_selected);
                    calculateEnergy();
                } else {
                    if (_bcurvess->getIsHermite() && _selected_bcurve != -1) {
                        _bcurvess->setSelectedPIndex(_selected_bcurve, -1);
                    }
                    _selected_bcurve = -1;
                    _bcurvess->setSelected(-1);
                }
            } else if(select_join == -1){
                if(_selected_bcurve == -1){
                    _selected_bcurve = closest_selected;
                    _bcurvess->setSelected(closest_selected);
                }
                else {
                    select_join = closest_selected;
                    _bcurvess->setSelected2(closest_selected);
                }
            }
            refreshData();
        }
    }



    delete[] pick_buffer;
    update();

}

void GLWidget::wheelEvent(QWheelEvent *event)
{
    event->accept();


    // wheel + Ctrl
    if (event->modifiers() & Qt::ControlModifier)
    {
        if(_page_number == 1){

            (*_bsurface)(_row, _column).x() += event->angleDelta().x() / 120.0 * _reposition_unit;
        }
        GLdouble modifyCoordinate = event->angleDelta().y() / 120.0 * _reposition_unit;
        modifyControlPointCoordinate(1, modifyCoordinate);

    }

    // wheel + Alt
    if (event->modifiers() & Qt::AltModifier)
    {
        GLdouble modifyCoordinate = event->angleDelta().x() / 120.0 * _reposition_unit;
        modifyControlPointCoordinate(0, modifyCoordinate);
    }

    if (event->modifiers() & Qt::ShiftModifier)
    {
        GLdouble modifyCoordinate = event->angleDelta().y() / 120.0 * _reposition_unit;
        modifyControlPointCoordinate(2, modifyCoordinate);

    }

    if(event->modifiers() & Qt::ShiftModifier && event->modifiers() & Qt::AltModifier){

        if(_selected_bcurve != -1 && _bcurvess->getShowUseAttractor(_selected_bcurve)){
            _bcurvess->getAttractor(_selected_bcurve)->setData(0, event->angleDelta().x() / 120.0 * _reposition_unit);
        } else if(_selected_bcurve != -1) {
            BCurve3 * actualBCurve = _bcurvess->getBCurve(_selected_bcurve);
            GLdouble modify_x = event->angleDelta().x() / 120.0 * _reposition_unit;
            if (_bcurvess->getIsHermite()) {
                _hermite_bcurve->setP(0, modify_x, _selected_bcurve);
                _hermite_bcurve->setP(0, modify_x, _selected_bcurve + 1);
                _hermite_bcurve->ModifyBCurve3(_selected_bcurve);
                if (_selected_bcurve >0) {
                    _hermite_bcurve->ModifyBCurve3(_selected_bcurve - 1);
                }
                if (_selected_bcurve < (GLint) _bcurvess->getSize() - 1) {
                    _hermite_bcurve->ModifyBCurve3(_selected_bcurve + 1);
                }
            } else {
                actualBCurve->setData(0, modify_x);
            }
        }
    }

    if(event->modifiers() & Qt::ControlModifier && event->modifiers() & Qt::ShiftModifier){

        if(_selected_bcurve != -1 && _bcurvess->getShowUseAttractor(_selected_bcurve)){
            _bcurvess->getAttractor(_selected_bcurve)->setData(1, event->angleDelta().y() / 120.0 * _reposition_unit);
        } else if(_selected_bcurve != -1) {
            BCurve3 * actualBCurve = _bcurvess->getBCurve(_selected_bcurve);
            GLdouble modify_y = event->angleDelta().y() / 120.0 * _reposition_unit;
            if (_bcurvess->getIsHermite()) {
                _hermite_bcurve->setP(1, modify_y, _selected_bcurve);
                _hermite_bcurve->setP(1, modify_y, _selected_bcurve + 1);
                _hermite_bcurve->ModifyBCurve3(_selected_bcurve);
                if (_selected_bcurve >0) {
                    _hermite_bcurve->ModifyBCurve3(_selected_bcurve - 1);
                }
                if (_selected_bcurve < (GLint) _bcurvess->getSize() - 1) {
                    _hermite_bcurve->ModifyBCurve3(_selected_bcurve + 1);
                }
            } else {
                actualBCurve->setData(1, modify_y);
            }
        }

    }

    if(_selected_bcurve != -1 && _bcurvess->getShowUseAttractor(_selected_bcurve)){
        calculateOptimalEnergy();
        _bcurvess->involveAttractor(_selected_bcurve);
    }



    if(event->modifiers() == Qt::NoModifier && _page_number != 1){
        if(_show_sphere){
            //  GLdouble _new_alpha = _bcurve_sphere->getAlpha() + event->angleDelta().y() / 120.0 * _reposition_unit;
            _sphere_beta += event->angleDelta().y() / 120.0 * _reposition_unit;
            _sphere_alpha += event->angleDelta().x() / 120.0 * _reposition_unit;

            calculateEnergySphere();
        }

        GLdouble modify_theta = event->angleDelta().y() / 120.0 * _reposition_unit;
        modifyTangentVectorCoordinate(0, 0.0, modify_theta, 0.0);


    }

    //LEHET VISSZA MASIK
    //    if(_selected_bcurve >=0){
    //        (*_bcurvess)[_selected_bcurve]->generateImages();
    //    }

    if(!_show_sphere)
        calculateEnergy();
    _bcurvess->CreateImage();

    update();
}



GLvoid GLWidget::keyPressEvent(QKeyEvent * event){
    event->accept();

    if( event->key() == Qt::Key_Plus){
        if(_show_sphere){
            _ray_of_sphere += 0.2;
            calculateEnergySphere();
        }

        modifyTangentVectorCoordinate(0, 0.2, 0.0, 0.0);

    }

    if(event->key() == Qt::Key_Minus){
        if(_show_sphere){
            _ray_of_sphere -= 0.2;
            calculateEnergySphere();
        }

        modifyTangentVectorCoordinate(0, -0.2, 0.0, 0.0);


    }
    _bcurvess->CreateImage();

    update();
}

GLvoid GLWidget::modifyCurveAndNeighbors() {
    _hermite_bcurve->ModifyBCurve3(_selected_bcurve);

    if (_selected_bcurve >0) {
        _hermite_bcurve->ModifyBCurve3(_selected_bcurve - 1);
    }
    if (_selected_bcurve < (GLint) _bcurvess->getSize() - 1) {
        _hermite_bcurve->ModifyBCurve3(_selected_bcurve + 1);
    }
}


GLvoid GLWidget::modifyControlPointCoordinate(GLint indexDCoordinate3, GLdouble modifyCoordinate) {
    if(_bcurvess->getIsHermite() && _selected_bcurve == -1 && _bcurvess->getSelectedP() >= 0){
        _hermite_bcurve->setP(indexDCoordinate3, modifyCoordinate, _bcurvess->getSelectedP());

        if(_bcurvess->getSelectedP() == 0){
            _hermite_bcurve->ModifyBCurve3(_bcurvess->getSelectedP());
        }
        else if(_bcurvess->getSelectedP() == (GLint) _bcurvess->getSize()){
            _hermite_bcurve->ModifyBCurve3(_bcurvess->getSelectedP() - 1);
        } else {
            _hermite_bcurve->ModifyBCurve3(_bcurvess->getSelectedP());
            _hermite_bcurve->ModifyBCurve3(_bcurvess->getSelectedP() - 1);
        }

    }


    if(_selected_bcurve != -1 && _bcurvess->getShowUseAttractor(_selected_bcurve)){
        _bcurvess->getAttractor(_selected_bcurve)->setMovableControlPoints(indexDCoordinate3, modifyCoordinate);
    } else if(_selected_bcurve != -1) {
        BCurve3 * actualBCurve = _bcurvess->getBCurve(_selected_bcurve);

        if (_bcurvess->getIsHermite()) {
            if((*_bcurvess)[_selected_bcurve]->getSelectedPIndex() != -1){
                actualBCurve->modifyControlPoint(indexDCoordinate3, modifyCoordinate, (*_bcurvess)[_selected_bcurve]->getSelectedPIndex());
            }
            if ((*_bcurvess)[_selected_bcurve]->getSelectedPIndex() == 0) {
                _hermite_bcurve->setP(indexDCoordinate3, modifyCoordinate, _selected_bcurve);
            }
            if ((*_bcurvess)[_selected_bcurve]->getSelectedPIndex() == (GLint) (*_bcurvess)[_selected_bcurve ]->getBCurve()->getN() * 2) {
                _hermite_bcurve->setP(indexDCoordinate3, modifyCoordinate, _selected_bcurve + 1);
            }
            modifyCurveAndNeighbors();
        } else {
            actualBCurve->setMovableControlPoints(indexDCoordinate3, modifyCoordinate);
            switch (mergeOrJoin) {
            case JOIN:
                _bcurvess->recalculateJoin(_selected_bcurve);
                break;
            case MERGE:
                if (_bcurvess->getIndexOfJoinBCurve(_selected_bcurve)[0] != -1)
                    _bcurvess->recalculateMerge(_bcurvess->getIndexOfJoinBCurve(_selected_bcurve)[0]);
                if (_bcurvess->getIndexOfJoinBCurve(_selected_bcurve)[1] != -1)
                    _bcurvess->recalculateMerge(_bcurvess->getIndexOfJoinBCurve(_selected_bcurve)[1]);
                break;
            case NONE:
                break;
            }


        }
    }
}

GLvoid GLWidget::modifyTangentVectorCoordinate(GLint i, GLdouble modify_r, GLdouble modify_theta, GLdouble modify_fi) {
    if (_bcurvess->getIsHermite() && _selected_bcurve == -1 && _bcurvess->getSelectedP() >= 0) {
        _hermite_bcurve->setT(i, _bcurvess->getSelectedP(), modify_r, modify_theta, modify_fi);

        if(_bcurvess->getSelectedP() == 0)
            _hermite_bcurve->ModifyBCurve3(_bcurvess->getSelectedP());
        else if(_bcurvess->getSelectedP() == (GLint)_bcurvess->getSize())
            _hermite_bcurve->ModifyBCurve3(_bcurvess->getSelectedP() - 1);

        else {
            _hermite_bcurve->ModifyBCurve3(_bcurvess->getSelectedP());
            _hermite_bcurve->ModifyBCurve3(_bcurvess->getSelectedP() - 1);

        }

    }else if (_bcurvess->getIsHermite() && _selected_bcurve != -1) {
        if ((*_bcurvess)[_selected_bcurve]->getSelectedPIndex() == 0) {
            _hermite_bcurve->setT(i, _selected_bcurve, modify_r, modify_theta, modify_fi);
        }
        if ((*_bcurvess)[_selected_bcurve]->getSelectedPIndex() == (GLint) (*_bcurvess)[_selected_bcurve ]->getBCurve()->getN() * 2) {
            _hermite_bcurve->setT(i, _selected_bcurve + 1, modify_r, modify_theta, modify_fi);
        }
        modifyCurveAndNeighbors();
    }
}



/****************************************************************************/
/************** QdataStream operator << and >> ******************************/
/****************************************************************************/

QDataStream & operator<<(QDataStream &stream, const DCoordinate3 &val) {
    stream << val.x();
    stream << val.y();
    stream << val.z();
    return stream;
}

QDataStream & operator>>(QDataStream &stream, DCoordinate3 &val) {
    stream >> val.x();
    stream >> val.y();
    stream >> val.z();
    return stream;
}

template <typename T>
QDataStream & operator<<(QDataStream &stream, const Matrix<T> &val) {
    stream << val.GetRowCount();
    stream << val.GetColumnCount();
    for(GLuint i = 0; i < val.GetRowCount(); i++)
        for(GLuint j = 0; j < val.GetColumnCount(); j++) {
            stream << val(i, j);
        }
    return stream;
}

template <typename T>
QDataStream & operator>>(QDataStream &stream, Matrix<T> &val) {
    GLuint rowCount = 0;
    GLuint columnCount = 0;
    stream >> rowCount;
    stream >> columnCount;
    val.ResizeRows(rowCount);
    val.ResizeColumns(columnCount);
    for(GLuint i = 0; i < val.GetRowCount(); i++)
        for(GLuint j = 0; j < val.GetColumnCount(); j++) {
            stream >> val(i, j);
        }
    return stream;
}

template <typename T>
QDataStream & operator<<(QDataStream &stream, const RowMatrix<T> &val) {
    stream << val.GetColumnCount();
    for(GLuint i = 0; i < val.GetColumnCount(); i++) {
        stream << val(i);
    }
    return stream;
}

template <typename T>
QDataStream & operator>>(QDataStream &stream, RowMatrix<T> &val) {
    GLuint columnCount = 0;
    stream >> columnCount;
    val.ResizeColumns(columnCount);
    for(GLuint i = 0; i < columnCount; i++) {
        stream >> val(i);
    }
    return stream;
}

template <typename T>
QDataStream & operator<<(QDataStream &stream, const ColumnMatrix<T> &val) {
    stream << val.GetRowCount();
    for(GLuint i = 0; i < val.GetRowCount(); i++) {
        stream << val(i);
    }
    return stream;
}

template <typename T>
QDataStream & operator>>(QDataStream &stream, ColumnMatrix<T> &val) {
    GLuint rowCount = 0;
    stream >> rowCount;
    val.ResizeRows(rowCount);
    for(GLuint i = 0; i < rowCount; i++) {
        stream >> val(i);
    }
    return stream;
}

//QDataStream & operator<<(QDataStream &stream, const ColumnMatrix<DCoordinate3> &val) {
//    stream << val.GetRowCount();
//    for(int i = 0; i < val.GetRowCount(); i++) {
//        stream << val(i);
//    }
//    return stream;
//}

//QDataStream & operator>>(QDataStream &stream, ColumnMatrix<DCoordinate3> &val) {
//    GLuint rowCount = 0;
//    stream >> rowCount;
//    val.ResizeRows(rowCount);
//    for(int i = 0; i < rowCount; i++) {
//        stream >> val(i);
//    }
//    return stream;
//}

QDataStream & operator<<(QDataStream &stream, BCurve3 *val) {
    stream << val->getType();
    stream << val->getN();
    stream << val->getAlpha();
    stream << val->getDataUsageFlag();
    stream << val->getData();
    stream << val->getMovableControlPoints();
    stream << val->getWeights();
    stream << val->getMaxOrderOfDerivatives();
    return stream;
}

QDataStream & operator>>(QDataStream &stream, BCurve3 * &val) {
    Basis::Type type;
    GLuint n;
    GLdouble alpha;
    GLenum data_usage_flag;
    RowMatrix<GLuint> movable_controlpoints;
    RowMatrix<GLdouble> weights;
    GLint maxOrderOfDerivatives;

    stream >> type;
    stream >> n;
    stream >> alpha;
    stream >> data_usage_flag;

    val = new BCurve3(type, n, alpha, data_usage_flag);

    stream >> val->getData();
    stream >> movable_controlpoints;
    val->setMovableControlPoints(movable_controlpoints);
    stream >> weights;
    val->setWeights(weights);
    stream >> maxOrderOfDerivatives;
    val->SetMaxOrderOfDerivatives(maxOrderOfDerivatives);

    return stream;
}

QDataStream & operator<<(QDataStream &stream, Color4 *val) {
    stream << val->r();
    stream << val->g();
    stream << val->b();
    stream << val->a();
    return stream;
}

QDataStream & operator>>(QDataStream &stream, Color4 * &val) {
    GLfloat r, g, b, a;

    stream >> r;
    stream >> g;
    stream >> b;
    stream >> a;

    //    val = new Color4(r,g,b,a);

    val = new Color4(0.0f, 0.502f, 0.502f, 0.0f);


    return stream;
}

QDataStream &
operator<<(QDataStream &stream, BCurve3Composite::ArcAttributes *val) {
    stream << val->arc;
    //stream << val->image;
    stream << val->color;
    stream << val->index_of_join_bcurve[0];
    stream << val->index_of_join_bcurve[1];
    stream << val->useAttractor;
    //  if (val->useAttractor) {
    stream << val->attractor;
    //        stream << val->img_attractor;
    //   }
    //stream << val->modified_bcurve;
    //         img_modified_bcurve;
    // stream << val->show_modified_bcurve;

    return stream;
}

QDataStream &
operator>>(QDataStream &stream, BCurve3Composite::ArcAttributes* &val) {
    if (val != nullptr)
        delete val;

    val = new BCurve3Composite::ArcAttributes();

    stream >> val->arc;
    //stream >>  val->image;
    val->image = nullptr;
    stream >>  val->color;
    //val->color = nullptr;
    stream >>  val->index_of_join_bcurve[0];
    stream >>  val->index_of_join_bcurve[1];
    stream >>  val->useAttractor;
    //    if ( val->useAttractor) {
    stream >>  val->attractor;
    //        stream >> val->img_attractor;
    val->img_attractor = nullptr;
    //    }
    //    stream >> val->modified_bcurve;
    //    val->img_modified_bcurve = nullptr;
    //    stream >> val->show_modified_bcurve;

    return stream;
}


QDataStream &
operator<<(QDataStream &stream,
           const std::vector<BCurve3Composite::ArcAttributes*> &val) {
    stream << static_cast<quint32>(val.size());
    for (BCurve3Composite::ArcAttributes *arcAttribute : val)
        stream << arcAttribute;
    return stream;
}

QDataStream &
operator>>(QDataStream &stream,
           std::vector<BCurve3Composite::ArcAttributes*> &val) {
    quint32 vectorsize;
    stream >> vectorsize;
    val.resize(vectorsize);
    for (GLuint i = 0; i < vectorsize; i++)
    {
        BCurve3Composite::ArcAttributes* arcAttribute = nullptr;
        stream >> arcAttribute;

        arcAttribute->generateImages();

        //val->push_back(arcAttribute);
        val[i] = arcAttribute;
    }

    return stream;
}

QDataStream & operator<<(QDataStream &stream, BCurve3Composite *val) {

    stream << val->getAlpha();
    stream << val->getAttributes();
    stream << val->getSelected();
    stream << val->getIsHermite();
    return stream;
}

QDataStream & operator>>(QDataStream &stream, BCurve3Composite * &val) {
    GLdouble alpha;
    std::vector<BCurve3Composite::ArcAttributes*> attributes;
    GLint selected;
    GLboolean isHermite;

    stream >> alpha;
    stream >> attributes;
    stream >> selected;
    stream >> isHermite;

    val = new BCurve3Composite(alpha, isHermite, selected);
    val->getAttributes() = attributes;
    return stream;
}

QDataStream & operator<<(QDataStream &stream, HermiteBCurve3Composite *val) {

    stream << val->getP();
    stream << val->getT();
    stream << val->getN();
    stream << val->getR();
    stream << val->getTheta();
    stream << val->getFi();
    stream << val->getAlpha();
    stream << val->getType();
    stream << val->getBCurve3Composite();

    return stream;
}

QDataStream & operator>>(QDataStream &stream, HermiteBCurve3Composite * &val) {
    RowMatrix<DCoordinate3> p;
    Matrix<DCoordinate3> t;
    RowMatrix<GLuint> n;
    Matrix<GLdouble> r;
    Matrix<GLdouble> theta;
    Matrix<GLdouble> fi;
    RowMatrix<GLdouble> alpha;
    Basis::Type  type;
    BCurve3Composite *bCurveComposite;

    stream >> p;
    stream >> t;
    stream >> n;
    stream >> r;
    stream >> theta;
    stream >> fi;
    stream >> alpha;
    stream >> type;
    stream >> bCurveComposite;

    val = new HermiteBCurve3Composite(p, t, n, alpha, type, bCurveComposite);
    val->getR() = r;
    val->getTheta() = theta;
    val->getFi() = fi;
    return stream;
}

GLvoid GLWidget::save()
{
    QString fileName =
            QFileDialog::getSaveFileName(this, tr("Save BCurve Composite"), "",
                                         tr("BCurve Composite (*.bcc);;All Files (*)"));

    if (fileName.isEmpty())
        return;
    else {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        }

        QDataStream out(&file);
        out.setVersion(QDataStream::Qt_4_5);

        out << _bcurvess->getIsHermite();
        if (_bcurvess->getIsHermite()) {
            out << _hermite_bcurve;
        }
        else {
            out << _bcurvess;
        }

        //        out << _bcurvess->getAttributes();
        //        out << _selected_bcurve;

    }
}


GLvoid GLWidget::open()
{
    QString fileName = QFileDialog::getOpenFileName(
                this, tr("Open File"), "", tr("BCC (*.bcc);;All Files (*)"));

    if (fileName.isEmpty())
        return;
    else {

        QFile file(fileName);

        if (!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        }

        QDataStream in(&file);
        in.setVersion(QDataStream::Qt_4_5);

        if ( _bcurvess ) {
            delete _bcurvess;
        }
        _bcurvess = nullptr;
        if ( _hermite_bcurve ) {
            delete _hermite_bcurve;
        }
        _hermite_bcurve = nullptr;

        //        _bcurvess->deleteAllArcAttributes();
        //        _selected_bcurve = -1;
        //        _bcurvess->getAttributes().clear();
        //        std::vector<BCurve3Composite::ArcAttributes*> *attributes = new std::vector<BCurve3Composite::ArcAttributes*>;
        //        std::vector<BCurve3Composite::ArcAttributes*> &attributes = *_bcurvess->getAttributes();
        //        in >> _bcurvess->getAttributes();
        //QVector<BCurve3Composite::ArcAttributes*> qvector;

        GLboolean isHeremite = false;
        in >> isHeremite;
        if ( isHeremite ) {
            in >> _hermite_bcurve;
            _bcurvess = _hermite_bcurve->getBCurve3Composite();

            hermite_m = _hermite_bcurve->getM();
            hermite_n = _hermite_bcurve->getN()[0];
            hermite_rho = _hermite_bcurve->getRho();

            _side_widget->hermite_m_spinBox->setValue(hermite_m);
            _side_widget->hermite_rho_spinBox->setValue(hermite_rho);
            _side_widget->hermite_n_spinBox->setValue(hermite_n);
        }
        else {
            in >> _bcurvess;
        }

        _bcurvess->setSelectedP(-1);
        _selected_bcurve = _bcurvess->getSelected();

        //        vector<BCurve3Composite::ArcAttributes*> qvector;
        //        in >> qvector;
        //        _bcurvess->getAttributes() = qvector;

        //        in >> _selected_bcurve;
        if (_selected_bcurve != -1) {
            RowMatrix<GLdouble> currentWeights = _bcurvess->getBCurve(_selected_bcurve)->getWeights();
            for(GLint i = 0; i< 6; i++){
                if(i<= _bcurvess->getBCurve(_selected_bcurve)->getMaxOrderOfDerivatives()){
                    setWeightValue(currentWeights[i], i);
                }
            }
            //_side_widget->w0_spinBox->setValue(currentWeights[0]);
        }

        update();
    }
}

//GLvoid GLWidget::save()
//{
//    QString fileName =
//            QFileDialog::getSaveFileName(this, tr("Save BCurve Composite"), "",
//                                         tr("BCurve Composite (*.bcc);;All Files (*)"));

//    if (fileName.isEmpty())
//        return;
//    else {
//        QFile file(fileName);
//        if (!file.open(QIODevice::WriteOnly)) {
//            QMessageBox::information(this, tr("Unable to open file"),
//                                     file.errorString());
//            return;
//        }

//        QDataStream out(&file);
//        out.setVersion(QDataStream::Qt_4_5);
//        out << _bcurvess->getAttributes();
//        out << _selected_bcurve;

//    }
//}


//GLvoid GLWidget::open()
//{
//    QString fileName = QFileDialog::getOpenFileName(
//                this, tr("Open File"), "", tr("BCC (*.bcc);;All Files (*)"));

//    if (fileName.isEmpty())
//        return;
//    else {

//        QFile file(fileName);

//        if (!file.open(QIODevice::ReadOnly)) {
//            QMessageBox::information(this, tr("Unable to open file"),
//                                     file.errorString());
//            return;
//        }

//        QDataStream in(&file);
//        in.setVersion(QDataStream::Qt_4_5);
//        _bcurvess->deleteAllArcAttributes();
//        _selected_bcurve = -1;
////        _bcurvess->getAttributes().clear();
//        //        std::vector<BCurve3Composite::ArcAttributes*> *attributes = new std::vector<BCurve3Composite::ArcAttributes*>;
//        //        std::vector<BCurve3Composite::ArcAttributes*> &attributes = *_bcurvess->getAttributes();
//        //        in >> _bcurvess->getAttributes();
//        //QVector<BCurve3Composite::ArcAttributes*> qvector;
//        vector<BCurve3Composite::ArcAttributes*> qvector;
//        in >> qvector;
//        _bcurvess->getAttributes() = qvector;
//        in >> _selected_bcurve;
//        if (_selected_bcurve != -1) {
//            RowMatrix<GLdouble> currentWeights = _bcurvess->getBCurve(_selected_bcurve)->getWeights();
//            for(GLint i = 0; i< 6; i++){
//                if(i<= _bcurvess->getBCurve(_selected_bcurve)->getMaxOrderOfDerivatives()){
//                    setWeightValue(currentWeights[i], i);
//                }
//            }
//            //_side_widget->w0_spinBox->setValue(currentWeights[0]);
//        }

//        update();
//    }
//}

/*
void GLWidget::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
                this, tr("Open File"), "", tr("BCC (*.bcc);;All Files (*)"));

    if (fileName.isEmpty())
        return;
    else {

        QFile file(fileName);

        if (!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        }

        QDataStream in(&file);
        in.setVersion(QDataStream::Qt_4_5);
        _bcurvess->getAttributes().clear();
        //        std::vector<BCurve3Composite::ArcAttributes*> *attributes = new std::vector<BCurve3Composite::ArcAttributes*>;
        //        std::vector<BCurve3Composite::ArcAttributes*> &attributes = *_bcurvess->getAttributes();
        //        in >> _bcurvess->getAttributes();
        //QVector<BCurve3Composite::ArcAttributes*> qvector;
        vector<BCurve3Composite::ArcAttributes*> qvector;
        in >> qvector;
        _bcurvess->getAttributes() = qvector;
        in >> _selected_bcurve;
        if (_selected_bcurve != -1) {
            RowMatrix<GLdouble> currentWeights = _bcurvess->getBCurve(_selected_bcurve)->getWeights();
            for(GLint i = 0; i< 6; i++){
                if(i<= _bcurvess->getBCurve(_selected_bcurve)->getMaxOrderOfDerivatives()){
                    setWeightValue(currentWeights[i], i);
                }
            }
            //_side_widget->w0_spinBox->setValue(currentWeights[0]);
        }

        update();
    }
}
*/
}

