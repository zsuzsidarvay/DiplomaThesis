#include "MainWindow.h"

namespace cagd
{
MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
{
    setupUi(this);

    /*

      the structure of the main window's central widget

     *---------------------------------------------------*
     |                 central widget                    |
     |                                                   |
     |  *---------------------------*-----------------*  |
     |  |     rendering context     |   scroll area   |  |
     |  |       OpenGL widget       | *-------------* |  |
     |  |                           | | side widget | |  |
     |  |                           | |             | |  |
     |  |                           | |             | |  |
     |  |                           | *-------------* |  |
     |  *---------------------------*-----------------*  |
     |                                                   |
     *---------------------------------------------------*

    */
    _side_widget = new SideWidget(this);

    _scroll_area = new QScrollArea(this);
    _scroll_area->setWidget(_side_widget);
    _scroll_area->setSizePolicy(_side_widget->sizePolicy());
    _scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    _gl_widget = new GLWidget(this);
    _gl_widget->set_side_widget(_side_widget);

    centralWidget()->setLayout(new QHBoxLayout());
    centralWidget()->layout()->addWidget(_gl_widget);
    centralWidget()->layout()->addWidget(_scroll_area);

    this->setWindowTitle("Adjusting the energies of free-form curves and surfaces defined by control points");

    // creating a signal slot mechanism between the rendering context and the side widget
    connect(_side_widget->rotate_x_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
    connect(_side_widget->rotate_y_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
    connect(_side_widget->rotate_z_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));

    connect(_side_widget->zoom_factor_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

    connect(_side_widget->trans_x_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_x(double)));
    connect(_side_widget->trans_y_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_y(double)));
    connect(_side_widget->trans_z_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_z(double)));

    connect(_side_widget->insert_surface_button, SIGNAL(clicked()), _gl_widget, SLOT(insert_surface()));
//    connect(_side_widget->material_combo_box, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_material(int)));
    connect(_side_widget->shader_combo_box, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_shader(int)));
    connect(_side_widget->u_directional_checkbox, SIGNAL(clicked(bool)), _gl_widget, SLOT(set_u_derivatives(bool)));
    connect(_side_widget->v_directional_checkbox, SIGNAL(clicked(bool)), _gl_widget, SLOT(set_v_derivatives(bool)));
    connect(_side_widget->u_iso_checkbox, SIGNAL(clicked(bool)), _gl_widget, SLOT(set_u_isoparametric(bool)));
    connect(_side_widget->v_iso_checkbox, SIGNAL(clicked(bool)), _gl_widget, SLOT(set_v_isoparametric(bool)));
//    connect(_side_widget->controlnet_checkbox, SIGNAL(clicked(bool)), _gl_widget, SLOT(set_control(bool)));
    connect(_side_widget->selected_surface_spinbox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(change_selected_surface(int)));

    connect(_side_widget->first_surface_spinbox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_index_of_surface1(int)));
    connect(_side_widget->second_surface_spinbox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_index_of_surface2(int)));
    connect(_side_widget->first_direction_combobox, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_direction_of_surface1(int)));
    connect(_side_widget->second_direction_combobox, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_direction_of_surface2(int)));
    connect(_side_widget->surface_merge_button, SIGNAL(clicked()), _gl_widget, SLOT(mergeSurface()));
//    connect(_side_widget->surface_join_button, SIGNAL(clicked()), _gl_widget, SLOT(joinSurface()));
//    connect(_side_widget->continue_surface_button, SIGNAL(clicked()), _gl_widget, SLOT(continueSurface()));

    connect(_side_widget->tabWidget, SIGNAL(currentChanged(int)), _gl_widget, SLOT(change_page(int)));

    //BCURVE
    connect(_side_widget->insert_bcurve_button, SIGNAL(clicked()), _gl_widget, SLOT(on_insert_bcurve_button_clicked()));
    connect(_side_widget->show_bcurve_tangent_check_box, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_show_bcurve_tangent_check_box_stateChanged(bool)));
    connect(_side_widget->show_bcurve_accelaration_check_box, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_show_bcurve_accelaration_check_box_stateChanged(bool)));
    connect(_side_widget->type_comboBox, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(on_type_comboBox_currentIndexChanged(int)));
    connect(_side_widget->subdivision_button, SIGNAL(clicked()), _gl_widget, SLOT(on_subdivision_button_clicked()));
    connect(_side_widget->increase_order_button, SIGNAL(clicked()), _gl_widget, SLOT(on_increase_order_button_clicked()));
    connect(_side_widget->setOrder_spinbox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_setOrder_spinbox_valueChanged(int)));
    connect(_side_widget->setAlpha_doubleSpinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_setAlpha_doubleSpinBox_valueChanged(double)));
    connect(_side_widget->join_curve_button_3, SIGNAL(clicked()), _gl_widget, SLOT(on_join_curve_button_3_clicked()));
    connect(_side_widget->order_of_derivative_join, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_order_of_derivative_join_valueChanged(int)));
    connect(_side_widget->order_of_derivative_join_3, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_order_of_derivative_join_3_valueChanged(int)));
    connect(_side_widget->curve_first_direction_combo_box_2, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(on_curve_first_direction_combo_box_2_currentIndexChanged(int)));
    connect(_side_widget->curve_second_direction_combo_box_2, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(on_curve_second_direction_combo_box_2_currentIndexChanged(int)));
    connect(_side_widget->deleteButton, SIGNAL(clicked()), _gl_widget, SLOT(on_deleteButton_clicked()));
    connect(_side_widget->global_min_checkBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_global_min_checkBox_clicked(bool)));
    connect(_side_widget->curvatureComb_checkBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_curvatureComb_checkBox_clicked(bool)));
    connect(_side_widget->hermite_pushButton, SIGNAL(clicked()), _gl_widget, SLOT(on_hermite_pushButton_clicked()));
    connect(_side_widget->scale_doubleSpinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_scale_doubleSpinBox_valueChanged(double)));
    connect(_side_widget->curve_attractorcheckBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_curve_attractorcheckBox_clicked(bool)));
    connect(_side_widget->setOrderOfDerivatives_spinbox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_setOrderOfDerivatives_spinbox_valueChanged(int)));
    connect(_side_widget->point_attractor_checkBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_point_attractor_checkBox_clicked(bool)));
    connect(_side_widget->w0_spinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_w0_spinBox_valueChanged(double)));
    connect(_side_widget->w1_spinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_w1_spinBox_valueChanged(double)));
    connect(_side_widget->w2_spinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_w2_spinBox_valueChanged(double)));
    connect(_side_widget->w3_spinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_w3_spinBox_valueChanged(double)));
    connect(_side_widget->w4_spinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_w4_spinBox_valueChanged(double)));
    connect(_side_widget->w5_spinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_w5_spinBox_valueChanged(double)));
    connect(_side_widget-> show_text_checkBox, SIGNAL(clicked(bool)), _gl_widget, SLOT( on_show_text_checkBox_clicked(bool)));
    connect(_side_widget->energy_checkBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_energy_checkBox_clicked(bool)));
    //   connect(actionOpen, SIGNAL(triggered()), _gl_widget, SLOT(on_actionOpen_triggered()));
    connect(_side_widget->render_tangent_checkBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_render_tangent_checkBox_clicked(bool)));
    connect(_side_widget->energy_on_sphere_checkBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_energy_on_sphere_checkBox_clicked(bool)));
    connect(_side_widget->setN_bsurface_spinbox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_setN_bsurface_spinbox_valueChanged(int)));
    connect(_side_widget->setM_bsurface_spinbox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_setM_bsurface_spinbox_valueChanged(int)));
    connect(_side_widget->setAlpha_bsurface_doubleSpinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_setAlpha_bsurface_doubleSpinBox_valueChanged(double)));
    connect(_side_widget->setBeta_doubleSpinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(on_setBeta_doubleSpinBox_valueChanged(double)));
    connect(_side_widget->energy_comboBox, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(on_energy_comboBox_currentIndexChanged(int)));
    connect(_side_widget->toolBox, SIGNAL(currentChanged(int)), _gl_widget, SLOT(on_toolBox_currentChanged(int)));
    connect(_side_widget->hermite_m_spinBox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_hermite_m_spinBox_valueChanged(int)));
    connect(_side_widget->hermite_rho_spinBox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_hermite_rho_spinBox_valueChanged(int)));
    connect(_side_widget->optimize_bsurface_pushButton, SIGNAL(clicked()), _gl_widget, SLOT(on_optimize_bsurface_pushButton_clicked()));
    connect(_side_widget->optimize_bsurface_checkBox, SIGNAL(clicked(bool)), _gl_widget, SLOT(on_optimize_bsurface_checkBox_clicked(bool)));
    connect(_side_widget->hermite_n_spinBox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_hermite_n_spinBox_valueChanged(int)));
    connect(_side_widget->tableWidget, SIGNAL(cellClicked(int,int)), _gl_widget, SLOT(on_tableWidget_cellClicked(int,int)));
    connect(_side_widget->u_increase_order_pushButton, SIGNAL(clicked()), _gl_widget, SLOT(on_u_increase_order_pushButton_clicked()));
    connect(_side_widget->v_increase_order_pushButton, SIGNAL(clicked()), _gl_widget, SLOT(on_v_increase_order_pushButton_clicked()));
    connect(_side_widget->u_subdivision_pushButton, SIGNAL(clicked()), _gl_widget, SLOT(on_u_subdivision_pushButton_clicked()));
    connect(_side_widget->v_subdivision_pushButton, SIGNAL(clicked()), _gl_widget, SLOT(on_v_subdivision_pushButton_clicked()));
    connect(_side_widget->surface_toolBox, SIGNAL(currentChanged(int)), _gl_widget, SLOT(on_surface_toolBox_currentChanged(int)));
    connect(_side_widget->merge_pushButton, SIGNAL(clicked()), _gl_widget, SLOT(on_merge_pushButton_clicked()));
    connect(_side_widget->order_surface_spinBox, SIGNAL(valueChanged(int)), _gl_widget, SLOT(on_order_surface_spinBox_valueChanged(int)));
    connect(_side_widget->delete_surface_pushButton, SIGNAL(clicked()), _gl_widget, SLOT(on_delete_surface_pushButton_clicked()));









}

//--------------------------------
// implementation of private slots
//--------------------------------
void MainWindow::on_action_Quit_triggered()
{
    qApp->exit(0);
}

void MainWindow::on_actionOpen_triggered(){
    _gl_widget->open();
}


void MainWindow::on_actionSave_triggered()
{
    _gl_widget->save();
}


}
