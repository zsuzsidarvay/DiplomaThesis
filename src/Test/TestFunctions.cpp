#include <cmath>
#include "TestFunctions.h"
#include "../Core/Constants.h"

using namespace cagd;
using namespace std;

GLdouble spiral_on_cone::u_min = -TWO_PI;
GLdouble spiral_on_cone::u_max = +TWO_PI;

DCoordinate3 spiral_on_cone::d0(GLdouble u)
{
    return DCoordinate3(u * cos(u), u * sin(u), u);
}

DCoordinate3 spiral_on_cone::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(c - u * s, s + u * c, 1.0);
}

DCoordinate3 spiral_on_cone::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-2.0 * s - u * c, 2.0 * c - u * s, 0);
}

GLdouble torus_knot::u_min=0;
GLdouble torus_knot::u_max=6.0*PI;

DCoordinate3 torus_knot::d0(GLdouble u)
{
    return DCoordinate3((2+cos((2*u)/3))*cos(u),(2+cos((2*u)/3))*sin(u),sin((2*u)/3));
}

DCoordinate3 torus_knot::d1(GLdouble u)
{
    return DCoordinate3(-(5*sin(5*u/3)+12*sin(u)+sin(u/3))/6,(5*cos(5*u/3)+12*cos(u)+cos(u/3))/6,2*cos(2*u/3)/3);
}

DCoordinate3 torus_knot::d2(GLdouble u)
{
    return DCoordinate3(-(25*cos(5*u/3)+36*cos(u)+cos(u/3))/18,-(25*sin(5*u/3)+36*sin(u)+sin(u/3))/18,-4*sin(2*u/3)/9);
}

GLdouble spiral::u_min = 0;
GLdouble spiral::u_max = PI;
GLdouble spiral::k=3;
GLdouble spiral::r=PI/2;

DCoordinate3 spiral::d0(GLdouble u)
{
    return DCoordinate3(r*sin(u)*cos(k*u),r*sin(u)*sin(k*u),r*cos(u));
}

DCoordinate3 spiral::d1(GLdouble u)
{
    return DCoordinate3(-r*(k*sin(u)*sin(k*u)-cos(u)*cos(k*u)),r*(cos(u)*sin(k*u)+k*sin(u)*cos(k*u)),-r*sin(u));
}

DCoordinate3 spiral::d2(GLdouble u)
{
    return DCoordinate3(-r*(2*k*cos(u)*sin(k*u)+(k*k+1)*sin(u)*cos(k*u)),-r*((k*k+1)*sin(u)*sin(k*u)-2*k*cos(u)*cos(k*u)),-r*cos(u));
}

GLdouble lissajou::u_min = -1.0;
GLdouble lissajou::u_max = +1.0;

DCoordinate3 lissajou::d0(GLdouble u)
{
    return DCoordinate3(sin(5*u + 1.5707), sin(4*u), 0);
}

DCoordinate3 lissajou::d1(GLdouble u)
{
    return DCoordinate3(5*cos(5*u + 1.5707), 4*cos(4*u), 0);
}

DCoordinate3 lissajou::d2(GLdouble u)
{
    return DCoordinate3(-25*sin(5*u + 1.5707), -16*sin(4*u), 0);
}

GLdouble hypo::u_min = 0;
GLdouble hypo::u_max = TWO_PI;
GLdouble hypo::R = 3;
GLdouble hypo::k = 3;
GLdouble hypo::r = 1;

DCoordinate3 hypo::d0(GLdouble u)
{
    return DCoordinate3((R-r)*cos(u) + r*cos((R-r)/r * u), (R-r)*sin(u) - r*sin((R-r)/r * u), 0);
}

DCoordinate3 hypo::d1(GLdouble u)
{
    return DCoordinate3((r-R)*(sin((R-r)*u/r)+sin(u)), (r-R)*(cos((r-R)*u/r) - cos(u)), 0);
}

DCoordinate3 hypo::d2(GLdouble u)
{
    return DCoordinate3((r-R)*((R-r)*cos((R-r)*u/r)/r + cos(u)), (r-R)*(sin(u) - (r-R)*sin((r-R)*u/r)/r), 0);
}

GLdouble cyclo::u_min = 0.0;
GLdouble cyclo::u_max = 40 * PI;

DCoordinate3 cyclo::d0(GLdouble u)
{
    return DCoordinate3(2*(u-sin(u)), 2*(1-cos(u)), 0);
}

DCoordinate3 cyclo::d1(GLdouble u)
{
    return DCoordinate3(2 - 2*(cos(u)), 2*sin(u), 0);
}

DCoordinate3 cyclo::d2(GLdouble u)
{
    return DCoordinate3(2*sin(u), 2*cos(u), 0);
}

GLdouble ellipse::u_min = -PI;
GLdouble ellipse::u_max = +PI;
DCoordinate3 ellipse::d0(GLdouble u)
{
    return DCoordinate3(6 * cos(u), 4* sin(u), 0);
}

DCoordinate3 ellipse::d1(GLdouble u)
{
    return DCoordinate3(-6 * sin(u),4* cos(u), 0);
}

DCoordinate3 ellipse::d2(GLdouble u)
{
    return DCoordinate3(-6 * cos(u),-4 * sin(u), 0);
}

GLdouble helix::u_min = 0;
GLdouble helix::u_max = TWO_PI;

DCoordinate3 helix::d0(GLdouble u)
{
    return 0.2* DCoordinate3(cos(u),sin(u), u);
}

DCoordinate3 helix::d1(GLdouble u)
{
    return 0.2* DCoordinate3(-sin(u),cos(u), 1.0);
}

DCoordinate3 helix::d2(GLdouble u)
{
    return 0.2* DCoordinate3(-cos(u),-sin(u), 0);
}

//Parametric Surfaces

GLdouble torus_surface::u_min = 0.0;
GLdouble torus_surface::u_max = TWO_PI;
GLdouble torus_surface::v_min = 0.0;
GLdouble torus_surface::v_max = TWO_PI;
GLdouble torus_surface::R = 1.0;
GLdouble torus_surface::r = 0.5;

DCoordinate3 torus_surface::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((R + r * sin(u)) * cos(v), (R + r * sin(u)) * sin(v), r * cos(u));
}

DCoordinate3 torus_surface::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(r * cos(u) * cos(v), r * cos(u) * sin(v), -r * sin(u));
}

DCoordinate3 torus_surface::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-(R + r * sin(u)) * sin(v), (R + r * sin(u)) * cos(v), 0.0);
}

GLdouble alfred_klein_bottle::u_min = 0;
GLdouble alfred_klein_bottle::u_max = TWO_PI;
GLdouble alfred_klein_bottle::v_min = 0;
GLdouble alfred_klein_bottle::v_max = TWO_PI;

DCoordinate3 alfred_klein_bottle::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(3 * cos(u) + 1.0/2.0 * (1 + cos(2 * u)) * sin(v) - 1.0/2.0 * sin(2 * u) * sin(2 * v),
                        3 * sin(u) + 1.0/2.0 * sin(2 * u) * sin(v) - 1.0/2.0 * (1 - cos(2 * u)) * sin(2 * v),
                        cos(u) * sin(2 * v) + sin(u) * sin(v));
}

DCoordinate3 alfred_klein_bottle::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(-3 * sin(u) - sin(2 * u) * sin(v) + cos(2 * u) * sin(2 * v),
                        3 * cos(u) + cos(2 * u) * sin(v) - sin(2 * u) * sin(2 * v),
                        -sin(u) * sin(2 * v) + cos(u) * sin(v));
}

DCoordinate3 alfred_klein_bottle::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(1.0/2.0 * (1 + cos(2 * u)) * cos(v) - sin(2 * u) * cos(2 * v),
                        1.0/2.0 * sin(2 * u) * cos(v) - (1 - cos(2 * u)) * cos(2 * v),
                        2 * cos(u) * cos(2 * v) + sin(u) * cos(v));
}

GLdouble cylindrical_helicoid::u_min = 0;
GLdouble cylindrical_helicoid::u_max = 2;
GLdouble cylindrical_helicoid::v_min = 0;
GLdouble cylindrical_helicoid::v_max = 3 * TWO_PI;
GLdouble alfa = PI/2;

DCoordinate3 cylindrical_helicoid::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(u * cos(v), u * sin(v), u * v);
}

DCoordinate3 cylindrical_helicoid::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(cos(v), sin(v), v);
}

DCoordinate3 cylindrical_helicoid::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-u * sin(v), u * cos(v), u);
}

GLdouble hyperboloid::u_min = 0;
GLdouble hyperboloid::u_max = 3;
GLdouble hyperboloid::v_min = 0;
GLdouble hyperboloid::v_max = TWO_PI;

DCoordinate3 hyperboloid::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((1 + cosh(u - 3.0/2.0)) * sin(v),
                        (1 + cosh(u - 3.0/2.0)) * cos(v),
                        sinh(u - 3.0/2.0));
}

DCoordinate3 hyperboloid::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(sinh(u - 3.0/2.0) * sin(v),
                        sinh(u - 3.0/2.0) * cos(v),
                        cosh(u - 3.0/2.0));
}

DCoordinate3 hyperboloid::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3((1 + cosh(u - 3.0/2.0)) * cos(v),
                        (1 + cosh(u - 3.0/2.0)) * sin(v),
                        0);
}

GLdouble sphere::u_min = 0;
GLdouble sphere::u_max = PI;
GLdouble sphere::v_min = 0;
GLdouble sphere::v_max = TWO_PI;
GLdouble sphere::r = 1.0;

DCoordinate3 sphere::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(r * cosh(v) * cos(u), r * cosh(v) * sin(u), r * sinh(v));
}

DCoordinate3 sphere::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(-r * cosh(v) * sin(u), r * cosh(v) * cos(u), 0);
}

DCoordinate3 sphere::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-r * sinh(v) * cos(u), -r * sinh(v) * sin(u), r * cosh(v));
}

GLdouble sine_surface::u_min=0;
GLdouble sine_surface::u_max=TWO_PI;
GLdouble sine_surface::v_min = 0;
GLdouble sine_surface::v_max = TWO_PI;

GLdouble sine_surface::c = 0.3;

DCoordinate3 sine_surface::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(c*sin(u),c*sin(v),c*sin(u+v));
}

DCoordinate3 sine_surface::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(c*cos(u),0,c*cos(u + v));
}

DCoordinate3 sine_surface::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(0,c*cos(v),c*cos(u+v));
}


GLdouble mobius_strip::u_min = 0;
GLdouble mobius_strip::u_max = TWO_PI;
GLdouble mobius_strip::v_min = -1;
GLdouble mobius_strip::v_max = 1;

DCoordinate3 mobius_strip::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((1+v*1/2*cos(u*1/2))*cos(u), (1+v*1/2*cos(u*1/2))*sin(u), v*1/2*sin(u*1/2));
}

DCoordinate3 mobius_strip::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(-v*(2*cos(u/2.0)*sin(u)+sin(u/2.0)*cos(u))/4,-v*(sin(u/2.0)*sin(u)-2*cos(u/2.0)*cos(u))/4,v*cos(u/2.0)/4);
}

DCoordinate3 mobius_strip::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(cos(u/2.0)*cos(u)/2,cos(u/2.0)*sin(u)/2,sin(u/2.0)/2);
}

GLdouble dupin_cyclide::u_min = 0;
GLdouble dupin_cyclide::u_max = 2 * PI;
GLdouble dupin_cyclide::v_min = 0;
GLdouble dupin_cyclide::v_max = 2 * PI;
GLdouble dupin_cyclide::a = 6;
GLdouble dupin_cyclide::b = 4 * sqrt(2);
GLdouble dupin_cyclide::c = 2;
GLdouble dupin_cyclide::d = 3;

DCoordinate3 dupin_cyclide::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((d * (c - a * cos(u) * cos(v)) + pow(b, 2) * cos(u)) / (a - c * cos(u) * cos(v)),
        b * sin(u) * (a - d * cos(v)) / (a - c * cos(u) * cos(v)),
        b * sin(v) * (c * cos(u) - d) / (a - c * cos(u) * cos(v)));
}

DCoordinate3 dupin_cyclide::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3((sin(u) * (d * (pow(a, 2) - pow(c, 2)) * cos(v) - a * b)) / pow(a - c * cos(u) * cos(v), 2),
        (b * (a - d * cos(v)) * (a * cos(u) - c * cos(v))) / pow(a - c * cos(u) * cos(v), 2),
        (b * c * sin(u) * sin(v) * (d * cos(v) - a)) / pow(a - c * cos(u) * cos(v), 2));
}

DCoordinate3 dupin_cyclide::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-(sin(v) * cos(u) * (d * (pow(c, 2) - pow(a, 2)) + pow(b, 2) * c * cos(u))) / pow(a - c * cos(v) * cos(u), 2),
        (a * b * sin(v) * sin(u) * (d - c * cos(u))) / pow(a - c * cos(v) * cos(u), 2),
        (b * (d - c * cos(u)) * (c * cos(u) - a * cos(v))) / pow(a - c * cos(v) * cos(u), 2));
}

GLdouble ellipsoid::a = 2.0;
GLdouble ellipsoid::b = 1.0;
GLdouble ellipsoid::c = 1.0;

GLdouble ellipsoid::u_min = 0.0;
GLdouble ellipsoid::u_max = PI;
GLdouble ellipsoid::v_min = 0.0;
GLdouble ellipsoid::v_max = TWO_PI;

// zeroth order partial derivative, i.e. surface point
DCoordinate3 ellipsoid::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(a * sin(u) * cos(v), b * sin(u) * sin(v), c * cos(u));
}

// first order partial derivative in direction u
DCoordinate3 ellipsoid::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(a * cos(u) * cos(v), b * cos(u) * sin(v), -c * sin(u));
}

// first order partial derivative in direction v
DCoordinate3 ellipsoid::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-a * sin(u) * sin(v), b * sin(u) * cos(v), 0.0);
}

GLdouble conchoid::u_min = -TWO_PI;
GLdouble conchoid::u_max = +TWO_PI;
GLdouble conchoid::v_min = -TWO_PI;
GLdouble conchoid::v_max = +TWO_PI;


DCoordinate3 conchoid::d00(GLdouble u, GLdouble v)
{
    GLdouble x = pow(1.2, u) * (1.0 + cos(v)) * cos(u);
    GLdouble y = pow(1.2, u) * (1.0 + cos(v)) * sin(u);
    GLdouble z = pow(1.2, u) * sin(v) - 1.5 * pow(1.2, u);
    return DCoordinate3(x, y, z);
}

DCoordinate3 conchoid::d10(GLdouble u, GLdouble v)
{
    GLdouble x = pow(1.2, u) * (cos(v) + 1.0) * (0.182322 * cos(u) - sin(u));
    GLdouble y = pow(1.2, u) * (cos(v) + 1.0) * (0.182322 * sin(u) + cos(u));
    GLdouble z = pow(1.2, u) * (0.182322 * sin(v) - 0.273482);
    return DCoordinate3(x, y, z);
}

DCoordinate3 conchoid::d01(GLdouble u, GLdouble v)
{
    GLdouble x = -pow(1.2, u) * cos(u) * sin(v);
    GLdouble y = -pow(1.2, u) * sin(u) * sin(v);
    GLdouble z = pow(1.2, u) * cos(v);
    return DCoordinate3(x, y, z);
}
