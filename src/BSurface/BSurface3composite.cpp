#pragma once

#include "BSurface3composite.h"
#include "Core/Exceptions.h"

using namespace std;
using namespace cagd;

BSurface3Composite::BSurface3Composite()
{
    _attributes.reserve(256);
}

//kirajzol
GLboolean BSurface3Composite::RenderData(GLenum render_mode) const
{
    for(int i = 0; i < (GLint) _attributes.size(); i++)
    {
        //        glColor4f(_attributes[i] -> _color_of_control ->r(), _attributes[i] -> _color_of_control ->g(), _attributes[i] -> _color_of_control ->b(), _attributes[i] -> _color_of_control ->a());
        glLoadName(i);
        _attributes[i] -> _bsurface -> RenderData(render_mode);
    }
    return GL_TRUE;
}

//kirajzolja a kontrol halot
GLboolean BSurface3Composite::RenderControlNet()
{
    return RenderData(GL_LINE_STRIP);
}

//kirajzolja a kontrolpontokat
GLboolean BSurface3Composite::RenderControlPoints()
{
    glPointSize(7.0);
    return RenderData(GL_POINTS);
}

//kirajzolja a feluletet
GLboolean BSurface3Composite::Render(Material* material, ShaderProgram* shader, GLboolean reflection)
{
    for (int i = 0; i < (GLint) _attributes.size(); i++)
    {
        //        _attributes[i] -> _bsurface -> UpdateVertexBufferObjectsOfData();
        //        delete _attributes[i] -> _image;

        if(i%2 != 0){
            material = &MatFBEmerald;
        } else
            material = &MatFBTurquoise;

        glEnable(GL_LIGHTING);
        glEnable(GL_NORMALIZE);
        glEnable(GL_LIGHT0);

        //        _attributes[i] -> _image = _attributes[i] -> _bsurface -> GenerateImage(10, 10, GL_STATIC_DRAW);
        //        _attributes[i] -> _image -> UpdateVertexBufferObjects();
        if(!reflection){
            glEnable(GL_BLEND);
            glDepthMask(GL_FALSE);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            material->SetShininess(GL_FRONT_AND_BACK, 0.01f);
            material->SetTransparency(0.1f);
        }
        material -> Apply();
        shader -> Enable();
        //                glLoadName(i);
        _attributes[i] -> _image -> Render();
        shader -> Disable();
        if(!reflection){

            glDepthMask(GL_TRUE);

            glDisable(GL_BLEND);
        }
        glDisable(GL_LIGHTING);
        glDisable(GL_NORMALIZE);
        glDisable(GL_LIGHT0);
    }
    return GL_TRUE;
}

//kirajzolja a normalvektorokat
GLboolean BSurface3Composite::RenderNormals()
{
    glColor4f(1.0f, 0.5f, 0.0f, 0.0f);
    for (int i = 0; i < (GLint) _attributes.size(); i++)
    {
        _attributes[i] -> _bsurface -> UpdateVertexBufferObjectsOfData();
        delete _attributes[i] -> _image;
        _attributes[i] -> _image = _attributes[i] -> _bsurface -> GenerateImage(20, 20, GL_STATIC_DRAW);
        _attributes[i] -> _image -> UpdateVertexBufferObjects();
        _attributes[i] -> _image -> RenderNormals();

    }
    return GL_TRUE;
}

//kirajzolja az u-ranyu parametrikus gorbe - 0.rendu
void BSurface3Composite::renderUIsoparametric()
{
    for (int i = 0; i < (GLint) _attributes.size(); i++)
    {
        _attributes[i] -> _material -> Apply();
        RowMatrix<GenericCurve3*>* lines = _attributes[i]->_bsurface->GenerateUIsoparametricLines(20, 2, 20);

        if(lines == nullptr){
            throw Exception("Error u-isoparametric lines");
        }

        glColor4f(0.0f, 1.0f, 1.0f, 1.0f);
        for (GLuint i = 0; i < lines -> GetColumnCount(); i++)
        {
            (*lines)[i] -> UpdateVertexBufferObjects();
            (*lines)[i] -> RenderDerivatives(0, GL_LINE_STRIP);
            delete (*lines)[i];
        }
        delete lines;
    }
}

//kirajzolja az v-iranyu parametrikus gorbe - 0.rendu
void BSurface3Composite::renderVIsoparametric()
{
    for (GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i] -> _material -> Apply();
        RowMatrix<GenericCurve3*>* lines = _attributes[i] -> _bsurface -> GenerateVIsoparametricLines(20, 2, 20);

        glColor4f(0.0f, 1.0f, 1.0f, 1.0f);
        for (GLuint i = 0; i < lines->GetColumnCount(); i++)
        {
            (*lines)[i] -> UpdateVertexBufferObjects();
            (*lines)[i] -> RenderDerivatives(0, GL_LINE_STRIP);
            delete (*lines)[i];
        }
        delete lines;
    }
}

GLvoid BSurface3Composite::renderIsoparametric(){
    for (GLuint i = 0; i < _attributes.size(); i++)
    {

        _attributes[i] -> _material -> Apply();
        RowMatrix<GenericCurve3*>* lines = _attributes[i] -> _bsurface -> GenerateUIsoparametricLines(20, 2, 20);
        glLineWidth(3.0);

        glColor4f(1.0f, 1.0f, 0.0f, 0.0f);
        //                for (GLuint i = 0; i < lines->GetColumnCount(); i++)
        //                {
        (*lines)[0] -> UpdateVertexBufferObjects();
        (*lines)[0] -> RenderDerivatives(0, GL_LINE_STRIP);

        glColor4f(0.0f, 0.0f, 1.0f, 0.0f);
        (*lines)[lines->GetColumnCount()-1] -> UpdateVertexBufferObjects();
        (*lines)[lines->GetColumnCount()-1] -> RenderDerivatives(0, GL_LINE_STRIP);

        for (GLuint i = 0; i < lines->GetColumnCount(); i++)
        {
            delete (*lines)[i];
            (*lines)[i] = nullptr;
        }

        delete lines;
        lines = nullptr;

        lines = _attributes[i] -> _bsurface -> GenerateVIsoparametricLines(20, 2, 20);

        glColor4f(1.0f, 0.0f, 0.0f, 0.0f);
        //        for (GLuint i = 0; i < lines->GetColumnCount(); i++)
        //        {
        (*lines)[0] -> UpdateVertexBufferObjects();
        (*lines)[0] -> RenderDerivatives(0, GL_LINE_STRIP);

        glColor4f(0.0f, 1.0f, 0.0f, 0.0f);
        (*lines)[lines->GetColumnCount()-1] -> UpdateVertexBufferObjects();
        (*lines)[lines->GetColumnCount()-1] -> RenderDerivatives(0, GL_LINE_STRIP);

        for (GLuint i = 0; i < lines->GetColumnCount(); i++)
        {
            delete (*lines)[i];
        }

        delete lines;
    }
    glLineWidth(1.0);
}


//kirajzolja az u-ranyu parcialis derivaltak - 1.rendu
void BSurface3Composite::renderUPartialDerivatives()
{
    for (GLuint i = 0; i < _attributes.size(); i++)
    {
        glColor3f(0.0f, 1.0f, 0.0f);
        _attributes[i] -> _material -> Apply();
        RowMatrix<GenericCurve3*>* lines = _attributes[i] -> _bsurface -> GenerateUIsoparametricLines(20, 2, 20);

        for (GLuint i = 0; i < lines->GetColumnCount(); i++)
        {
            (*lines).operator [](i) -> UpdateVertexBufferObjects();
            (*lines).operator [](i) -> RenderDerivatives(1, GL_LINES);
            delete (*lines).operator [](i);
        }
        delete lines;
    }
}

//kirajzolja az v-ranyu parcialis derivaltak - 1.rendu
void BSurface3Composite::renderVPartialDerivatives()
{
    glColor3f(0.0f, 1.0f, 0.0f);
    for (GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i] -> _material->Apply();
        RowMatrix<GenericCurve3*>* lines = _attributes[i] -> _bsurface -> GenerateVIsoparametricLines(20, 2, 20);

        for (GLuint i = 0; i < lines->GetColumnCount(); i++)
        {
            (*lines).operator [](i) -> UpdateVertexBufferObjects();
            (*lines).operator [](i) -> RenderDerivatives(1, GL_LINES);
            delete (*lines).operator [](i);
        }
        delete lines;
    }
}

//beszur egy uj feluletet
void BSurface3Composite::insertNewSurface(BSurface3* newSurface, Material* material, ShaderProgram* shader) {
    SurfaceAttributes* attribute = new SurfaceAttributes();
    attribute -> _shader = shader;
    attribute -> _material = material;
    //alapbol sarga a kontrol halo es pontok
    attribute -> _color_of_control = new Color4(1.0f, 1.0f, 0.0f, 0.0f);
    attribute -> _bsurface = new BSurface3(*newSurface);

    GLuint sizeM = newSurface->getM() *2 +1;
    GLuint sizeN = newSurface->getN() *2 +1;

    for (GLuint i = 0; i < sizeN; i++)
    {
        for (GLuint j = 0; j < sizeM; j++)
        {
            double x, y, z;
            newSurface -> GetData(i, j, x, y, z);
            attribute -> _bsurface -> SetData(i, j, x, y, z);

            pair<GLint, GLint> p(i, j);

            attribute ->fixed_index_pairs.push_back(p);
        }
    }

    attribute -> _bsurface -> UpdateVertexBufferObjectsOfData();
    attribute -> _image = attribute -> _bsurface -> GenerateImage(30, 2, GL_STATIC_DRAW);
    attribute -> _image -> UpdateVertexBufferObjects();

    for (int i = 0; i < 4; i++)
    {
        attribute -> neighbours.push_back(nullptr);
    }

    _attributes.push_back(attribute);

}

GLvoid BSurface3Composite::addFixedIndex(GLint i, pair<GLint, GLint> p){
    _attributes[i]->fixed_index_pairs.push_back(p);
}

GLvoid BSurface3Composite::eraseFixedIndex(GLint i, pair<GLint, GLint> p){
    _attributes[i]->fixed_index_pairs.erase(std::remove(_attributes[i]->fixed_index_pairs.begin(), _attributes[i]->fixed_index_pairs.end(), p), _attributes[i]->fixed_index_pairs.end());
}

GLboolean BSurface3Composite::findFixedIndex(GLint i, pair<GLint, GLint> p){
    if (::find(_attributes[i]->fixed_index_pairs.begin(), _attributes[i]->fixed_index_pairs.end(), p) == _attributes[i]->fixed_index_pairs.end())
        return GL_TRUE;
    return GL_FALSE;

}


std::vector< pair<GLint, GLint> > BSurface3Composite::getFixedIndex(GLint i){
    return _attributes[i]->fixed_index_pairs;
}

//kitorol egy adott feluletet
void BSurface3Composite::deleteSurface(int indexOfSurface)
{
    if (_attributes.size() != 0)
    {
        SurfaceAttributes* index = _attributes[indexOfSurface];
        for (GLuint i = 0; i < _attributes.size(); i++)
        {
            for (GLuint j = 0; j < 4; j++)
            {
                if (_attributes[i] -> neighbours[j] == index)
                {
                    _attributes[i] -> neighbours[j] = nullptr;
                }
            }
        }

        if (_attributes[indexOfSurface] -> _bsurface != nullptr)
        {
            delete _attributes[indexOfSurface] -> _bsurface;
        }
        if (_attributes[indexOfSurface] -> _image != nullptr)
        {
            delete _attributes[indexOfSurface] -> _image;
        }

        _attributes.erase(_attributes.begin() + indexOfSurface);
    }
}



GLint BSurface3Composite::getSize()
{
    return _attributes.size();
}

BSurface3* BSurface3Composite::operator[](GLint index){
    return _attributes[index]->_bsurface;
}

GLvoid BSurface3Composite::setSubdivision(GLint index, GLint direction, ShaderProgram* shader){
    if(_attributes[index]->_bsurface != nullptr) {
        BSurface3* oldBsurface = _attributes[index]->_bsurface;

        GLdouble min, max;

        if(direction == 0){

            _attributes[index]->_bsurface->GetUInterval(min, max);

            RowMatrix<BSurface3*>* subdivisionSurfaces = _attributes[index]->_bsurface->USubdivision(0.5 * max);


            //delete oldBsurface;
            deleteSurface(index);
            insertNewSurface((*subdivisionSurfaces)[0], &MatFBTurquoise, shader);
            insertNewSurface((*subdivisionSurfaces)[1], &MatFBTurquoise, shader);



        } else if(direction == 1){
            _attributes[index]->_bsurface->GetVInterval(min, max);

            RowMatrix<BSurface3*>* subdivisionSurfaces = _attributes[index]->_bsurface->VSubdivision(0.5 * max);


            //  delete oldBsurface;

            deleteSurface(index);
            insertNewSurface((*subdivisionSurfaces)[0], &MatFBTurquoise, shader);
            insertNewSurface((*subdivisionSurfaces)[1], &MatFBTurquoise, shader);


        }

    }
}

GLvoid BSurface3Composite::setIncreaseOrder(GLint index, GLint direction, ShaderProgram* shader){
    if(_attributes[index]->_bsurface != nullptr) {
        BSurface3* oldBsurface = _attributes[index]->_bsurface;

        GLuint z = 1;

        if(direction == 0){
            _attributes[index]->_bsurface = _attributes[index]->_bsurface->UIncreaseOrder(z);
        }else if(direction == 1){
            _attributes[index]->_bsurface = _attributes[index]->_bsurface->VIncreaseOrder(z);

        }
        delete oldBsurface;

        //        _n_bsurface += z;
        //        u_modify_fixed_index_pairs();

        createImage(index);
        generateImage(index, BSurface3::DEFAULT_NULL_FRAGMENT);


    }

}


GLvoid BSurface3Composite::setBSurface(GLint index, BSurface3* bsurface){
    _attributes[index]->_bsurface = bsurface;

    if(_attributes[index] -> _image){
        delete _attributes[index] -> _image;
        _attributes[index] -> _image = nullptr;
    }

    _attributes[index] -> _bsurface -> UpdateVertexBufferObjectsOfData();
    _attributes[index] -> _image = _attributes[index] -> _bsurface -> GenerateImage(30, 2, GL_STATIC_DRAW);
    _attributes[index] -> _image -> UpdateVertexBufferObjects();
}

GLvoid BSurface3Composite::generateImage(GLint index, BSurface3::ImageColorScheme color_scheme){


    if (_attributes[index]->_image)
    {
        delete _attributes[index]->_image;
        _attributes[index]->_image = nullptr;
    }

    _attributes[index]->_image = _attributes[index]->_bsurface -> GenerateImage(30, 30, GL_STATIC_DRAW, color_scheme);

    if( !_attributes[index]->_image){

        throw Exception( "Error: GenerateImage returned nullptr in generate_bsurface_image()." ) ;
    }

    if( ! _attributes[index]->_image -> UpdateVertexBufferObjects()){

        throw Exception( "Error: UpdateVertexBufferObjects returned false in generate_bsurface_image()." ) ;
    }

}

GLvoid BSurface3Composite::generateImage(BSurface3::ImageColorScheme color_scheme){
    for(GLint i = 0; i< (GLint) _attributes.size(); i++){
        createImage(i);

        if (_attributes[i]->_image)
        {
            delete _attributes[i]->_image;
            _attributes[i]->_image = nullptr;
        }

        _attributes[i]->_image = _attributes[i]->_bsurface -> GenerateImage(30, 30, GL_STATIC_DRAW, color_scheme);

        if( !_attributes[i]->_image){

            throw Exception( "Error: GenerateImage returned nullptr in generate_bsurface_image()." ) ;
        }

        if( ! _attributes[i]->_image -> UpdateVertexBufferObjects()){

            throw Exception( "Error: UpdateVertexBufferObjects returned false in generate_bsurface_image()." ) ;
        }

    }

}


GLvoid BSurface3Composite::createImage(GLint index){
    if( ! _attributes[index] -> _bsurface){

        throw Exception( "Error: _bsurface is nullptr in create_bsurface_image()." ) ;
    }

    if( ! _attributes[index] -> _bsurface->UpdateVertexBufferObjectsOfData( )){

        throw Exception( "Error: UpdateVertexBufferObjectsOfData returned false in create_bsurface_image()." ) ;
    }
}

BSurface3* BSurface3Composite::getBSurface(GLint index) {
    return _attributes[index]->_bsurface;
}

GLvoid BSurface3Composite::UmergeSurfacesRight(GLint indexOfFixedSurface, GLint indexOfMergeSurface, const Matrix<GLdouble>& dF_fixedSurface, const Matrix<GLdouble>& dG_fixedSurface, const Matrix<GLdouble>& dF_mergeSurface, GLint directionOfFixedSurface, GLint r) {

    BSurface3 *fixedSurface = getBSurface(indexOfFixedSurface);
    BSurface3 *mergeSurface = getBSurface(indexOfMergeSurface);

    GLint fixedSizeN = 2 * fixedSurface->getN() + 1;
    //GLint fixedSizeM = 2 * fixedSurface->getM() + 1;
    GLint mergeSizeN = 2 * mergeSurface->getN() + 1;
    GLint mergeSizeM = 2 * mergeSurface->getM() + 1;

    for(GLint k = 0; k < mergeSizeM; k++) {
        switch (directionOfFixedSurface) {
        // right
        case 0:
            (*mergeSurface)(mergeSizeN-1, k) = (*fixedSurface)(fixedSizeN-1, k);
            break;
            // left
        case 1:
            (*mergeSurface)(mergeSizeN-1, k) = (*fixedSurface)(0, k);
            break;
        }
    }

    for(GLint k = 0; k < mergeSizeM; k++) {
        for(GLint rho = 1; rho <= r; rho++) {
            //left
            if(directionOfFixedSurface == 1) {
                DCoordinate3 left_sum;
                for (GLint j = 0; j <= rho; j++)
                {
                    left_sum += (*fixedSurface)(j, k) * dG_fixedSurface(rho, j);
                }
                DCoordinate3 right_sum;
                for (GLint j = 0; j < rho; j++)
                {
                    right_sum += (*mergeSurface)(mergeSizeN - 1 - j, k) * dF_mergeSurface(rho, mergeSizeN - 1 - j);
                }
                (*mergeSurface)(mergeSizeN - 1 - rho, k) = (left_sum-right_sum) / dF_mergeSurface(rho, mergeSizeN - 1 - rho);
            } else {
                //right
                DCoordinate3 left_sum;
                for (GLint j = 0; j <= rho; j++)
                {
                    left_sum += (*fixedSurface)(fixedSizeN - 1 - j, k) * dF_fixedSurface(rho, fixedSizeN - 1 - j);
                }

                if(rho % 2 != 0){
                    left_sum = -left_sum;
                }


                DCoordinate3 right_sum;
                for (GLint j = 0; j < rho; j++)
                {
                    right_sum += (*mergeSurface)(mergeSizeN - 1 - j, k) * dF_mergeSurface(rho, mergeSizeN - 1 - j);
                }

                (*mergeSurface)(mergeSizeN - 1 - rho, k) = (left_sum-right_sum) / dF_mergeSurface(rho, mergeSizeN - 1 - rho);
            }
        }
    }
}




GLvoid BSurface3Composite::UmergeSurfacesLeft(GLint indexOfFixedSurface, GLint indexOfMergeSurface, const Matrix<GLdouble>& dF_fixedSurface, const Matrix<GLdouble>& dG_fixedSurface, const Matrix<GLdouble>& dG_mergeSurface, GLint directionOfFixedSurface, GLint r) {

    BSurface3 *fixedSurface = getBSurface(indexOfFixedSurface);
    BSurface3 *mergeSurface = getBSurface(indexOfMergeSurface);

    GLint fixedSizeN = 2 * fixedSurface->getN() + 1;
    //    GLint fixedSizeM = 2 * fixedSurface->getM() + 1;
    //    GLint mergeSizeN = 2 * mergeSurface->getN() + 1;
    GLint mergeSizeM = 2 * mergeSurface->getM() + 1;

    for(GLint k = 0; k < mergeSizeM; k++) {
        switch (directionOfFixedSurface) {
        // right
        case 0:
            (*mergeSurface)(0, k) = (*fixedSurface)(fixedSizeN - 1, k);
            break;
            // left
        case 1:
            (*mergeSurface)(0, k) = (*fixedSurface)(0, k);
            break;
        }
    }

    for(GLint k = 0; k < mergeSizeM; k++) {
        for(GLint rho = 1; rho <= r; rho++) {
            //right
            if(directionOfFixedSurface == 0) {
                DCoordinate3 left_sum;
                for (GLint j = 0; j <= rho; j++)
                {
                    left_sum += (*fixedSurface)(fixedSizeN - 1 - j, k) * dF_fixedSurface(rho, fixedSizeN - 1 - j);
                }
                DCoordinate3 right_sum;
                for (GLint j = 0; j < rho; j++)
                {
                    right_sum += (*mergeSurface)(j, k) * dG_mergeSurface(rho, j);
                }
                (*mergeSurface)(rho, k) = (left_sum-right_sum) / dG_mergeSurface(rho, rho);
            } else {
                //left
                DCoordinate3 left_sum;
                for (GLint j = 0; j <= rho; j++) {
                    left_sum += (*fixedSurface)(j, k) * dG_fixedSurface(rho, j);
                }
                if(rho % 2 != 0){
                    left_sum = -left_sum;
                }
                DCoordinate3 right_sum;
                for (GLint j = 0; j < rho; j++)
                {
                    right_sum += (*mergeSurface)(j, k) * dG_mergeSurface(rho, j);
                }
                (*mergeSurface)(rho, k) = (left_sum-right_sum) / dG_mergeSurface(rho, rho);
            }
        }
    }
}




GLvoid BSurface3Composite::UmergeSurfaces(GLint indexOfFixedSurface, GLint indexOfMergeSurface, GLint directionOfFixedSurface, GLint directionOfMergeSurface, GLint r) {

    BSurface3 *fixedSurface = getBSurface(indexOfFixedSurface);
    BSurface3 *mergeSurface = getBSurface(indexOfMergeSurface);

    RowMatrix<GLdouble> G_fixedSurface;
    Matrix<GLdouble> dG_fixedSurface;

    fixedSurface->UBlendingFunctionValues(0.0, G_fixedSurface);
    fixedSurface->UBlendingFunctionDerivatives(r, G_fixedSurface, dG_fixedSurface);

    RowMatrix<GLdouble> F_fixedSurface;
    Matrix<GLdouble> dF_fixedSurface;

    fixedSurface->UBlendingFunctionValues(fixedSurface->getAlpha(), F_fixedSurface);
    fixedSurface->UBlendingFunctionDerivatives(r, F_fixedSurface, dF_fixedSurface);

    RowMatrix<GLdouble> F_mergeSurface;
    Matrix<GLdouble> dF_mergeSurface;

    mergeSurface->UBlendingFunctionValues(mergeSurface->getAlpha(), F_mergeSurface);
    mergeSurface->UBlendingFunctionDerivatives(r, F_mergeSurface, dF_mergeSurface);

    RowMatrix<GLdouble> G_mergeSurface;
    Matrix<GLdouble> dG_mergeSurface;

    mergeSurface->UBlendingFunctionValues(0.0, G_mergeSurface);
    mergeSurface->UBlendingFunctionDerivatives(r, G_mergeSurface, dG_mergeSurface);

    switch (directionOfMergeSurface) {
    // right
    case 0:
        UmergeSurfacesRight(indexOfFixedSurface, indexOfMergeSurface, dF_fixedSurface, dG_fixedSurface, dF_mergeSurface, directionOfFixedSurface, r);
        break;

        // left
    case 1:
        UmergeSurfacesLeft(indexOfFixedSurface, indexOfMergeSurface, dF_fixedSurface, dG_fixedSurface, dG_mergeSurface, directionOfFixedSurface, r);
        break;
    }
    generateImage(indexOfMergeSurface, BSurface3::DEFAULT_NULL_FRAGMENT);
}



GLvoid BSurface3Composite::VmergeSurfacesRight(GLint indexOfFixedSurface, GLint indexOfMergeSurface, const Matrix<GLdouble>& dF_fixedSurface, const Matrix<GLdouble>& dG_fixedSurface, const Matrix<GLdouble>& dF_mergeSurface, GLint directionOfFixedSurface, GLint r) {

    BSurface3 *fixedSurface = getBSurface(indexOfFixedSurface);
    BSurface3 *mergeSurface = getBSurface(indexOfMergeSurface);

    GLint fixedSizeN = 2 * fixedSurface->getN() + 1;
    GLint fixedSizeM = 2 * fixedSurface->getM() + 1;
    GLint mergeSizeN = 2 * mergeSurface->getN() + 1;
    GLint mergeSizeM = 2 * mergeSurface->getM() + 1;

    for(GLint k = 0; k < mergeSizeN; k++) {
        switch (directionOfFixedSurface) {
        // right
        case 2:
            (*mergeSurface)(k, mergeSizeM - 1) = (*fixedSurface)(k, fixedSizeM - 1);
            break;
            // left
        case 3:
            (*mergeSurface)(k, mergeSizeM - 1) = (*fixedSurface)(k, 0);
            break;
        }
    }

    for(GLint k = 0; k < mergeSizeN; k++) {
        for(GLint rho = 1; rho <= r; rho++) {
            //left 1=3
            if(directionOfFixedSurface == 3) {
                DCoordinate3 left_sum;
                for (GLint j = 0; j <= rho; j++)
                {
                    left_sum += (*fixedSurface)(k, j) * dG_fixedSurface(rho, j);
                }
                DCoordinate3 right_sum;
                for (GLint j = 0; j < rho; j++)
                {
                    right_sum += (*mergeSurface)(k, mergeSizeM - 1 - j) * dF_mergeSurface(rho, mergeSizeM - 1 - j);
                }
                (*mergeSurface)(k, mergeSizeM - 1 - rho) = (left_sum-right_sum) / dF_mergeSurface(rho, mergeSizeM - 1 - rho);
            } else {
                //right
                DCoordinate3 left_sum;
                for (GLint j = 0; j <= rho; j++)
                {
                    left_sum += (*fixedSurface)(k, fixedSizeM - 1 - j) * dF_fixedSurface(rho, fixedSizeM - 1 - j);
                }

                if(rho % 2 != 0){
                    left_sum = -left_sum;
                }


                DCoordinate3 right_sum;
                for (GLint j = 0; j < rho; j++)
                {
                    right_sum += (*mergeSurface)(k, mergeSizeM - 1 - j) * dF_mergeSurface(rho, mergeSizeM - 1 - j);
                }

                (*mergeSurface)(k, mergeSizeM - 1 - rho) = (left_sum-right_sum) / dF_mergeSurface(rho, mergeSizeM - 1 - rho);
            }
        }
    }
}

GLvoid BSurface3Composite::VmergeSurfacesLeft(GLint indexOfFixedSurface, GLint indexOfMergeSurface, const Matrix<GLdouble>& dF_fixedSurface, const Matrix<GLdouble>& dG_fixedSurface, const Matrix<GLdouble>& dG_mergeSurface, GLint directionOfFixedSurface, GLint r) {

    BSurface3 *fixedSurface = getBSurface(indexOfFixedSurface);
    BSurface3 *mergeSurface = getBSurface(indexOfMergeSurface);

    //    GLint fixedSizeN = 2 * fixedSurface->getN() + 1;
    GLint fixedSizeM = 2 * fixedSurface->getM() + 1;
    GLint mergeSizeN = 2 * mergeSurface->getN() + 1;
    //    GLint mergeSizeM = 2 * mergeSurface->getM() + 1;

    for(GLint k = 0; k < mergeSizeN; k++) {
        switch (directionOfFixedSurface) {
        // right
        case 2:
            (*mergeSurface)(k, 0) = (*fixedSurface)(k, fixedSizeM - 1);
            break;
            // left
        case 3:
            (*mergeSurface)(k, 0) = (*fixedSurface)(k, 0);
            break;
        }
    }

    for(GLint k = 0; k < mergeSizeN; k++) {
        for(GLint rho = 1; rho <= r; rho++) {
            //right 0 = 2
            if(directionOfFixedSurface == 2) {
                DCoordinate3 left_sum;
                for (GLint j = 0; j <= rho; j++)
                {
                    left_sum += (*fixedSurface)(k, fixedSizeM - 1 - j) * dF_fixedSurface(rho, fixedSizeM - 1 - j);
                }
                DCoordinate3 right_sum;
                for (GLint j = 0; j < rho; j++)
                {
                    right_sum += (*mergeSurface)(k, j) * dG_mergeSurface(rho, j);
                }
                (*mergeSurface)(k, rho) = (left_sum-right_sum) / dG_mergeSurface(rho, rho);
            } else {
                //left
                DCoordinate3 left_sum;
                for (GLint j = 0; j <= rho; j++) {
                    left_sum += (*fixedSurface)(k, j) * dG_fixedSurface(rho, j);
                }
                if(rho % 2 != 0){
                    left_sum = -left_sum;
                }
                DCoordinate3 right_sum;
                for (GLint j = 0; j < rho; j++)
                {
                    right_sum += (*mergeSurface)(k, j) * dG_mergeSurface(rho, j);
                }
                (*mergeSurface)(k, rho) = (left_sum-right_sum) / dG_mergeSurface(rho, rho);
            }
        }
    }
}

GLvoid BSurface3Composite::VmergeSurfaces(GLint indexOfFixedSurface, GLint indexOfMergeSurface, GLint directionOfFixedSurface, GLint directionOfMergeSurface, GLint r) {

    BSurface3 *fixedSurface = getBSurface(indexOfFixedSurface);
    BSurface3 *mergeSurface = getBSurface(indexOfMergeSurface);

    RowMatrix<GLdouble> G_fixedSurface;
    Matrix<GLdouble> dG_fixedSurface;

    fixedSurface->VBlendingFunctionValues(0.0, G_fixedSurface);
    fixedSurface->VBlendingFunctionDerivatives(r, G_fixedSurface, dG_fixedSurface);

    RowMatrix<GLdouble> F_fixedSurface;
    Matrix<GLdouble> dF_fixedSurface;

    fixedSurface->VBlendingFunctionValues(fixedSurface->getBeta(), F_fixedSurface);
    fixedSurface->VBlendingFunctionDerivatives(r, F_fixedSurface, dF_fixedSurface);

    RowMatrix<GLdouble> F_mergeSurface;
    Matrix<GLdouble> dF_mergeSurface;

    mergeSurface->VBlendingFunctionValues(mergeSurface->getBeta(), F_mergeSurface);
    mergeSurface->VBlendingFunctionDerivatives(r, F_mergeSurface, dF_mergeSurface);

    RowMatrix<GLdouble> G_mergeSurface;
    Matrix<GLdouble> dG_mergeSurface;

    mergeSurface->VBlendingFunctionValues(0.0, G_mergeSurface);
    mergeSurface->VBlendingFunctionDerivatives(r, G_mergeSurface, dG_mergeSurface);

    switch (directionOfMergeSurface) {
    // right
    case 2:
        VmergeSurfacesRight(indexOfFixedSurface, indexOfMergeSurface, dF_fixedSurface, dG_fixedSurface, dF_mergeSurface, directionOfFixedSurface, r);
        break;

        // left
    case 3:
        VmergeSurfacesLeft(indexOfFixedSurface, indexOfMergeSurface, dF_fixedSurface, dG_fixedSurface, dG_mergeSurface, directionOfFixedSurface, r);
        break;
    }
    generateImage(indexOfMergeSurface, BSurface3::DEFAULT_NULL_FRAGMENT);
}



// join
GLvoid BSurface3Composite::joinSurfaces(GLint indexOfFirstSurface, GLint indexOfSecondSurface, GLint directionOfFirstSurface, GLint directionOfSecondSurface, GLint r1, GLint r2,  ShaderProgram* shader, Material* material)
{
    if (indexOfFirstSurface != -1 && indexOfSecondSurface!= -1)
    {

    }

    BSurface3 *firstSurface = _attributes[indexOfFirstSurface] -> _bsurface;
    BSurface3 *secondSurface = _attributes[indexOfSecondSurface] -> _bsurface;

    BSurface3 *joinedSurface = new BSurface3((*firstSurface));

    GLint sizeN = firstSurface->getN()*2+1;
    GLint sizeM = firstSurface->getM()*2+1;

    for(GLint i=0; i< sizeN; i++){
        joinedSurface->SetData(i,0,(*firstSurface)(i,0));
        joinedSurface->SetData(i,sizeN-1, (*secondSurface)(i,secondSurface->getN()*2));
        //            (*joinedSurface)(0,i) = (*firstSurface)(firstSurface->getN()*2, i);
        //            (*joinedSurface)(joinedSurface->getN()*2,i) = (*secondSurface)(0,i);

    }

    r1=1;

    RowMatrix<GLdouble> F_joinedSurface;
    Matrix<GLdouble> dF_joinedSurface;

    joinedSurface->UBlendingFunctionValues(joinedSurface->getAlpha(), F_joinedSurface);
    joinedSurface->UBlendingFunctionDerivatives(r1, F_joinedSurface, dF_joinedSurface);


    RowMatrix<GLdouble> G_secondSurface;
    Matrix<GLdouble> dG_secondSurface;

    secondSurface->UBlendingFunctionValues(0.0, G_secondSurface);
    secondSurface->UBlendingFunctionDerivatives(r1, G_secondSurface, dG_secondSurface);



    for(GLint i=0; i<sizeN; i++){
        for(GLint rho = 1; rho <= r1; rho++) {
            // if(indexOfFirstSurface == 0){
            DCoordinate3 left_sum;
            for (GLint j = 0; j <= rho; j++)
            {
                left_sum += (*secondSurface)(i,j) * dG_secondSurface(rho, j);
                //    left_sum += (*secondArc)[2*secondArc->getN()-j] * dG_secondArc(rho, 2*secondArc->getN()-j);

            }

            DCoordinate3 right_sum;
            for (GLint j = 0; j < rho; j++)
            {
                right_sum += (*joinedSurface)(i,2*joinedSurface->getM()-j) * dF_joinedSurface(rho, 2*joinedSurface->getN()-j);
            }

            joinedSurface->SetData(i, 2*joinedSurface->getN() - rho, (left_sum-right_sum) / dF_joinedSurface(rho, joinedSurface->getN()*2-rho));
            //                    (*joinedArc)[joined->getN()*2 - rho] = (left_sum-right_sum) / dF_joinedSurface(rho, joinedSurface->getN()*2-rho);

        }
    }



    //        if (r1 + r2 + 2 > (GLint) joinedArc->getN()*2+1 ) {
    //            return GL_FALSE;
    //        }



    //        //right right
    //        if(directionOfFirstArc == 0 && directionOfSecondArc == 0){
    //            (*joinedArc)[0] = (*firstArc)[firstArc->getN()*2];
    //            (*joinedArc)[joinedArc->getN()*2] = (*secondArc)[secondArc->getN()*2];
    //        } else if(directionOfFirstArc == 0 && directionOfSecondArc == 1) {
    //            //right left
    //            (*joinedArc)[0] = (*firstArc)[firstArc->getN()*2];
    //            (*joinedArc)[joinedArc->getN()*2] = (*secondArc)[0];
    //        } else if (directionOfFirstArc == 1 && directionOfSecondArc == 0 ) {
    //            // left right
    //            (*joinedArc)[0] = (*firstArc)[0];
    //            (*joinedArc)[joinedArc->getN()*2] = (*secondArc)[secondArc->getN()*2];
    //        } else {
    //            //left left
    //            (*joinedArc)[0] = (*firstArc)[0];
    //            (*joinedArc)[joinedArc->getN()*2] = (*secondArc)[0];


    //        }

    insertNewSurface(joinedSurface, material, shader);


}



/*
void SecondOrderAlgTrigoCompositePatch3::setAlpha(double newAlpha)
{
    this -> _alpha = newAlpha;
    double u_min, u_max, v_min, v_max;
    for (GLuint i = 0; i < _attributes.size(); i++)
    {
        SecondOrderAlgTrigoPatch3* mypatch = _attributes[i] -> _patch;
        mypatch -> GetUInterval(u_min, u_max);
        mypatch -> GetVInterval(v_min, v_max);
        mypatch -> SetUInterval(u_min, _alpha);
        mypatch -> SetVInterval(v_min, _alpha);
    }
}

GLdouble SecondOrderAlgTrigoCompositePatch3::getAlpha()
{
    return this -> _alpha;
}

double SecondOrderAlgTrigoCompositePatch3::getX(int index, int i, int j)
{
    if (index != -1)
    {
        double x, y, z;
        SecondOrderAlgTrigoPatch3* selected = _attributes[index] -> _patch;
        selected -> GetData(i, j, x, y, z);
        return x;
    }
    else
    {
        return 0;
    }
}

double SecondOrderAlgTrigoCompositePatch3::getY(int index, int i, int j)
{
    if (index != -1)
    {
        double x, y, z;
        SecondOrderAlgTrigoPatch3* selected = _attributes[index] -> _patch;
        selected->GetData(i, j, x, y, z);
        return y;
    }
    else
    {
        return 0;
    }
}

double SecondOrderAlgTrigoCompositePatch3::getZ(int index, int i, int j)
{
    if (index != -1)
    {
        double x, y, z;
        SecondOrderAlgTrigoPatch3* selected = _attributes[index] -> _patch;
        selected->GetData(i, j, x, y, z);
        return z;
    }
    else
    {
        return 0;
    }
}

void SecondOrderAlgTrigoCompositePatch3::setX(double newValue, int index, int i, int j)
{
    if (index != -1)
    {
        double x, y, z;
        _attributes[index] -> _patch -> GetData(i, j, x, y, z);
        x = newValue;
        _attributes[index] -> _patch -> SetData(i, j, x, y, z);
        setNeighbours(index, i , j, index, 0, -1);
    }
}

void SecondOrderAlgTrigoCompositePatch3:: setY(double newValue, int index, int i, int j)
{
    if (index != -1)
    {
        double x, y, z;
        _attributes[index] -> _patch -> GetData(i, j, x, y, z);
        y = newValue;
        _attributes[index] -> _patch -> SetData(i, j, x, y, z);
        setNeighbours(index, i , j, index, 0, -1);
    }
}

void SecondOrderAlgTrigoCompositePatch3::setZ(double newValue, int index, int i, int j)
{
    if (index != -1)
    {
        double x, y, z;
        _attributes[index] -> _patch -> GetData(i, j, x, y, z);
        z = newValue;
        _attributes[index] -> _patch -> SetData(i, j, x, y, z);
        setNeighbours(index, i , j, index, 0, -1);
    }
}

*/

//megvaltoztatja az index. felulet szinet
GLboolean BSurface3Composite::SetMaterial(int index, Material *material)
{
    _attributes[index]->_material = material;
    return GL_TRUE;
}

//megvaltoztatja az index. felulet shaderet
GLboolean BSurface3Composite::SetShader(int index, ShaderProgram *shader)
{
    _attributes[index]->_shader = shader;
    return GL_TRUE;
}

//megvaltoztatja a kontrolhalo szinet
GLboolean BSurface3Composite::SetControlColor(int index, Color4* color)
{
    _attributes[index]->_color_of_control = color;
    return GL_TRUE;
}

/*

//megvaltoztatja a szomszed kontrolpontokat, ha szukseges
void SecondOrderAlgTrigoCompositePatch3::setNeighbours(int index, int i, int j, int dontReachThis, GLint depth, GLuint prevInd)
{
    if (index != -1)
                {
                    if (depth == 3)
                        return;

                    if (index == dontReachThis && depth == 3)
                        return;

                    PatchAttributes* modified = _attributes[index];


                    double x, y, z;
                    double xx, yy, zz;

                    // a kontrol halo negy reszet nezzuk
                    if (i < 2 && j < 2)
                    {
                        // eszakra es nyugatra megyunk el
                         PatchAttributes* neighbourN = _attributes[index]->neighbours[0];
                        PatchAttributes* neighbourW = _attributes[index]->neighbours[3];


                        // eszakra
                        if (neighbourN)
                        {
                            modified->_patch->GetData(0, j, x, y, z);
                            modified->_patch->GetData(1, j, xx, yy, zz);

                            if (i == 0)         // ha az osszekapcsolt pont valtozott meg eszakra
                            {
                                neighbourN->_patch->SetData(3, j, x, y, z);
                            }
                                neighbourN->_patch->SetData(2, j, 2*x - xx, 2*y - yy, 2*z - zz);

                            // megkeressuk, hogy hanyadik index (k) a szomszed es tovabbmegyunk
                            for (GLuint k = 0; k < _attributes.size(); k++)
                            {
                                if (_attributes[k] == neighbourN)
                                {
                                    if (k != prevInd)
                                    {
                                        setNeighbours(k, 3, j, dontReachThis, depth+1, index);
                                        setNeighbours(k, 2, j, dontReachThis, depth+1, index);
                                    }
                                }
                            }
                        }

                        // nyugatra
                        if (neighbourW)
                        {
                            modified->_patch->GetData(i, 0, x, y, z);
                            modified->_patch->GetData(i, 1, xx, yy, zz);

                            if (j == 0)         // ha az osszekapcsolt pont valtozott meg
                                neighbourW->_patch->SetData(i, 3, x, y, z);
                            neighbourW->_patch->SetData(i, 2, 2*x - xx, 2*y - yy, 2*z - zz);

                            // megkeressuk, hogy hanyadik index (k) a szomszed es tovabbmegyunk
                            for (GLuint k = 0; k < _attributes.size(); k++)
                            {
                                if (_attributes[k] == neighbourW)
                                {
                                    if (k != prevInd)
                                    {
                                            setNeighbours(k, i, 3, dontReachThis, depth+1, index);
                                            setNeighbours(k, i, 2, dontReachThis, depth+1, index);
                                    }
                                }
                            }
                        }
                    }

                    else if (i < 2 && j >= 2)
                    {
                        // eszakra es keletre megyunk el
                        PatchAttributes* neighbourN = _attributes[index]->neighbours[0];
                        PatchAttributes* neighbourE = _attributes[index]->neighbours[1];


                        // eszakra
                        if (neighbourN)
                        {
                            modified->_patch->GetData(0, j, x, y, z);
                            modified->_patch->GetData(1, j, xx, yy, zz);

                            if (i == 0)         // ha az osszekapcsolt pont valtozott meg eszakra
                                neighbourN->_patch->SetData(3, j, x, y, z);
                            neighbourN->_patch->SetData(2, j, 2*x - xx, 2*y - yy, 2*z - zz);

                            // megkeressuk, hogy hanyadik index (k) a szomszed es tovabbmegyunk
                            for (GLuint k = 0; k < _attributes.size(); k++)
                            {
                                if (_attributes[k] == neighbourN)
                                {
                                    if (k != prevInd)
                                    {
                                        setNeighbours(k, 3, j, dontReachThis, depth+1, index);
                                        setNeighbours(k, 2, j, dontReachThis, depth+1, index);
                                    }
                                }
                            }
                        }


                        // keletre
                        if (neighbourE)
                        {
                            modified->_patch->GetData(i, 3, x, y, z);
                            modified->_patch->GetData(i, 2, xx, yy, zz);

                            if (j == 3)         // ha az osszekapcsolt pont valtozott meg
                                neighbourE->_patch->SetData(i, 0, x, y, z);
                            neighbourE->_patch->SetData(i, 1, 2*x - xx, 2*y - yy, 2*z - zz);

                            // megkeressuk, hogy hanyadik index (k) a szomszed es tovabbmegyunk
                            for (GLuint k = 0; k < _attributes.size(); k++)
                            {
                                if (_attributes[k] == neighbourE)
                                {
                                    if (k != prevInd)
                                    {
                                        setNeighbours(k, i, 0, dontReachThis, depth+1, index);
                                        setNeighbours(k, i, 1, dontReachThis, depth+1, index);
                                    }
                                }
                            }
                        }


                    }
                    else if (i >= 2 && j < 2)
                    {
                        // delre es nyugatra megyunk el

                       PatchAttributes* neighbourS = _attributes[index]->neighbours[2];
                       PatchAttributes* neighbourW = _attributes[index]->neighbours[3];

                        // delre
                        if (neighbourS)
                        {
                            modified->_patch->GetData(3, j, x, y, z);
                            modified->_patch->GetData(2, j, xx, yy, zz);

                            if (i == 3)         // ha az osszekapcsolt pont valtozott meg eszakra
                                neighbourS->_patch->SetData(0, j, x, y, z);
                            neighbourS->_patch->SetData(1, j, 2*x - xx, 2*y - yy, 2*z - zz);

                            // megkeressuk, hogy hanyadik index (k) a szomszed es tovabbmegyunk
                            for (GLuint k = 0; k < _attributes.size(); k++)
                            {
                                if (_attributes[k] == neighbourS)
                                {
                                    if (k != prevInd)
                                    {
                                        setNeighbours(k, 0, j, dontReachThis, depth+1, index);
                                        setNeighbours(k, 1, j, dontReachThis, depth+1, index);
                                    }
                                }
                            }
                        }

                        // nyugatra
                        if (neighbourW)
                        {
                            modified->_patch->GetData(i, 0, x, y, z);
                            modified->_patch->GetData(i, 1, xx, yy, zz);

                            if (j == 0)         // ha az osszekapcsolt pont valtozott meg
                                neighbourW->_patch->SetData(i, 3, x, y, z);
                            neighbourW->_patch->SetData(i, 2, 2*x - xx, 2*y - yy, 2*z - zz);

                            // megkeressuk, hogy hanyadik index (k) a szomszed es tovabbmegyunk
                            for (GLuint k = 0; k < _attributes.size(); k++)
                            {
                                if (_attributes[k] == neighbourW)
                                {
                                    if (k != prevInd)
                                    {
                                        setNeighbours(k, i, 3, dontReachThis, depth+1, index);
                                        setNeighbours(k, i, 2, dontReachThis, depth+1, index);
                                    }
                                }
                            }
                        }
                    }
                    else if (i >= 2 && j >= 2)
                    {
                        // delre es keletre megyunk el
                        PatchAttributes* neighbourS = _attributes[index]->neighbours[2];
                        PatchAttributes* neighbourE = _attributes[index]->neighbours[1];

                        // delre
                        if (neighbourS)
                        {
                            modified->_patch->GetData(3, j, x, y, z);
                            modified->_patch->GetData(2, j, xx, yy, zz);

                            if (i == 3)         // ha az osszekapcsolt pont valtozott meg eszakra
                                neighbourS->_patch->SetData(0, j, x, y, z);
                            neighbourS->_patch->SetData(1, j, 2*x - xx, 2*y - yy, 2*z - zz);

                            // megkeressuk, hogy hanyadik index (k) a szomszed es tovabbmegyunk
                            for (GLuint k = 0; k < _attributes.size(); k++)
                            {
                                if (_attributes[k] == neighbourS)
                                {
                                    if (k != prevInd)
                                    {
                                        setNeighbours(k, 0, j, dontReachThis, depth+1, index);
                                        setNeighbours(k, 1, j, dontReachThis, depth+1, index);
                                    }
                                }
                            }
                        }

                        // keletre
                        if (neighbourE)
                        {
                            modified->_patch->GetData(i, 3, x, y, z);
                            modified->_patch->GetData(i, 2, xx, yy, zz);

                            if (j == 3)         // ha az osszekapcsolt pont valtozott meg
                                neighbourE->_patch->SetData(i, 0, x, y, z);
                            neighbourE->_patch->SetData(i, 1, 2*x - xx, 2*y - yy, 2*z - zz);

                            // megkeressuk, hogy hanyadik index (k) a szomszed es tovabbmegyunk
                            for (GLuint k = 0; k < _attributes.size(); k++)
                            {
                                if (_attributes[k] == neighbourE)
                                {
                                    if (k != prevInd)
                                    {
                                        setNeighbours(k, i, 0, dontReachThis, depth+1, index);
                                        setNeighbours(k, i, 1, dontReachThis, depth+1, index);
                                    }
                                }
                            }
                        }
                    }
                }
}

//meghosszabitja az index. feluletet directoin iranyba
void SecondOrderAlgTrigoCompositePatch3::continuePatch(int indexOfPatch, int directionOfPatch)
{
    if (indexOfPatch != -1 && !_attributes[indexOfPatch] -> neighbours[directionOfPatch])
    {
        PatchAttributes* selected = _attributes[indexOfPatch];
        PatchAttributes* attribute = new PatchAttributes();
        attribute -> _patch = new SecondOrderAlgTrigoPatch3(_alpha);
        DCoordinate3 firstData[4];
        DCoordinate3 secondData[4];

        //lekerjuk az adatokat
        if (directionOfPatch == 0)
        {
            //LEFT
            for (int j = 0; j < 4; j++)
            {
                selected -> _patch -> GetData(0, j, firstData[j]);
                selected -> _patch -> GetData(1, j, secondData[j]);
            }
            for (int i = 0; i < 4; i++)
            {
                attribute -> _patch -> SetData(3, i, firstData[i]);
                attribute -> _patch -> SetData(2, i, firstData[i] + 1.0 * (firstData[i] - secondData[i]));
                attribute -> _patch -> SetData(1, i, firstData[i] + 2.0 * (firstData[i] - secondData[i]));
                attribute -> _patch -> SetData(0, i, firstData[i] + 3.0 * (firstData[i] - secondData[i]));
            }
        }
        else if (directionOfPatch == 1)
        {
            //UP
            for (int i = 0; i < 4; i++)
            {
                selected -> _patch -> GetData(i, 3, firstData[i]);
                selected -> _patch -> GetData(i, 2, secondData[i]);
            }
            for (int i = 0; i < 4; i++)
            {
                attribute -> _patch -> SetData(i, 0, firstData[i]);
                attribute -> _patch -> SetData(i, 1, firstData[i] + 1.0 * (firstData[i] - secondData[i]));
                attribute -> _patch -> SetData(i, 2, firstData[i] + 2.0 * (firstData[i] - secondData[i]));
                attribute -> _patch -> SetData(i, 3, firstData[i] + 3.0 * (firstData[i] - secondData[i]));
            }
        }
        else if (directionOfPatch == 2)
        {
            //RIGHT
            for (int j = 0; j < 4; j++)
            {
                selected -> _patch -> GetData(3, 3 - j, firstData[j]);
                selected -> _patch -> GetData(2, 3 - j, secondData[j]);

            }
            for (int i = 0; i < 4; i++)
            {
                attribute -> _patch -> SetData(0, 3 - i, firstData[i]);
                attribute -> _patch -> SetData(1, 3 - i, firstData[i] + 1.0 * (firstData[i] - secondData[i]));
                attribute -> _patch -> SetData(2, 3 - i, firstData[i] + 2.0 * (firstData[i] - secondData[i]));
                attribute -> _patch -> SetData(3, 3 - i, firstData[i] + 3.0 * (firstData[i] - secondData[i]));
            }
        }
        else if (directionOfPatch == 3)
        {
            //DOWN
            for (int i = 0; i < 4; i++)
            {
                selected -> _patch -> GetData(3 - i, 0, firstData[i]);
                selected -> _patch -> GetData(3 - i, 1, secondData[i]);
            }
            for (int i = 0; i < 4; i++)
            {
                attribute -> _patch -> SetData(3 - i, 3, firstData[i]);
                attribute -> _patch -> SetData(3 - i, 2, firstData[i] + 1.0 * (firstData[i] - secondData[i]));
                attribute -> _patch -> SetData(3 - i, 1, firstData[i] + 2.0 * (firstData[i] - secondData[i]));
                attribute -> _patch -> SetData(3 - i, 0, firstData[i] + 3.0 * (firstData[i] - secondData[i]));
            }
        }

        attribute -> _patch -> UpdateVertexBufferObjectsOfData();
        attribute -> _image = attribute -> _patch->GenerateImage(30, 30, GL_STATIC_DRAW);
        attribute -> _image -> UpdateVertexBufferObjects();
        attribute -> _material = selected -> _material;
        attribute -> _shader = selected -> _shader;
        attribute -> _color_of_control = new Color4(1.0f, 1.0f, 0.0f, 0.0f);
        for (int i = 0; i < 4; i++)
        {
            attribute -> neighbours.push_back(nullptr);
        }
        _attributes.push_back(attribute);

        // beallitjuk a szomszedokat
        if (directionOfPatch < 4)
        {
            _attributes[indexOfPatch] -> neighbours[directionOfPatch] = attribute;
            if (directionOfPatch == 0)
            {
                //LEFT
                attribute -> neighbours[2] = _attributes[indexOfPatch];
            }
            else if (directionOfPatch == 1)
            {
                //UP
                attribute -> neighbours[3] = _attributes[indexOfPatch];
            }
            else if (directionOfPatch == 2)
            {
                //RIGHT
                attribute -> neighbours[0] = _attributes[indexOfPatch];
            }
            else if (directionOfPatch == 3)
            {
                //DOWN
                attribute -> neighbours[1] = _attributes[indexOfPatch];
            }
        }
    }

}

// join
void SecondOrderAlgTrigoCompositePatch3::joinSurfaces(int indexOfFirstSurface, int indexOfSecondSurface, int directionOfFirstSurface, int directionOfSecondSurface, ShaderProgram* shader, Material* material)
{
    if (indexOfFirstSurface != -1 && indexOfSecondSurface!= -1)
    {
        PatchAttributes* selected = new PatchAttributes();
        selected -> _patch = new SecondOrderAlgTrigoPatch3(_alpha);
        selected -> _patch -> UpdateVertexBufferObjectsOfData();
        selected -> _image = selected -> _patch->GenerateImage(30, 30, GL_STATIC_DRAW);
        selected -> _image -> UpdateVertexBufferObjects();
        selected -> _material = material;
        selected -> _shader = shader;
        selected -> _color_of_control = new Color4(1.0f, 1.0f, 0.0f, 0.0f);
        SecondOrderAlgTrigoPatch3* firstPatch = _attributes[indexOfFirstSurface] -> _patch;

        if (directionOfFirstSurface == 0)
        {
            //LEFT
            if(directionOfSecondSurface == 0)
            {
                //LEFT
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(3, i, getX(indexOfFirstSurface, 0, i),
                                                         getY(indexOfFirstSurface, 0, i),
                                                          getZ(indexOfFirstSurface, 0, i));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfFirstSurface,  0, i) - getX(indexOfFirstSurface,  1, i),
                                                            2.0 * getY(indexOfFirstSurface,  0, i) - getY(indexOfFirstSurface,  1, i),
                                                            2.0 * getZ(indexOfFirstSurface,  0, i) - getZ(indexOfFirstSurface,  1, i));
                    selected -> _patch -> SetData(0, i, getX(indexOfSecondSurface, 0, i),
                                                         getY(indexOfSecondSurface, 0, i),
                                                          getZ(indexOfSecondSurface, 0, i));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfSecondSurface, 0, i) - getX(indexOfSecondSurface,  1, i),
                                                            2.0 * getY(indexOfSecondSurface, 0, i) - getY(indexOfSecondSurface,  1, i),
                                                            2.0 * getZ(indexOfSecondSurface, 0, i) - getZ(indexOfSecondSurface,  1, i));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes.push_back(selected);
                _attributes[indexOfFirstSurface]->neighbours[0] = selected;
                _attributes[indexOfSecondSurface]->neighbours[0] = selected;
            }
            else if(directionOfSecondSurface == 1)
            {
                //UP
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(3, i, getX(indexOfFirstSurface, 0, i),
                                                         getY(indexOfFirstSurface, 0, i),
                                                          getZ(indexOfFirstSurface, 0, i));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfFirstSurface,  0, i) - getX(indexOfFirstSurface,  1, i),
                                                            2.0 * getY(indexOfFirstSurface,  0, i) - getY(indexOfFirstSurface,  1, i),
                                                            2.0 * getZ(indexOfFirstSurface,  0, i) - getZ(indexOfFirstSurface,  1, i));
                    selected -> _patch -> SetData(0, i, getX(indexOfSecondSurface, 3 - i, 3),
                                                         getY(indexOfSecondSurface, 3 - i, 3),
                                                          getZ(indexOfSecondSurface, 3 - i, 3));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfSecondSurface, 3 - i, 3) - getX(indexOfSecondSurface,  3 - i, 2),
                                                            2.0 * getY(indexOfSecondSurface, 3 - i, 3) - getY(indexOfSecondSurface,  3 - i, 2),
                                                            2.0 * getZ(indexOfSecondSurface, 3 - i, 3) - getZ(indexOfSecondSurface,  3 - i, 2));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                _attributes.push_back(selected);
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes[indexOfFirstSurface]->neighbours[0] = selected;
                _attributes[indexOfSecondSurface]->neighbours[1] = selected;
            }
            else if(directionOfSecondSurface == 2)
            {
                //RIGHT
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(3, i, getX(indexOfFirstSurface, 0, i),
                                                         getY(indexOfFirstSurface, 0, i),
                                                          getZ(indexOfFirstSurface, 0, i));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfFirstSurface,  0, i) - getX(indexOfFirstSurface,  1, i),
                                                            2.0 * getY(indexOfFirstSurface,  0, i) - getY(indexOfFirstSurface,  1, i),
                                                            2.0 * getZ(indexOfFirstSurface,  0, i) - getZ(indexOfFirstSurface,  1, i));
                    selected -> _patch -> SetData(0, i, getX(indexOfSecondSurface, 3, i),
                                                         getY(indexOfSecondSurface, 3, i),
                                                          getZ(indexOfSecondSurface, 3, i));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfSecondSurface, 3, i) - getX(indexOfSecondSurface,  2, i),
                                                            2.0 * getY(indexOfSecondSurface, 3, i) - getY(indexOfSecondSurface,  2, i),
                                                            2.0 * getZ(indexOfSecondSurface, 3, i) - getZ(indexOfSecondSurface,  2, i));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes.push_back(selected);
                _attributes[indexOfFirstSurface]->neighbours[0] = selected;
                _attributes[indexOfSecondSurface]->neighbours[2] = selected;
            }
            else if(directionOfSecondSurface == 3)
            {
                //DOWN
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(3, i, getX(indexOfFirstSurface, 0, i),
                                                         getY(indexOfFirstSurface, 0, i),
                                                          getZ(indexOfFirstSurface, 0, i));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfFirstSurface,  0, i) - getX(indexOfFirstSurface,  1, i),
                                                            2.0 * getY(indexOfFirstSurface,  0, i) - getY(indexOfFirstSurface,  1, i),
                                                            2.0 * getZ(indexOfFirstSurface,  0, i) - getZ(indexOfFirstSurface,  1, i));
                    selected -> _patch -> SetData(0, i, getX(indexOfSecondSurface, i, 0),
                                                         getY(indexOfSecondSurface, i, 0),
                                                          getZ(indexOfSecondSurface, i, 0));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfSecondSurface, i, 0) - getX(indexOfSecondSurface,  i, 1),
                                                            2.0 * getY(indexOfSecondSurface, i, 0) - getY(indexOfSecondSurface, i, 1),
                                                            2.0 * getZ(indexOfSecondSurface, i, 0) - getZ(indexOfSecondSurface, i, 1));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                _attributes.push_back(selected);
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes[indexOfFirstSurface]->neighbours[0] = selected;
                _attributes[indexOfSecondSurface]->neighbours[3] = selected;
            }
        }
        else if (directionOfFirstSurface == 1)
        {
            //UP
            if(directionOfSecondSurface == 0)
            {
                //LEFT
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(3, i, getX(indexOfSecondSurface, 0, 3 - i),
                                                         getY(indexOfSecondSurface, 0, 3 - i),
                                                          getZ(indexOfSecondSurface, 0, 3 - i));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfSecondSurface,  0, 3 - i) - getX(indexOfSecondSurface,  1, 3 - i),
                                                            2.0 * getY(indexOfSecondSurface,  0, 3 - i) - getY(indexOfSecondSurface,  1, 3 - i),
                                                            2.0 * getZ(indexOfSecondSurface,  0, 3 - i) - getZ(indexOfSecondSurface,  1, 3 - i));
                    selected -> _patch -> SetData(0, i, getX(indexOfFirstSurface, 3 - i, 3),
                                                         getY(indexOfFirstSurface, 3 - i, 3),
                                                          getZ(indexOfFirstSurface, 3 - i, 3));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfFirstSurface, 3 - i, 3) - getX(indexOfFirstSurface,  3 - i, 2),
                                                            2.0 * getY(indexOfFirstSurface, 3 - i, 3) - getY(indexOfFirstSurface, 3 - i, 2),
                                                            2.0 * getZ(indexOfFirstSurface, 3 - i, 3) - getZ(indexOfFirstSurface, 3 - i, 2));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                selected -> neighbours[0] = _attributes[indexOfFirstSurface];
                selected -> neighbours[2] = _attributes[indexOfSecondSurface];
                _attributes.push_back(selected);
                _attributes[indexOfFirstSurface]->neighbours[1] = selected;
                _attributes[indexOfSecondSurface]->neighbours[0] = selected;
            }
            else if(directionOfSecondSurface == 1)
            {
                //UP
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(0, i, getX(indexOfSecondSurface, 3 - i, 3),
                                                         getY(indexOfSecondSurface, 3 - i, 3),
                                                          getZ(indexOfSecondSurface, 3 - i, 3));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfSecondSurface, 3 - i, 3) - getX(indexOfSecondSurface, 3 - i, 2),
                                                            2.0 * getY(indexOfSecondSurface, 3 - i, 3) - getY(indexOfSecondSurface, 3 - i, 2),
                                                            2.0 * getZ(indexOfSecondSurface,  3 - i, 3) - getZ(indexOfSecondSurface, 3 - i, 2));
                    selected -> _patch -> SetData(3, i, getX(indexOfFirstSurface, i, 3),
                                                         getY(indexOfFirstSurface, i, 3),
                                                          getZ(indexOfFirstSurface, i, 3));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfFirstSurface, i, 3) - getX(indexOfFirstSurface,  i, 2),
                                                            2.0 * getY(indexOfFirstSurface, i, 3) - getY(indexOfFirstSurface, i, 2),
                                                            2.0 * getZ(indexOfFirstSurface, i, 3) - getZ(indexOfFirstSurface, i, 2));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                _attributes.push_back(selected);
                selected -> neighbours[0] = _attributes[indexOfFirstSurface];
                selected -> neighbours[2] = _attributes[indexOfSecondSurface];
                _attributes[indexOfFirstSurface]->neighbours[1] = selected;
                _attributes[indexOfSecondSurface]->neighbours[1] = selected;
            }
            else if(directionOfSecondSurface == 2)
            {
                //RIGHT
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(0, i, getX(indexOfSecondSurface, 3, i),
                                                         getY(indexOfSecondSurface, 3, i),
                                                          getZ(indexOfSecondSurface, 3, i));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfSecondSurface, 3, i) - getX(indexOfSecondSurface, 2, i),
                                                            2.0 * getY(indexOfSecondSurface, 3, i) - getY(indexOfSecondSurface, 2, i),
                                                            2.0 * getZ(indexOfSecondSurface,  3, i) - getZ(indexOfSecondSurface, 2, i));
                    selected -> _patch -> SetData(3, i, getX(indexOfFirstSurface, i, 3),
                                                         getY(indexOfFirstSurface, i, 3),
                                                          getZ(indexOfFirstSurface, i, 3));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfFirstSurface, i, 3) - getX(indexOfFirstSurface,  i, 2),
                                                            2.0 * getY(indexOfFirstSurface, i, 3) - getY(indexOfFirstSurface, i, 2),
                                                            2.0 * getZ(indexOfFirstSurface, i, 3) - getZ(indexOfFirstSurface, i, 2));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes.push_back(selected);
                _attributes[indexOfFirstSurface]->neighbours[1] = selected;
                _attributes[indexOfSecondSurface]->neighbours[3] = selected;
            }
            else if(directionOfSecondSurface == 3)
            {
                //DOWN
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(0, i, getX(indexOfSecondSurface, i, 0),
                                                         getY(indexOfSecondSurface, i, 0),
                                                          getZ(indexOfSecondSurface, i, 0));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfSecondSurface, i, 0) - getX(indexOfSecondSurface, i, 1),
                                                            2.0 * getY(indexOfSecondSurface, i, 0) - getY(indexOfSecondSurface, i, 1),
                                                            2.0 * getZ(indexOfSecondSurface,  i, 0) - getZ(indexOfSecondSurface, i, 1));
                    selected -> _patch -> SetData(3, i, getX(indexOfFirstSurface, i, 3),
                                                         getY(indexOfFirstSurface, i, 3),
                                                          getZ(indexOfFirstSurface, i, 3));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfFirstSurface, i, 3) - getX(indexOfFirstSurface,  i, 2),
                                                            2.0 * getY(indexOfFirstSurface, i, 3) - getY(indexOfFirstSurface, i, 2),
                                                            2.0 * getZ(indexOfFirstSurface, i, 3) - getZ(indexOfFirstSurface, i, 2));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                _attributes.push_back(selected);
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes[indexOfFirstSurface]->neighbours[0] = selected;
                _attributes[indexOfSecondSurface]->neighbours[3] = selected;
            }
        }
        else if (directionOfFirstSurface == 2)
        {
            //RIGHT
            if(directionOfSecondSurface == 0)
            {
                //LEFT
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(0, i, getX(indexOfFirstSurface, 3, i),
                                                         getY(indexOfFirstSurface, 3, i),
                                                          getZ(indexOfFirstSurface, 3, i));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfFirstSurface,  3, i) - getX(indexOfFirstSurface, 2, i),
                                                            2.0 * getY(indexOfFirstSurface, 3, i) - getY(indexOfFirstSurface, 2, i),
                                                            2.0 * getZ(indexOfFirstSurface, 3, i) - getZ(indexOfFirstSurface, 2, i));
                    selected -> _patch -> SetData(3, i, getX(indexOfSecondSurface, 0, i),
                                                         getY(indexOfSecondSurface, 0, i),
                                                          getZ(indexOfSecondSurface, 0, i));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfSecondSurface, 0, i) - getX(indexOfSecondSurface,  1, i),
                                                            2.0 * getY(indexOfSecondSurface, 0, i) - getY(indexOfSecondSurface,  1, i),
                                                            2.0 * getZ(indexOfSecondSurface, 0, i) - getZ(indexOfSecondSurface,  1, i));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                _attributes.push_back(selected);
                selected -> neighbours[0] = _attributes[indexOfFirstSurface];
                selected -> neighbours[2] = _attributes[indexOfSecondSurface];
                _attributes[indexOfFirstSurface]->neighbours[2] = selected;
                _attributes[indexOfSecondSurface]->neighbours[0] = selected;
            }
            else if(directionOfSecondSurface == 1)
            {
                //UP
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(0, i, getX(indexOfFirstSurface, 3, i),
                                                         getY(indexOfFirstSurface, 3, i),
                                                          getZ(indexOfFirstSurface, 3, i));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfFirstSurface,  3, i) - getX(indexOfFirstSurface, 2, i),
                                                            2.0 * getY(indexOfFirstSurface, 3, i) - getY(indexOfFirstSurface, 2, i),
                                                            2.0 * getZ(indexOfFirstSurface, 3, i) - getZ(indexOfFirstSurface, 2, i));
                    selected -> _patch -> SetData(3, i, getX(indexOfSecondSurface, i, 3),
                                                         getY(indexOfSecondSurface, i, 3),
                                                          getZ(indexOfSecondSurface, i, 3));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfSecondSurface, i, 3) - getX(indexOfSecondSurface,  i, 2),
                                                            2.0 * getY(indexOfSecondSurface, i, 3) - getY(indexOfSecondSurface, i, 2),
                                                            2.0 * getZ(indexOfSecondSurface, i, 3) - getZ(indexOfSecondSurface, i, 2));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                _attributes.push_back(selected);
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes[indexOfFirstSurface]->neighbours[2] = selected;
                _attributes[indexOfSecondSurface]->neighbours[1] = selected;
            }
            else if(directionOfSecondSurface == 2)
            {
                //RIGHT
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(0, i, getX(indexOfFirstSurface, 3, i),
                                                         getY(indexOfFirstSurface, 3, i),
                                                          getZ(indexOfFirstSurface, 3, i));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfFirstSurface,  3, i) - getX(indexOfFirstSurface, 2, i),
                                                            2.0 * getY(indexOfFirstSurface, 3, i) - getY(indexOfFirstSurface, 2, i),
                                                            2.0 * getZ(indexOfFirstSurface, 3, i) - getZ(indexOfFirstSurface, 2, i));
                    selected -> _patch -> SetData(3, i, getX(indexOfSecondSurface, 3, i),
                                                         getY(indexOfSecondSurface, 3, i),
                                                          getZ(indexOfSecondSurface, 3, i));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfSecondSurface, 3, i) - getX(indexOfSecondSurface,  2, i),
                                                            2.0 * getY(indexOfSecondSurface, 3, i) - getY(indexOfSecondSurface, 2, i),
                                                            2.0 * getZ(indexOfSecondSurface, 3, i) - getZ(indexOfSecondSurface, 2, i));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes.push_back(selected);
                _attributes[indexOfFirstSurface]->neighbours[2] = selected;
                _attributes[indexOfSecondSurface]->neighbours[2] = selected;
            }
            else if(directionOfSecondSurface == 3)
            {
                //DOWN
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(0, i, getX(indexOfFirstSurface, 3, i),
                                                         getY(indexOfFirstSurface, 3, i),
                                                          getZ(indexOfFirstSurface, 3, i));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfFirstSurface,  3, i) - getX(indexOfFirstSurface, 2, i),
                                                            2.0 * getY(indexOfFirstSurface, 3, i) - getY(indexOfFirstSurface, 2, i),
                                                            2.0 * getZ(indexOfFirstSurface, 3, i) - getZ(indexOfFirstSurface, 2, i));
                    selected -> _patch -> SetData(3, i, getX(indexOfSecondSurface, 3 - i, 0),
                                                         getY(indexOfSecondSurface, 3 - i, 0),
                                                          getZ(indexOfSecondSurface, 3 - i, 0));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfSecondSurface, 3 - i, 0) - getX(indexOfSecondSurface, 3 - i, 1),
                                                            2.0 * getY(indexOfSecondSurface, 3 - i, 0) - getY(indexOfSecondSurface, 3 - i, 1),
                                                            2.0 * getZ(indexOfSecondSurface, 3 - i, 0) - getZ(indexOfSecondSurface, 3 - i, 1));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                _attributes.push_back(selected);
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes[indexOfFirstSurface]->neighbours[2] = selected;
                _attributes[indexOfSecondSurface]->neighbours[3] = selected;
            }
        }
        else if (directionOfFirstSurface == 3)
        {
            //DOWN
            if(directionOfSecondSurface == 0)
            {
                //LEFT
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(3, i, getX(indexOfSecondSurface, 0, i),
                                                         getY(indexOfSecondSurface, 0, i),
                                                          getZ(indexOfSecondSurface, 0, i));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfSecondSurface,  0, i) - getX(indexOfSecondSurface,  1, i),
                                                            2.0 * getY(indexOfSecondSurface,  0, i) - getY(indexOfSecondSurface,  1, i),
                                                            2.0 * getZ(indexOfSecondSurface,  0, i) - getZ(indexOfSecondSurface,  1, i));
                    selected -> _patch -> SetData(0, i, getX(indexOfFirstSurface, i, 0),
                                                         getY(indexOfFirstSurface, i, 0),
                                                          getZ(indexOfFirstSurface, i, 0));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfFirstSurface, i, 0) - getX(indexOfFirstSurface, i, 1),
                                                            2.0 * getY(indexOfFirstSurface, i, 0) - getY(indexOfFirstSurface, i, 1),
                                                            2.0 * getZ(indexOfFirstSurface, i, 0) - getZ(indexOfFirstSurface, i, 1));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                selected -> neighbours[0] = _attributes[indexOfFirstSurface];
                selected -> neighbours[2] = _attributes[indexOfSecondSurface];
                _attributes.push_back(selected);
                _attributes[indexOfFirstSurface]->neighbours[3] = selected;
                _attributes[indexOfSecondSurface]->neighbours[0] = selected;
            }
            else if(directionOfSecondSurface == 1)
            {
                //UP
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(3, i, getX(indexOfSecondSurface, i, 3),
                                                         getY(indexOfSecondSurface, i, 3),
                                                          getZ(indexOfSecondSurface, i, 3));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfSecondSurface,  i, 3) - getX(indexOfSecondSurface,  i, 2),
                                                            2.0 * getY(indexOfSecondSurface, i, 3) - getY(indexOfSecondSurface, i, 2),
                                                            2.0 * getZ(indexOfSecondSurface, i, 3) - getZ(indexOfSecondSurface, i, 2));
                    selected -> _patch -> SetData(0, i, getX(indexOfFirstSurface, i, 0),
                                                         getY(indexOfFirstSurface, i, 0),
                                                          getZ(indexOfFirstSurface, i, 0));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfFirstSurface, i, 0) - getX(indexOfFirstSurface, i, 1),
                                                            2.0 * getY(indexOfFirstSurface, i, 0) - getY(indexOfFirstSurface, i, 1),
                                                            2.0 * getZ(indexOfFirstSurface, i, 0) - getZ(indexOfFirstSurface, i, 1));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                _attributes.push_back(selected);
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes[indexOfFirstSurface]->neighbours[3] = selected;
                _attributes[indexOfSecondSurface]->neighbours[1] = selected;
            }
            else if(directionOfSecondSurface == 2)
            {
                //RIGHT
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(3, i, getX(indexOfSecondSurface, 3, 3 - i),
                                                         getY(indexOfSecondSurface, 3, 3 - i),
                                                          getZ(indexOfSecondSurface, 3, 3 - i));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfSecondSurface, 3, 3 - i) - getX(indexOfSecondSurface,  2, 3 - i),
                                                            2.0 * getY(indexOfSecondSurface, 3, 3 - i) - getY(indexOfSecondSurface, 2, 3 - i),
                                                            2.0 * getZ(indexOfSecondSurface, 3, 3 - i) - getZ(indexOfSecondSurface, 2, 3 - i));
                    selected -> _patch -> SetData(0, i, getX(indexOfFirstSurface, i, 0),
                                                         getY(indexOfFirstSurface, i, 0),
                                                          getZ(indexOfFirstSurface, i, 0));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfFirstSurface, i, 0) - getX(indexOfFirstSurface, i, 1),
                                                            2.0 * getY(indexOfFirstSurface, i, 0) - getY(indexOfFirstSurface, i, 1),
                                                            2.0 * getZ(indexOfFirstSurface, i, 0) - getZ(indexOfFirstSurface, i, 1));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes.push_back(selected);
                _attributes[indexOfFirstSurface]->neighbours[3] = selected;
                _attributes[indexOfSecondSurface]->neighbours[2] = selected;
            }
            else if(directionOfSecondSurface == 3)
            {
                //DOWN
                for (int i = 0; i < 4; i++)
                {
                    selected -> _patch -> SetData(3, i, getX(indexOfSecondSurface, 3 - i, 0),
                                                         getY(indexOfSecondSurface, 3 - i, 0),
                                                          getZ(indexOfSecondSurface, 3 - i, 0));
                    selected -> _patch -> SetData(2, i, 2.0 * getX(indexOfSecondSurface, 3 - i, 0) - getX(indexOfSecondSurface, 3 - i, 1),
                                                            2.0 * getY(indexOfSecondSurface, 3 - i, 0) - getY(indexOfSecondSurface, 3 - i, 1),
                                                            2.0 * getZ(indexOfSecondSurface, 3 - i, 0) - getZ(indexOfSecondSurface, 3 - i, 1));
                    selected -> _patch -> SetData(0, i, getX(indexOfFirstSurface, i, 0),
                                                         getY(indexOfFirstSurface, i, 0),
                                                          getZ(indexOfFirstSurface, i, 0));
                    selected -> _patch -> SetData(1, i, 2.0 * getX(indexOfFirstSurface, i, 0) - getX(indexOfFirstSurface, i, 1),
                                                            2.0 * getY(indexOfFirstSurface, i, 0) - getY(indexOfFirstSurface, i, 1),
                                                            2.0 * getZ(indexOfFirstSurface, i, 0) - getZ(indexOfFirstSurface, i, 1));
                }
                for (int i = 0; i < 4; i++)
                {
                    selected -> neighbours.push_back(nullptr);
                }
                _attributes.push_back(selected);
                selected -> neighbours[2] = _attributes[indexOfFirstSurface];
                selected -> neighbours[0] = _attributes[indexOfSecondSurface];
                _attributes[indexOfFirstSurface]->neighbours[3] = selected;
                _attributes[indexOfSecondSurface]->neighbours[3] = selected;
            }
        }
    }

}

// merge
void SecondOrderAlgTrigoCompositePatch3::mergePatches(int indexOfFirstSurface, int indexOfSecondSurface, int directionOfFirstSurface, int directionOfSecondSurface)
{

    if (indexOfFirstSurface!= -1 && indexOfSecondSurface != -1)
    {
        DCoordinate3 fromFirstPatch[4];
        DCoordinate3 fromSecondPatch[4];

        //lekerjuk az adatokat
        //elso felulet
        if (directionOfFirstSurface == 0)
        {
            //LEFT
            if(directionOfSecondSurface == 0)
            {
                //LEFT
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, 1, j);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, 1, j);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, 1, j);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 1, j);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 1, j);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 1, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, 0, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, 0, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, 0, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 0, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 0, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 0, j);
                 }
            }
            else if(directionOfSecondSurface == 1)
            {
                //UP
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, 1, j);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, 1, j);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, 1, j);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 3 - j, 2);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 3 - j, 2);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 3 - j, 2);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, 0, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, 0, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, 0, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3 - j, 3);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3 - j, 3);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3 - j, 3);

                }
            }
            else if(directionOfSecondSurface == 2)
            {
                //RIGHT
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, 1, j);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, 1, j);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, 1, j);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 2, j);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 2, j);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 2, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, 0, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, 0, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, 0, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3, j);

                }
            }
            else if(directionOfSecondSurface == 3)
            {
                //DOWN
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, 1, j);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, 1, j);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, 1, j);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 3 - j, 1);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 3 - j, 1);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 3 - j, 1);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, 0, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, 0, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, 0, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3 - j, 0);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3 - j, 0);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3 - j, 0);

                }
            }
        }
        if (directionOfFirstSurface == 1)
        {
            //UP
            if(directionOfSecondSurface == 0)
            {
                //LEFT
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, j, 2);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, j, 2);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, j, 2);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 1, j);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 1, j);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 1, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, j, 3);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, j, 3);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, j, 3);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 0, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 0, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 0, j);
                 }
            }
            else if(directionOfSecondSurface == 1)
            {
                //UP
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, j, 2);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, j, 2);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, j, 2);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 3 - j, 2);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 3 - j, 2);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 3 - j, 2);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, j, 3);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, j, 3);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, j, 3);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3 - j, 3);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3 - j, 3);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3 - j, 3);

                }
            }
            else if(directionOfSecondSurface == 2)
            {
                //RIGHT
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, j, 2);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, j, 2);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, j, 2);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 2, j);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 2, j);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 2, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, j, 3);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, j, 3);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, j, 3);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3, j);

                }
            }
            else if(directionOfSecondSurface == 3)
            {
                //DOWN
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, j, 2);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, j, 2);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, j, 2);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 3 - j, 1);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 3 - j, 1);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 3 - j, 1);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, j, 3);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, j, 3);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, j, 3);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3 - j, 0);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3 - j, 0);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3 - j, 0);

                }
            }
        }
        else if (directionOfFirstSurface == 2)
        {
            //RIGHT
            if(directionOfSecondSurface == 0)
            {
                //LEFT
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, 2, j);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, 2, j);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, 2, j);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 1, j);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 1, j);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 1, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, 3, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, 3, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, 3, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 0, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 0, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 0, j);
                 }
            }
            else if(directionOfSecondSurface == 1)
            {
                //UP
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, 2, j);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, 2, j);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, 2, j);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 3 - j, 2);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 3 - j, 2);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 3 - j, 2);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, 3, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, 3, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, 3, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3 - j, 3);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3 - j, 3);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3 - j, 3);

                }
            }
            else if(directionOfSecondSurface == 2)
            {
                //RIGHT
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, 2, j);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, 2, j);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, 2, j);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 2, j);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 2, j);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 2, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, 3, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, 3, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, 3, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3, j);

                }
            }
            else if(directionOfSecondSurface == 3)
            {
                //DOWN
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, 2, j);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, 2, j);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, 2, j);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 3 - j, 1);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 3 - j, 1);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 3 - j, 1);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, 3, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, 3, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, 3, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3 - j, 0);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3 - j, 0);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3 - j, 0);

                }
            }
        }
        else if (directionOfFirstSurface == 3)
        {
            //DOWN
            if(directionOfSecondSurface == 0)
            {
                //LEFT
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, j, 1);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, j, 1);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, j, 1);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 1, j);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 1, j);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 1, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, j, 0);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, j, 0);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, j, 0);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 0, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 0, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 0, j);
                 }
            }
            else if(directionOfSecondSurface == 1)
            {
                //UP
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, j, 1);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, j, 1);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, j, 1);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 3 - j, 2);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 3 - j, 2);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 3 - j, 2);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, j, 0);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, j, 0);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, j, 0);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3 - j, 3);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3 - j, 3);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3 - j, 3);

                }
            }
            else if(directionOfSecondSurface == 2)
            {
                //RIGHT
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, j, 1);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, j, 1);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, j, 1);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 2, j);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 2, j);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 2, j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, j, 0);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, j, 0);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, j, 0);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 3, j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 3, j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 3, j);

                }
            }
            else if(directionOfSecondSurface == 3)
            {
                //DOWN
                for (int j = 0; j < 4; j++)
                {
                    fromFirstPatch[j].x() = getX(indexOfFirstSurface, j, 1);
                    fromFirstPatch[j].y() = getY(indexOfFirstSurface, j, 1);
                    fromFirstPatch[j].z() = getZ(indexOfFirstSurface, j, 1);
                    fromSecondPatch[j].x() = getX(indexOfSecondSurface, 1, 3 - j);
                    fromSecondPatch[j].y() = getY(indexOfSecondSurface, 1, 3 - j);
                    fromSecondPatch[j].z() = getZ(indexOfSecondSurface, 1, 3 - j);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfFirstSurface, j, 0);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfFirstSurface, j, 0);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfFirstSurface, j, 0);
                    setX(0.5*(fromFirstPatch[j].x() + fromSecondPatch[j].x()), indexOfSecondSurface, 0, 3 - j);
                    setY(0.5*(fromFirstPatch[j].y() + fromSecondPatch[j].y()), indexOfSecondSurface, 0, 3 - j);
                    setZ(0.5*(fromFirstPatch[j].z() + fromSecondPatch[j].z()), indexOfSecondSurface, 0, 3 - j);

                }
            }
        }

        // beallitjuk a szomszedokat
        _attributes[indexOfFirstSurface] -> neighbours[directionOfFirstSurface] = _attributes[indexOfSecondSurface];
        _attributes[indexOfSecondSurface] -> neighbours[directionOfSecondSurface] = _attributes[indexOfFirstSurface];
    }
}

*/
