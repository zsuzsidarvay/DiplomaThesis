#include "BSurface3.h"

#include <cmath>

using namespace std;

namespace cagd
{
// c a l c u l a t e s b i n omi a l c o e f f i c i e n t s f rom 0 to up to the g i v e n o r d e r
GLvoid BSurface3::CalculateBinomialCoefficients(GLuint order, TriangularMatrix<GLdouble> &bc){

    bc.ResizeRows(order + 1) ;

    bc(0,0) = 1.0;

    for(GLuint r = 1; r <= order; ++r){

        bc(r,0) = 1.0;
        bc(r, r) = 1.0;

        for(GLuint i = 1; i <= r / 2; ++i){

            bc(r, i) = bc(r-1, i-1) + bc(r-1, i);
            bc(r, r-i) = bc(r, i);
        }
    }
}

// c a l c u l a t e s the n o rma l i z i n g c o e f f i c i e n t s

GLboolean BSurface3::CalculateNormalizingCoefficients(GLuint order, GLdouble alpha, RowMatrix<GLdouble> &c) const{

    if(!order){

        c.ResizeColumns(0);
        return GL_FALSE;
    }

    GLuint size = 2* order + 1;
    c.ResizeColumns(size);

    GLdouble sa = pow( sin(alpha / 2.0), (GLint)( 2 * order));
    GLdouble ca = 2.0 * cos( alpha / 2.0);

    for(GLuint i = 0; i <= order; ++i){

        c[i] = 0.0;
        for(GLuint r = 0; r <= i / 2; ++r){

            c[i] += _bc(order, i-r) * _bc(i-r, r) * pow(ca, (GLint)( i - 2 * r));
        }

        c[i] /= sa;
        c[size - 1 - i] = c[i];
    }

    return GL_TRUE;
}

// s p e c i a l c o n s t r u c t o r
BSurface3:: BSurface3(GLdouble alpha, GLuint n, GLdouble beta, GLuint m): TensorProductSurface3(0.0, alpha, 0.0, beta, 2*n + 1, 2*m + 1), _alpha( alpha) , _beta(beta), _n(n), _m(m)
{

    GLuint max_order = (_n > _m ? _n : _m);
    CalculateBinomialCoefficients(2*max_order, _bc);

    CalculateNormalizingCoefficients(_n, _alpha, _u_c);
    CalculateNormalizingCoefficients ( _m, _beta, _v_c);
}

BSurface3::BSurface3(BSurface3 const &bsurface): TensorProductSurface3(bsurface){
    this->_n = bsurface._n;
    this->_m = bsurface._m;
    this->_alpha = bsurface._alpha;
    this->_beta = bsurface._beta;
    this->_u_c = bsurface._u_c;
    this->_v_c = bsurface._v_c;
    this->_bc = bsurface._bc;
}

RowMatrix<GLdouble> BSurface3::getU_c()
{
    return _u_c;
}
RowMatrix<GLdouble> BSurface3::getV_c(){
    return _v_c;
}


TriangularMatrix<GLdouble> BSurface3::getBc(){
    return _bc;
}


// c a l c u l a t e s b l e n d i n g f u n c t i o n s

GLboolean BSurface3::UBlendingFunctionValues(GLdouble u, RowMatrix<GLdouble>& values) const
{
    if(u < 0.0 && u > _alpha){

        values.ResizeColumns(0);
        return GL_FALSE;
    }

    GLuint size = 2 * _n + 1 ;

    values.ResizeColumns(size);

    for(GLuint i = 0; i < size; ++i){
        values[i] = 0.0;
    }

    if(u == 0.0)
    {
        values[0] = 1.0;
    } else {
        if(u == _alpha){

            values[size - 1] = 1.0;
        } else {

            GLdouble sau = sin((_alpha - u )/ 2.0 ), su = sin(u / 2.0);

            if(u < _alpha / 2.0){

                GLdouble factor = su / sau ;
                GLdouble sau_order = pow(sau, (GLint)(size - 1));

                values[0] = sau_order;

                for(GLint i = 1; i < (GLint) size; ++i){

                    values[i] = values[i - 1] * factor;
                }
            } else {
                GLdouble factor = sau / su ;
                GLdouble su_order = pow(su, (GLint)(size - 1));

                values[size - 1] = su_order;

                for(GLint i = size - 2; i >= 0; --i){
                    values[i] = values[i + 1] * factor;
                }
            }
            for(GLuint i = 0; i < size; ++i){

                values[i] *= _u_c[i];
            }
        }
    }
    return GL_TRUE;
}

// c a l c u l a t e s b l e n d i n g f u n c t i o n s

GLboolean BSurface3::VBlendingFunctionValues(GLdouble v, RowMatrix<GLdouble>& values) const {

    if(v < 0.0 && v > _beta){

        values.ResizeColumns(0);
        return GL_FALSE;
    }

    GLuint size = 2 * _m + 1 ;

    values.ResizeColumns(size);

    for(GLuint j = 0; j < size; ++j){

        values[j] = 0.0;
    }

    if (v == 0.0) {

        values[0] = 1.0;
    } else {

        if( v == _beta){

            values[size-1] = 1.0;
        } else {

            GLdouble sbv = sin(( _beta - v ) / 2.0), sv = sin ( v / 2.0);

            if( v < _beta / 2.0){

                GLdouble factor = sv / sbv;
                GLdouble sbv_order = pow(sbv , (GLint)(size - 1));

                values[0] = sbv_order;

                for(GLint j = 1; j < size; ++j){

                    values[j] = values[j - 1] * factor;
                }
            } else{

                GLdouble factor = sbv / sv;
                GLdouble sv_order = pow(sv , (GLint)(size - 1));

                values[size - 1] = sv_order;

                for(GLint j = size - 2; j >= 0; --j){

                    values[j] = values[j + 1] * factor;
                }
            }

            for( GLuint j = 0; j < size; ++j){

                values[j] *= _v_c[j];
            }
        }
    }

    return GL_TRUE;
}
// C a l c u l a t e s the s u r f a c e p o i n t s

GLboolean BSurface3::UBlendingFunctionDerivatives(GLuint maximum_order_of_partialderivatives, RowMatrix<GLdouble>& Au, Matrix<GLdouble>& dAu) const {
    GLuint u_size = 2 * _n + 1;

    GLdouble sua = 2.0 * sin(_alpha / 2.0), tua = tan(_alpha / 2.0);

    dAu.ResizeRows(maximum_order_of_partialderivatives + 1);
    dAu.ResizeColumns(u_size);


    dAu.SetRow(0, Au);

    for(GLuint d=1; d <= maximum_order_of_partialderivatives; d++) {
        for(GLuint i = 0; i < u_size; i++) {
            dAu(d,i) = (( i > 0) ? _u_c[i] / _u_c[i-1] * i / sua * dAu(d - 1 , i - 1) : 0.0)
                    - ((GLint)_n - (GLint)i) / tua * dAu(d - 1 , i)
                    - ((i < 2 * _n ) ? ( _u_c[i] / _u_c [i+1] * (2.0 * _n - i) / sua) * dAu(d - 1 , i + 1) : 0.0);
        }

    }

    return GL_TRUE;
}

GLboolean BSurface3::VBlendingFunctionDerivatives(GLuint maximum_order_of_partialderivatives, RowMatrix<GLdouble>& Av, Matrix<GLdouble>& dAv) const {

    GLuint v_size = 2 * _m + 1;

    GLdouble sva = 2.0 * sin(_beta / 2.0), tva = tan(_beta / 2.0);

    dAv.ResizeRows(maximum_order_of_partialderivatives+1);
    dAv.ResizeColumns(v_size);
    //    dAv(maximum_order_of_partialderivatives + 1, v_size);



    dAv.SetRow(0, Av);


    for(GLuint d=1; d <= maximum_order_of_partialderivatives; d++) {
        for(GLuint j = 0; j < v_size; j++) {
            dAv(d,j) = (( j > 0) ? _v_c[j] / _v_c[j-1] * j / sva * dAv(d - 1 , j - 1) : 0.0)
                    - ((GLint)_m - (GLint)j) / tva * dAv(d - 1 , j)
                    - ((j < 2 * _m ) ? ( _v_c[j] / _v_c [j+1] * (2.0 * _m - j) / sva) * dAv(d - 1 , j + 1) : 0.0);
        }
    }

    return GL_TRUE;

}



// The f i r s t and h i g h e r o r d e r d e r i v a t i v e s o f the b l e n d i n g f u n c t i o n s a r e c a l c u l a t e d
// by u s i n g the r e c u r s i v e f o rmul a (21)
GLboolean BSurface3:: CalculatePartialDerivatives(GLuint maximum_order_of_partialderivatives, GLdouble u, GLdouble v, PartialDerivatives& pd) const
{
    if(u < _u_min || u > _u_max || v < _v_min || v > _v_max){
        pd.ResizeRows(0);
        return GL_FALSE;
    }

    pd.ResizeRows(maximum_order_of_partialderivatives + 1);
    pd.LoadNullVectors();

    GLuint u_size = 2 * _n + 1, v_size = 2 * _m + 1;

    RowMatrix<GLdouble> Au , Av;

    if(!UBlendingFunctionValues(u, Au) || !VBlendingFunctionValues(v, Av)) {
        pd.ResizeRows(0);
        return GL_FALSE;
    }


    Matrix<GLdouble> dAu(maximum_order_of_partialderivatives + 1, u_size);
    Matrix<GLdouble> dAv(maximum_order_of_partialderivatives + 1, v_size);



    if(!UBlendingFunctionDerivatives(maximum_order_of_partialderivatives, Au, dAu)) {
        return GL_FALSE;
    }

    if(!VBlendingFunctionDerivatives(maximum_order_of_partialderivatives, Av, dAv)) {
        return GL_FALSE;
    }

    //    GLdouble sua = 2.0 * sin(_alpha / 2.0), tua = tan(_alpha / 2.0);
    //    GLdouble sva = 2.0 * sin(_beta / 2.0), tva = tan(_beta / 2.0);

    //    Matrix<GLdouble> dAu(maximum_order_of_partialderivatives + 1, u_size);
    //    Matrix<GLdouble> dAv(maximum_order_of_partialderivatives + 1, v_size);



    //    dAu.SetRow(0, Au);
    //    dAv.SetRow(0, Av);


    //    for(GLuint d=1; d <= maximum_order_of_partialderivatives; d++) {
    //        for(GLuint i = 0; i < u_size; i++) {
    //            dAu(d,i) = (( i > 0) ? _u_c[i] / _u_c[i-1] * i / sua * dAu(d - 1 , i - 1) : 0.0)
    //                    - ((GLint)_n - (GLint)i) / tua * dAu(d - 1 , i)
    //                    - ((i < 2 * _n ) ? ( _u_c[i] / _u_c [i+1] * (2.0 * _n - i) / sua) * dAu(d - 1 , i + 1) : 0.0);
    //        }

    //        for(GLuint j = 0; j < v_size; j++) {
    //            dAv(d,j) = (( j > 0) ? _v_c[j] / _v_c[j-1] * j / sva * dAv(d - 1 , j - 1) : 0.0)
    //                    - ((GLint)_m - (GLint)j) / tva * dAv(d - 1 , j)
    //                    - ((j < 2 * _m ) ? ( _v_c[j] / _v_c [j+1] * (2.0 * _m - j) / sva) * dAv(d - 1 , j + 1) : 0.0);
    //        }
    //    }

    for(GLuint i = 0; i < u_size; ++i) {

        RowMatrix<DCoordinate3> diff_v(maximum_order_of_partialderivatives + 1);

        for(GLuint j = 0; j < v_size; ++j){
            for(GLuint d = 0; d <= maximum_order_of_partialderivatives ; d++)
                diff_v[d] += _data(i,j) * dAv(d , j);
        }

        for(GLuint d = 0; d <= maximum_order_of_partialderivatives; ++d){
            for(GLuint r = 0; r <= d ; r++)
                pd(d,r)+= diff_v[r] * dAu(d-r , i) ;
        }



    }

    return GL_TRUE;
}


GLuint BSurface3::getN(){
    return _n;
}

GLuint BSurface3::getM(){
    return _m;
}

BSurface3& BSurface3::operator=(BSurface3& bsurface){
    if (this != &bsurface) {
        _u_closed = bsurface._u_closed;
        _v_closed = bsurface._v_closed;
        _vbo_data = bsurface._vbo_data;
        _u_min    = bsurface._u_min;
        _u_max    = bsurface._u_max;
        _v_min    = bsurface._v_min;
        _v_max    = bsurface._v_max;
        _data     = bsurface._data;

        _n = bsurface._n;
        _m = bsurface._m;
        _alpha = bsurface._alpha;
        _beta = bsurface._beta;
        _u_c = bsurface._u_c;
        _v_c = bsurface._v_c;
        _bc = bsurface._bc;

        if (this->_vbo_data) {
            UpdateVertexBufferObjectsOfData();
        }
    }


    return (*this);
}


BSurface3* BSurface3::UIncreaseOrder(GLuint z) const {
    BSurface3* result = new BSurface3(_alpha, _n+z, _beta, _m);

    RowMatrix<GLdouble> z_c;

    CalculateNormalizingCoefficients(z, _alpha, z_c);

    DCoordinate3 x(0.0,0.0,0.0);
    for (GLuint i = 0; i <= 2*(_n+z); i++) {
        for(GLuint j = 0; j <= 2*_m; j++) {
            result->_data(i, j) = x;
        }
    }

    for(GLuint i = 0; i <= 2*_n; i++) {
        for(GLuint j = 0; j <= 2*z; j++) {
            for(GLuint k = 0; k <= 2 * _m; k++) {
                result->_data(i+j, k) += _data(i, k) * (_u_c[i] * z_c[j]) / result->_u_c[i+j];
            }
        }
    }

    return result;
}

GLdouble BSurface3::getAlpha(){
    return _alpha;
}

GLdouble BSurface3::getBeta(){
    return _beta;
}

BSurface3* BSurface3::VIncreaseOrder(GLuint z) const {
    BSurface3* result = new BSurface3(_alpha, _n, _beta, _m+z);

    RowMatrix<GLdouble> z_c;

    CalculateNormalizingCoefficients(z, _beta, z_c);

    DCoordinate3 x(0.0,0.0,0.0);
    for (GLuint i = 0; i <= 2*_n; i++) {
        for(GLuint j = 0; j <= 2*(_m+z); j++) {
            result->_data(i, j) = x;
        }
    }

    for(GLuint i = 0; i <= 2*_m; i++) {
        for(GLuint j = 0; j <= 2*z; j++) {
            for(GLuint k = 0; k <= 2 * _n; k++) {
                result->_data(k, i+j) += _data(k, i) * (_v_c[i] * z_c[j]) / result->_v_c[i+j];
            }
        }
    }

    return result;
}

RowMatrix<BSurface3*>* BSurface3::USubdivision(GLdouble u) const {
    if (u < 0.0 || u >= _alpha)
    {
        return nullptr;
    }

    RowMatrix<BSurface3*> *result = new (nothrow) RowMatrix<BSurface3*>(2);

    if (!result)
    {
        return nullptr;
    }

    enum Child{LEFT = 0, RIGHT = 1};

    (*result)[LEFT] = new (nothrow) BSurface3(u, _n, _beta, _m);

    if (!(*result)[LEFT])
    {
        delete result;
        return nullptr;
    }

    (*result)[RIGHT] = new (nothrow) BSurface3(_alpha - u, _n, _beta, _m);

    if (!(*result)[RIGHT])
    {
        delete (*result)[LEFT];
        delete result;
        return nullptr;
    }

    GLint sizeN = 2 *_n + 1;
    GLint sizeM = 2 *_m + 1;

    //Calculate weigths
    TriangularMatrix<GLdouble> w(sizeN);
    RowMatrix<TriangularMatrix<DCoordinate3>> d(sizeM);
    for(GLint k = 0; k < sizeM; k++) {
        d[k].ResizeRows(sizeN);
    }

#pragma omp parallel
    for(GLint i = 0; i < sizeN; i++) {
        w(i, 0) = _u_c[i] / _bc(2*_n, i);
        for(GLint k = 0; k < sizeM; k++) {
            d[k](i, 0) = _data(i, k);
        }
    }

    GLdouble v = 0.5 + tan(u / 2.0 - _alpha / 4.0) / 2.0 / tan(_alpha / 4.0);

#pragma omp for
    for (GLint r = 1; r < sizeN; r++)
    {
        for (GLint i = r-1; i < sizeN - 1; i++)
        {
            w(i + 1, r) = (1.0 - v) * w(i, r - 1) + v * w(i + 1, r - 1);
            for(GLint k = 0; k < sizeM; k++) {
                d[k](i + 1, r) = ((1.0 - v) * w(i, r - 1) * d[k](i, r - 1)
                        + v * w(i + 1, r - 1) * d[k](i + 1, r - 1)) / w(i + 1, r);
            }
        }
    }

    BSurface3 &left = *(*result)[LEFT];
    BSurface3 &right = *(*result)[RIGHT];

    for (GLint i = 0; i < sizeN; i++)
    {
        for(GLint k = 0; k < sizeM; k++) {
            left(i, k) = d[k](i, i);
            right(i, k) = d[k](sizeN - 1, sizeN - 1 - i);
        }
    }

    return result;
}


RowMatrix<BSurface3*>* BSurface3::VSubdivision(GLdouble v) const {

    if (v < 0.0 || v >= _beta)
    {
        return nullptr;
    }

    RowMatrix<BSurface3*> *result = new (nothrow) RowMatrix<BSurface3*>(2);

    if (!result)
    {
        return nullptr;
    }

    enum Child{LEFT = 0, RIGHT = 1};

    (*result)[LEFT] = new (nothrow) BSurface3(_alpha, _n, v, _m);

    if (!(*result)[LEFT])
    {
        delete result;
        return nullptr;
    }

    (*result)[RIGHT] = new (nothrow) BSurface3(_alpha, _n, _beta - v, _m);

    if (!(*result)[RIGHT])
    {
        delete (*result)[LEFT];
        delete result;
        return nullptr;
    }

    GLint sizeN = 2 *_n + 1;
    GLint sizeM = 2 *_m + 1;

    //Calculate weigths
    TriangularMatrix<GLdouble> w(sizeM);
    RowMatrix<TriangularMatrix<DCoordinate3>> d(sizeN);
    for(GLint k = 0; k < sizeN; k++) {
        d[k].ResizeRows(sizeM);
    }

#pragma omp parallel
    for(GLint i = 0; i < sizeM; i++) {
        w(i, 0) = _v_c[i] / _bc(2*_m, i);
        for(GLint k = 0; k < sizeN; k++) {
            d[k](i, 0) = _data(k, i);
        }
    }

    GLdouble u = 0.5 + tan(v / 2.0 - _beta / 4.0) / 2.0 / tan(_beta / 4.0);

#pragma omp for
    for (GLint r = 1; r < sizeM; r++)
    {
        for (GLint i = r-1; i < sizeM - 1; i++)
        {
            w(i + 1, r) = (1.0 - u) * w(i, r - 1) + u * w(i + 1, r - 1);
            for(GLint k = 0; k < sizeN; k++) {
                d[k](i + 1, r) = ((1.0 - u) * w(i, r - 1) * d[k](i, r - 1)
                        + u * w(i + 1, r - 1) * d[k](i + 1, r - 1)) / w(i + 1, r);
            }
        }
    }

    BSurface3 &left = *(*result)[LEFT];
    BSurface3 &right = *(*result)[RIGHT];

    for (GLint i = 0; i < sizeM; i++)
    {
        for(GLint k = 0; k < sizeN; k++) {
            left(k, i) = d[k](i, i);
            right(k, i) = d[k](sizeM - 1, sizeM - 1 - i);
        }
    }

    return result;
}



}
