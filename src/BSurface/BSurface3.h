#pragma once

#include "../Core/Matrices.h"
#include "../Core/RealSquareMatrices.h"
#include "../Core/TensorProductSurfaces3.h"

// TODO:
//          enum Direction{U = 0, V = 1};
//          based on curves: order-elevation and subdivision in specified direction
namespace cagd
{
class BSurface3: public TensorProductSurface3
{
protected:

    //    enum Direction{U=0, V=1};

    // shape pa r ame t e r s i n d i r e c t i o n s u and v , r e s p e c t i v e l y
    GLdouble _alpha, _beta;

    // o r d e r s o f b a s i s f u n c t i o n s i n d i r e c t i o n s u and v , r e s p e c t i v e l y
    GLuint _n, _m;

    // n o rma l i z i n g c o e f f i c i e n t s i n d i r e c t i o n s u and v , r e s p e c t i v e l y
    RowMatrix<GLdouble> _u_c, _v_c ;

    // b i n omi a l c o e f f i c i e n t s
    TriangularMatrix<GLdouble> _bc ;

    // a u x i l i a r p r o t e c t e d methods
    GLvoid CalculateBinomialCoefficients(GLuint order, TriangularMatrix<GLdouble> &bc);
    GLboolean CalculateNormalizingCoefficients(GLuint order, GLdouble alpha, RowMatrix<GLdouble> &c) const;
    // GLboolean CalculateNormalizingCoefficients(GLuint order, GLdouble alpha, RowMatrix<GLdouble> &c);

public:
    // s p e c i a l c o n s t r u c t o r
    BSurface3(GLdouble alpha, GLuint n, GLdouble beta, GLuint m);
    BSurface3(BSurface3 const &bsurface);

    // i n h e r i t e d pur e v i r t u a l a b s t r a c t methods t h a t must be r e d e c l a r e d and d e f i n e d
    GLboolean UBlendingFunctionValues(GLdouble u, RowMatrix<GLdouble>& values) const;

    GLboolean VBlendingFunctionValues(GLdouble v, RowMatrix<GLdouble>& values) const;

    GLboolean UBlendingFunctionDerivatives(GLuint max_order_of_derivatives, RowMatrix<GLdouble>& Au, Matrix<GLdouble>& dAu) const;

    GLboolean VBlendingFunctionDerivatives(GLuint max_order_of_derivatives, RowMatrix<GLdouble>& Av, Matrix<GLdouble>& dAv) const;


    GLboolean CalculatePartialDerivatives(GLuint maximum_order_of_partialderivatives, GLdouble u, GLdouble v, PartialDerivatives& pd) const;



    GLdouble getAlpha();
    GLdouble getBeta();
    GLuint getN();
    GLuint getM();
    RowMatrix<GLdouble> getU_c();
    RowMatrix<GLdouble> getV_c();


    TriangularMatrix<GLdouble> getBc();

    BSurface3& operator=(BSurface3& bsurface);

    BSurface3* UIncreaseOrder(GLuint z = 1) const;
    BSurface3* VIncreaseOrder(GLuint z = 1) const;

    RowMatrix<BSurface3*>* USubdivision(GLdouble u) const;
    RowMatrix<BSurface3*>* VSubdivision(GLdouble u) const;




};
}
