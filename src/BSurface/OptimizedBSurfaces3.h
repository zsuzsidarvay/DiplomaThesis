#pragma once

#include "../Core/Matrices.h"
#include "../Core/RealSquareMatrices.h"
#include "../Core/TensorProductSurfaces3.h"
#include "BSurface3.h"
#include "../Core/RealMatrices.h"


#include <iostream>
#include <utility>

using namespace std;

// TODO:    look-up tables stored in class variables;
//          updating look-up tables whenever the orders or shape parameters are changed;
namespace cagd {

class OptimizedBSurfaces3: public BSurface3
{
protected:

    vector<RealSquareMatrix> int_dF_dF;
    vector<RealSquareMatrix> int_dG_dG;

    TriangularMatrix< vector< vector< vector< vector<GLdouble> > > > > varphi;

    vector< vector< vector< vector<GLdouble> > > > phi;

    ColumnMatrix<DCoordinate3> B;


public:
    OptimizedBSurfaces3(GLdouble _alpha, GLuint _n, GLdouble _beta, GLuint _m);
    OptimizedBSurfaces3(const BSurface3 &bsurface);

    GLboolean calculateIntdFdF(GLint u_div, GLint theta);
    GLboolean calculateIntdGdG(GLint v_div, GLint theta);
    GLboolean calculateVarPhi(GLint theta, GLint u_size, GLint v_size);
    GLboolean calculatePhi(const RowMatrix<GLdouble> &w, GLint theta, GLint u_size, GLint v_size);
    GLboolean calculateB(const RowMatrix<GLdouble> &w, GLint theta, GLint u_size, GLint v_size);

    GLboolean performOptimization(
            const RowMatrix<GLdouble> &w,
            const std::vector< pair<GLint, GLint> > &fixed_index_pairs,
            GLint u_div, GLint v_div);

    GLdouble calculateEnergy();

    GLvoid setBSurface(BSurface3* bsurface);

    GLvoid setData(const RowMatrix<GLdouble> &w, GLint u_div, GLint v_div);

};

}
