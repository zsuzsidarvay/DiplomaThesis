#pragma once

#ifndef BSURFACE3COMPOSITE_H
#define BSURFACE3COMPOSITE_H

#include "../Core/ShaderPrograms.h"
#include "../Core/Materials.h"
#include "BSurface3.h"
#include <iostream>

using namespace std;

namespace cagd
{
class BSurface3Composite
{

    //protected:
    //beta is kellene ...
    //    GLdouble _alpha;
public:
    class SurfaceAttributes
    {

    public:
        BSurface3* _bsurface;
        TriangulatedMesh3* _image;
        Material* _material;
        ShaderProgram* _shader;
        Color4* _color_of_control;

        std::vector< pair<GLint, GLint> > fixed_index_pairs;


        std::vector<SurfaceAttributes*> neighbours;


    };

protected:
    std::vector<SurfaceAttributes*> _attributes;

public:
    BSurface3Composite();
    //    BSurface3Composite(const BSurface3Composite& attribute);
    //    ~BSurface3Composite();
    //    BSurface3Composite& operator =(BSurface3Composite& rhs);

    GLboolean RenderData(GLenum render_mode) const;
    GLboolean RenderControlNet();
    GLboolean RenderControlPoints();
    GLboolean Render(Material* material, ShaderProgram* shader, GLboolean reflection = GL_FALSE);
    GLboolean RenderNormals();
    void renderUIsoparametric();
    void renderVIsoparametric();
    void renderUPartialDerivatives();
    void renderVPartialDerivatives();
    GLvoid renderIsoparametric();
    GLvoid generateImage(BSurface3::ImageColorScheme color_scheme);

    GLvoid addFixedIndex(GLint i, pair<GLint, GLint> p);
    GLvoid eraseFixedIndex(GLint i, pair<GLint, GLint> p);
    std::vector< pair<GLint, GLint> > getFixedIndex(GLint i);
    GLboolean findFixedIndex(GLint i, pair<GLint, GLint> p);


    void insertNewSurface(BSurface3* newSurface, Material* material, ShaderProgram* shader);
    void deleteSurface(int indexOfSurface);

    //    double getY(int indexOfPatch, int i, int j);
    //    double getZ(int indexOfPatch, int i, int j);
    //    GLdouble getAlpha();
    GLboolean SetMaterial(int indexOfPatch, Material *material);
    GLboolean SetShader(int indexOfPatch, ShaderProgram *shader);
    GLboolean SetControlColor(int indexOfPatch, Color4* color);
    GLint getSize();

    BSurface3* operator[](GLint index);

    GLvoid setSubdivision(GLint index, GLint direction, ShaderProgram* shader);
    GLvoid setIncreaseOrder(GLint index, GLint direction, ShaderProgram* shader);

    BSurface3* getBSurface(GLint index);
    GLvoid setBSurface(GLint index, BSurface3* bsurface);

    GLvoid generateImage(GLint index, BSurface3::ImageColorScheme color_scheme);
    GLvoid createImage(GLint index);

    GLvoid UmergeSurfacesRight(GLint indexOfFixedSurface, GLint indexOfMergeSurface, const Matrix<GLdouble>& dF_fixedSurface, const Matrix<GLdouble>& dG_fixedSurface, const Matrix<GLdouble>& dF_mergeSurface, GLint directionOfFixedSurface, GLint r);
    GLvoid UmergeSurfacesLeft(GLint indexOfFixedSurface, GLint indexOfMergeSurface, const Matrix<GLdouble>& dF_fixedSurface, const Matrix<GLdouble>& dG_fixedSurface, const Matrix<GLdouble>& dG_mergeSurface, GLint directionOfFixedSurface, GLint r);
    GLvoid UmergeSurfaces(GLint indexOfFixedSurface, GLint indexOfMergeSurface, GLint directionOfFixedSurface, GLint directionOfMergeSurface, GLint r);

    GLvoid VmergeSurfacesRight(GLint indexOfFixedSurface, GLint indexOfMergeSurface, const Matrix<GLdouble>& dF_fixedSurface, const Matrix<GLdouble>& dG_fixedSurface, const Matrix<GLdouble>& dF_mergeSurface, GLint directionOfFixedSurface, GLint r);
    GLvoid VmergeSurfacesLeft(GLint indexOfFixedSurface, GLint indexOfMergeSurface, const Matrix<GLdouble>& dF_fixedSurface, const Matrix<GLdouble>& dG_fixedSurface, const Matrix<GLdouble>& dG_mergeSurface, GLint directionOfFixedSurface, GLint r);
    GLvoid VmergeSurfaces(GLint indexOfFixedSurface, GLint indexOfMergeSurface, GLint directionOfFixedSurface, GLint directionOfMergeSurface, GLint r);



    //    void setX(double newValue, int indexOfPatch, int i, int j);
    //    void setY(double newValue, int indexOfPatch, int i, int j);
    //    void setZ(double newValue, int indexOfPatch, int i, int j);
    //    void setAlpha(double value);
    //    GLboolean GetMaterial(int indexOfPatch, Material *material);
    //    GLboolean GetShader(int indexOfPatch, ShaderProgram *shader);

    //    void setNeighbours(int index, int i, int j, int stopIndex, GLint depth, GLuint previousIndex);
    //    void continuePatch(int index, int direction);
    GLvoid joinSurfaces(GLint indexOfFirstSurface, GLint indexOfSecondSurface, GLint directionOfFirstSurface, GLint directionOfSecondSurface, GLint r1, GLint r2,  ShaderProgram* shader, Material* material);
    //    void mergePatches(int indexOfFirstPatch, int indexOfSecondPatch, int directionOfFirstdPatch, int directionOfSecondPatch);
};

}

#endif // BSURFACE3COMPOSITE_H
