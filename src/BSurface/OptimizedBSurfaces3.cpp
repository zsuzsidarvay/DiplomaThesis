#include "OptimizedBSurfaces3.h"
#include <map>
//#include <../Core/Math/RealMatrixDecompositions.h>

namespace cagd
{
OptimizedBSurfaces3::OptimizedBSurfaces3(GLdouble _alpha, GLuint _n, GLdouble _beta, GLuint _m): BSurface3(_alpha, _n, _beta, _m)
{

}

OptimizedBSurfaces3::OptimizedBSurfaces3(const BSurface3 &bsurface): BSurface3(bsurface){

}


GLboolean OptimizedBSurfaces3::calculateIntdFdF(GLint u_div, GLint theta){
    GLint u_size = _n*2+1;

    if (u_size <= 1)
    {
        return GL_FALSE;
    }

    if (u_div <= 0)
    {
        cout << "Error 3" << endl;
        return GL_FALSE;
    }

    if (u_div % 2)
    {
        u_div += 1;
    }

    GLdouble du = _alpha / u_div;

    vector< Matrix<GLdouble> > dF(u_div + 1, Matrix<GLdouble>(theta + 1, u_size));

    for(GLint i = 0; i <= u_div; i++){
        GLdouble u = i*du;

        RowMatrix<GLdouble> F;

        if(!UBlendingFunctionValues(u, F)) {
            return GL_FALSE;
        }

        //        dF[i].SetRow(0, F);

        if(!UBlendingFunctionDerivatives(theta, F, dF[i])) {
            return GL_FALSE;
        }

    }

    //    vector<RealSquareMatrix> int_dF_dF(theta + 1, RealSquareMatrix(u_size));
    int_dF_dF.resize(theta+1, RealSquareMatrix(u_size));
    GLint n = u_size-1;

    for (GLint z = 0; z <= theta; z++)
    {
#pragma omp parallel for
        for (GLint i_1_j_1 = 0; i_1_j_1 < (n + 1) * (n + 1); i_1_j_1++)
        {
            GLint i_1 = i_1_j_1 / (n + 1);
            GLint j_1 = i_1_j_1 % (n + 1);

            GLdouble integral = 0.0;

            GLdouble even = 0.0;
            for (GLint j = 0; j <= (u_div / 2) - 1; j++)
            {
                GLint index = 2 * j;
                even += dF[index](z, i_1) * dF[index](z, j_1);
            }
            even *= 2.0;

            GLdouble odd = 0.0;
            for (GLint j = 1; j <= u_div / 2; j++)
            {
                GLint index = 2 * j - 1;
                odd += dF[index](z, i_1) * dF[index](z, j_1);
            }
            odd *= 4.0;

            integral += even;
            integral += odd;

            integral += dF[0](z, i_1) * dF[0](z, j_1);
            integral += dF[u_div](z, i_1) * dF[u_div](z, j_1);

            integral *= du;
            integral /= 3.0;

            int_dF_dF[z](i_1, j_1) = integral;
            int_dF_dF[z](j_1, i_1) = integral;

        }
    }

    return GL_TRUE;


}

GLboolean OptimizedBSurfaces3::calculateIntdGdG(GLint v_div, GLint theta){
    GLint v_size = _m*2+1;

    if (v_size <= 1)
    {
        return GL_FALSE;
    }

    if (v_div <= 0)
    {
        cout << "Error 3" << endl;
        return GL_FALSE;
    }

    if (v_div % 2)
    {
        v_div += 1;
    }

    GLdouble dv = _beta / v_div;

    vector< Matrix<GLdouble> > dG(v_div + 1, Matrix<GLdouble>(theta + 1, v_size));

    for(GLint i = 0; i <= v_div; i++){
        GLdouble v = i*dv;

        RowMatrix<GLdouble> G;

        if(!VBlendingFunctionValues(v, G)) {
            return GL_FALSE;
        }

        if(!VBlendingFunctionDerivatives(theta, G, dG[i])) {
            return GL_FALSE;
        }


    }

    int_dG_dG.resize(theta + 1, RealSquareMatrix(v_size));
    GLint n = v_size -1;

    for (GLint z = 0; z <= theta; z++)
    {
#pragma omp parallel for
        for (GLint i_2_j_2 = 0; i_2_j_2 < (n + 1) * (n + 1); i_2_j_2++)
        {
            GLint i_2 = i_2_j_2 / (n + 1);
            GLint j_2 = i_2_j_2 % (n + 1);

            GLdouble integral = 0.0;

            GLdouble even = 0.0;
            for (GLint j = 0; j <= (v_div / 2) - 1; j++)
            {
                GLint index = 2 * j;
                even += dG[index](z, i_2) * dG[index](z, j_2);
            }
            even *= 2.0;

            GLdouble odd = 0.0;
            for (GLint j = 1; j <= v_div / 2; j++)
            {
                GLint index = 2 * j - 1;
                odd += dG[index](z, i_2) * dG[index](z, j_2);
            }
            odd *= 4.0;

            integral += even;
            integral += odd;

            integral += dG[0](z, i_2) * dG[0](z, j_2);
            integral += dG[v_div](z, i_2) * dG[v_div](z, j_2);

            integral *= dv;
            integral /= 3.0;

            int_dG_dG[z](i_2, j_2) = integral;
            int_dG_dG[z](j_2, i_2) = integral;

        }
    }


    return GL_TRUE;


}

GLboolean OptimizedBSurfaces3::calculateVarPhi(GLint theta, GLint u_size, GLint v_size){
    GLint n[2] = {u_size - 1, v_size - 1};

    varphi.ResizeRows(theta + 1);

    for (GLint r = 1; r <= theta; r++)
    {
        for (GLint z = 0; z <= r; z++)
        {
            varphi(r, z).resize(u_size);
            for (GLint i_1 = 0; i_1 <= n[0]; i_1++)
            {
                varphi(r, z)[i_1].resize(v_size);
                for (GLint i_2 = 0; i_2 <= n[1]; i_2++)
                {
                    varphi(r, z)[i_1][i_2].resize(u_size);
                    for (GLint j_1 = 0; j_1 <= n[0]; j_1++)
                    {
                        varphi(r, z)[i_1][i_2][j_1].resize(v_size);
                        for (GLint j_2 = 0; j_2 <= n[1]; j_2++)
                        {
                            varphi(r, z)[i_1][i_2][j_1][j_2] = int_dF_dF[z](i_1, j_1) * int_dG_dG[r - z](i_2, j_2);

                        }
                    }
                }
            }
        }
    }

    return GL_TRUE;
}

GLboolean OptimizedBSurfaces3::calculatePhi(const RowMatrix<GLdouble> &w, GLint theta, GLint u_size, GLint v_size){
    GLint n[2] = {u_size - 1, v_size - 1};

    TriangularMatrix<GLdouble> bc;
    CalculateBinomialCoefficients(theta, bc);

    phi.resize(u_size);
    for (GLint i_1 = 0; i_1 <= n[0]; i_1++)
    {
        phi[i_1].resize(v_size);
        for (GLint i_2 = 0; i_2 <= n[1]; i_2++)
        {
            phi[i_1][i_2].resize(u_size);
            for (GLint j_1 = 0; j_1 <= n[0]; j_1++)
            {
                phi[i_1][i_2][j_1].resize(v_size);
                for (GLint j_2 = 0; j_2 <= n[1]; j_2++)
                {
                    phi[i_1][i_2][j_1][j_2] = 0.0;
                    for (GLint r = 1; r <= theta; r++)
                    {
                        for (GLint z = 0; z <= r; z++)
                        {
                            phi[i_1][i_2][j_1][j_2] += w[r] * bc(r, z) * varphi(r, z)[i_1][i_2][j_1][j_2];
                        }
                    }
                    //                        cout << i_1 << " " << i_2 << " " << j_1 << " " << j_2 << " " << phi[i_1][i_2][j_1][j_2] << endl;
                }
            }
        }
    }
    return GL_TRUE;
}

GLboolean OptimizedBSurfaces3::calculateB(const RowMatrix<GLdouble> &w, GLint theta, GLint u_size, GLint v_size){
    GLint n[2] = {u_size - 1, v_size - 1};


    return GL_TRUE;
}


GLboolean OptimizedBSurfaces3::performOptimization(
        const RowMatrix<GLdouble> &w,
        const std::vector< pair<GLint, GLint> > &fixed_index_pairs,
        GLint u_div, GLint v_div)
{
    GLint theta = (GLint)w.GetColumnCount()- 1;

    if (theta < 1)
    {
        return GL_FALSE;
    }

    GLint u_size = _n*2+1;
    GLint v_size = _m*2+1;

    if (u_size <= 1 || v_size <= 1)
    {
        return GL_FALSE;
    }

    GLint n[2] = {u_size - 1, v_size - 1};

    for (GLint i = 0; i < (GLint)fixed_index_pairs.size(); i++)
    {
        if (fixed_index_pairs[i].first < 0 || fixed_index_pairs[i].first > n[0])
        {
            cout << "Error 1 " << fixed_index_pairs[i].first << " / " << n[0] << endl;
            return GL_FALSE;
        }

        if (fixed_index_pairs[i].second < 0 || fixed_index_pairs[i].second > n[1])
        {
            cout << "Error 2" << endl;
            return GL_FALSE;
        }
    }

    if (u_div <= 0 || v_div <= 0)
    {
        cout << "Error 3" << endl;
        return GL_FALSE;
    }

    if (u_div % 2)
    {
        u_div += 1;
    }

    if (v_div % 2)
    {
        v_div += 1;
    }


    //    calculateIntdFdF(u_div, theta);
    //    calculateIntdGdG(v_div, theta);

    //    calculateVarPhi(theta, u_size, v_size);

    //    calculatePhi(w, theta, u_size, v_size);


    vector< pair<GLint, GLint> > movable;
    for (GLint i_1 = 0; i_1 <= n[0]; i_1++)
    {
        for (GLint i_2 = 0; i_2 <= n[1]; i_2++)
        {
            pair<GLint, GLint> p(i_1, i_2);

            if (find(fixed_index_pairs.begin(), fixed_index_pairs.end(), p) == fixed_index_pairs.end())
            {
                //                    cout << p.first << " " << p.second << endl;
                movable.push_back(p);
            }
        }
    }

    //        cout << "movable size = " << movable.size() << endl;
    GLint size = u_size * v_size - (GLint)fixed_index_pairs.size();

    RealSquareMatrix M(size);


    map< GLint, pair<GLint, GLint> > mp;
    for (GLint i = 0; i < (GLint) movable.size(); i++)
    {
        mp[i] = movable[i];
    }

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            M(i, j) = phi[mp[i].first][mp[i].second][mp[j].first][mp[j].second];
        }
    }

    ColumnMatrix<DCoordinate3> B(size);

    for (int i = 0; i < size; i++)
    {
        GLint xi   = mp[i].first;
        GLint zeta = mp[i].second;

        for (GLint j = 0; j < (GLint)fixed_index_pairs.size(); j++)
        {
            GLint k_1 = fixed_index_pairs[j].first;
            GLint k_2 = fixed_index_pairs[j].second;
            B[i] += _data(k_1, k_2) * phi[k_1][k_2][xi][zeta];
        }
        B[i] *= -1.0;
    }


    ColumnMatrix<DCoordinate3> X;
    if (!M.SolveLinearSystem(B, X))
    {
        cout << "Error 4" << endl;
        //            cout << X << endl;

        return GL_FALSE;
    }
    //        cout << X << endl;

    for (int i = 0; i < size; i++)
    {
        GLint xi   = mp[i].first;
        GLint zeta = mp[i].second;

        _data(xi, zeta) = X[i];
    }

    return GL_TRUE;
}

GLdouble OptimizedBSurfaces3::calculateEnergy(){
    GLdouble result = 0.0;

    GLint u_size = 2*_n+1;
    GLint v_size = 2*_m+1;

    for(GLint k_1 = 0; k_1<u_size;k_1++){
        for(GLint k_2 = 0; k_2<v_size;k_2++){
            for(GLint l_1 = 0; l_1<u_size;l_1++){
                for(GLint l_2 = 0; l_2<v_size;l_2++){
                    result += _data(k_1,k_2)*_data(l_1,l_2) + phi[k_1][k_2][l_1][l_2];
//                    cout  << k_1 << k_2 << l_1 << l_2 << " *** " << result << endl;
                }
            }
        }
    }

    return result;
}

GLvoid OptimizedBSurfaces3::setData(const RowMatrix<GLdouble> &w,GLint u_div, GLint v_div)
{
    GLint theta = (GLint)w.GetColumnCount()- 1;

    if (theta < 1)
    {
        return;
    }

    GLint u_size = _n*2+1;
    GLint v_size = _m*2+1;


    calculateIntdFdF(u_div, theta);
    calculateIntdGdG(v_div, theta);

    calculateVarPhi(theta, u_size, v_size);

    calculatePhi(w, theta, u_size, v_size);
}


GLvoid OptimizedBSurfaces3::setBSurface(BSurface3* bsurface){
    _n = bsurface->getN();
    _m = bsurface->getM();
    _alpha = bsurface->getAlpha();
    _beta = bsurface->getBeta();
    _u_c = bsurface->getU_c();
    _v_c = bsurface->getV_c();
    _bc = bsurface->getBc();

    GLint sizeN = _n*2+1;
    GLint sizeM = _m*2+1;

    GLdouble scale = 1.0;
    for (GLint i = 0; i < sizeN; ++i) {
        for (GLint j = 0; j < sizeM; ++j) {


            SetData(i, j, (*bsurface)(i,j).x(), (*bsurface)(i,j).y(), (*bsurface)(i,j).z());

            //            pair<GLint, GLint> p(i, j);

            //            fixed_index_pairs.push_back(p);
            //            _bsurfaces->addFixedIndex(_selected_bsurface,p);
        }
    }

}


}
