#include "CurvatureComb.h"

using namespace cagd;
using namespace std;

CurvatureComb::CurvatureComb(){
    p.ResizeColumns(0);
    n0_curvature.ResizeColumns(0);
}


RowMatrix<DCoordinate3> CurvatureComb::getP(){
    return p;
}

RowMatrix<DCoordinate3> CurvatureComb::getN0_curvature(){
    return n0_curvature;
}

GLvoid CurvatureComb::setP( RowMatrix<DCoordinate3> temp){
    p.ResizeColumns(temp.GetColumnCount());

    for(GLuint i=0;i<temp.GetColumnCount();i++){
        p[i] = temp[i];
    }
}

GLvoid CurvatureComb::setN0_curvature( RowMatrix<DCoordinate3> temp){
    n0_curvature.ResizeColumns(temp.GetColumnCount());

    for(GLuint i=0;i<temp.GetColumnCount();i++){
        n0_curvature[i] = temp[i];
    }
}
