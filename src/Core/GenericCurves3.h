#pragma once

#include "Core/Colors4.h"
#include "DCoordinates3.h"
#include <GL/glew.h>
#include "Matrices.h"
#include "CurvatureComb.h"
#include <iostream>

namespace cagd
{
    //--------------------
    // class GenericCurve3
    //--------------------
    class GenericCurve3
    {
        friend class LinearCombination3;

        //----------------------------
        // input/output from/to stream
        //----------------------------
        friend std::ostream& operator <<(std::ostream& lhs, const GenericCurve3& rhs);
        friend std::istream& operator >>(std::istream& lhs, GenericCurve3& rhs);

    protected:
        GLenum               _usage_flag;
        RowMatrix<GLuint>    _vbo_derivative;
        Matrix<DCoordinate3> _derivative;

    public:
        // default and special constructor
        GenericCurve3(
                GLuint maximum_order_of_derivatives = 2,
                GLuint point_count = 0,
                GLenum usage_flag = GL_STATIC_DRAW);

        // special constructor
        GenericCurve3(const Matrix<DCoordinate3>& derivative, GLenum usage_flag = GL_STATIC_DRAW);

        // copy constructor
        GenericCurve3(const GenericCurve3& curve);

        // assignment operator
        GenericCurve3& operator =(const GenericCurve3& rhs);

        // vertex buffer object handling methods
        GLvoid DeleteVertexBufferObjects();
        GLboolean RenderDerivatives(GLuint order, GLenum render_mode) const;
        GLboolean UpdateVertexBufferObjects(GLdouble scale = 0.5, GLenum usage_flag = GL_STATIC_DRAW);

        GLfloat* MapDerivatives(GLuint order, GLenum access_mode = GL_READ_ONLY) const;
        GLboolean UnmapDerivatives(GLuint order) const;

        // get derivative by value
        DCoordinate3 operator ()(GLuint order, GLuint index) const;

        // get derivative by reference
        DCoordinate3& operator ()(GLuint order, GLuint index);

        // other update and query methods
        GLboolean SetDerivative(GLuint order, GLuint index, GLdouble x, GLdouble y, GLdouble z = 0.0);
        GLboolean SetDerivative(GLuint order, GLuint index, const DCoordinate3& d);
        GLboolean GetDerivative(GLuint order, GLuint index, GLdouble& x, GLdouble& y, GLdouble& z) const;
        GLboolean GetDerivative(GLuint order, GLuint index, DCoordinate3& d) const;

        GLuint GetMaximumOrderOfDerivatives() const;
        GLuint GetPointCount() const;
        GLenum GetUsageFlag() const;

        CurvatureComb GenerateCurvatureComb(GLdouble scale);

        //(*@\Green{// The next method can be used for generating Matlab code, by using which one can create scalable}@*)
        //(*@\Green{// vector graphic file formats like EPS.}@*)
        //(*@\Green{// Variable file\_name specifies the name of the output file. Make sure that its extension is ``.m".}@*)
        //(*@\Green{// Variable access\_mode can be either std::ios\_base::out, or std::ios\_base::out $|$ std::ios\_base::app, in case of}@*)
        //(*@\Green{// which the given file will be either overwritten, or appended.}@*)
        //(*@\Green{// Variable line\_color specifies the red, green and blue components of the color `Color' property of the}@*)
        //(*@\Green{// Matlab-commands plot and plot3.}@*)
        //(*@\Green{// Variable line\_style can be used to specify formatum strings for Matlab-like line styles, which are used by}@*)
        //(*@\Green{// the Matlab-commands plot and plot3. It accepts one of the formatum strings ``'-'", ``'-.'", ``':'" and ``'.'".}@*)
        //(*@\Green{// Variable line\_width specifies the positive line width which will be used by the Matlab-commands plot}@*)
        //(*@\Green{// and plot3.}@*)
        //(*@\Green{// Variables \{x$|$y$|$z\}\_coordinate\_name store the names of arrays that will contain the $\{x|y|z\}$-coordinates}@*)
        //(*@\Green{// of the curve points. These array names will be passed as input arguments to the plotting commands}@*)
        //(*@\Green{// of Matlab.}@*)
        GLboolean generateMatlabCodeForRendering(
            const std::string &file_name,
            std::ios_base::openmode access_mode = std::ios_base::out | std::ios_base::app,
            const Color4 &line_color = colors::blue,
            const std::string &line_style = "'-'",
            const GLdouble &line_width = 1.0,
            const std::string &x_coordinate_name = "x",
            const std::string &y_coordinate_name = "y",
            const std::string &z_coordinate_name = "z") const;



        // destructor
        virtual ~GenericCurve3();
    };
}
