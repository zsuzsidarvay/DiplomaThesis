#pragma once

#include "Matrices.h"
#include "DCoordinates3.h"

namespace cagd
{
    class CurvatureComb
    {
    protected:
        RowMatrix<DCoordinate3> p;
        RowMatrix<DCoordinate3> n0_curvature;
    public:
        RowMatrix<DCoordinate3> getP();
        RowMatrix<DCoordinate3> getN0_curvature();

        GLvoid setP( RowMatrix<DCoordinate3> temp);
        GLvoid setN0_curvature( RowMatrix<DCoordinate3> temp);

        CurvatureComb();
    };
}
