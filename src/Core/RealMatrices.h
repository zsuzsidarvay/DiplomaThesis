#ifndef REALMATRICES_H
#define REALMATRICES_H

#include "Matrices.h"

namespace cagd
{
    class RealMatrix: public Matrix<GLdouble>
    {
    public:
        RealMatrix(GLuint row_count = 1, GLuint column_count = 1);

        RealMatrix operator +(const RealMatrix &rhs) const;
        RealMatrix& operator +=(const RealMatrix &rhs);

        RealMatrix operator -(const RealMatrix &rhs) const;
        RealMatrix& operator -=(const RealMatrix &rhs);

        RealMatrix operator *(const GLdouble &rhs) const;
        RealMatrix& operator *=(const RealMatrix &rhs);

        RealMatrix operator /(const GLdouble &rhs) const;
        RealMatrix& operator /=(const RealMatrix &rhs);

        // other matrix-related arithmetic operators
    };
}

#endif // REALMATRICES_H
