
#ifndef UTILITIES_H
#define UTILITIES_H

#include "Colors4.h"

#include <sstream>

namespace cagd
{
    bool        platformIsSupported();
    Color4      coldToHotColormap(GLfloat value, GLfloat min_value, GLfloat max_value);


    template <typename T>
    std::string toString(const T &value)
    {
        std::ostringstream stream;
        stream << value;
        return stream.str();
    }
}

#endif
