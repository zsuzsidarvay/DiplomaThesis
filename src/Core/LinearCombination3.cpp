#include "../Core/LinearCombination3.h"
#include "RealSquareMatrices.h"
#include <fstream>

using namespace cagd;
using namespace std;

// special/default constructor
LinearCombination3::Derivatives::Derivatives(GLuint maximum_order_of_derivatives): ColumnMatrix<DCoordinate3>(maximum_order_of_derivatives + 1)
{
}

// copy constructor
LinearCombination3::Derivatives::Derivatives(const LinearCombination3::Derivatives& d): ColumnMatrix<DCoordinate3>(d)
{
}

// assignment operator
LinearCombination3::Derivatives& LinearCombination3::Derivatives::operator =(const LinearCombination3::Derivatives& rhs)
{
    if (this != &rhs)
    {
        ColumnMatrix<DCoordinate3>::operator =(rhs);
    }
    return *this;
}

// set every derivative to null vector
GLvoid LinearCombination3::Derivatives::LoadNullVectors()
{
    for (GLuint i = 0; i < _data.size(); ++i)
    {
        for (GLuint j = 0; j < 3; ++j)
            _data[i][0][j] = 0.0;
    }
}

// special constructor
LinearCombination3::LinearCombination3(GLdouble u_min, GLdouble u_max, GLuint data_count, GLenum data_usage_flag):
    _vbo_data(0),
    _data_usage_flag(data_usage_flag),
    _u_min(u_min), _u_max(u_max),
    _data(data_count)
{
}

// copy constructor
LinearCombination3::LinearCombination3(const LinearCombination3 &lc):
    _vbo_data(0),
    _data_usage_flag(lc._data_usage_flag),
    _u_min(lc._u_min), _u_max(lc._u_max),
    _data(lc._data)
{
    if (lc._vbo_data)
        UpdateVertexBufferObjectsOfData(_data_usage_flag);
}

// assignment operator
LinearCombination3& LinearCombination3::operator =(const LinearCombination3& rhs)
{
    if (this != &rhs)
    {
        DeleteVertexBufferObjectsOfData();

        _data_usage_flag = rhs._data_usage_flag;
        _u_min = rhs._u_min;
        _u_max = rhs._u_max;
        _data = rhs._data;

        if (rhs._vbo_data)
            UpdateVertexBufferObjectsOfData(_data_usage_flag);
    }

    return *this;
}

// vbo handling methods
GLvoid LinearCombination3::DeleteVertexBufferObjectsOfData()
{
    if (_vbo_data)
    {
        glDeleteBuffers(1, &_vbo_data);
        _vbo_data = 0;
    }
}

GLboolean LinearCombination3::RenderData(GLenum render_mode) const
{
    if (!_vbo_data)
        return GL_FALSE;

    if (render_mode != GL_LINE_STRIP && render_mode != GL_LINE_LOOP && render_mode != GL_POINTS)
        return GL_FALSE;

    glEnableClientState(GL_VERTEX_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo_data);
    glVertexPointer(3, GL_FLOAT, 0, (const GLvoid*)0);
    glDrawArrays(render_mode, 0, _data.GetRowCount());
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisableClientState(GL_VERTEX_ARRAY);

    return GL_TRUE;
}


GLboolean LinearCombination3::UpdateVertexBufferObjectsOfData(GLenum usage_flag, GLfloat scale)
{
    GLuint data_count = _data.GetRowCount();
    if (!data_count)
        return GL_FALSE;

    if (usage_flag != GL_STREAM_DRAW  && usage_flag != GL_STREAM_READ  && usage_flag != GL_STREAM_COPY
            && usage_flag != GL_DYNAMIC_DRAW && usage_flag != GL_DYNAMIC_READ && usage_flag != GL_DYNAMIC_COPY
            && usage_flag != GL_STATIC_DRAW  && usage_flag != GL_STATIC_READ  && usage_flag != GL_STATIC_COPY)
        return GL_FALSE;

    _data_usage_flag = usage_flag;

    DeleteVertexBufferObjectsOfData();

    glGenBuffers(1, &_vbo_data);
    if (!_vbo_data)
        return GL_FALSE;

    glBindBuffer(GL_ARRAY_BUFFER, _vbo_data);
    glBufferData(GL_ARRAY_BUFFER, data_count * 3 * sizeof(GLfloat), 0, _data_usage_flag);

    GLfloat *coordinate = (GLfloat*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    if (!coordinate)
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        DeleteVertexBufferObjectsOfData();
        return GL_FALSE;
    }

    for (GLuint i = 0; i < data_count; ++i)
    {
        for (GLuint j = 0; j < 3; ++j)
        {
            *coordinate = (GLfloat)_data[i][j] * scale;
            ++coordinate;
        }
    }

    if (!glUnmapBuffer(GL_ARRAY_BUFFER))
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        DeleteVertexBufferObjectsOfData();
        return GL_FALSE;
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return GL_TRUE;
}

// get data by value
DCoordinate3 LinearCombination3::operator [](GLuint index) const
{
    return _data[index];
}

// get data by reference
DCoordinate3& LinearCombination3::operator [](GLuint index)
{
    return _data[index];
}



// assure interpolation
GLboolean LinearCombination3::UpdateDataForInterpolation(const ColumnMatrix<GLdouble>& knot_vector, const ColumnMatrix<DCoordinate3>& data_points_to_interpolate)
{
    GLuint data_count = _data.GetRowCount();

    if (data_count != knot_vector.GetRowCount() ||
            data_count != data_points_to_interpolate.GetRowCount())
        return GL_FALSE;

    RealSquareMatrix collocation_matrix(data_count);

    RowMatrix<GLdouble> current_blending_function_values(data_count);
    for (GLuint r = 0; r < knot_vector.GetRowCount(); ++r)
    {
        if (!BlendingFunctionValues(knot_vector(r), current_blending_function_values))
            return GL_FALSE;
        else
            collocation_matrix.SetRow(r, current_blending_function_values);
    }

    return collocation_matrix.SolveLinearSystem(data_points_to_interpolate, _data);
}


// set/get definition domain
GLvoid LinearCombination3::SetDefinitionDomain(GLdouble u_min, GLdouble u_max)
{
    // homework
    this->_u_min = u_min;
    this->_u_max = u_max;
}

GLvoid LinearCombination3::GetDefinitionDomain(GLdouble& u_min, GLdouble& u_max) const
{
    // homework
    u_min = this->_u_min;
    u_max = this->_u_max;
}

// generate image/arc
GenericCurve3* LinearCombination3::GenerateImage(GLuint max_order_of_derivatives, GLuint div_point_count, GLenum usage_flag) const {
    if (div_point_count < 2) {
        return 0;
    }

    GenericCurve3* result = 0;
    result = new GenericCurve3(max_order_of_derivatives, div_point_count, usage_flag);

    if (!result) {
        return 0;
    }

    GLdouble step = (_u_max - _u_min) / (div_point_count - 1);
    Derivatives d;

    // #pragma omp for
    for (GLuint i = 0; i < div_point_count; ++i) {
        GLdouble u = _u_min + i * step;
        if (!CalculateDerivatives(max_order_of_derivatives, u, d)) {
            delete result;
            return 0;
        }
        result->_derivative.SetColumn(i, d);
    }

    return result;
}

ColumnMatrix<DCoordinate3>& LinearCombination3::getData()
{
    return _data;
}

GLenum LinearCombination3::getDataUsageFlag(){
    return _data_usage_flag;
}


// destructor
LinearCombination3::~LinearCombination3()
{
    DeleteVertexBufferObjectsOfData();
}

GLboolean LinearCombination3::generateMatlabCodeForRendering(const string &file_name,
    ios_base::openmode access_mode,
    const Color4 &line_color,
    const std::string &line_style,
    const GLdouble &line_width,
    const string &x_coordinate_name,
    const string &y_coordinate_name,
    const string &z_coordinate_name) const
{
    if (file_name.empty() || x_coordinate_name.empty() ||
        y_coordinate_name.empty() || z_coordinate_name.empty() ||
        (x_coordinate_name == y_coordinate_name) ||
        (x_coordinate_name == z_coordinate_name) ||
        (y_coordinate_name == z_coordinate_name) ||
        line_width <= 0.0 || _data.GetColumnCount() == 0)
    {
        return GL_FALSE;
    }

    if (line_style != "'-'" && line_style != "'-.'" && line_style != "':'" && line_style != "'.'")
    {
        return GL_FALSE;
    }

    if ((access_mode != (fstream::out | fstream::app)) &&
        (access_mode != fstream::out))
    {
        return GL_FALSE;
    }

    fstream f(file_name.c_str(), access_mode);

    if (!f || !f.good())
    {
        f.close();
        return GL_FALSE;
    }

    f << endl << "hold all;" << endl << endl;

    f << x_coordinate_name << " = [..." << endl;

    for (GLint i = 0; i < _data.GetRowCount(); i++)
    {
        f << _data[i].x() << " ";
    }
    f << "..." << endl << "];" << endl;

    f << endl;

    f << y_coordinate_name << " = [..." << endl;

    for (GLint i = 0; i < _data.GetRowCount(); i++)
    {
        f << _data[i].y() << " ";
    }
    f << "..." << endl << "];" << endl;

    f << endl;

    f << z_coordinate_name << " = [..." << endl;

    for (GLint i = 0; i < _data.GetRowCount(); i++)
    {
        f << _data[i].z() << " ";
    }
    f << "..." << endl << "];" << endl;

    f << endl << "plot3(" << x_coordinate_name << ", "
                            << y_coordinate_name << ", "
                            << z_coordinate_name << ", "
                            << line_style << ", "
                            << "'Color', ["
                            << line_color[0] << ", "
                            << line_color[1] << ", "
                            << line_color[2] << "], "
                            << "'LineWidth', " << line_width
                            << ");" << endl;


    f << endl << "axis equal;" << endl;

    return GL_TRUE;
}


GLdouble LinearCombination3::calculateEnergy(AbstractCurveEnergyFragment *ptr, GLuint div_point_count) const{
    return (*ptr)(div_point_count);
}



