#pragma once

#include <GL/glew.h>

namespace cagd
{
    class Color4
    {
    protected:
        GLfloat _data[4]; // (r, g, b, a)

    public:
        Color4();

        Color4(GLfloat r, GLfloat g, GLfloat b, GLfloat a = 1.0f);

        //get components by value
        GLfloat operator [](GLuint rhs) const;
        GLfloat r() const;
        GLfloat g() const;
        GLfloat b() const;
        GLfloat a() const;

        //get components by reference
        GLfloat& operator [](GLuint rhs);
        GLfloat& r();
        GLfloat& g();
        GLfloat& b();
        GLfloat& a();
    };

    inline Color4::Color4()
    {
        _data[0] = _data[1] = _data[2] = 0.0;
        _data[3] = 1.0;
    }

    inline Color4::Color4(GLfloat r, GLfloat g, GLfloat b, GLfloat a)
    {
        _data[0] = r;
        _data[1] = g;
        _data[2] = b;
        _data[3] = a;
    }

    //get components by value
    inline GLfloat Color4::operator [](GLuint rhs) const
    {
        return _data[rhs];
    }

    inline GLfloat Color4::r() const
    {
        return _data[0];
    }

    inline GLfloat Color4::g() const
    {
        return _data[1];
    }

    inline GLfloat Color4::b() const
    {
        return _data[2];
    }

    inline GLfloat Color4::a() const
    {
        return _data[3];
    }

    //get components by reference
    inline GLfloat& Color4::operator [](GLuint rhs)
    {
        return _data[rhs];
    }

    inline GLfloat& Color4::r()
    {
        return _data[0];
    }
    inline GLfloat& Color4::g()
    {
        return _data[1];
    }

    inline GLfloat& Color4::b()
    {
        return _data[2];
    }

    inline GLfloat& Color4::a()
    {
       return  _data[3];
    }

    namespace colors
    {
        const Color4        black(0.000000f, 0.000000f, 0.000000f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.000000, 0.000000, 0.000000}\rule{0.2cm}{0.2cm}}}@*)
        const Color4         gray(0.375000f, 0.375000f, 0.375000f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.375000, 0.375000, 0.375000}\rule{0.2cm}{0.2cm}}}@*)
        const Color4   light_gray(0.500000f, 0.500000f, 0.500000f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.500000, 0.500000, 0.500000}\rule{0.2cm}{0.2cm}}}@*)
        const Color4       silver(0.708232f, 0.708232f, 0.708232f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.708232, 0.708232, 0.708232}\rule{0.2cm}{0.2cm}}}@*)
        const Color4        white(1.000000f, 1.000000f, 1.000000f, 0.500000f);   //(*@\Green{// {\color[rgb]{1.000000, 1.000000, 1.000000}\rule{0.2cm}{0.2cm}}}@*)
        const Color4     dark_red(0.474937f, 0.080613f, 0.063188f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.474937, 0.080613, 0.063188}\rule{0.2cm}{0.2cm}}}@*)
        const Color4          red(0.851563f, 0.144531f, 0.113281f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.851563, 0.144531, 0.113281}\rule{0.2cm}{0.2cm}}}@*)
        const Color4    light_red(1.000000f, 0.276371f, 0.244388f, 0.500000f);   //(*@\Green{// {\color[rgb]{1.000000, 0.276371, 0.244388}\rule{0.2cm}{0.2cm}}}@*)
        const Color4        green(0.000000f, 0.570313f, 0.246094f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.000000, 0.570313, 0.246094}\rule{0.2cm}{0.2cm}}}@*)
        const Color4  light_green(0.659983f, 0.750576f, 0.457252f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.659983, 0.750576, 0.457252}\rule{0.2cm}{0.2cm}}}@*)
        const Color4    dark_blue(0.156863f, 0.086275f, 0.435294f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.156863, 0.086275, 0.435294}\rule{0.2cm}{0.2cm}}}@*)
        const Color4         blue(0.156863f, 0.086275f, 0.652941f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.156863, 0.086275, 0.652941}\rule{0.2cm}{0.2cm}}}@*)
        const Color4    baby_blue(0.400000f, 0.478431f, 0.701961f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.400000, 0.478431, 0.701961}\rule{0.2cm}{0.2cm}}}@*)
        const Color4         cyan(0.000000f, 0.576471f, 0.866667f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.000000, 0.576471, 0.866667}\rule{0.2cm}{0.2cm}}}@*)
        const Color4   light_blue(0.456214f, 0.728115f, 1.000000f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.456214, 0.728115, 1.000000}\rule{0.2cm}{0.2cm}}}@*)
        const Color4     ice_blue(0.458824f, 0.772549f, 0.941176f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.458824, 0.772549, 0.941176}\rule{0.2cm}{0.2cm}}}@*)
        const Color4  dark_purple(0.592157f, 0.270588f, 0.470588f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.592157, 0.270588, 0.470588}\rule{0.2cm}{0.2cm}}}@*)
        const Color4       purple(0.710582f, 0.324697f, 0.564721f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.710582, 0.324697, 0.564721}\rule{0.2cm}{0.2cm}}}@*)
        const Color4 light_purple(0.850000f, 0.700000f, 1.000000f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.850000, 0.700000, 1.000000}\rule{0.2cm}{0.2cm}}}@*)
        const Color4  dark_orange(0.902344f, 0.468750f, 0.089844f, 0.500000f);   //(*@\Green{// {\color[rgb]{0.902344, 0.468750, 0.089844}\rule{0.2cm}{0.2cm}}}@*)
        const Color4       orange(1.000000f, 0.705730f, 0.448600f, 0.500000f);   //(*@\Green{// {\color[rgb]{1.000000, 0.705730, 0.448600}\rule{0.2cm}{0.2cm}}}@*)
    } //(*@\label{src:Color4:colors:end}@*)
}
