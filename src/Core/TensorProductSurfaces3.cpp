#include "TensorProductSurfaces3.h"
#include "RealSquareMatrices.h"
#include "Utilities.h"
#include <algorithm>
#include <limits>

using namespace cagd;
using namespace std;

GLvoid TensorProductSurface3::PartialDerivatives::LoadNullVectors() {
    for (GLuint i = 0; i < _row_count; i++) {
        for (GLuint j = 0; j <= i; j++) {
            for (GLuint k = 0; k < 3; k++) {
                _data[i][j][k] = 0.0;
            }
        }
    }
}

TensorProductSurface3::TensorProductSurface3(
        GLdouble u_min, GLdouble u_max,
        GLdouble v_min, GLdouble v_max,
        GLuint row_count, GLuint column_count,
        GLboolean u_closed, GLboolean v_closed):
    _u_closed(u_closed), _v_closed(v_closed),
    _u_min(u_min), _u_max(u_max),
    _v_min(v_min), _v_max(v_max),
    _data(row_count, column_count)
{}

TensorProductSurface3::TensorProductSurface3(const TensorProductSurface3& surface):
    _u_closed(surface._u_closed), _v_closed(surface._v_closed),
    _vbo_data(surface._vbo_data),
    _u_min(surface._u_min), _u_max(surface._u_max),
    _v_min(surface._v_min), _v_max(surface._v_max),
    _data(surface._data)
{
    if (surface._vbo_data) {
        UpdateVertexBufferObjectsOfData();
    }
}

TensorProductSurface3& TensorProductSurface3::operator =(const TensorProductSurface3& surface) {
    if (this != &surface) {
        _u_closed = surface._u_closed;
        _v_closed = surface._v_closed;
        _vbo_data = surface._vbo_data;
        _u_min    = surface._u_min;
        _u_max    = surface._u_max;
        _v_min    = surface._v_min;
        _v_max    = surface._v_max;
        _data     = surface._data;

        if (surface._vbo_data) {
            UpdateVertexBufferObjectsOfData();
        }
    }

    return *this;
}

GLvoid TensorProductSurface3::SetUInterval(GLdouble u_min, GLdouble u_max) {
    _u_min = u_min;
    _u_max = u_max;
}
GLvoid TensorProductSurface3::SetVInterval(GLdouble v_min, GLdouble v_max) {
    _v_min = v_min;
    _v_max = v_max;
}

GLvoid TensorProductSurface3::GetUInterval(GLdouble& u_min, GLdouble& u_max) const {
    u_min = _u_min;
    u_max = _u_max;
}
GLvoid TensorProductSurface3::GetVInterval(GLdouble& v_min, GLdouble& v_max) const {
    v_min = _v_min;
    v_max = _v_max;
}

GLboolean TensorProductSurface3::SetData(GLuint row, GLuint column, GLdouble x, GLdouble y, GLdouble z) {
    if (row >= _data.GetRowCount() || column >= _data.GetColumnCount()) {
        return GL_FALSE;
    }

    _data(row, column)[0] = x;
    _data(row, column)[1] = y;
    _data(row, column)[2] = z;

    return GL_TRUE;
}

GLboolean TensorProductSurface3::SetData(GLuint row, GLuint column, const DCoordinate3& point) {
    if (row >= _data.GetRowCount() || column >= _data.GetColumnCount()) {
        return GL_FALSE;
    }

    _data(row, column) = point;

    return GL_TRUE;
}

GLboolean TensorProductSurface3::GetData(GLuint row, GLuint column, GLdouble& x, GLdouble& y, GLdouble& z) const {
    if (row >= _data.GetRowCount() || column >= _data.GetColumnCount()) {
        return GL_FALSE;
    }

    x = _data(row, column)[0];
    y = _data(row, column)[1];
    z = _data(row, column)[2];

    return GL_TRUE;
}

GLboolean TensorProductSurface3::GetData(GLuint row, GLuint column, DCoordinate3& point) const {
    if (row >= _data.GetRowCount() || column >= _data.GetColumnCount()) {
        return GL_FALSE;
    }

    point = _data(row, column);

    return GL_TRUE;
}

DCoordinate3 TensorProductSurface3::operator ()(GLuint row, GLuint column) const {
    return _data(row, column);
}

DCoordinate3& TensorProductSurface3::operator ()(GLuint row, GLuint column) {
    return _data(row, column);
}

// special constructor
TensorProductSurface3::PartialDerivatives::PartialDerivatives(GLuint maximum_order_of_partial_derivatives):
    TriangularMatrix<DCoordinate3>(maximum_order_of_partial_derivatives + 1)
{
}

// generates the image (i.e., the approximating triangulated mesh) of the tensor product surface
TriangulatedMesh3* TensorProductSurface3::GenerateImage(GLuint u_div_point_count, GLuint v_div_point_count, GLenum usage_flag, ImageColorScheme color_scheme) const
{
    if (u_div_point_count <= 1 || v_div_point_count <= 1)
        return GL_FALSE;

    // calculating number of vertices, unit normal vectors and texture coordinates
    GLuint vertex_count = u_div_point_count * v_div_point_count;

    // calculating number of triangular faces
    GLuint face_count = 2 * (u_div_point_count - 1) * (v_div_point_count - 1);

    TriangulatedMesh3 *result = nullptr;
    result = new TriangulatedMesh3(vertex_count, face_count, usage_flag);

    if (!result)
        return nullptr;

    // uniform subdivision grid in the definition domain
    GLdouble du = (_u_max - _u_min) / (u_div_point_count - 1);
    GLdouble dv = (_v_max - _v_min) / (v_div_point_count - 1);

    // uniform subdivision grid in the unit square
    GLfloat sdu = 1.0f / (u_div_point_count - 1);
    GLfloat tdv = 1.0f / (v_div_point_count - 1);

    // for face indexing
    GLuint current_face = 0;

    // partial derivatives of order 0, 1, 2, and 3
    PartialDerivatives pd;

    std::vector<GLdouble> color_fragments(vertex_count);

    for (GLuint i = 0; i < u_div_point_count; ++i)
    {
        GLdouble u = min(_u_min + i * du, _u_max);
        GLfloat  s = min(i * sdu, 1.0f);
        for (GLuint j = 0; j < v_div_point_count; ++j)
        {
            GLdouble v = min(_v_min + j * dv, _v_max);
            GLfloat  t = min(j * tdv, 1.0f);

            /*
                3-2
                |/|
                0-1
            */
            GLuint index[4];

            index[0] = i * v_div_point_count + j;
            index[1] = index[0] + 1;
            index[2] = index[1] + v_div_point_count;
            index[3] = index[2] - 1;



            // calculating all needed surface data
            CalculatePartialDerivatives(2, u, v, pd);

            // surface point
            (*result)._vertex[index[0]] = pd(0, 0);

            // unit surface normal
            (*result)._normal[index[0]] = pd(1, 0);
            (*result)._normal[index[0]] ^= pd(1, 1);

            GLdouble norm_length = (*result)._normal[index[0]].length();

            (*result)._normal[index[0]] /= norm_length; //.normalize();


            // texture coordinates
            (*result)._tex[index[0]].s() = s;
            (*result)._tex[index[0]].t() = t;

            // faces
            if (i < u_div_point_count - 1 && j < v_div_point_count - 1)
            {
                (*result)._face[current_face][0] = index[0];
                (*result)._face[current_face][1] = index[1];
                (*result)._face[current_face][2] = index[2];
                ++current_face;

                (*result)._face[current_face][0] = index[0];
                (*result)._face[current_face][1] = index[2];
                (*result)._face[current_face][2] = index[3];
                ++current_face;
            }

            GLdouble e1 = 0.0, f1 = 0.0, g1 = 0.0;
            GLdouble e2 = 0.0, f2 = 0.0, g2 = 0.0;
            GLdouble K = 0.0, H = 0.0;

            e1 = pd (1 , 0) * pd (1 , 0 ) ;
            f1 = pd (1 , 0) * pd (1 , 1 ) ;
            g1 = pd (1 , 1) * pd (1 , 1 ) ;

            e2 = (*result)._normal[index[0]] * pd (2 , 0 ) ;
            f2 = (*result)._normal[index[0]] * pd (2 , 1 ) ;
            g2 = (*result)._normal[index[0]] * pd (2 , 2 ) ;

            K = ( e2 * g2 - f2 * f2) / ( e1 * g1 - f1 * f1);
            H = ( g1 * e2 - 2.0 * f1 * f2 + e1 * g2) / ( e1 * g1 - f1 * f1);


            switch(color_scheme) {
            case DEFAULT_NULL_FRAGMENT:
            case X_VARIATION_FRAGMENT:
            case Y_VARIATION_FRAGMENT:
            case Z_VARIATION_FRAGMENT:
                break;
            case NORMAL_LENGTH_FRAGMENT:
                color_fragments[index[0]] = norm_length;
                break;
            case GAUSSIAN_CURVATURE_FRAGMENT:
                color_fragments[index[0]] = K * norm_length;
                break;
            case MEAN_CURVATURE_FRAGMENT:
                color_fragments[index[0]] = H * norm_length;
                break;
            case  WILLMORE_ENERGY_FRAGMENT:
                color_fragments[index[0]] = H * H * norm_length;
                break;
            case LOG_WILLMORE_ENERGY_FRAGMENT:
                break;
            case UMBILIC_DEVIATION_ENERGY_FRAGMENT:
                color_fragments[index[0]] = 4 * (H * H - K) * norm_length;
                break;
            case LOG_UMBILIC_DEVIATION_ENERGY_FRAGMENT:
                color_fragments[index[0]] = log(1.0 + 4 * (H * H - K)) * norm_length;
                break;
            case TOTAL_CURVATURE_ENERGY_FRAGMENT:
                color_fragments[index[0]] = (1.5 * H * H - 0.5 * K) * norm_length;
                break;
            case LOG_TOTAL_CURVATURE_ENERGY_FRAGMENT:
                color_fragments[index[0]] = log(1.0 + (1.5 * H * H - 0.5 * K)) * norm_length;
                break;
            default:
                break;
            }

        }
    }

    if (color_scheme > DEFAULT_NULL_FRAGMENT)
    {
        GLdouble min = numeric_limits<GLdouble>::max(), max = -numeric_limits<GLdouble>::max();

        for(GLuint i=0; i < vertex_count; i++){
            if(min > color_fragments[i])
                min = color_fragments[i];
            if(max < color_fragments[i])
                max = color_fragments[i];
        }

        cout << min << " " << max << endl;
        for(GLuint i = 0; i<vertex_count; i++) {
            (*result)._color[i] = coldToHotColormap(static_cast<GLfloat>(color_fragments[i]), static_cast<GLfloat>(min), static_cast<GLfloat>(max));
            //        cout << (*result)._color[i] << endl;
        }
    }


    return result;
}

// ensures interpolation, i.e. s(u_i, v_j) = d_{i,j}
GLboolean TensorProductSurface3::UpdateDataForInterpolation(const RowMatrix<GLdouble>& u_knot_vector, const ColumnMatrix<GLdouble>& v_knot_vector, Matrix<DCoordinate3>& data_points_to_interpolate)
{
    GLuint row_count = _data.GetRowCount();
    if (!row_count)
        return GL_FALSE;

    GLuint column_count = _data.GetColumnCount();
    if (!column_count)
        return GL_FALSE;

    if (u_knot_vector.GetColumnCount() != row_count || v_knot_vector.GetRowCount() != column_count || data_points_to_interpolate.GetRowCount() != row_count || data_points_to_interpolate.GetColumnCount() != column_count)
        return GL_FALSE;

    // 1: calculate the u-collocation matrix and perfom LU-decomposition on it
    RowMatrix<GLdouble> u_blending_values;

    RealSquareMatrix u_collocation_matrix(row_count);

    for (GLuint i = 0; i < row_count; ++i)
    {
        if (!UBlendingFunctionValues(u_knot_vector(i), u_blending_values))
            return GL_FALSE;
        u_collocation_matrix.SetRow(i, u_blending_values);
    }

    if (!u_collocation_matrix.PerformLUDecomposition())
        return GL_FALSE;

    // 2: calculate the v-collocation matrix and perform LU-decomposition on it
    RowMatrix<GLdouble> v_blending_values;

    RealSquareMatrix v_collocation_matrix(column_count);

    for (GLuint j = 0; j < column_count; ++j)
    {
        if (!VBlendingFunctionValues(v_knot_vector(j), v_blending_values))
            return GL_FALSE;
        v_collocation_matrix.SetRow(j, v_blending_values);
    }

    if (!v_collocation_matrix.PerformLUDecomposition())
        return GL_FALSE;

    // 3:   for all fixed j in {0, 1,..., column_count} determine control points
    //
    //      a_k(v_j) = sum_{l=0}^{column_count} _data(l, j) G_l(v_j), k = 0, 1,..., row_count
    //
    //      such that
    //
    //      sum_{k=0}^{row_count} a_k(v_j) F_k(u_i) = data_points_to_interpolate(i, j),
    //
    //      for all i = 0, 1,..., row_count.
    Matrix<DCoordinate3> a(row_count, column_count);
    if (!u_collocation_matrix.SolveLinearSystem(data_points_to_interpolate, a))
        return GL_FALSE;

    // 4:   for all fixed i in {0, 1,..., row_count} determine control point
    //
    //      _data[i][j], j = 0, 1,..., column_count
    //
    //      such that
    //
    //      sum_{l=0}^{column_count} _data(i, l) G_l(v_j) = a_i(v_j)
    //
    //      for all j = 0, 1,..., column_count.
    if (!v_collocation_matrix.SolveLinearSystem(a, _data, GL_FALSE))
        return GL_FALSE;

    return GL_TRUE;
}

GLvoid TensorProductSurface3::DeleteVertexBufferObjectsOfData() {
    if (_vbo_data) {
        glDeleteBuffers(1, &_vbo_data);
        _vbo_data = 0;
    }
}

GLboolean TensorProductSurface3::RenderData(GLenum render_mode) const {
    if (!_vbo_data) {
        return GL_FALSE;
    }

    if (render_mode != GL_LINE_STRIP && render_mode != GL_LINE_LOOP && render_mode != GL_POINTS) {
        return GL_FALSE;
    }

    glEnableClientState(GL_VERTEX_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo_data);

    glVertexPointer(3, GL_FLOAT, 0, (const GLvoid *)0);

    GLuint offset = 0;
    GLuint row_count = _data.GetRowCount();
    GLuint column_count = _data.GetColumnCount();
    for (GLuint i = 0; i < row_count; ++i, offset += column_count) {
        glDrawArrays(render_mode, offset, column_count);
    }

    for (GLuint i = 0; i < column_count; ++i, offset += row_count) {
        glDrawArrays(render_mode, offset, row_count);
    }


    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisableClientState(GL_VERTEX_ARRAY);

    return GL_TRUE;
}

GLboolean TensorProductSurface3::UpdateVertexBufferObjectsOfData(GLenum usage_flag) {

    if (usage_flag != GL_STREAM_DRAW  && usage_flag != GL_STREAM_READ  && usage_flag != GL_STREAM_COPY
            && usage_flag != GL_STATIC_DRAW  && usage_flag != GL_STATIC_READ  && usage_flag != GL_STATIC_COPY
            && usage_flag != GL_DYNAMIC_DRAW && usage_flag != GL_DYNAMIC_READ && usage_flag != GL_DYNAMIC_COPY)
        return GL_FALSE;

    DeleteVertexBufferObjectsOfData();

    glGenBuffers(1, &_vbo_data);
    if (!_vbo_data) {
        return GL_FALSE;
    }

    GLuint data_size = 2 * 3 * (GLuint)_data.GetRowCount() * _data.GetColumnCount() * sizeof(GLfloat);

    glBindBuffer(GL_ARRAY_BUFFER, _vbo_data);
    glBufferData(GL_ARRAY_BUFFER, data_size, 0, usage_flag);

    GLfloat* vertex_coordinate = (GLfloat*) glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

    if (!vertex_coordinate) {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        DeleteVertexBufferObjectsOfData();
        return GL_FALSE;
    }

    for (GLuint i = 0; i < _data.GetRowCount(); ++i) {
        for (GLuint j = 0; j < _data.GetColumnCount(); ++j) {
            for (GLuint coordinate = 0; coordinate < 3; ++coordinate, ++vertex_coordinate) {
                *vertex_coordinate = (GLfloat) _data(i, j)[coordinate];
            }
        }
    }

    for (GLuint i = 0; i < _data.GetColumnCount(); ++i) {
        for (GLuint j = 0; j < _data.GetRowCount(); ++j) {
            for (GLuint coordinate = 0; coordinate < 3; ++coordinate, ++vertex_coordinate) {
                *vertex_coordinate = (GLfloat) _data(j, i)[coordinate];
            }
        }
    }

    if (!glUnmapBuffer(GL_ARRAY_BUFFER)) {
        DeleteVertexBufferObjectsOfData();
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return GL_FALSE;
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return GL_TRUE;
}

RowMatrix<GenericCurve3*>* TensorProductSurface3::GenerateUIsoparametricLines(GLuint iso_line_count,
                                                                              GLuint maximum_order_of_derivatives,
                                                                              GLuint div_point_count,
                                                                              GLenum usage_flag) const
{
    RowMatrix<GenericCurve3*>* curves = new RowMatrix<GenericCurve3*>(iso_line_count);

    GLdouble u_step = (_u_max - _u_min) / (div_point_count - 1);
    GLdouble v_step = (_v_max - _v_min) / (iso_line_count  - 1);
    GLdouble v = _v_min;

    for (GLuint i = 0; i < iso_line_count; ++i) {
        (*curves)[i] = new GenericCurve3(maximum_order_of_derivatives, div_point_count, usage_flag);

        if (!(*curves)[i]) {
            for (GLuint j = 0; j <= i; ++j) {
                delete (*curves)[j];
            }
            delete curves;

            return nullptr;
        }
        GLdouble u = _u_min;
        for (GLuint j = 0; j < div_point_count; ++j) {
            PartialDerivatives pd;
            if (!CalculatePartialDerivatives(maximum_order_of_derivatives, u, v, pd)) {
                for (GLuint k = 0; k <= i; ++k) {
                    delete (*curves)[k];
                    (*curves)[k] = nullptr;
                }
                delete curves;
                return nullptr;
            }
            for (GLuint k = 0; k <= maximum_order_of_derivatives; ++k) {
                // 0 indicates the pd with respect to the first(u) parameter
                //                cout << k << "-th order " << pd(k, 0) << endl;
                (*curves)[i]->SetDerivative(k, j, pd(k, 0));
            }
            u += u_step;
            if(u > _u_max)
                u= _u_max;
        }
        v += v_step;
        if(v > _v_max)
            v = _v_max;
    }

    return curves;
}

RowMatrix<GenericCurve3*>* TensorProductSurface3::GenerateVIsoparametricLines(GLuint iso_line_count,
                                                                              GLuint maximum_order_of_derivatives,
                                                                              GLuint div_point_count,
                                                                              GLenum usage_flag) const
{
    RowMatrix<GenericCurve3*>* curves = new RowMatrix<GenericCurve3*>(iso_line_count);

    GLdouble u_step = (_u_max - _u_min) / (iso_line_count  - 1);
    GLdouble v_step = (_v_max - _v_min) / (div_point_count - 1);
    GLdouble u = _u_min;

    for (GLuint i = 0; i < iso_line_count; ++i) {
        (*curves)[i] = new GenericCurve3(maximum_order_of_derivatives, div_point_count, usage_flag);

        if (!(*curves)[i]) {
            for (GLuint j = 0; j <= i; ++j) {
                delete (*curves)[j];
            }
            delete curves;

            return nullptr;
        }
        GLdouble v = _v_min;
        for (GLuint j = 0; j < div_point_count; ++j) {
            PartialDerivatives pd;
            if (!CalculatePartialDerivatives(maximum_order_of_derivatives, u, v, pd)) {
                for (GLuint k = 0; k <= i; ++k) {
                    delete (*curves)[k];
                    (*curves)[k] = nullptr;
                }
                delete curves;

                return nullptr;
            }
            for (GLuint k = 0; k <= maximum_order_of_derivatives; ++k) {
                // k indicates the pd with respect to the second(v) parameter
                (*curves)[i]->SetDerivative(k, j, pd(k, k));
            }
            v += v_step;
            if(v > _v_max)
                v = _v_max;
        }
        u += u_step;
        if(u > _u_max)
            u = _u_max;
    }

    return curves;
}

GLvoid AbstractSurfaceEnergyFragment::setData(double u, double v) {
    TensorProductSurface3::PartialDerivatives pd;
    ptr_to_surface->CalculatePartialDerivatives(2,u,v,pd);

    normal = pd(1, 0);
    normal ^= pd(1, 1);

    norm_length = normal.length();

    normal /= norm_length; //.normalize();


    e1 = pd (1 , 0) * pd (1 , 0 ) ;
    f1 = pd (1 , 0) * pd (1 , 1 ) ;
    g1 = pd (1 , 1) * pd (1 , 1 ) ;

    e2 = normal * pd (2 , 0 ) ;
    f2 = normal * pd (2 , 1 ) ;
    g2 = normal * pd (2 , 2 ) ;

    K = ( e2 * g2 - f2 * f2) / ( e1 * g1 - f1 * f1);
    H = ( g1 * e2 - 2.0 * f1 * f2 + e1 * g2) / ( e1 * g1 - f1 * f1);

}


GLdouble TensorProductSurface3::DoubleIntegral(AbstractSurfaceEnergyFragment *ptr, GLdouble u_min , GLdouble u_max , GLdouble v_min , GLdouble v_max ,GLuint u_div , GLuint v_div){
    GLdouble result = 0.0;

    if(u_div % 2) {
        u_div += 1;
    }
    if( v_div % 2) {
        v_div += 1;
    }

    GLdouble du = (u_max - u_min ) / u_div , dv = (v_max - v_min) / v_div;

    RowMatrix<GLdouble> u(u_div + 1);

    for(GLint i = 0 ; i < (GLint) u_div; ++i){
        u[i] = u_min + i * du;
    }


    u[u_div] = u_max;

    RowMatrix<GLdouble> v(v_div + 1);

    for(GLint j = 0; j < (GLint) v_div ; ++j){
        v[j] = v_min + j * dv;
    }

    v[v_div] = v_max;

    Matrix<GLdouble> phi(u_div + 1, v_div + 1);

    for(GLint i = 0 ; i <= (GLint) u_div; ++i){
        for(GLint j = 0 ; j <= (GLint) v_div; ++j){
            phi(i , j) = (*ptr)(u[i] , v[j]);
        }
    }

    Matrix<GLdouble> weight(3, 3);

    weight(0, 0) = weight(0, 2) = weight(2, 0) = weight(2, 2) = 1.0;
    weight(0, 1) = weight(1, 0) = weight(1, 2) = weight(2, 1) = 4.0 ;
    weight(1, 1) = 16.0;

    for(GLint i = 1; i <= (GLint) u_div; i += 2) {
        for(GLint j = 1 ; j <= (GLint) v_div; j += 2){
            GLdouble S = 0.0;
            for(GLint k = -1; k <= 1 ; ++k){
                for(GLint l = -1; l <= 1 ; ++l){
                    S += weight(k + 1, l + 1)* phi(i+k, j+l);
                }
            }
            result += S ;
        }
    }


    result *= du * dv / 9.0;

    return result;

}

GLdouble TensorProductSurface3::calculateEnergy(AbstractSurfaceEnergyFragment *ptr, GLuint u_div_point_count, GLuint v_div_point_count){
    return DoubleIntegral(ptr, _u_min, _u_max, _v_min, _v_max, u_div_point_count, v_div_point_count);
}



GLdouble TensorProductSurface3::calculateEnergy(ImageColorScheme scheme, GLuint u_div_point_count, GLuint v_div_point_count) const{
    return 0.0;
}


TensorProductSurface3::~TensorProductSurface3() {
    DeleteVertexBufferObjectsOfData();
}
