#pragma once

#include "../Core/Matrices.h"
#include "../Core/LinearCombination3.h"
#include "BasisTypes.h"
#include "../Core/RealMatrices.h"
#include "../Core/RealSquareMatrices.h"
#include "BCurves3Composite.h"
#include "Core/Exceptions.h"

namespace cagd {
class HermiteBCurve3Composite
{
private:
    RowMatrix<DCoordinate3> p;
    Matrix<DCoordinate3> t; //derivatives
    RowMatrix<GLuint> n;
    // sphere:
    // x = r cos(theta) sin(fi)
    // y = r sin(theta) sin(fi)
    // z = r cos(fi)
    Matrix<GLdouble> r;
    Matrix<GLdouble> theta;
    Matrix<GLdouble> fi;
    RowMatrix<GLdouble> alpha;
    Basis::Type  type;
    GLuint m; //dimension
    GLuint rho; //order of derivatives

    BCurve3Composite *bcurve3composite;

public:
    HermiteBCurve3Composite();
    HermiteBCurve3Composite(RowMatrix<DCoordinate3> p, Matrix<DCoordinate3> t, RowMatrix<GLuint> n, RowMatrix<GLdouble> alpha, Basis::Type  type, BCurve3Composite *bcurve3composite);
    HermiteBCurve3Composite(GLuint m, GLuint rho, GLuint ni, BCurve3Composite *bcurve3composite);
    //HermiteBCurve3Composite(GLuint m, GLuint rho, BCurve3Composite *bcurve3composite);

    BCurve3* GenerateBCurve3(GLint index);
    GLvoid ModifyBCurve3(GLint index);

    void GenerateOptimalHermiteComposite();

    BCurve3Composite * getBCurve3Composite();

    GLuint getRho();
    GLvoid setRho(GLuint rho);

    RowMatrix<DCoordinate3> getP();
    DCoordinate3 getP(GLint i);
    GLvoid setP(GLint indexDCoordinate3, GLdouble value, GLint indexP);

    GLvoid modifyTRThetaFi();   // modify t, r, theta and fi if rho was changed

    Matrix<DCoordinate3>& getT();
    DCoordinate3 getT(GLint i, GLint j);
    //GLvoid setT(GLint i, GLint j, GLdouble alpha, GLdouble r);
    GLvoid setT(GLint i, GLint j, GLdouble modify_r, GLdouble modify_theta, GLdouble modify_fi);

    RowMatrix<GLuint>& getN();

    //DCoordinate3 getP(GLint i);

    GLuint getM();

    GLvoid initializeRThetaFi();
    GLvoid initializeMovebleControlPoints();

    Matrix<GLdouble>& getR();
    GLdouble getR(GLint index);
    Matrix<GLdouble>& getTheta();
    Matrix<GLdouble>& getFi();
    RowMatrix<GLdouble>& getAlpha();
    Basis::Type getType();
};
}
