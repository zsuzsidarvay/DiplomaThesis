#pragma once

#include "BCurves3.h"
#include "Core/Colors4.h"
#include "Core/Constants.h"
#include <memory>

namespace cagd
{
class BCurve3Composite
{

protected:
    GLdouble _alpha;

public:
    class ArcAttributes
    {

    public:
        BCurve3* arc;
        GenericCurve3* image;
        Color4* color;


        BCurve3* attractor;
        GenericCurve3* img_attractor;
        GLboolean useAttractor;

        GLint index_of_join_bcurve[2] = {-1, -1};
        GLint order_of_join_bcurve[2] = {-1, -1};
        GLint direction_of_join_bcurve[2] = {-1, -1};

        //        BCurve3* modified_bcurve;
        //        GenericCurve3* img_modified_bcurve;
        //        GLboolean show_modified_bcurve;

        GLdouble energy;
        //        GLboolean show_energy = false;
        GLint selected_p_index;

        GLvoid generateImages(GLdouble scale = 0.5);


        BCurve3* getBCurve();
        BCurve3* getAttractor();

        GLvoid setEnergy();
        GLdouble getEnergy();

        GLvoid setSelectedPIndex(GLint index);
        GLint getSelectedPIndex();

        GLvoid involveAttractor();

        GLvoid renderTangent();

        GLvoid setIncreaseOrder(GLint i);

        //  ArcAttributes* next;
        //  ArcAttributes* previous;

    };

protected:
    std::vector<ArcAttributes*> _attributes;

    GLboolean _isHermite;

    GLint _selected;
    GLint _selected2;

    GLint selected_p;



public:

    BCurve3Composite(GLdouble alpha = 1.0, GLboolean isHermite = false, GLint _selected = -1, GLint selected2 = -2);
    BCurve3Composite(BCurve3Composite& attribute);
    ~BCurve3Composite();
    BCurve3Composite& operator =(BCurve3Composite& rhs);

    GLboolean CreateImage(GLdouble scale = 0.5);

    ArcAttributes* operator[](GLint index);

    GLboolean RenderData(GLenum render_mode, Color4* color) const;
    GLboolean RenderData(GLenum render_mode, int indexOfArc, Color4* color) const;
    GLboolean RenderDerivatives(GLuint order, GLenum render_mode);
    GLboolean RenderDerivative(GLuint order, GLenum render_mode, GLint selected_bcurve = -1, Color4* color = nullptr);
    GLboolean RenderPoints(GLint indexOfArc, GLint x, Color4* color = nullptr);


    void insertNewArc(BCurve3* newArc, Color4* c, GLint indexofArc = -1);
    void insertNewArc(BCurve3* newArc, GLint indexofArc = -1);
    void deleteArc(int indexOfArc);

    GLvoid generateMatlabCode();

    GLvoid involveAttractor(GLint index);

    BCurve3* getBCurve(GLint index);
    BCurve3* getAttractor(GLint index);

    GLvoid setSelectedP(GLint index);
    GLint getSelectedP();

    GLvoid setColor(GLint i);

    GLvoid setSelected(GLint selected);
    GLint getSelected();

    GLvoid setSelected2(GLint selected2);
    GLint getSelected2();

    std::vector<ArcAttributes*>& getAttributes();

    GLvoid setSubdivision(GLint index);



    GLint* getIndexOfJoinBCurve(GLint index);
    GLvoid setIndexOfJoinBCurve(GLint index, GLint index_of_join_curve, GLint direction);

    GLint* getOrderOfJoinBCurve(GLint index);
    GLvoid setOrderOfJoinBCurve(GLint index, GLint order, GLint direction);

    GLint* getDirectionOfJoinBCurve(GLint index);
    GLvoid setDirectionOfJoinBCurve(GLint index, GLint direction, GLint x);


    GLvoid setBCurve(GLint index, BCurve3* bcurve);

    GLvoid recalculateJoin(GLint index);
    GLvoid recalculateMerge(GLint index);

    GLdouble getAlpha();
    Color4* getColor(GLuint index_of_arc);
    GLuint getSize();

    GLboolean getIsHermite();
    GLvoid setIsHermite(GLboolean isHermite);

    void setAlpha(double value);
    GLdouble getAlpha(GLint index) const;
    GLboolean SetColor(GLuint index_of_arc, Color4* c);


    GLvoid setUseAttractor(GLint index, GLboolean value);
    GLvoid setShowModifiedBcurve(GLint index, GLboolean value);

    GLboolean getShowModifiedBcurve(GLint index);

    GLboolean getShowUseAttractor(GLint index);

    GLboolean currentOrPreviousSelected(GLint index);
    GLboolean currentOrNextSelected(GLint index);
    GLvoid setSelectedPIndex(GLint indexOfArc, GLint pIndex);
    GLboolean neighborsInSelectedPoint(GLint currentArcIndex, GLint newArcIndex);

    GLvoid setModifiedBCurve(GLint index, BCurve3* bcurve);
    BCurve3* getModifiedBCurve(GLint index);
    GLint getselectedPIndex(GLint index_of_arc);

    GLboolean joinArcs(BCurve3* &joinedArc, GLint indexOfFirstArc = 0, GLint indexOfSecondArc = 1, GLint directionOfFirstArc = 0, GLint directionOfSecondArc = 1, GLint r1=1, GLint r2=1, GLboolean modify = GL_FALSE);

    GLvoid mergeArcsRight(GLint indexOfFixedArc, GLint indexOfMergeArc, const Matrix<GLdouble>& dF_fixedArc, const Matrix<GLdouble>& dG_fixedArc, const Matrix<GLdouble>& dF_mergeArc, GLint directionOfFixedArc, GLint r);
    GLvoid mergeArcsLeft(GLint indexOfFixedArc, GLint indexOfMergeArc, const Matrix<GLdouble>& dF_fixedArc, const Matrix<GLdouble>& dG_fixedArc, const Matrix<GLdouble>& dG_mergeArc, GLint directionOfFixedArc, GLint r);
    GLvoid mergeArcs(GLint indexOfFixedArc = 0, GLint indexOfMergeArc = 1, GLint directionOfFixedArc = 0, GLint directionOfMergeArc = 1, GLint r=1);


    GLvoid deleteAllArcAttributes();

};

}

