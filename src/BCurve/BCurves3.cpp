#include "BCurves3.h"
#include "BasisTypes.h"

using namespace cagd;
using namespace std;



GLvoid BCurve3::_CalculateBinomialCoefficients(GLuint order, TriangularMatrix<GLdouble> &bc) const {
    bc.ResizeRows(order +1);

    bc(0, 0) = 1.0;

    for (GLuint r=1; r <= order; ++r) {
        bc(r, 0) = 1.0;
        bc(r, r) = 1.0;

        for(GLuint i=1; i <= r/2; ++i) {
            bc(r, i) = bc(r-1, i-1) + bc(r-1, i);
            bc(r, r - i) = bc(r, i);
        }
    }
}

GLvoid BCurve3::_CalculateNormalizingCoefficient(GLuint order, RowMatrix<GLdouble> &c) const{
    if (!order){
        c.ResizeColumns(0);
    } else {
        GLuint size = 2 *order+1;
        c.ResizeColumns(size);

        GLdouble sa = pow(_S(_alpha / 2.0), (GLint)(2*order));
        GLdouble ca = 2.0 * _C(_alpha / 2.0);

        for(GLuint i =0; i <= order; ++i){
            c[i] = 0.0;
            for(GLuint r = 0; r<=i/2; ++r) {
                c[i] += _bc(order, i-r) * _bc(i-r,r) * pow(ca, (GLint)(i-2*r));
            }
            c[i] /= sa;
            c[size - 1 - i] = c[i];
        }
    }
}



// special constructor
BCurve3::BCurve3(Basis::Type type, GLuint n, GLdouble alpha, GLenum data_usage_flag) : LinearCombination3(0.0, alpha, 2*n+1) {
    _type = type;
    _n = n;
    _alpha = alpha;
    _S = (_type == Basis::TRIGONOMETRIC ? (_RealValuedRealFunction)sin : (_RealValuedRealFunction)sinh);
    _C = (_type == Basis::TRIGONOMETRIC ? (_RealValuedRealFunction)cos : (_RealValuedRealFunction)cosh);
    _T = (_type == Basis::TRIGONOMETRIC ? (_RealValuedRealFunction)tan : (_RealValuedRealFunction)tanh);
    _data_usage_flag = data_usage_flag;

    //konstr parameterbe
    _div_point_count = 301;
    _max_order_of_derivatives = 5;

    weights.ResizeColumns(_max_order_of_derivatives+1);
    for(GLint i = 0; i<=_max_order_of_derivatives; i++){
        weights[i] = 1.0;
    }
    //    weights[0] = 1.0;
    //    weights[1] = 1.0;
    //    weights[2] = 1.0;


    _D.resize(_max_order_of_derivatives+1);

    _CalculateBinomialCoefficients(2*_n, _bc);
    _CalculateNormalizingCoefficient(_n, _c);

    CalculateCollocationMatrix();
    CalculateIntegrals();

    movable_controlpoints.ResizeColumns(0);

//    CalculateB();

    original_cp.ResizeRows(_data.GetRowCount());

    _max_order_of_derivatives = 2;

};


RowMatrix<GLuint> BCurve3::getMovableControlPoints(){
    return movable_controlpoints;
}

GLvoid BCurve3::modifyControlPoint(GLint x, GLdouble a, GLint index){
    _data[index][x] += a;
    //  CalculateB();
}



GLvoid BCurve3::setMovableControlPoints(RowMatrix<GLuint> index){
    movable_controlpoints.ResizeColumns(index.GetColumnCount());
    //  fixed_controlpoints.ResizeColumns(2*_n+1-index.GetColumnCount());

    for(GLuint i=0; i<index.GetColumnCount();i++){
        movable_controlpoints[i]=index[i];
    }

//    CalculateB();
}

GLvoid BCurve3::setMovableControlPoints(GLint x, GLdouble a){
    for(GLuint i=0; i<movable_controlpoints.GetColumnCount();i++){
        _data[movable_controlpoints[i]][x] += a;
    }
//    CalculateB();
}

GLvoid BCurve3::setData(GLint x, GLdouble a){
    for(GLuint i=0; i<_data.GetRowCount();i++){
        _data[i][x] += a;
    }
//    CalculateB();
}



GLvoid BCurve3::addMovableControlPoint(GLuint index){
    movable_controlpoints.ResizeColumns(movable_controlpoints.GetColumnCount()+1);
    movable_controlpoints[movable_controlpoints.GetColumnCount()-1] = index;
//    CalculateB();
}

GLvoid BCurve3::removeMovableControlPoint(GLuint index){
    RowMatrix<GLuint> temp(0);
    for(GLuint i=0; i<2*_n+1;i++){
        for(GLuint j=0; j<movable_controlpoints.GetColumnCount();j++){
            if(i == movable_controlpoints[j] && i!=index){
                temp.ResizeColumns(temp.GetColumnCount()+1);
                temp[temp.GetColumnCount()-1] = movable_controlpoints[j];
            }
        }
    }
    movable_controlpoints.ResizeColumns(temp.GetColumnCount());
    movable_controlpoints.SetRow(0, temp);
//    CalculateB();
}

GLboolean BCurve3::isMovableControlPoint(GLuint index){
    for(GLuint j=0; j<movable_controlpoints.GetColumnCount();j++){
        if(index == movable_controlpoints[j])
            return GL_TRUE;
    }
    return GL_FALSE;
}

GLint BCurve3::countMovableControlPoints(){
    return movable_controlpoints.GetColumnCount();
}


GLboolean BCurve3::BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const {
    if (u < 0.0 || u > _alpha) {
        values.ResizeColumns(0);
        return GL_FALSE;
    }

    GLuint size = 2 * _n + 1;

    values.ResizeColumns(size);

    for(GLuint i = 0; i < size; ++i) {
        values[i] = 0.0;
    }

    if (u == 0.0 ) {
        values[0] = 1.0;
    } else {
        if(u == _alpha) {
            values[size - 1] = 1.0;
        } else {
            GLdouble sau = _S((_alpha - u) / 2.0);
            GLdouble su = _S(u / 2.0);

            if(u < _alpha / 2.0) {
                GLdouble factor = su / sau;
                GLdouble sau_order = pow(sau, (GLint)(size - 1));

                values[0] = sau_order;

                for(GLuint i =1; i < size; ++i){
                    values[i] = values[i - 1] * factor;
                }
            } else {
                GLdouble factor = sau / su;
                GLdouble su_order = pow(su, (GLint)(size -1));

                values[size -1] = su_order;

                for(GLint i = size - 2; i >= 0; --i){
                    values[i] = values[i+1] * factor;
                }
            }

            for(GLuint i =0; i < size; ++i) {
                values[i] *= _c[i];
            }
        }
    }

    return GL_TRUE;
}

GLboolean BCurve3::BlendingFunctionDerivatives(GLuint max_order_of_derivatives, RowMatrix<GLdouble>& A, Matrix<GLdouble>& dA) const {
    GLuint size = 2 * _n + 1;
    dA.ResizeRows(max_order_of_derivatives+1);
    dA.ResizeColumns(size);

    GLdouble sa = 2.0 * _S(_alpha / 2.0), ta = _T(_alpha / 2.0);

    dA.SetRow(0, A);

    for(GLuint j=1; j <= max_order_of_derivatives; j++) {
        for(GLuint i = 0; i < size; i++) {
            dA(j,i) = (( i > 0) ? _c[i] / _c[i-1] * i / sa * dA(j - 1 , i - 1) : 0.0)
                    - ((GLint)_n - (GLint)i) / ta * dA(j - 1 , i)
                    - ((i < 2 * _n ) ? ( _c[i] / _c [i+1] * (2.0 * _n - i) / sa) * dA(j - 1 , i + 1) : 0.0);
        }
    }

    //    #pragma omp parallel for
    //    for(GLint j_i = 1; j_i <= max_order_of_derivatives * size; j_i++) {

    //        GLuint j = j_i / size;
    //        GLuint i = j_i % size;

    //        dA(j,i) = (( i > 0) ? _c[i] / _c[i-1] * i / sa * dA(j - 1 , i - 1) : 0.0)
    //                - ((GLint)_n - (GLint)i) / ta * dA(j - 1 , i)
    //                - ((i < 2 * _n ) ? ( _c[i] / _c [i+1] * (2.0 * _n - i) / sa) * dA(j - 1 , i + 1) : 0.0);
    //    }

    return GL_TRUE;
}

void BCurve3::CalculateIntegrals() {
    _A.resize(_max_order_of_derivatives+1);

    GLint size = _n*2+1;

    for(GLint r=0; r<=_max_order_of_derivatives; r++){

        _A[r].ResizeRows(size);
        _A[r].ResizeColumns(size);

#pragma omp for
        for(int i = 0; i < size; i++)
            for(int j = 0; j<= i; j++) {
                GLdouble partial_sum = 0.0, partial_sum_2 = 0.0;


#pragma omp parallel for reduction (+ : partial_sum)
                for(GLint k = 1; k <= _div_point_count/2-1; k++) {
                    partial_sum += _D[r](2*k, i) * _D[r](2*k, j);
                }

#pragma omp parallel for reduction (+ : partial_sum_2)
                for(GLint k = 1; k <= _div_point_count/2; k++) {
                    partial_sum_2 += _D[r](2*k-1, i) * _D[r](2*k-1, j);
                }

                _A[r](i,j) = (_alpha/(3*(_div_point_count-1)))*(_D[r](0,i) * _D[r](0,j) + 2*partial_sum + 4*partial_sum_2 + _D[r](_div_point_count-1, i) * _D[r](_div_point_count-1, j));
                        _A[r](j,i) = _A[r](i,j);
                // cout << _A[r](i,j) << " " << i << " " << j << " " << endl;
            }
        //cout << endl;
    }
}

GLvoid BCurve3::CalculateIntegrals(BCurve3* attractor){


    GLint size = _n*2+1;
    GLint size_attr = attractor->getN()*2+1;

    GLint max_size = (size > size_attr ? size : size_attr);
    GLint min_size = (size < size_attr ? size : size_attr);


    attractor_integrals.ResizeColumns(max_size);
    attractor_integrals.ResizeRows(max_size);

    for(int i = 0; i < size; i++)
        for(int j = 0; j< size_attr; j++) {
            GLdouble partial_sum = 0.0, partial_sum_2 = 0.0;

            for(GLint k = 1; k <= _div_point_count/2-1; k++) {
                partial_sum += _D[0](2*k, i) * attractor->getD(0, 2*k, j);
            }

            for(GLint k = 1; k <= _div_point_count/2; k++) {
                partial_sum_2 += _D[0](2*k-1, i) * attractor->getD(0, 2*k-1, j);
            }

            attractor_integrals(i,j) = (_alpha/(3*(_div_point_count-1)))*(_D[0](0,i) * attractor->getD(0,0,j) + 2*partial_sum + 4*partial_sum_2 + _D[0](_div_point_count-1, i) * attractor->getD(0,_div_point_count-1, j));
            //                attractor_integrals(j,i) = attractor_integrals(i,j);

        }
    //    cout << attractor_integrals << endl;
}


GLboolean BCurve3::CalculateCollocationMatrix() {
    RowMatrix<GLdouble> A;

    GLint size = 2*_n+1;

    GLdouble step = (_u_max - _u_min) / (_div_point_count - 1);

#pragma omp parallel for
    for(GLint i = 0; i <= _max_order_of_derivatives; ++i) {
        _D[i].ResizeRows(_div_point_count);
        _D[i].ResizeColumns(size);
    }

    for(GLint i = 0; i < _div_point_count; ++i) {

        GLdouble u = _u_min + i * step;

        if(!BlendingFunctionValues(u, A)) {
            return GL_FALSE;
        }


        _D[0].SetRow(i,A);


        Matrix<GLdouble> dA(_max_order_of_derivatives+1, size);

        if(!BlendingFunctionDerivatives(_max_order_of_derivatives, A, dA)) {
            return GL_FALSE;
        }

#pragma omp parallel for
        for(GLint k = 0; k <= _max_order_of_derivatives; k++)
            for(GLint j = 0; j< size; j++) {
                _D[k](i,j) = dA(k,j);
            }

    }
    return GL_TRUE;
}

GLboolean BCurve3::CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const {
    if(u < _u_min || u > _u_max ) {
        d.ResizeRows(0);
        return GL_FALSE;
    }

    d.ResizeRows(max_order_of_derivatives + 1);
    d.LoadNullVectors();

    GLuint size = 2 * _n + 1;

    RowMatrix<GLdouble> A;

    if(!BlendingFunctionValues(u, A)) {
        d.ResizeRows(0);
        return GL_FALSE;
    }

    Matrix<GLdouble> dA(max_order_of_derivatives + 1, size);

    if(!BlendingFunctionDerivatives(max_order_of_derivatives, A, dA)) {
        return GL_FALSE;
    }


    for(GLuint i = 0; i < _data.GetRowCount(); ++i){
        for(GLuint j = 0; j <= max_order_of_derivatives ; j++)
            d[j] += _data[i] * dA(j , i) ;
    }


    return GL_TRUE;
}

RowMatrix<BCurve3*>* BCurve3::Subdivision(GLdouble u, GLenum usage_flag) const {

    if (u < 0.0 || u >= _alpha)
    {
        return nullptr;
    }

    RowMatrix<BCurve3*> *result = new (nothrow) RowMatrix<BCurve3*>(2);

    if (!result)
    {
        return nullptr;
    }

    enum Child{LEFT = 0, RIGHT = 1};

    (*result)[LEFT] = new (nothrow) BCurve3(_type, _n, u, usage_flag);

    if (!(*result)[LEFT])
    {
        delete result;
        return nullptr;
    }

    (*result)[RIGHT] = new (nothrow) BCurve3(_type, _n, _alpha - u, usage_flag);

    if (!(*result)[RIGHT])
    {
        delete (*result)[LEFT];
        delete result;
        return nullptr;
    }

    GLint size = 2 *_n+1;

    //Calculate weigths
    TriangularMatrix<GLdouble> w(size);
    TriangularMatrix<DCoordinate3> d(size);

#pragma omp parallel
    for(GLint i = 0; i < static_cast<GLint>(size); i++) {
        w(i, 0) = _c[i] / _bc(2*_n, i);
        d(i, 0) = _data[i];
    }

    GLdouble v = 0.5 + _T(u / 2.0 - _alpha / 4.0) / 2.0 / _T(_alpha / 4.0);


    // javitva, masodik for r-1-tol kell induljon 0 helyett
#pragma omp for
    for (GLint r = 1; r < size; r++)
    {
        for (GLint i = r-1; i < size - 1; i++)
        {
            w(i + 1, r) = (1.0 - v) * w(i, r - 1) + v * w(i + 1, r - 1);
            d(i + 1, r) = ((1.0 - v) * w(i, r - 1) * d(i, r - 1)
                           + v * w(i + 1, r - 1) * d(i + 1, r - 1)) / w(i + 1, r);
        }
    }

    BCurve3 &left = *(*result)[LEFT];
    BCurve3 &right = *(*result)[RIGHT];

    for (GLint i = 0; i < size; i++)
    {
        left[i] = d(i, i);
        right[i] = d(size - 1, size - 1 - i);
    }

    return result;
}


BCurve3* BCurve3::IncreaseOrder(GLuint z) const {
    BCurve3* result = new BCurve3(_type, _n+z, _alpha);

    RowMatrix<GLdouble> z_c;

    _CalculateNormalizingCoefficient(z,z_c);

    DCoordinate3 x(0.0,0.0,0.0);
    for (GLuint i = 0; i <= 2*(_n+z);i++) {
        result->_data[i] = x;
    }

    for(GLuint i = 0; i <= 2*_n; i++) {
        for(GLuint j = 0; j <= 2*z; j++) {
            result->_data[i+j] += _data[i] * (_c[i] * z_c[j]) / result->_c[i+j];
        }
    }

    return result;
}

GLdouble BCurve3::getAlpha() const {
    return _alpha;
}

GLuint BCurve3::getN() const {
    return _n;
}

Basis::Type BCurve3::getType() const {
    return _type;
}

void BCurve3::SetAlpha(GLdouble alpha)
{
    if(alpha != _alpha)
    {
        _alpha = alpha;
    }
}

GLvoid BCurve3::CalculateB(BCurve3* attractor){
    GLint size = 2*_n+1;
    GLint mu = movable_controlpoints.GetColumnCount();

    b.ResizeRows(mu);

    for(GLint i = 0; i<mu;i++){
        b[i].x() = 0.0;
        b[i].y() = 0.0;
        b[i].z() = 0.0;
    }

    //    DCoordinate3 temp(0.0,0.0,0.0);

    //    for(GLint r = 1; r <= _max_order_of_derivatives; r++ ){

    //        for(GLint i = 0; i < mu; i++) {

    //            for(GLint j = 0; j < size; j++) {
    //                if (!isMovableControlPoint(j)) {
    //                    temp += _data[j] * _A[r](j, movable_controlpoints[i]);
    //                }
    //            }
    //            b[i] += weights[r]*temp;

    //            temp.x() = 0.0;
    //            temp.y() = 0.0;
    //            temp.z() = 0.0;

    //            if(attractor){

    //                for(GLint j=0; j < 2*attractor->getN()+1;j++){
    //                    b[i] -= weights[0]*attractor_integrals(movable_controlpoints[i], j) * (*attractor)[j];
    //                }
    //            }
    //        }

    //    }

    //    ColumnMatrix<DCoordinate3> b;

    //    b.ResizeRows(mu);


    DCoordinate3 temp(0.0,0.0,0.0);

    for(GLint i = 0; i < mu; i++) {
        for(GLint r = 0; r <= _max_order_of_derivatives; r++ ){



            for(GLint j = 0; j < size; j++) {
                if (!isMovableControlPoint(j)) {
                    temp += _data[j] * _A[r](j, movable_controlpoints[i]);
                }
            }
            b[i] += weights[r]*temp;

            temp.x() = 0.0;
            temp.y() = 0.0;
            temp.z() = 0.0;
        }

        if(attractor){
            for(GLuint j=0; j <= attractor->getN()*2; j++) {
                b[i] -= weights[0] *_A[0](movable_controlpoints[i], j) * (*attractor)[j];
            }
        }

    }


}

GLvoid BCurve3::CalculateB(GLint r1, GLint r2, BCurve3* attractor) {

    GLint mu = 2 *_n + 1 - r1 - r2;
    b.ResizeRows(mu);

    for(GLint i = 0; i< mu; i++){
        b[i].x() = 0.0;
        b[i].y() = 0.0;
        b[i].z() = 0.0;
    }

    GLint size = 2 *_n + 1;

    DCoordinate3 temp(0.0,0.0,0.0);

    for(GLint i = 0; i < mu; i++) {

        for(GLint r = 0; r <= _max_order_of_derivatives; r++ ){

            for(GLint j = 0; j < r1; j++) {
                temp += _data[j] * _A[r](j, r1+i);
            }

            for(GLint j = r1+mu; j < size; j++){
                temp += _data[j] * _A[r](j, r1+i);
            }

            b[i] += weights[r] * temp;

            temp.x() = 0.0;
            temp.y() = 0.0;
            temp.z() = 0.0;

            if(attractor){
                for(GLuint j=0; j <= attractor->getN()*2; j++) {
                    b[i] -= weights[0] *_A[0](r1 + i, j) * (*attractor)[j];
                }
            }
        }
    }
}


//GLdouble BCurve3::CalculateEnergyFunctional(GLint r1, GLint r2) {
//    GLdouble energy = 0.0;

//    GLint mu = 2*_n+1 - r1- r2;


//    RealSquareMatrix A;

//    GLint size = 2*_n+1;

//    //Calculate A
//    A.ResizeRows(size);
//    A.ResizeColumns(size);

//    for(GLint i = 0; i < size; i++) {
//        for(GLint j = 0; j < size; j++) {
//            A(i,j) = 0.0;
//        }
//    }

//    for(GLint r=0; r <= _max_order_of_derivatives; r++) {
//        for(GLint i = 0; i < size; i++) {
//            for(GLint j = 0; j < size; j++) {
//                A(i,j) += weights[r]*_A[r](i,j);
//            }
//        }

//    }

//    //    //    GLdouble psi = 0.0;

//    //    //    for(GLint i = 0;i <)

//    //    for(GLint i=0; i<size; i++) {
//    //        for(GLint j=0;j<size;j++) {
//    //            energy += (_data[i]*_data[j]) *A(i,j);
//    //        }
//    //    }


//    //calculate b
//    ColumnMatrix<DCoordinate3> b;

//    b.ResizeRows(mu);



//    DCoordinate3 temp(0.0,0.0,0.0);

//    for(GLint i = 0; i < mu; i++) {

//        for(GLint r = 0; r <= _max_order_of_derivatives; r++ ){

//            for(GLint j = 0; j < r1; j++) {
//                temp += _data[j] * _A[r](j, r1+i);
//            }

//            for(GLint j = r1+mu; j <size; j++){
//                temp += _data[j] * _A[r](j,r1+i);
//            }

//            b[i] += weights[r]*temp;

//            temp.x() = 0.0;
//            temp.y() = 0.0;
//            temp.z() = 0.0;
//        }

//    }


//    GLdouble psi = 0.0;
//    for(GLint r=1; r<=_max_order_of_derivatives;r++) {

//        for(GLint i = 0; i < size; i++) {
//            GLdouble temp =0.0;
//            if(i < r1 || i >= r1 + mu) {
//                for(GLint j = 0; j<size; j++) {

//                    if(j < r1 || j >= r1 + mu) {
//                        temp += _A[r](i,j) * _data[i] * _data[j];
//                    }
//                }
//            }
//            psi += weights[r] * temp;
//        }
//    }

//    energy = psi;


//    for (GLint i = r1; i < r1 + mu; i++) {
//        energy += 2*_data[i]*b[i-r1];
//        for (GLint j = r1; j < r1 + mu; j++) {
//            energy += A(i,j)*_data[i]*_data[j];
//        }
//    }


//    return energy;
//}

GLdouble BCurve3::CalculateEnergyFunctional(BCurve3* attractor){
    GLdouble energy = 0.0;

    GLint mu = movable_controlpoints.GetColumnCount();


    RealSquareMatrix A;

    GLint size = 2*_n+1;

    //Calculate A
    A.ResizeRows(size);
    A.ResizeColumns(size);

    for(GLint i = 0; i < size; i++) {
        for(GLint j = 0; j < size; j++) {
            A(i,j) = 0.0;
        }
    }

    for(GLint r=0; r <= _max_order_of_derivatives; r++) {
        for(GLint i = 0; i < size; i++) {
            for(GLint j = 0; j < size; j++) {
                A(i,j) += weights[r]*_A[r](i,j);
            }
        }

    }

    if(attractor) {
        CalculateIntegrals(attractor);
        //        CalculateB(attractor);
    }

    CalculateB(attractor);




    //calculate b
    //    ColumnMatrix<DCoordinate3> b;

    //    b.ResizeRows(mu);


    //    DCoordinate3 temp(0.0,0.0,0.0);

    //    for(GLint r = 1; r <= _max_order_of_derivatives; r++ ){

    //        for(GLint i = 0; i < mu; i++) {


    //            //            for(GLint j = 0; j < size; j++) {
    //            //                if (!moveableControlPoint(j, index, mu)) {
    //            //                    temp += _data[j] * _A[r](j, index[i]);
    //            //                }
    //            //            }
    //            for(GLint j = 0; j < size; j++) {
    //                if (!isMovableControlPoint(j)) {
    //                    temp += _data[j] * _A[r](j, movable_controlpoints[i]);
    //                }
    //            }
    //            b[i] += weights[r]*temp;

    //            temp.x() = 0.0;
    //            temp.y() = 0.0;
    //            temp.z() = 0.0;

    //            if(attractor){
    //                //                for(GLint j=0; j < size; j++) {
    //                //                    b[i] -= _A[0](movable_controlpoints[i], j) * attractor->_data[j];
    //                //                }
    //                for(GLint j=0; j < 2*attractor->getN()+1;j++){
    //                    b[i] -= weights[0]*attractor_integrals(movable_controlpoints[i], j) * (*attractor)[j];
    //                }
    //            }
    //        }

    //    }

    //JOOO
    //    ColumnMatrix<DCoordinate3> b;

    //    b.ResizeRows(mu);


    //    DCoordinate3 temp(0.0,0.0,0.0);

    //    for(GLint i = 0; i < mu; i++) {
    //        for(GLint r = 0; r <= _max_order_of_derivatives; r++ ){



    //            for(GLint j = 0; j < size; j++) {
    //                if (!isMovableControlPoint(j)) {
    //                    temp += _data[j] * _A[r](j, movable_controlpoints[i]);
    //                }
    //            }
    //            b[i] += weights[r]*temp;

    //            temp.x() = 0.0;
    //            temp.y() = 0.0;
    //            temp.z() = 0.0;
    //        }

    //        if(attractor){
    //        for(GLint j=0; j <= attractor->getN()*2; j++) {
    //            b[i] -= weights[0] *_A[0](movable_controlpoints[i], j) * (*attractor)[j];
    //        }
    //        }

    //    }



    GLdouble psi = 0.0;
    for(GLint r=1; r<=_max_order_of_derivatives;r++) {

        for(GLint i = 0; i < size; i++) {
            GLdouble temp =0.0;
            if(!isMovableControlPoint(i)) {
                for(GLint j = 0; j<size; j++) {

                    if(!isMovableControlPoint(j)) {
                        temp += _A[r](i,j) * _data[i] * _data[j];
                    }
                }
            }
            psi += weights[r] * temp;
        }
    }


    if(attractor){
        for(GLint i = 0; i < size; i++) {
            GLdouble temp =0.0;
            if(!isMovableControlPoint(i)) {
                for(GLint j = 0; j<size; j++) {

                    if(!isMovableControlPoint(j)) {
                        temp += _A[0](i,j) * _data[i] * _data[j];
                    }
                }
            }
            psi += weights[0] * temp;
        }

        GLint size_attr = attractor->getN()*2+1;


        for(GLint i = 0; i < size_attr; i++) {
            GLdouble temp =0.0;
            for(GLint j = 0; j<size_attr; j++) {
                temp += attractor->getA(0,i,j) * (*attractor)[i] * (*attractor)[j];
            }
            psi += weights[0] * temp;
        }


        for(GLint i = 0; i < size; i++) {
            GLdouble temp =0.0;
            if(!isMovableControlPoint(i)) {
                for(GLint j = 0; j<size_attr; j++) {

                    temp += attractor_integrals(i,j) * _data[i] * (*attractor)[j];
                }
            }

            psi -= 2*weights[0] * temp;
        }

    }




    energy = psi;


    for (GLint i = 0; i < mu; i++) {
        energy += 2*_data[movable_controlpoints[i]]*b[i];
        for (GLint j = 0; j < mu; j++) {
            energy += A(movable_controlpoints[i],movable_controlpoints[j])*_data[movable_controlpoints[i]]*_data[movable_controlpoints[j]];
        }
    }


    return energy;
}


GLdouble BCurve3::getA(GLint r, GLint i, GLint j){
    return _A[r](i,j);
}



BCurve3* BCurve3::CalculateGlobalMinimum(GLint r1, GLint r2, BCurve3* attractor){
    BCurve3* bcurve_global_min = new BCurve3(*this);

    bcurve_global_min->ModifyUsingGlobalMinimum(r1, r2, attractor);

    return bcurve_global_min;

}

GLvoid BCurve3::ModifyUsingGlobalMinimum(GLint r1, GLint r2, BCurve3* attractor)
{
    GLint mu = 2*_n+1 - r1- r2;
    GLint size = 2*_n+1;

    RealSquareMatrix A;

    //Calculate A
    A.ResizeRows(mu);
    A.ResizeColumns(mu);

    for(GLint i = 0; i < mu; i++) {
        for(GLint j = 0; j < mu; j++) {
            A(i,j) = 0.0;
        }
    }

    for(GLint i = r1; i < r1+mu; i++) {
        for(GLint j = r1; j <= i; j++) {

            for(GLint r=0; r <= _max_order_of_derivatives; r++){
                A(i-r1,j-r1) += weights[r]*_A[r](i,j);
            }
            A(j-r1,i-r1) = A(i-r1,j-r1);
        }
    }

    //calculate b
//    ColumnMatrix<DCoordinate3> b;

//    b.ResizeRows(mu);

//    DCoordinate3 temp(0.0,0.0,0.0);

//    for(GLint r = 1; r <= _max_order_of_derivatives; r++ ){

//        for(GLint i = 0; i < mu; i++) {

//            for(GLint j = 0; j < r1; j++) {
//                temp += _data[j] * _A[r](j, r1+i);
//            }

//            for(GLint j = r1+mu; j <size; j++){
//                temp += _data[j] * _A[r](j,r1+i);
//            }

//            b[i] += weights[r]*temp;

//            temp.x() = 0.0;
//            temp.y() = 0.0;
//            temp.z() = 0.0;
//        }
//    }

    CalculateB(r1, r2, attractor);

    ColumnMatrix<DCoordinate3> d;

    d.ResizeRows(mu);

    A.SolveLinearSystem(b,d);

    for(GLint i = r1; i < r1+mu; i++){
        _data[i] = -d[i-r1];
    }
}



bool moveableControlPoint(GLint j, RowMatrix<GLuint> index, GLint mu)
{
    for(int i = 0; i < mu; i++)
        if (index[i] == (GLint) j)
            return true;
    return false;
}

GLvoid BCurve3::CalculateGlobalMin(BCurve3* attractor){

    GLint mu = movable_controlpoints.GetColumnCount();


    GLint size = 2*_n+1;


    RealSquareMatrix A;


    //Calculate A
    A.ResizeRows(mu);
    A.ResizeColumns(mu);

    for(GLint i = 0; i < mu; i++) {
        for(GLint j = 0; j < mu; j++) {
            A(i,j) = 0.0;
        }
    }

    for(GLint i = 0; i < mu; i++) {
        for(GLint j = 0; j <= i; j++) {

            for(GLint r=0; r <= _max_order_of_derivatives; r++){
                A(i,j) += weights[r]*_A[r](movable_controlpoints[i],movable_controlpoints[j]);
            }
            A(j,i) = A(i,j);
        }

    }

     CalculateB(attractor);


    ColumnMatrix<DCoordinate3> d;

    d.ResizeRows(mu);

    A.SolveLinearSystem(b,d);

    for(GLint i = 0; i < mu; i++){
        _data[movable_controlpoints[i]] = -d[i];
    }

}


BCurve3* BCurve3::CalculateCurveAttractor(BCurve3* attractor, RowMatrix<GLuint> index){
    BCurve3* bcurve_global_min = new BCurve3(*this);

    GLint mu = index.GetColumnCount();

    GLint size = 2*_n+1;


    RealSquareMatrix A;


    //Calculate A
    A.ResizeRows(mu);
    A.ResizeColumns(mu);

    for(GLint i = 0; i < mu; i++) {
        for(GLint j = 0; j < mu; j++) {
            A(i,j) = 0.0;
        }
    }

    for(GLint i = 0; i < mu; i++) {
        for(GLint j = 0; j <= i; j++) {

            for(GLint r=0; r <= _max_order_of_derivatives; r++){
                A(i,j) += weights[r]*_A[r](index[i],index[j]);
            }
            A(j,i) = A(i,j);
        }

    }


    CalculateIntegrals(attractor);
    // CalculateB(attractor);

    //calculate b
    ColumnMatrix<DCoordinate3> b;

    b.ResizeRows(mu);


    DCoordinate3 temp(0.0,0.0,0.0);

    for(GLint i = 0; i < mu; i++) {
        for(GLint r = 0; r <= _max_order_of_derivatives; r++ ){



            for(GLint j = 0; j < size; j++) {
                if (!moveableControlPoint(j, index, mu)) {
                    temp += _data[j] * _A[r](j, index[i]);
                }
            }
            b[i] += weights[r]*temp;

            temp.x() = 0.0;
            temp.y() = 0.0;
            temp.z() = 0.0;
        }

        for(GLint j=0; j <= attractor->getN()*2; j++) {
            b[i] -= weights[0] *_A[0](index[i], j) * (*attractor)[j];
        }

    }



    ColumnMatrix<DCoordinate3> d;

    d.ResizeRows(mu);

    A.SolveLinearSystem(b,d);

    for(GLint i = 0; i < mu; i++){
        bcurve_global_min->_data[index[i]] = -d[i];
    }

    return bcurve_global_min;

}




void BCurve3::SetControlPoint(GLint i, GLdouble alpha, GLdouble beta, GLdouble r, DCoordinate3 _global_min){
    _data[i].x() = _global_min.x() + sin(alpha)*cos(beta) *r;
    _data[i].y() = _global_min.y() + sin(alpha)*sin(beta) *r;
    _data[i].z() = _global_min.z() + cos(alpha)*r;
    //    _data[i][] += alpha;
    //    _data[i].y() = _global_min.y() + sin(alpha)*sin(beta) *r;
    //    _data[i].z() = _global_min.z() + cos(alpha)*r;
}


GLvoid BCurve3::SetControlPoints(GLdouble alpha, GLdouble beta, GLdouble r){
    for(GLuint i = 0; i < movable_controlpoints.GetColumnCount(); i++){
        _data[movable_controlpoints[i]].x() = original_cp[movable_controlpoints[i]].x()+sin(alpha)*cos(beta) *r;
        _data[movable_controlpoints[i]].y() = original_cp[movable_controlpoints[i]].y()+ sin(alpha)*sin(beta) *r;
        _data[movable_controlpoints[i]].z() =  original_cp[movable_controlpoints[i]].z()+ cos(alpha)*r;
    }
}

GLvoid BCurve3::SetOrginalCp(){
    for(GLuint i=0; i<_data.GetRowCount(); i++){
        original_cp[i] = _data[i];
    }

}


GLdouble BCurve3::getD(GLint r, GLint i, GLint j){
    return _D[r](i,j);
}

GLvoid BCurve3::SetMaxOrderOfDerivatives(GLint val){
    _max_order_of_derivatives = val;
}

GLint BCurve3::getMaxOrderOfDerivatives() {
    return _max_order_of_derivatives;
}


GLvoid BCurve3::IdenticalControlPoints() {
    SetOrginalCp();
    GLuint size = 2 * _n + 1;
    for(GLuint i = 0; i < size; i++){
        if (i != _n)
            _data[i] = _data[_n];
        addMovableControlPoint(i);
    }
}

GLvoid BCurve3::RestoreControlPoints() {
    _data = original_cp;
}

RowMatrix<GLdouble> BCurve3::getWeights() {
    return weights;
}

GLvoid BCurve3::setWeights(RowMatrix<GLdouble> temp){
    for(GLint i = 0; i<=_max_order_of_derivatives; i++){
        weights[i] = temp[i];
    }
}

GLvoid BCurve3::setWeight(GLint index, GLdouble temp){
    weights[index] = temp;
}


GLvoid BCurve3::setHermiteMovebleControlPoints(GLint rho) {
    GLuint size = 2 * _n + 1;
    if (size - 2 * rho - 2 > 0) {
        movable_controlpoints.ResizeColumns(size - 2 * rho - 2);
        for(GLuint i = rho + 1; i < size - rho - 1; i++){
            movable_controlpoints[i - rho -1] = i;
        }
//        CalculateB();
    }
}





