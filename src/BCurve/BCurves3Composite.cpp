#pragma once

#include "BCurves3Composite.h"

using namespace std;
using namespace cagd;

BCurve3Composite::BCurve3Composite(GLdouble alpha, GLboolean isHermite, GLint selected, GLint selected2)
{
    _alpha = alpha;
    _isHermite = isHermite;
    _selected = selected;
    _selected2 = selected2;
    
}

BCurve3Composite::BCurve3Composite(BCurve3Composite& curves)
{
    this -> _attributes = curves._attributes;
    this -> _alpha = curves._alpha;
    this -> _isHermite = curves._isHermite;
    this -> _selected = curves._selected;
    this -> _selected2 = curves._selected2;
    
}

GLdouble BCurve3Composite::getAlpha(GLint index) const {
    return _attributes[index]->arc->getAlpha();
}


BCurve3Composite::~BCurve3Composite()
{
    deleteAllArcAttributes();
}

BCurve3Composite::ArcAttributes* BCurve3Composite::operator[](GLint index){
    return _attributes[index];
}

BCurve3Composite& BCurve3Composite::operator =(BCurve3Composite& rhs)
{
    this -> _attributes = rhs._attributes;
    this -> _alpha = rhs._alpha;
    
    return (*this);
}

GLboolean BCurve3Composite::RenderData(GLenum render_mode, Color4* color) const
{
    for (GLint i = 0; i < (GLint) _attributes.size(); i++)
    {
//        if(i == _selected)
//            glLineWidth(3.0);
//        else
//            glLineWidth(1.0);

        if(_selected == i || _selected2 == i) {
            glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
        } else
            glColor4f(_attributes[i] ->color->r(), _attributes[i] ->color->g(), _attributes[i] ->color->b(), _attributes[i] ->color->a());
        
        
        //        glColor4f(_attributes[i] ->color->r(), _attributes[i] ->color->g(), _attributes[i] ->color->b(), _attributes[i] ->color->a());
        if(_attributes[i] -> arc)
        {
            
            _attributes[i] -> arc -> RenderData(render_mode);
            
            //itt van valami bug
            if( _attributes[i] -> useAttractor) {
                glColor4f(0.180f, 0.545f, 0.341f, 0.0f);
                _attributes[i] -> attractor -> RenderData(render_mode);
                
            }
            
            //            if(_attributes[i]->show_modified_bcurve){
            //                _attributes[i]->modified_bcurve -> RenderData(render_mode);
            //            }
        }
    }
    return GL_TRUE;
}

GLboolean BCurve3Composite::RenderData(GLenum render_mode, int indexOfArc, Color4* color) const
{
//    if(indexOfArc == _selected)
//        glLineWidth(3.0);
//    else
//        glLineWidth(1.0);

    if(_selected == indexOfArc || _selected2 == indexOfArc) {
        glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
    } else
        glColor4f(_attributes[indexOfArc] ->color->r(), _attributes[indexOfArc] ->color->g(), _attributes[indexOfArc] ->color->b(), _attributes[indexOfArc] ->color->a());
    //    setColor(indexOfArc);
    
    
    if(_attributes[indexOfArc] -> arc)
    {
        
        if(! _attributes[indexOfArc] -> arc -> RenderData(render_mode))
            cout << "Hiba" << endl;
        
        if( _attributes[indexOfArc] -> useAttractor) {
            glColor4f(0.180f, 0.545f, 0.341f, 0.0f);
            _attributes[indexOfArc] -> attractor -> RenderData(render_mode);
        }
        
        //        if(_attributes[indexOfArc]->show_modified_bcurve) {
        //            _attributes[indexOfArc]->modified_bcurve -> RenderData(render_mode);
        //        }
    }
    return GL_TRUE;
}

GLboolean BCurve3Composite::CreateImage(GLdouble scale){
    
    for (GLuint i = 0; i < _attributes.size(); i++)
    {
        _attributes[i]->generateImages(scale);
        
        //        _attributes[i] -> arc -> UpdateVertexBufferObjectsOfData();
        //        delete _attributes[i] -> image;
        //        if(!_attributes[i] -> arc)
        //        {
        //            cout << "error : insertNewArc -> attribute -> arc" << endl;
        //        }
        //        _attributes[i] -> image = nullptr;
        //        _attributes[i] -> image = _attributes[i] -> arc -> GenerateImage(2, 101, GL_STATIC_DRAW);
        //        if(!_attributes[i] -> image)
        //        {
        //            cout << "error : insertNewArc -> attribute -> image" << endl;
        //        }
        //        if(!_attributes[i] -> image -> UpdateVertexBufferObjects(scale))
        //        {
        //            cout << "Could_not_update_the_vertex_buffer_object_of_the_curve!" << endl;
        //        }
        
        //        _attributes[i] -> attractor -> UpdateVertexBufferObjectsOfData();
        //        if(!_attributes[i] -> attractor)
        //        {
        //            cout << "error : insertNewArc -> attribute -> arc" << endl;
        //        }
        //        _attributes[i] -> img_attractor = nullptr;
        //        _attributes[i] -> img_attractor = _attributes[i] -> attractor -> GenerateImage(2, 101, GL_STATIC_DRAW);
        //        if(!_attributes[i] -> img_attractor)
        //        {
        //            cout << "error : insertNewArc -> attribute -> image" << endl;
        //        }
        //        if(!_attributes[i] -> img_attractor -> UpdateVertexBufferObjects())
        //        {
        //            cout << "Could_not_update_the_vertex_buffer_object_of_the_curve!" << endl;
        //        }
        
        
        
    }
    return GL_TRUE;
}

GLboolean BCurve3Composite::RenderDerivative(GLuint order, GLenum render_mode, GLint i, Color4* color){
//    if(i == _selected)
//        glLineWidth(3.0);
//    else
//        glLineWidth(1.0);

    if (_attributes[i] -> image)
    {
        switch (order)
        {
        case 0:
        {
            setColor(i);
            //            if(color == nullptr){
            //                float r = _attributes[i] -> color -> r();
            //                float g = _attributes[i] -> color -> g();
            //                float b = _attributes[i] -> color -> b();
            //                float a = _attributes[i] -> color -> a();
            //                glColor4f(r, g, b, a);
            
            //        } else {
            //            glColor4f(color->r(), color->g(), color->b(), color->a());
            //        }
        }
            break;
        case 1:
        {
            glColor3f(0.0f, 0.5f, 0.0f);
        }
            break;
        case 2:
        {
            glColor3f(0.0f, 0.8f, 1.0f);
        }
        case 3:
        {
            //            glColor3f(0.0f, 0.0f, 0.0f);
//            glColor3f(0.482f, 0.408f, 0.933f);

            glColor4f(colors::dark_orange.r(), colors::dark_orange.g(), colors::dark_orange.b(), colors::dark_orange.a());

        }
        }
        //        if(order == 0 || order == 3) {
        glLoadName(i);
        
        _attributes[i] -> image -> RenderDerivatives(order, render_mode);
        //        }else{
        //            GenericCurve3 &image = *_attributes[i] -> image;
        //            GLuint n = image.GetPointCount() - 1;
        //            glBegin(GL_LINES);
        //            {
        //                glVertex3dv(&image(0, 0)[0]);
        //                DCoordinate3 sum = image(0, 0) + image(order, 0);
        //                glVertex3dv(&sum[0]);
        //            }
        //            {
        //                glVertex3dv(&image(0, n)[0]);
        //                DCoordinate3 sum = image(0, n) + image(order, n);
        //                glVertex3dv(&sum[0]);
        //            }
        //            glEnd();
        //        }
    }
    
    if(_attributes[i] ->img_attractor && _attributes[i] -> useAttractor) {
        glColor3f(0.180f, 0.545f, 0.341f);
        _attributes[i] ->img_attractor ->RenderDerivatives(order, render_mode);
    }
    
    
    
    return GL_TRUE;
}


GLboolean BCurve3Composite::RenderDerivatives(GLuint order, GLenum render_mode)
{
    
    for (GLuint i = 0; i < _attributes.size(); i++)
    {

//        if(i == _selected)
//            glLineWidth(3.0);
//        else
//            glLineWidth(1.0);
        RenderDerivative(order, render_mode, i);
        RenderPoints(i,0);

    }
    return GL_TRUE;
}

GLvoid BCurve3Composite::setSubdivision(GLint index){
    
    RowMatrix<BCurve3*> *subdivision = _attributes[index]->arc->Subdivision(0.5*getAlpha(index));
    
    deleteArc(index);
    insertNewArc((*subdivision)[0], index);
    insertNewArc((*subdivision)[1], index+1);

//    mergeArcs(index, index+1, 0, 1, 0);
    
    delete[] subdivision;
}


GLvoid BCurve3Composite::setUseAttractor(GLint index, GLboolean value){
    if(index == -1) {
        for (GLuint i = 0; i < _attributes.size(); i++)
        {
            _attributes[i]->useAttractor = value;
        }
    } else {
        _attributes[index]->useAttractor = value;
    }
}



void BCurve3Composite::insertNewArc(BCurve3* newArc, Color4* c, GLint indexofArc)
{
    ArcAttributes* attribute = new ArcAttributes();
    attribute -> arc = new BCurve3(*newArc);
    
    //    attribute->p= -1;
    attribute->selected_p_index = -1;
    
    
    
    GLuint n = newArc->getN();
    
    BCurve3* attractor = new BCurve3(newArc->getType(), n, newArc->getAlpha());
    
    DCoordinate3 &a = (*newArc)[0];
    DCoordinate3 &b = (*newArc)[newArc->getN()*2];
    
    /*(*attractor)[0] = a - DCoordinate3(0.3,0.3,0); //(*newArc)[0];
    (*attractor)[attractor->getN()*2] = b - DCoordinate3(0.3,0.3,0); // (*newArc)[newArc->getN()*2];
    
    
    (*attractor)[1] = (a*3 + b)/4 - DCoordinate3(0.3,0.3,0);
    (*attractor)[2] = (a + b)/2 - DCoordinate3(0.3,0.3,0);
    (*attractor)[3] = (a + b*3)/4 - DCoordinate3(0.3,0.3,0);
    
*/
    
    (*attractor)[0] = a;
    (*attractor)[2 * n] = b;
    
    GLdouble step = 1.0 / (2 * n);
    GLdouble t = 0.0;
    for (GLuint i = 1; i < 2 * n; i++)
    {
        t += step;
        (*attractor)[i] = (1 - t) * a + t * b;
    }
    
    DCoordinate3 v = b - a;
    DCoordinate3 normal;
    normal.x() = -v.y();
    normal.y() = v.x();
    normal.normalize();
    
    for (GLuint i = 0; i <= 2 * n; i++)
    {
        (*attractor)[i] -= normal;
    }
    //    (*attractor)[n] -= normal;
    
    attribute -> attractor = attractor;
    attribute ->useAttractor = false;
    
    
    attribute -> arc -> UpdateVertexBufferObjectsOfData();
    if(!attribute -> arc)
    {
        cout << "error : insertNewArc -> attribute -> arc" << endl;
    }
    attribute -> image = nullptr;
    attribute -> image = attribute -> arc -> GenerateImage(2, 101, GL_STATIC_DRAW);
    if(!attribute -> image)
    {
        cout << "error : insertNewArc -> attribute -> image" << endl;
    }
    if(!attribute -> image -> UpdateVertexBufferObjects())
    {
        cout << "Could_not_update_the_vertex_buffer_object_of_the_curve!" << endl;
    }
    
    
    attribute -> attractor -> UpdateVertexBufferObjectsOfData();
    if(!attribute -> attractor)
    {
        cout << "error : insertNewArc -> attribute -> arc" << endl;
    }
    attribute -> img_attractor = nullptr;
    attribute -> img_attractor = attribute -> attractor -> GenerateImage(2, 101, GL_STATIC_DRAW);
    if(!attribute -> img_attractor)
    {
        cout << "error : insertNewArc -> attribute -> image" << endl;
    }
    if(!attribute -> img_attractor -> UpdateVertexBufferObjects())
    {
        cout << "Could_not_update_the_vertex_buffer_object_of_the_curve!" << endl;
    }
    
    attribute -> color = new Color4();
    attribute -> color->r() = c->r();
    attribute -> color->g() = c->g();
    attribute -> color->b() = c->b();
    attribute -> color->a() = c->a();
    
    //    attribute -> next = nullptr;
    //    attribute -> previous = nullptr;
    attribute->arc->SetOrginalCp();
    attribute->attractor->SetOrginalCp();
    
    if(indexofArc == -1){
        _attributes.push_back(attribute);
    } else {
        _attributes.insert(_attributes.begin()+indexofArc, attribute);
    }
}

void BCurve3Composite::insertNewArc(BCurve3* newArc, GLint indexofArc)
{
        Color4* color = new Color4(rand()%100*0.01f, rand()%100*0.01f, rand()%100*0.01f, 0.0f);
    //    Color4* color = new Color4(1.0f, 0.5f, 0.0f, 0.0f);
//    Color4* color = new Color4(0.0f, 0.502f, 0.502f, 0.0f);

    insertNewArc(newArc, color, indexofArc);
    
    delete color;
}

GLvoid BCurve3Composite::ArcAttributes::involveAttractor(){
    
    if(useAttractor){
        arc = arc->CalculateCurveAttractor(attractor, arc->getMovableControlPoints());
        generateImages();
    }
    
}


GLvoid BCurve3Composite::involveAttractor(GLint index){
    
    _attributes[index]->arc = _attributes[index]->arc->CalculateCurveAttractor(_attributes[index]->attractor, _attributes[index]->arc->getMovableControlPoints());
    _attributes[index]->generateImages();
    
}



GLdouble BCurve3Composite::getAlpha() {
    return _alpha;
}


std::vector<BCurve3Composite::ArcAttributes*>& BCurve3Composite::getAttributes(){
    return _attributes;
}


BCurve3* BCurve3Composite::ArcAttributes::getBCurve(){
    return arc;
}

BCurve3* BCurve3Composite::ArcAttributes::getAttractor(){
    if(useAttractor)
        return attractor;
    return nullptr;
}


BCurve3* BCurve3Composite::getBCurve(GLint index){
    return _attributes[index]->arc;
}

BCurve3* BCurve3Composite::getAttractor(GLint index){
    //if (_attributes[index]->useAttractor)
        return _attributes[index]->attractor;
    //return nullptr;
}

GLboolean BCurve3Composite::getShowUseAttractor(GLint index){
    return _attributes[index]->useAttractor;
}

GLint BCurve3Composite::getselectedPIndex(GLint index_of_arc) {
    return _attributes[index_of_arc]->getSelectedPIndex();
}

void BCurve3Composite::deleteArc(int indexOfArc)
{
    if (_attributes.size() != 0)
    {
        
        
        if (_attributes[indexOfArc] -> arc != nullptr)
        {
            delete _attributes[indexOfArc] -> arc;
        }
        
        if (_attributes[indexOfArc]->image != nullptr)
        {
            delete _attributes[indexOfArc] -> image;
        }
        
        if (_attributes[indexOfArc] -> attractor != nullptr)
        {
            delete _attributes[indexOfArc] -> attractor;
        }
        
        if (_attributes[indexOfArc]->img_attractor != nullptr)
        {
            delete _attributes[indexOfArc] -> img_attractor;
        }
        
        //        if (_attributes[indexOfArc] -> modified_bcurve != nullptr)
        //        {
        //            delete _attributes[indexOfArc] -> modified_bcurve;
        //        }
        
        //        if (_attributes[indexOfArc]->img_modified_bcurve != nullptr)
        //        {
        //            delete _attributes[indexOfArc] -> img_modified_bcurve;
        //        }
        
        if (indexOfArc >= 0 && indexOfArc < (GLint) _attributes.size()) {
            _attributes.erase(_attributes.begin() + indexOfArc);
        }
        
        
    }
}

void BCurve3Composite::setAlpha(double value)
{
    _alpha = value;
    for (GLuint i = 0; i < _attributes.size(); i++)
    {
        double u_min, u_max;
        BCurve3* selectedArc = _attributes[i]->arc;
        selectedArc -> SetAlpha(value);
        selectedArc -> GetDefinitionDomain(u_min, u_max);
        selectedArc -> SetDefinitionDomain(u_min, _alpha);
    }
}

GLint* BCurve3Composite::getIndexOfJoinBCurve(GLint index){
    return _attributes[index]->index_of_join_bcurve;
}

GLvoid BCurve3Composite::setIndexOfJoinBCurve(GLint index, GLint index_of_join_curve, GLint direction){
    _attributes[index]->index_of_join_bcurve[direction] = index_of_join_curve;
}

GLint* BCurve3Composite::getOrderOfJoinBCurve(GLint index){
    return _attributes[index]->order_of_join_bcurve;
    
}

GLvoid BCurve3Composite::setOrderOfJoinBCurve(GLint index, GLint order, GLint direction){
    _attributes[index]->order_of_join_bcurve[direction] = order;
}

GLint* BCurve3Composite::getDirectionOfJoinBCurve(GLint index){
    return _attributes[index]->direction_of_join_bcurve;
    
}
GLvoid BCurve3Composite::setDirectionOfJoinBCurve(GLint index, GLint direction, GLint x){
    _attributes[index]->direction_of_join_bcurve[x] = direction;
}


GLvoid BCurve3Composite::recalculateJoin(GLint index){

    if(_attributes[index]->index_of_join_bcurve[0] != -1){
        ArcAttributes* attr = _attributes[_attributes[index]->index_of_join_bcurve[0]];
        BCurve3* joined_curve = attr->arc;
//        joined_curve->setWeights(attr->arc->getWeights());
        joinArcs(joined_curve, attr->index_of_join_bcurve[0], attr->index_of_join_bcurve[1], attr->direction_of_join_bcurve[0], attr->direction_of_join_bcurve[1], attr->order_of_join_bcurve[0], attr->order_of_join_bcurve[1], GL_TRUE);
    }

    if(_attributes[index]->index_of_join_bcurve[1] != -1){
        ArcAttributes* attr = _attributes[_attributes[index]->index_of_join_bcurve[1]];

        BCurve3* bcurve;
        joinArcs(bcurve, attr->index_of_join_bcurve[0], attr->index_of_join_bcurve[1], attr->direction_of_join_bcurve[0], attr->direction_of_join_bcurve[1], attr->order_of_join_bcurve[0], _attributes[index]->index_of_join_bcurve[1], GL_TRUE);
    }
}

GLvoid BCurve3Composite::recalculateMerge(GLint index){
    ArcAttributes* mergeAttr = _attributes[index];
    if(mergeAttr->index_of_join_bcurve[0] != -1){
        ArcAttributes* fixedAttr = _attributes[mergeAttr->index_of_join_bcurve[0]];
        mergeArcs(mergeAttr->index_of_join_bcurve[0],
                index,
                mergeAttr->direction_of_join_bcurve[0],
                fixedAttr->direction_of_join_bcurve[1],
                mergeAttr->order_of_join_bcurve[0]);
    }
    if(mergeAttr->index_of_join_bcurve[1] != -1){
        ArcAttributes* fixedAttr = _attributes[mergeAttr->index_of_join_bcurve[1]];
        mergeArcs(mergeAttr->index_of_join_bcurve[1],
                index,
                mergeAttr->direction_of_join_bcurve[1],
                fixedAttr->direction_of_join_bcurve[0],
                mergeAttr->order_of_join_bcurve[1]);
    }
}



GLboolean BCurve3Composite::joinArcs(BCurve3* &joinedArc, int indexOfFirstArc, int indexOfSecondArc, int directionOfFirstArc, int directionOfSecondArc, GLint r1, GLint r2, GLboolean modify) {
    //    BCurve3* joinedArc;
    
    if (indexOfFirstArc == -1 || indexOfSecondArc == -1) {
        return GL_FALSE;
    }
        
    
    // firstArcComposite = new BCurve3Composite();
    
    BCurve3 *firstArc = getBCurve(indexOfFirstArc);
    BCurve3 *secondArc = getBCurve(indexOfSecondArc);
    
    joinedArc = new BCurve3(Basis::Type(0), 4, firstArc->getAlpha());
    
    if (r1 + r2 + 2 > (GLint) joinedArc->getN()*2+1 ) {
        return GL_FALSE;
    }
    
    //right right
    if(directionOfFirstArc == 0 && directionOfSecondArc == 0){
        (*joinedArc)[0] = (*firstArc)[firstArc->getN()*2];
        (*joinedArc)[joinedArc->getN()*2] = (*secondArc)[secondArc->getN()*2];
    } else if(directionOfFirstArc == 0 && directionOfSecondArc == 1) {
        //right left
        (*joinedArc)[0] = (*firstArc)[firstArc->getN()*2];
        (*joinedArc)[joinedArc->getN()*2] = (*secondArc)[0];
    } else if (directionOfFirstArc == 1 && directionOfSecondArc == 0 ) {
        // left right
        (*joinedArc)[0] = (*firstArc)[0];
        (*joinedArc)[joinedArc->getN()*2] = (*secondArc)[secondArc->getN()*2];
    } else {
        //left left
        (*joinedArc)[0] = (*firstArc)[0];
        (*joinedArc)[joinedArc->getN()*2] = (*secondArc)[0];
        
        
    }
    
    //    GLdouble step = (joinedArc->getAlpha()) / (joinedArc->getN()*2+1);
    GLdouble step = joinedArc->getAlpha() / (joinedArc->getN()*2+1);
    
    GLdouble u = 0.0;
    for (GLuint i = 1; i < joinedArc->getN()*2; i++)
    {
        u += step;
        DCoordinate3 &cp = (*joinedArc)[i];
        cp.x() = cos(u) + this -> getSize();
        cp.y() = -2 + sin(u);
        cp.z() = 0.0;
    }
    
    
    RowMatrix<GLdouble> F_firstArc;
    Matrix<GLdouble> dF_firstArc;
    
    firstArc->BlendingFunctionValues(firstArc->getAlpha(), F_firstArc);
    firstArc->BlendingFunctionDerivatives(r2, F_firstArc, dF_firstArc);
    
    RowMatrix<GLdouble> G_firstArc;
    Matrix<GLdouble> dG_firstArc;
    
    firstArc->BlendingFunctionValues(0.0, G_firstArc);
    firstArc->BlendingFunctionDerivatives(r2, G_firstArc, dG_firstArc);
    
    RowMatrix<GLdouble> F_joinedArc;
    Matrix<GLdouble> dF_joinedArc;
    
    joinedArc->BlendingFunctionValues(joinedArc->getAlpha(), F_joinedArc);
    joinedArc->BlendingFunctionDerivatives(r1, F_joinedArc, dF_joinedArc);
    
    RowMatrix<GLdouble> G_joinedArc;
    Matrix<GLdouble> dG_joinedArc;
    
    joinedArc->BlendingFunctionValues(0.0, G_joinedArc);
    joinedArc->BlendingFunctionDerivatives(r2, G_joinedArc, dG_joinedArc);
    
    RowMatrix<GLdouble> G_secondArc;
    Matrix<GLdouble> dG_secondArc;
    
    secondArc->BlendingFunctionValues(0.0, G_secondArc);
    secondArc->BlendingFunctionDerivatives(r1, G_secondArc, dG_secondArc);
    
    RowMatrix<GLdouble> F_secondArc;
    Matrix<GLdouble> dF_secondArc;
    
    secondArc->BlendingFunctionValues(secondArc->getAlpha(), F_secondArc);
    secondArc->BlendingFunctionDerivatives(r1, F_secondArc, dF_secondArc);
    
    for(GLuint i = 1; i < joinedArc->getN()*2; i++){
        joinedArc->addMovableControlPoint(i);
    }
    
    
    for(GLint rho = 1; rho <= r1; rho++) {
        //     if(rho <= r1) {
        
        //left
        if(directionOfSecondArc == 1) {
            DCoordinate3 left_sum;
            for (GLint j = 0; j <= rho; j++)
            {
                left_sum += (*secondArc)[j] * dG_secondArc(rho, j);
                //    left_sum += (*secondArc)[2*secondArc->getN()-j] * dG_secondArc(rho, 2*secondArc->getN()-j);
                
            }
            
            DCoordinate3 right_sum;
            for (GLint j = 0; j < rho; j++)
            {
                right_sum += (*joinedArc)[2*joinedArc->getN()-j] * dF_joinedArc(rho, 2*joinedArc->getN()-j);
            }
            
            (*joinedArc)[joinedArc->getN()*2 - rho] = (left_sum-right_sum) / dF_joinedArc(rho, joinedArc->getN()*2-rho);
            joinedArc->removeMovableControlPoint(joinedArc->getN()*2 - rho);
            //       }
            
        }else {
            //right
            DCoordinate3 left_sum;
            for (GLint j = 0; j <= rho; j++)
            {
                //  left_sum += (*secondArc)[j] * dG_secondArc(rho, j);
                left_sum += (*secondArc)[2*secondArc->getN()-j] * dF_secondArc(rho, 2*secondArc->getN()-j);
                
            }
            
            if(rho % 2 != 0){
                left_sum = -left_sum;
            }
            
            
            DCoordinate3 right_sum;
            for (GLint j = 0; j < rho; j++)
            {
                right_sum += (*joinedArc)[2*joinedArc->getN()-j] * dF_joinedArc(rho, 2*joinedArc->getN()-j);
            }
            
            (*joinedArc)[joinedArc->getN()*2 - rho] = (left_sum-right_sum) / dF_joinedArc(rho, joinedArc->getN()*2-rho);
            joinedArc->removeMovableControlPoint(joinedArc->getN()*2 - rho);
            
            //       }
            
        }
    }
    
    
    for(GLint rho = 1; rho <= r2; rho++) {
        //       if(rho <= r2){
        
        //right
        if(directionOfFirstArc == 0) {
            DCoordinate3 left_sum;
            for (GLint j = 0; j <= rho; j++)
            {
                left_sum += (*firstArc)[2*firstArc->getN()-j] * dF_firstArc(rho, 2*firstArc->getN()-j);
                //                left_sum += (*firstArc)[j] * dF_firstArc(rho, j);
                
            }
            
            DCoordinate3 right_sum;
            for (GLint j = 0; j < rho; j++)
            {
                right_sum += (*joinedArc)[j] * dG_joinedArc(rho, j);
            }
            
            (*joinedArc)[rho] = (left_sum-right_sum) / dG_joinedArc(rho, rho);
            joinedArc->removeMovableControlPoint(rho);
            
        } else {
            //left
            DCoordinate3 left_sum;
            for (GLint j = 0; j <= rho; j++) {
                
                //                left_sum += (*firstArc)[2*firstArc->getN()-j] * dF_firstArc(rho, 2*firstArc->getN()-j);
                left_sum += (*firstArc)[j] * dG_firstArc(rho, j);
                
            }
            
            if(rho % 2 != 0){
                left_sum = -left_sum;
            }
            
            DCoordinate3 right_sum;
            for (GLint j = 0; j < rho; j++)
            {
                right_sum += (*joinedArc)[j] * dG_joinedArc(rho, j);
            }
            
            (*joinedArc)[rho] = (left_sum-right_sum) / dG_joinedArc(rho, rho);
            joinedArc->removeMovableControlPoint(rho);
            
        }
    }
    // }
    
    
    //*****INNENTOL
    
    
    joinedArc->CalculateGlobalMin();
    
    
    //    if(getIndexOfJoinBCurve(indexOfFirstArc)[directionOfFirstArc] == getIndexOfJoinBCurve(indexOfSecondArc)[directionOfSecondArc] && getIndexOfJoinBCurve(indexOfSecondArc)[directionOfSecondArc] != -1 ) {
    if(modify){
        setBCurve(getIndexOfJoinBCurve(indexOfFirstArc)[0], joinedArc);

        setOrderOfJoinBCurve(getIndexOfJoinBCurve(indexOfFirstArc)[0], r1, 0);
        setOrderOfJoinBCurve(getIndexOfJoinBCurve(indexOfFirstArc)[0], r2, 1);

        setDirectionOfJoinBCurve(getIndexOfJoinBCurve(indexOfFirstArc)[0], directionOfFirstArc, 0);
        setDirectionOfJoinBCurve(getIndexOfJoinBCurve(indexOfFirstArc)[0], directionOfSecondArc, 1);
        
//        setOrderOfJoinBCurve(getIndexOfJoinBCurve(indexOfFirstArc)[directionOfFirstArc], r1, 0);
//        setOrderOfJoinBCurve(getIndexOfJoinBCurve(indexOfFirstArc)[directionOfFirstArc], r2, 1);
        
//        setDirectionOfJoinBCurve(getIndexOfJoinBCurve(indexOfFirstArc)[directionOfFirstArc], directionOfFirstArc, 0);
//        setDirectionOfJoinBCurve(getIndexOfJoinBCurve(indexOfFirstArc)[directionOfFirstArc], directionOfSecondArc, 1);
        
    } else {
        insertNewArc(joinedArc);
        
        setIndexOfJoinBCurve(indexOfFirstArc,getSize()-1, 0);
        setIndexOfJoinBCurve(indexOfSecondArc,getSize()-1, 1);
//        setIndexOfJoinBCurve(indexOfFirstArc,getSize()-1, directionOfFirstArc);
//        setIndexOfJoinBCurve(indexOfSecondArc,getSize()-1, directionOfSecondArc);
        
        setIndexOfJoinBCurve(getSize()-1, indexOfFirstArc, 0);
        setIndexOfJoinBCurve(getSize()-1, indexOfSecondArc, 1);
        
        
        setOrderOfJoinBCurve(getSize()-1, r1, 0);
        setOrderOfJoinBCurve(getSize()-1, r2, 1);
        
        setDirectionOfJoinBCurve(getSize()-1,directionOfFirstArc, 0);
        setDirectionOfJoinBCurve(getSize()-1, directionOfSecondArc, 1);
        
    }
    
    
    
    
    
    //    delete firstArc;
    //    delete secondArc;
    
    
    //    delete joinedArc;
    
    return GL_TRUE;
}


GLvoid BCurve3Composite::mergeArcsRight(GLint indexOfFixedArc, GLint indexOfMergeArc, const Matrix<GLdouble>& dF_fixedArc, const Matrix<GLdouble>& dG_fixedArc, const Matrix<GLdouble>& dF_mergeArc, GLint directionOfFixedArc, GLint r) {

    BCurve3 *fixedArc = getBCurve(indexOfFixedArc);
    BCurve3 *mergeArc = getBCurve(indexOfMergeArc);

    // "fixed" is on the right of "merge"
    setIndexOfJoinBCurve(indexOfMergeArc, indexOfFixedArc, 0); // 0 = right
    // "merge" is on the left of "fixed"
    setIndexOfJoinBCurve(indexOfFixedArc, indexOfMergeArc, 1); // 1 = left

    // the order is r on the right of "merge"
    setOrderOfJoinBCurve(indexOfMergeArc, r, 0);
    // the order is 0 on the left of "fixed"
    setOrderOfJoinBCurve(indexOfFixedArc, 0, 1);

    // direction is "directionOfFixedArc" on the right of "merge"
    setDirectionOfJoinBCurve(indexOfMergeArc, directionOfFixedArc, 0);
    // direction is right on the left of "fixed"
    setDirectionOfJoinBCurve(indexOfFixedArc, 0, 1);

    switch (directionOfFixedArc) {
    // right
    case 0:
        (*mergeArc)[mergeArc->getN()*2] = (*fixedArc)[fixedArc->getN()*2];
        break;
        // left
    case 1:
        (*mergeArc)[mergeArc->getN()*2] = (*fixedArc)[0];
        break;
    }

    for(GLint rho = 1; rho <= r; rho++) {

        //left
        if(directionOfFixedArc == 1) {
            DCoordinate3 left_sum;
            for (GLint j = 0; j <= rho; j++)
            {
                left_sum += (*fixedArc)[j] * dG_fixedArc(rho, j);
            }

            DCoordinate3 right_sum;
            for (GLint j = 0; j < rho; j++)
            {
                right_sum += (*mergeArc)[2*mergeArc->getN()-j] * dF_mergeArc(rho, 2*mergeArc->getN()-j);
            }

            (*mergeArc)[mergeArc->getN()*2 - rho] = (left_sum-right_sum) / dF_mergeArc(rho, mergeArc->getN()*2-rho);
            //            mergeArc->removeMovableControlPoint(mergeArc->getN()*2 - rho);

        } else {
            //right
            DCoordinate3 left_sum;
            for (GLint j = 0; j <= rho; j++)
            {
                left_sum += (*fixedArc)[2*fixedArc->getN()-j] * dF_fixedArc(rho, 2*fixedArc->getN()-j);
            }

            if(rho % 2 != 0){
                left_sum = -left_sum;
            }


            DCoordinate3 right_sum;
            for (GLint j = 0; j < rho; j++)
            {
                right_sum += (*mergeArc)[2*mergeArc->getN()-j] * dF_mergeArc(rho, 2*mergeArc->getN()-j);
            }

            (*mergeArc)[mergeArc->getN()*2 - rho] = (left_sum-right_sum) / dF_mergeArc(rho, mergeArc->getN()*2-rho);
            //            mergeArc->removeMovableControlPoint(mergeArc->getN()*2 - rho);
        }
    }
}



GLvoid BCurve3Composite::mergeArcsLeft(GLint indexOfFixedArc, GLint indexOfMergeArc, const Matrix<GLdouble>& dF_fixedArc, const Matrix<GLdouble>& dG_fixedArc, const Matrix<GLdouble>& dG_mergeArc, GLint directionOfFixedArc, GLint r) {

    BCurve3 *fixedArc = getBCurve(indexOfFixedArc);
    BCurve3 *mergeArc = getBCurve(indexOfMergeArc);

    // "fixed" is on the left of "merge"
    setIndexOfJoinBCurve(indexOfMergeArc, indexOfFixedArc, 1); // 1 = left
    // "merge" is on the right of "fixed"
    setIndexOfJoinBCurve(indexOfFixedArc, indexOfMergeArc, 0); // 0 = right

    // the order is r on the left of "merge"
    setOrderOfJoinBCurve(indexOfMergeArc, r, 1);
    // the order is 0 on the right of "fixed"
    setOrderOfJoinBCurve(indexOfFixedArc, 0, 0);

    // direction is "directionOfFixedArc" on the left of "merge"
    setDirectionOfJoinBCurve(indexOfMergeArc, directionOfFixedArc, 1);
    // direction is left on the right of "fixed"
    setDirectionOfJoinBCurve(indexOfFixedArc, 1, 0);

    switch (directionOfFixedArc) {
    // right
    case 0:
        (*mergeArc)[0] = (*fixedArc)[fixedArc->getN()*2];
        break;
        // left
    case 1:
        (*mergeArc)[0] = (*fixedArc)[0];
        break;
    }


    for(GLint rho = 1; rho <= r; rho++) {
        //       if(rho <= r2){

        //right
        if(directionOfFixedArc == 0) {
            DCoordinate3 left_sum;
            for (GLint j = 0; j <= rho; j++)
            {
                left_sum += (*fixedArc)[2*fixedArc->getN()-j] * dF_fixedArc(rho, 2*fixedArc->getN()-j);
                //                left_sum += (*firstArc)[j] * dF_firstArc(rho, j);

            }

            DCoordinate3 right_sum;
            for (GLint j = 0; j < rho; j++)
            {
                right_sum += (*mergeArc)[j] * dG_mergeArc(rho, j);
            }

            (*mergeArc)[rho] = (left_sum-right_sum) / dG_mergeArc(rho, rho);
            //            mergeArc->removeMovableControlPoint(rho);

        } else {
            //left
            DCoordinate3 left_sum;
            for (GLint j = 0; j <= rho; j++) {

                //                left_sum += (*firstArc)[2*firstArc->getN()-j] * dF_firstArc(rho, 2*firstArc->getN()-j);
                left_sum += (*fixedArc)[j] * dG_fixedArc(rho, j);

            }

            if(rho % 2 != 0){
                left_sum = -left_sum;
            }

            DCoordinate3 right_sum;
            for (GLint j = 0; j < rho; j++)
            {
                right_sum += (*mergeArc)[j] * dG_mergeArc(rho, j);
            }

            (*mergeArc)[rho] = (left_sum-right_sum) / dG_mergeArc(rho, rho);
            //            mergeArc->removeMovableControlPoint(rho);

        }
    }
}



GLvoid BCurve3Composite::mergeArcs(GLint indexOfFixedArc, GLint indexOfMergeArc, GLint directionOfFixedArc, GLint directionOfMergeArc, GLint r) {

    BCurve3 *fixedArc = getBCurve(indexOfFixedArc);
    BCurve3 *mergeArc = getBCurve(indexOfMergeArc);

    RowMatrix<GLdouble> G_fixedArc;
    Matrix<GLdouble> dG_fixedArc;

    fixedArc->BlendingFunctionValues(0.0, G_fixedArc);
    fixedArc->BlendingFunctionDerivatives(r, G_fixedArc, dG_fixedArc);

    RowMatrix<GLdouble> F_fixedArc;
    Matrix<GLdouble> dF_fixedArc;

    fixedArc->BlendingFunctionValues(fixedArc->getAlpha(), F_fixedArc);
    fixedArc->BlendingFunctionDerivatives(r, F_fixedArc, dF_fixedArc);

    RowMatrix<GLdouble> F_mergeArc;
    Matrix<GLdouble> dF_mergeArc;

    mergeArc->BlendingFunctionValues(mergeArc->getAlpha(), F_mergeArc);
    mergeArc->BlendingFunctionDerivatives(r, F_mergeArc, dF_mergeArc);

    RowMatrix<GLdouble> G_mergeArc;
    Matrix<GLdouble> dG_mergeArc;

    mergeArc->BlendingFunctionValues(0.0, G_mergeArc);
    mergeArc->BlendingFunctionDerivatives(r, G_mergeArc, dG_mergeArc);

    switch (directionOfMergeArc) {
    // right
    case 0:
        mergeArcsRight(indexOfFixedArc, indexOfMergeArc, dF_fixedArc, dG_fixedArc, dF_mergeArc, directionOfFixedArc, r);
        break;

        // left
    case 1:
        mergeArcsLeft(indexOfFixedArc, indexOfMergeArc, dF_fixedArc, dG_fixedArc, dG_mergeArc, directionOfFixedArc, r);
        break;
    }

    _attributes[indexOfMergeArc]->generateImages();
}




GLuint BCurve3Composite::getSize(){
    return (GLuint) _attributes.size();
}

GLboolean BCurve3Composite::SetColor(GLuint indexOfArc, Color4* c)
{
    if (indexOfArc >= _attributes.size() || !_attributes[indexOfArc]->arc)
    {
        return GL_FALSE;
    }
    Color4* color = new Color4(c->r(), c->g(), c->b(), c->a());
    _attributes[indexOfArc] -> color = color;

    return GL_TRUE;
}

Color4* BCurve3Composite::getColor(GLuint indexOfArc)
{
    Color4* color = new Color4(_attributes[indexOfArc] -> color->r(), _attributes[indexOfArc] -> color->g(), _attributes[indexOfArc] -> color->b(), _attributes[indexOfArc] -> color->a());
    return color;
}

GLvoid BCurve3Composite::ArcAttributes::generateImages(GLdouble scale) {
    
    arc -> UpdateVertexBufferObjectsOfData();
    if(!arc)
    {
        cout << "error : insertNewArc -> attribute -> arc" << endl;
    }
    image = nullptr;
    image = arc -> GenerateImage(2, 101, GL_STATIC_DRAW);
    if(!image)
    {
        cout << "error : insertNewArc -> attribute -> image" << endl;
    }
    if(!image -> UpdateVertexBufferObjects(scale))
    {
        cout << "Could_not_update_the_vertex_buffer_object_of_the_curve!" << endl;
    }
    
    
    attractor -> UpdateVertexBufferObjectsOfData();
    if(!attractor)
    {
        cout << "error : insertNewArc -> attribute -> arc" << endl;
    }
    img_attractor = nullptr;
    img_attractor = attractor -> GenerateImage(2, 101, GL_STATIC_DRAW);
    if(!img_attractor)
    {
        cout << "error : insertNewArc -> attribute -> image" << endl;
    }
    if(!img_attractor -> UpdateVertexBufferObjects(scale))
    {
        cout << "Could_not_update_the_vertex_buffer_object_of_the_curve!" << endl;
    }
    
}


GLvoid BCurve3Composite::setBCurve(GLint index, BCurve3* bcurve){
    _attributes[index]->arc = bcurve;
    
    _attributes[index]->arc->SetOrginalCp();
    
    
    _attributes[index]->arc -> UpdateVertexBufferObjectsOfData();
    if(! _attributes[index]->arc)
    {
        cout << "error : insertNewArc -> attribute -> arc" << endl;
    }
    _attributes[index]->image = nullptr;
    _attributes[index]->image =  _attributes[index]->arc -> GenerateImage(2, 101, GL_STATIC_DRAW);
    if(! _attributes[index]->image)
    {
        cout << "error : insertNewArc -> attribute -> image" << endl;
    }
    if(! _attributes[index]->image -> UpdateVertexBufferObjects())
    {
        cout << "Could_not_update_the_vertex_buffer_object_of_the_curve!" << endl;
    }
}

GLboolean BCurve3Composite::RenderPoints(GLint indexOfArc, GLint x, Color4* color){
    BCurve3* curve;
    
    switch (x) {
    case 0:
        curve = getBCurve(indexOfArc);
        break;
    case 1:
        curve = getAttractor(indexOfArc);
        break;
    }
    
    GLint size = curve->getN()*2+1;
    
    GLint p_actual_hermite = getselectedPIndex(indexOfArc);
    
    for(GLint i =0; i< size; i++) {
        
        DCoordinate3 point = curve->getControlPoint(i);
        
        
        glPointSize(10.0);
        glEnable(GL_POINT_SMOOTH);
        if(_isHermite && _selected == -1){
            if(i == 0)
                glLoadName(indexOfArc);
            else if(i == size - 1)
                glLoadName(indexOfArc + 1);
        } else{
            if(x==0)
                glLoadName(i);
            else if(x==1)
                glLoadName(i*100);
        }
        
        
        
        glBegin (GL_POINTS);
        
        if(curve->isMovableControlPoint(i)){
            //            glColor3f(1.0f, 1.0f, 0.0f);
//            glColor4f(0.251f, 0.878f, 0.816f, 0.0f);
            glColor4f(colors::light_red.r(), colors::light_red.g(), colors::light_red.b(), colors::light_red.a());
        } else {
            if(_isHermite && _selected == -1 && (i == 0 || i == size - 1)){
                //            if(_isHermite && _selected == -1 && (i == 0)){

                if(selected_p == indexOfArc ){
                    //                    cout << "itt " << selected_p << endl;
                    //                    glColor3f(0.0f, 1.0f, 0.0f);

//                    glColor4f(0.439f, 0.502f, 0.56f, 0.0f);
                    glColor3f(0.1f, 0.1f, 0.1f); // grey

                }else
                    glColor3f(0.1f, 0.1f, 0.1f); // grey
                //                    glColor3f(0.0f, 0.5f, 0.5f);
            } else
                
                //            glColor4f(1.0f, 0.0f, 0.0f, 0.0f);
                if(getShowUseAttractor(indexOfArc) && x==1){
                    glColor3f(0.180f, 0.545f, 0.341f);
                }
                else if (currentOrPreviousSelected(indexOfArc) && i == 0 && _isHermite && p_actual_hermite == 0 && x == 0)
                    //            glColor4f(1.0f, 0.55f, 0.0f, 0.0f);
                    glColor4f(0.439f, 0.502f, 0.56f, 0.0f);
            
                else if (currentOrNextSelected(indexOfArc) && i == size - 1 && _isHermite && p_actual_hermite == size - 1  && x == 0)
                    //            glColor4f(1.0f, 0.55f, 0.0f, 0.0f);
                    glColor4f(0.439f, 0.502f, 0.56f, 0.0f);
            
                else if (currentOrPreviousSelected(indexOfArc) && i == 0 && _isHermite && x == 0)
                    glColor3f(0.1f, 0.1f, 0.1f); // grey
                else if (currentOrNextSelected(indexOfArc) && i == size - 1 && _isHermite && x == 0)
                    glColor3f(0.1f, 0.1f, 0.1f); // grey
                else if(_selected == indexOfArc) {
                    glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
                } else if ((i == 0 || i == size - 1) && _isHermite && x == 0)
                    glColor3f(0.1f, 0.1f, 0.1f); // grey
                else
                    glColor4f(_attributes[indexOfArc] ->color->r(), _attributes[indexOfArc] ->color->g(), _attributes[indexOfArc] ->color->b(), _attributes[indexOfArc] ->color->a());
            
            if(_selected2 == indexOfArc)
                glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
            
            //            if(color){
            //                glColor4f(color->r(), color->g(), color->b(), color->a());
            
            //            } else {
            //                glColor4f(_attributes[indexOfArc] ->color->r(), _attributes[indexOfArc] ->color->g(), _attributes[indexOfArc] ->color->b(), _attributes[indexOfArc] ->color->a());
            //            }
            //            glColor3f(1.0f, 0.0f, 0.0f);
        }
        
        
        
        
        //    if(curve->isMovableControlPoint(i)){
        //        glColor3f(1.0f, 1.0f, 0.0f);
        //    } else {
        //        //            glColor4f(1.0f, 0.0f, 0.0f, 0.0f);
        //        if(getShowUseAttractor(indexOfArc) && x==1){
        //            glColor3f(1.0f, 0.0f, 0.0f);
        //        } else if (currentOrPreviousSelected(indexOfArc) && i == 0 && _isHermite && p_actual_hermite == 0 && x == 0)
        //            glColor4f(1.0f, 0.55f, 0.0f, 0.0f);
        //        else if (currentOrNextSelected(indexOfArc) && i == size - 1 && _isHermite && p_actual_hermite == size - 1  && x == 0)
        //            glColor4f(1.0f, 0.55f, 0.0f, 0.0f);
        //        else if (currentOrPreviousSelected(indexOfArc) && i == 0 && _isHermite && x == 0)
        //            glColor4f(1.0f, 0.0f, 0.0f, 0.0f);
        //        else if (currentOrNextSelected(indexOfArc) && i == size - 1 && _isHermite && x == 0)
        //            glColor4f(1.0f, 0.0f, 0.0f, 0.0f);
        //        else if(_selected == indexOfArc) {
        //            glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
        //        } else if ((i == 0 || i == size - 1) && _isHermite && x == 0)
        //            glColor4f(0.87f, 0.12f, 0.87f, 0.0f);
        //        else
        //            glColor4f(_attributes[indexOfArc] ->color->r(), _attributes[indexOfArc] ->color->g(), _attributes[indexOfArc] ->color->b(), _attributes[indexOfArc] ->color->a());
        
        //        if(_selected2 == indexOfArc)
        //            glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
        
        //        //            if(color){
        //        //                glColor4f(color->r(), color->g(), color->b(), color->a());
        
        //        //            } else {
        //        //                glColor4f(_attributes[indexOfArc] ->color->r(), _attributes[indexOfArc] ->color->g(), _attributes[indexOfArc] ->color->b(), _attributes[indexOfArc] ->color->a());
        //        //            }
        //        //            glColor3f(1.0f, 0.0f, 0.0f);
        //    }
        
        
        glVertex3d (point.x(), point.y(), point.z());
        glEnd ();
        
        
        glDisable(GL_POINT_SMOOTH);
        
    }
    
    
    return GL_TRUE;
}


GLboolean BCurve3Composite::getIsHermite() {
    return _isHermite;
}

GLvoid BCurve3Composite::setIsHermite(GLboolean isHermite) {
    _isHermite = isHermite;
}

GLvoid BCurve3Composite::ArcAttributes::setEnergy(){
    if(useAttractor)
        energy = arc->CalculateEnergyFunctional(attractor);
    else
        energy = arc->CalculateEnergyFunctional();
}


GLdouble BCurve3Composite::ArcAttributes::getEnergy(){
    return energy;
}

GLvoid BCurve3Composite::ArcAttributes::renderTangent(){
    glColor3f(0.0f, 0.0f, 0.0f);
    
    GLuint n = image->GetPointCount();
    for(GLuint i = 0; i<n;i++){
        glBegin(GL_LINES);
        {
            glVertex3dv(&(*image)(0, i)[0]);
            DCoordinate3 sum = (*image)(0, i) + (*image)(1, i);
            glVertex3dv(&sum[0]);
        }
        glEnd();
    }
}

GLvoid BCurve3Composite::ArcAttributes::setIncreaseOrder(GLint i){
    arc = arc->IncreaseOrder(i);
    
    //     srand(time(0));
    //    BCurve3* increaseorder;
    //     for(GLint i = 1; i<= 4;i++) {
    //        Color4* color = new Color4(rand()%100*0.01f, rand()%100*0.01f, rand()%100*0.01f, 0.0f);
    //       increaseorder= arc->IncreaseOrder(i);
    //            _bcurvess->insertNewArc(increaseorder);
    //            delete color;
    //        }
    //    delete increaseorder;
    
}

GLvoid BCurve3Composite::setColor(GLint i){
    
    if(_selected == i || _selected2 == i) {
        glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
    } else
        glColor4f(_attributes[i] ->color->r(), _attributes[i] ->color->g(), _attributes[i] ->color->b(), _attributes[i] ->color->a());
}


GLvoid BCurve3Composite::setSelected(GLint selected){
    _selected = selected;
}

GLint BCurve3Composite::getSelected(){
    return _selected;
}

GLvoid BCurve3Composite::setSelected2(GLint selected2){
    _selected2 = selected2;
}

GLint BCurve3Composite::getSelected2(){
    return _selected2;
}


GLvoid BCurve3Composite::ArcAttributes::setSelectedPIndex(GLint index){
    selected_p_index = index;
}
GLint BCurve3Composite::ArcAttributes::getSelectedPIndex(){
    return selected_p_index;
}

GLvoid BCurve3Composite::deleteAllArcAttributes() {
    for(GLint i = 0; i < (GLint) _attributes.size(); i++)
        delete _attributes[i];
    _attributes.clear();
    _selected = -1;
    _selected2 = -2;
}

GLboolean BCurve3Composite::currentOrPreviousSelected(GLint index) {
    if (_selected == index)
        return true;
    if (index > 0)
        if (_selected == index - 1)
            return true;
    return false;
}

GLboolean BCurve3Composite::currentOrNextSelected(GLint index) {
    if (_selected == index)
        return true;
    if (index <  2 * (GLint) getBCurve(index)->getN())
        if (_selected == index + 1)
            return true;
    return false;
    
}

GLvoid BCurve3Composite::setSelectedPIndex(GLint indexOfArc, GLint pIndex) {
    if (indexOfArc < 0)
        return;
    GLint oldPIndex = _attributes[indexOfArc]->getSelectedPIndex();
    GLint currentLast = 2 * _attributes[indexOfArc]->getBCurve()->getN();
    _attributes[indexOfArc]->setSelectedPIndex(pIndex);
    if (pIndex == 0 && indexOfArc > 0) {
        GLint previousLast = 2 * _attributes[indexOfArc - 1]->getBCurve()->getN();
        _attributes[indexOfArc - 1]->setSelectedPIndex(previousLast);
    }
    if (pIndex == currentLast && indexOfArc < (GLint) getSize() - 1) {
        _attributes[indexOfArc + 1]->setSelectedPIndex(0);
    }
    if (pIndex != currentLast && oldPIndex == currentLast && indexOfArc < (GLint) getSize() - 1) {
        _attributes[indexOfArc + 1]->setSelectedPIndex(-1);
    }
    if (pIndex != 0 && oldPIndex == 0 && indexOfArc > 0) {
        _attributes[indexOfArc - 1]->setSelectedPIndex(-1);
    }
}

GLboolean  BCurve3Composite::neighborsInSelectedPoint(GLint currentArcIndex, GLint newArcIndex) {
    GLint currentLast = 2 * _attributes[currentArcIndex]->getBCurve()->getN();
    GLint currentPIndex = getselectedPIndex(currentArcIndex);
    if (currentArcIndex == newArcIndex - 1 && currentPIndex == currentLast)
        return true;
    if (currentArcIndex == newArcIndex + 1 && currentPIndex == 0)
        return true;
    return false;
}

GLvoid BCurve3Composite::setSelectedP(GLint index){
    selected_p = index;
}

GLint BCurve3Composite::getSelectedP(){
    return selected_p;
}

GLvoid BCurve3Composite::generateMatlabCode(){
    for(GLint i = 0; i< (GLint) _attributes.size(); i++){
        _attributes[i]->arc->generateMatlabCodeForRendering("importGapFilling.m");
        _attributes[i]->image->generateMatlabCodeForRendering("importGapFilling.m");
        if(_attributes[i]->useAttractor){
            _attributes[i]->attractor->generateMatlabCodeForRendering("importGapFilling.m");
            _attributes[i]->img_attractor->generateMatlabCodeForRendering("importGapFilling.m");
        }
    }
}

