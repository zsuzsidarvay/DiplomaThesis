#pragma once

#include "../Core/Matrices.h"
#include "../Core/LinearCombination3.h"
#include "BasisTypes.h"

namespace cagd
{
    class BCurve3: public LinearCombination3
    {
    protected:
        typedef GLdouble (*_RealValuedRealFunction)(GLdouble);

        Basis::Type                 _type;      // either trigonometric, or hyperbolic
        GLuint                      _n;         // order
        GLdouble                    _alpha;     // shape parameter (or interval length)
        RowMatrix<GLdouble>         _c;         // normalizing coefficients {t_{2n,i}^{\alpha}}_{i = 0}^{2n} or {h_{2n,i}^{\alpha}}_{i = 0}^{2n}
        TriangularMatrix<GLdouble> _bc;         //binomial coefficients
        _RealValuedRealFunction     _S, _C, _T; // pointers to trigonometric/hyperbolic sine, cosine and tangent functions
												//_S = (_type == TRIGONOMETRIC ? (_RealValuedRealFunction)sin : (_RealValuedRealFunction)sinh);
												//_C = (_type == TRIGONOMETRIC ? (_RealValuedRealFunction)cos : (_RealValuedRealFunction)cosh);
												//_T = (_type == TRIGONOMETRIC ? (_RealValuedRealFunction)tan : (_RealValuedRealFunction)tanh);

        GLvoid                      _CalculateBinomialCoefficients(GLuint m, TriangularMatrix<GLdouble> &bc) const;
        GLvoid                      _CalculateNormalizingCoefficient(GLuint n, RowMatrix<GLdouble> &c) const;


    public:
        // special constructor
        BCurve3(Basis::Type type, GLuint n, GLdouble alpha, GLenum data_usage_flag = GL_STATIC_DRAW);

        // calculates a row matrix which consists either of function values {T_{2n,i}^{\alpha}(u)}_{i=0}^{2n} or {H_{2n,i}^{\alpha}(u)}_{i=0}^{2n}
		// for efficiency reasons apply the evaluation formulas derived during the Seminar! 
        GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble>& values) const;

        // calculates the point and its associated (higher) order derivatives either of the linear
        // combination \sum_{i=0}^{2n} d_{i} T_{2n,i}^{\alpha}(u) or \sum_{i=0}^{2n} d_{i} H_{2n,i}^{\alpha}(u) at
        // the user specified parameter value u
		// for efficiency reasons apply the evaluation formulas specified during the Seminar! 
        GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives& d) const;

        // performs subdivision at a given parameter value u
                // [apply eq. (4), Remark 2.1, and eqs. (16)--(17) in R�th__Exact_description__2014_arXiv.pdf]
        RowMatrix<BCurve3*>* Subdivision(GLdouble u, GLenum usage_flag = GL_STATIC_DRAW) const;

        // order elevation from n to n + z [apply formula (6) of R�th_JCAM_Control_point_based_exact_description_2015.pdf]
        BCurve3* IncreaseOrder(GLuint z = 1) const;
    };
}
