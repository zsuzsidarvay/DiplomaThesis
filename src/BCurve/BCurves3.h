#pragma once

#include "../Core/Matrices.h"
#include "../Core/LinearCombination3.h"
#include "BasisTypes.h"
#include "../Core/RealMatrices.h"
#include "../Core/RealSquareMatrices.h"


namespace cagd
{
class BCurve3: public LinearCombination3
{
protected:
    typedef GLdouble (*_RealValuedRealFunction)(GLdouble);

    Basis::Type                 _type;      // either trigonometric, or hyperbolic
    GLuint                      _n;         // order
    GLdouble                    _alpha;     // shape parameter (or interval length)
    RowMatrix<GLdouble>         _c;         // normalizing coefficients {t_{2n,i}^{\alpha}}_{i = 0}^{2n} or {h_{2n,i}^{\alpha}}_{i = 0}^{2n}
    TriangularMatrix<GLdouble> _bc;         //binomial coefficients
    _RealValuedRealFunction     _S, _C, _T; // pointers to trigonometric/hyperbolic sine, cosine and tangent functions

    std::vector< Matrix<GLdouble> > _D;   //collocation matrix
    GLint _div_point_count;               // 2*eta+1
    GLint _max_order_of_derivatives;      // theta

    std::vector<RealSquareMatrix> _A;

    ColumnMatrix<DCoordinate3> b;


    RealSquareMatrix attractor_integrals;

    RowMatrix<GLdouble> weights;

    RowMatrix<GLuint> movable_controlpoints;

    ColumnMatrix<DCoordinate3> original_cp;

    GLvoid _CalculateBinomialCoefficients(GLuint m, TriangularMatrix<GLdouble> &bc) const;
    GLvoid _CalculateNormalizingCoefficient(GLuint n, RowMatrix<GLdouble> &c) const;

    GLdouble _CalculateCombination(GLdouble n, GLdouble k) const;

public:
    // special constructor
    BCurve3(Basis::Type type, GLuint n, GLdouble alpha, GLenum data_usage_flag = GL_STATIC_DRAW);

    // calculates a row matrix which consists either of function values {T_{2n,i}^{\alpha}(u)}_{i=0}^{2n} or {H_{2n,i}^{\alpha}(u)}_{i=0}^{2n}
    // for efficiency reasons apply the evaluation formulas derived during the Seminar!
    GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble>& values) const;

    GLboolean BlendingFunctionDerivatives(GLuint max_order_of_derivatives, RowMatrix<GLdouble>& A, Matrix<GLdouble>& dA) const;

    GLdouble getAlpha() const;

    GLdouble getA(GLint r, GLint i, GLint j);

    GLdouble getD(GLint r, GLint i, GLint j);

    GLuint getN() const;

    Basis::Type getType() const;

    void SetAlpha(GLdouble alpha);

    GLint getMaxOrderOfDerivatives();
    GLvoid SetMaxOrderOfDerivatives(GLint val);

    void SetControlPoint(GLint i, GLdouble alpha, GLdouble beta, GLdouble r, DCoordinate3 _global_min);
    GLvoid SetControlPoints(GLdouble alpha, GLdouble beta, GLdouble r);

    GLvoid SetOrginalCp();

    GLvoid setData(GLint x, GLdouble a);

    GLboolean CalculateCollocationMatrix();

    void CalculateIntegrals();

    GLvoid CalculateIntegrals(BCurve3* attractor);

    // GLdouble CalculateEnergyFunctional(GLint r1, GLint r2);

    GLdouble CalculateEnergyFunctional(BCurve3* attractor = nullptr);

    GLvoid CalculateB(BCurve3* attractor = nullptr);
    GLvoid CalculateB(GLint r1, GLint r2, BCurve3* attractor = nullptr);

    //    GLvoid ModifyUsingGlobalMinimum(GLint r1, GLint r2);
    GLvoid ModifyUsingGlobalMinimum(GLint r1, GLint r2, BCurve3* attractor = nullptr);


    //    BCurve3* CalculateGlobalMinimum(GLint r1, GLint r2);
    BCurve3* CalculateGlobalMinimum(GLint r1, GLint r2, BCurve3* attractor = nullptr);


    //    GLvoid CalculateGlobalMin();
    GLvoid CalculateGlobalMin(BCurve3* attractor = nullptr);

    BCurve3* CalculateCurveAttractor(BCurve3* attractor, RowMatrix<GLuint> index);


    RowMatrix<GLuint> getMovableControlPoints();
    GLvoid setMovableControlPoints(RowMatrix<GLuint> index);
    GLvoid addMovableControlPoint(GLuint index);
    GLvoid removeMovableControlPoint(GLuint index);
    GLboolean isMovableControlPoint(GLuint index);


    GLvoid IdenticalControlPoints();

    GLvoid RestoreControlPoints();


    RowMatrix<GLdouble> getWeights();
    GLvoid setWeights(RowMatrix<GLdouble> weights);
    GLvoid setWeight(GLint index, GLdouble temp);

    // calculates the point and its associated (higher) order derivatives either of the linear
    // combination \sum_{i=0}^{2n} d_{i} T_{2n,i}^{\alpha}(u) or \sum_{i=0}^{2n} d_{i} H_{2n,i}^{\alpha}(u) at
    // the user specified parameter value u
    // for efficiency reasons apply the evaluation formulas specified during the Seminar!
    GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives& d) const;


    // performs subdivision at a given parameter value u
    // [apply eq. (4), Remark 2.1, and eqs. (16)--(17) in Róth__Exact_description__2014_arXiv.pdf]
    RowMatrix<BCurve3*>* Subdivision(GLdouble u, GLenum usage_flag = GL_STATIC_DRAW) const;

    // order elevation from n to n + z [apply formula (6) of Róth_JCAM_Control_point_based_exact_description_2015.pdf]
    BCurve3* IncreaseOrder(GLuint z = 1) const;

    //GLvoid setMovableControlPoint(GLint x, GLdouble a, GLint index);
    GLvoid modifyControlPoint(GLint x, GLdouble a, GLint index);

    DCoordinate3& getControlPoint(GLint i){
        return _data[i];
    }

    GLvoid setMovableControlPoints(GLint x, GLdouble a);

    GLint countMovableControlPoints();

    GLvoid setHermiteMovebleControlPoints(GLint rho);

};
}
