#include "HermiteBCurve3Composite.h"

using namespace cagd;
using namespace std;

HermiteBCurve3Composite::HermiteBCurve3Composite()
{
    m = 5;
    rho = 2;

    p.ResizeColumns(m);

    t.ResizeColumns(m);
    t.ResizeRows(rho);

    n.ResizeColumns(m);

    alpha.ResizeColumns(m);

    type = Basis::TRIGONOMETRIC;

    GLint a = 3, b = 2;

    for(GLuint i = 0; i < m; i++) {
        GLdouble u = PI*i/m;
        u*=2;

        p[i].x() = a*cos(u);
        p[i].y() = b*sin(u);
        p[i].z() = 0.0;

        t(0,i).x() = - a*sin(u);
        t(0,i).y() = b*cos(u);
        t(0,i).z() = 0.0;

        t(1,i).x() = - a*cos(u);
        t(1,i).y() = -b*sin(u);
        t(1,i).z() = 0.0;

        n[i] = 3;

        alpha[i] = PI/3;

    }

    initializeRThetaFi();

    bcurve3composite = new BCurve3Composite(1.0, true);
}

HermiteBCurve3Composite::HermiteBCurve3Composite(RowMatrix<DCoordinate3> p, Matrix<DCoordinate3> t, RowMatrix<GLuint> n,
                                                 RowMatrix<GLdouble> alpha, Basis::Type  type, BCurve3Composite *bcurve3composite)
{
    this->p = p;
    this->t = t;
    this->n = n;
    this->alpha = alpha;
    this->type = type;
    this->bcurve3composite = bcurve3composite;
    m = p.GetColumnCount();
    if (t.GetColumnCount() != m || alpha.GetColumnCount() != m || n.GetColumnCount() != m) {
        throw Exception("Different dimension in HermiteBCurve3Composite constructor");
    }
    rho = t.GetRowCount();

    initializeRThetaFi();
}

HermiteBCurve3Composite::HermiteBCurve3Composite(GLuint m, GLuint rho, GLuint ni, BCurve3Composite *bcurve3composite)
{
    this->m = m;
    this->rho = rho;

    p.ResizeColumns(m);
    t.ResizeColumns(m);
    t.ResizeRows(rho);
    n.ResizeColumns(m);
    alpha.ResizeColumns(m);
    type = Basis::TRIGONOMETRIC;
    this->bcurve3composite = bcurve3composite;

    GLdouble a = 3.0;
    GLdouble step = 2 * a / m;
    GLdouble u = -a;

//    GLuint ni = 5;
    if (rho + 1 > ni)
        ni = rho + 1;

    for(GLuint i = 0; i < m; i++) {
        p[i].x() = u;
        p[i].y() = 0.0;
        p[i].z() = 0.0;

        u += step;

        t(0,i).x() = 1.0;
        t(0,i).y() = 0.0;
        t(0,i).z() = 0.0;

        n[i] = ni;
        alpha[i] = PI/3;
    }

    for(GLuint i = 0; i < m; i++)
        for(GLuint j = 1; j < rho; j++) {
            t(j,i).x() = 0.0;
            t(j,i).y() = 0.0;
            t(j,i).z() = 0.0;
        }

    initializeRThetaFi();
}



BCurve3* HermiteBCurve3Composite::GenerateBCurve3(GLint index){
    BCurve3 *bcurve = new BCurve3(type, n[index], alpha[index]);

    (*bcurve)[0] = p[index];
    (*bcurve)[2*n[index]] = p[index+1];


    RowMatrix<GLdouble> F;
    Matrix<GLdouble> dF;

    bcurve->BlendingFunctionValues(bcurve->getAlpha(), F);
    bcurve->BlendingFunctionDerivatives(rho, F, dF);

    RowMatrix<GLdouble> G;
    Matrix<GLdouble> dG;

    bcurve->BlendingFunctionValues(0.0, G);
    bcurve->BlendingFunctionDerivatives(rho, G, dG);




    for(GLint r = 1; r<= (GLint) rho;r++){


        DCoordinate3 right_sum;
        for (GLint j = 0; j < r; j++)
        {
            right_sum += (*bcurve)[j] * dG(r, j);
        }

        (*bcurve)[r] = (t(r-1,index)-right_sum) / dG(r, r);



        DCoordinate3 right_sum2;
        for (GLint j = 0; j < r; j++)
        {
            right_sum2 += (*bcurve)[2*bcurve->getN()-j] * dF(r, 2*bcurve->getN()-j);
        }

        (*bcurve)[bcurve->getN()*2 - r] = (t(r-1, index+1)-right_sum2) / dF(r, bcurve->getN()*2-r);

    }

    BCurve3* optimal_bcurve = bcurve->CalculateGlobalMinimum(rho+1, rho+1);

    delete bcurve;

    return optimal_bcurve;

}

GLvoid HermiteBCurve3Composite::ModifyBCurve3(GLint index){
    BCurve3 *bcurve =  bcurve3composite->getBCurve(index);

    (*bcurve)[0] = p[index];
    (*bcurve)[2*n[index]] = p[index+1];

    RowMatrix<GLdouble> F;
    Matrix<GLdouble> dF;

    bcurve->BlendingFunctionValues(bcurve->getAlpha(), F);
    bcurve->BlendingFunctionDerivatives(rho, F, dF);

    RowMatrix<GLdouble> G;
    Matrix<GLdouble> dG;

    bcurve->BlendingFunctionValues(0.0, G);
    bcurve->BlendingFunctionDerivatives(rho, G, dG);

    for(GLint r = 1; r<=rho;r++){

        DCoordinate3 right_sum;
        for (GLint j = 0; j < r; j++)
        {
            right_sum += (*bcurve)[j] * dG(r, j);
        }

        (*bcurve)[r] = (t(r-1,index)-right_sum) / dG(r, r);

        DCoordinate3 right_sum2;
        for (GLint j = 0; j < r; j++)
        {
            right_sum2 += (*bcurve)[2*bcurve->getN()-j] * dF(r, 2*bcurve->getN()-j);
        }

        (*bcurve)[bcurve->getN()*2 - r] = (t(r-1, index+1)-right_sum2) / dF(r, bcurve->getN()*2-r);
    }

     bcurve->ModifyUsingGlobalMinimum(rho+1, rho+1, bcurve3composite->getAttractor(index));
}


void HermiteBCurve3Composite::GenerateOptimalHermiteComposite(){
    //    Color4* color = new Color4(rand()%100*0.01f, rand()%100*0.01f, rand()%100*0.01f, 0.0f);
//    Color4* color = new Color4(0.0f, 1.0f, 0.0f, 0.0f);
//    Color4* color = new Color4(1.0f, 0.5f, 0.0f, 0.0f);
    Color4* color = new Color4(0.0f, 0.502f, 0.502f, 0.0f);


    for(GLuint i = 0; i<m-1; i++) {
        BCurve3 *bcurve = GenerateBCurve3(i);
        bcurve3composite->insertNewArc(bcurve, color);

        delete bcurve;
    }
    delete color;
    bcurve3composite->CreateImage();
    initializeMovebleControlPoints();
}

BCurve3Composite* HermiteBCurve3Composite::getBCurve3Composite(){
    return bcurve3composite;
}

GLuint HermiteBCurve3Composite::getRho() {
    return rho;
}

GLvoid HermiteBCurve3Composite::setRho(GLuint rho) {
    this->rho = rho;
}

Matrix<DCoordinate3>& HermiteBCurve3Composite::getT() {
    return t;
}

DCoordinate3 HermiteBCurve3Composite::getT(GLint i, GLint j){
    return t(i,j);
}

GLvoid HermiteBCurve3Composite::setT(GLint i, GLint j, GLdouble modify_r, GLdouble modify_theta, GLdouble modify_fi){
    r(i,j) += modify_r;
    theta(i,j) += modify_theta;
    fi(i,j) += modify_fi;

    t(i,j).x() = r(i,j) * cos(theta(i,j)) * sin(fi(i,j));
    t(i,j).y() = r(i,j) * sin(theta(i,j)) * sin(fi(i,j));
    t(i,j).z() = r(i,j) * cos(fi(i,j));
}

GLvoid HermiteBCurve3Composite::modifyTRThetaFi() {
    GLuint old_rho = t.GetRowCount();
    if (old_rho < rho) {
        t.ResizeRows(rho);
        r.ResizeRows(rho);
        theta.ResizeRows(rho);
        fi.ResizeRows(rho);

        for(GLuint i = 0; i < m; i++)
            for(GLuint j = old_rho; j < rho; j++) {
                t(j,i).x() = 0.0;
                t(j,i).y() = 0.0;
                t(j,i).z() = 0.0;
        }

        for(GLuint i = old_rho; i < rho; i++)
            for(GLuint j = 0; j < m; j++) {
                r(i, j) = 1.0;
                theta(i, j) = 0.0;
                fi(i, j) = PI / 2;
        }
    }
}

//GLvoid BCurve3::SetControlPoints(GLdouble alpha, GLdouble beta, GLdouble r){
//    for(GLuint i = 0; i < movable_controlpoints.GetColumnCount(); i++){
//        _data[movable_controlpoints[i]].x() = original_cp[movable_controlpoints[i]].x()+sin(alpha)*cos(beta) *r;
//        _data[movable_controlpoints[i]].y() = original_cp[movable_controlpoints[i]].y()+ sin(alpha)*sin(beta) *r;
//        _data[movable_controlpoints[i]].z() =  original_cp[movable_controlpoints[i]].z()+ cos(alpha)*r;
//    }
//}

RowMatrix<GLuint>& HermiteBCurve3Composite::getN() {
    return n;
}

GLuint HermiteBCurve3Composite::getM(){
    return m;
}

RowMatrix<DCoordinate3> HermiteBCurve3Composite::getP() {
    return p;
}

DCoordinate3 HermiteBCurve3Composite::getP(GLint i){
    return p[i];
}

GLvoid HermiteBCurve3Composite::setP(GLint indexDCoordinate3, GLdouble value, GLint indexP) {
    p[indexP][indexDCoordinate3] += value;
}

GLvoid HermiteBCurve3Composite::initializeRThetaFi() {
    r.ResizeColumns(m);
    theta.ResizeColumns(m);
    fi.ResizeColumns(m);

    r.ResizeRows(rho);
    theta.ResizeRows(rho);
    fi.ResizeRows(rho);

    for(GLuint i = 0; i < rho; i++)
        for(GLuint j = 0; j < m; j++) {
            r(i, j) = 1.0;
            theta(i, j) = 0.0;
            fi(i, j) = PI / 2;
        }
}

GLvoid HermiteBCurve3Composite::initializeMovebleControlPoints() {
    for(GLint i = 0; i < bcurve3composite->getSize(); i++) {
        bcurve3composite->getBCurve(i)->setHermiteMovebleControlPoints(rho);
    }
}

Matrix<GLdouble>& HermiteBCurve3Composite::getR() {
    return r;
}

GLdouble HermiteBCurve3Composite::getR(GLint index){
    return r(1, index);
}

Matrix<GLdouble>& HermiteBCurve3Composite::getTheta() {
    return theta;
}

Matrix<GLdouble>& HermiteBCurve3Composite::getFi() {
    return fi;
}

RowMatrix<GLdouble>& HermiteBCurve3Composite::getAlpha() {
    return alpha;
}

Basis::Type HermiteBCurve3Composite::getType() {
    return type;
}



